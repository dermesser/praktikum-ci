import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from ResistanceToTemperature import ResistanceToTemperature
import nice_plots
from scipy import constants as const
import latextable

dir_path = os.path.dirname(os.path.realpath(__file__))

pd.set_option("display.max_rows", None)
pd.set_option("display.max_columns", None)

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

os.chdir(dir_path + "/../")
resistance_platinum = ResistanceToTemperature("platinum")
resistance_carbon = ResistanceToTemperature("carbon")
resistance_platinum.fit_data()
resistance_carbon.fit_data()
resistance_error_materials = pd.DataFrame(
    [resistance_platinum.get_calibration_cu_ta_si()],
    columns=["copper", "tantal", "silicon"],
)
os.chdir(dir_path)


def lin_fit(x, A, B):
    return A * x + B


def KelvinToCelsius(K):
    return K - 273.15


def lin_res_coefficient(T, R_0, alpha):
    return R_0 * (1 + alpha * T)


class AnalysisOfMaterial:
    def __init__(self, material):
        self.material = material
        df_nit = pd.read_csv(
            "../data/Stickstoff",
            decimal=",",
            delimiter=" ",
            skiprows=1,
            engine="python",
            index_col=False,
            header=None,
            names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"],
        )
        df_hel = pd.read_csv(
            "../data/Helium",
            decimal=",",
            delimiter=" ",
            skiprows=1,
            engine="python",
            index_col=False,
            header=None,
            names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"],
        )
        df_nit["measurement"] = "nitrogen"
        df_hel["measurement"] = "helium"

        df_plat = self.convert_platinum(
            pd.concat(
                [
                    df_nit[["platinum", material, "measurement"]],
                    df_hel[["platinum", material, "measurement"]],
                ],
                ignore_index=True,
            )
        )
        df_carb = self.convert_carbon(
            pd.concat(
                [
                    df_hel[["carbon", material, "measurement"]],
                    df_nit[["carbon", material, "measurement"]],
                ],
                ignore_index=True,
            )
        )

        # df2 = pd.read_csv("../data/Helium", decimal=",", delimiter=" ", skiprows=1, engine='python', index_col=False, header=None, names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"])
        # df2 = df2[["carbon", material]]
        # df2 = self.convert_carbon(df2)
        # self.beginHelium = df2["temperatures"].head(1)

        self.df = pd.concat([df_plat, df_carb], ignore_index=True)

        self.df[material + " error"] = float(resistance_error_materials[material])
        # self.df[material + " error"] =

        if material == "silicon":
            self.df = self.df.drop(
                self.df[np.array(self.df[self.material]) > 10 ** 10].index
            )
        self.df = self.df.sort_values("temperatures", axis=0)
        self.df = self.df.reset_index(drop=True)
        # print(self.df[self.df[material] < 0.1])
        # print(self.df[self.df["measurement"] == "helium"])

    def getPlotVariables(self, material, measurement, start, stop):
        x = self.df["temperatures"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        y = self.df[self.material][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        yerr = self.df[self.material + " error"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        xerr = self.df["temperatures error"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        return x, y, yerr, xerr

    def plotRaw(self):
        plt.figure(figsize=(15, 8))
        plt.grid(True)
        plt.xlabel("Temperatur [K]")
        plt.title("Rohdaten für " + self.material)
        plt.ylabel("Widerstand [Ω]")
        x, y, yerr, xerr = self.getPlotVariables(
            "platinum", "helium", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of platinum of helium with errors",
            alpha=0.5,
            linestyle="-",
        )

        x, y, yerr, xerr = self.getPlotVariables(
            "platinum", "nitrogen", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of platinum of nitrogen with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "helium", start=0, stop=400)
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of carbon of helium with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables(
            "carbon", "nitrogen", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of carbon of nitrogen with errors",
            alpha=0.5,
            linestyle="-",
        )
        # plt.errorbar(self.df[self.df["material"] == "platinum"]["temperatures"], self.df[self.df["material"] == "platinum"][self.material], yerr=self.df[self.df["material"] == "platinum"][self.material + " error"], xerr = self.df[self.df["material"] == "platinum"]["temperatures error"], label=self.material + " Raw Data of platinum with errors")
        # plt.errorbar(self.df[self.df["material"] == "carbon"]["temperatures"], self.df[self.df["material"] == "carbon"][self.material], yerr=self.df[self.df["material"] == "carbon"][self.material + " error"], xerr = self.df[self.df["material"] == "carbon"]["temperatures error"], label=self.material + " Raw Data of carbon with errors")
        plt.legend()
        plt.savefig(dir_path + "/../protokoll/images/" + self.material + "_raw.pdf")
        plt.show()

    def plotLogarithmic(
        self, xInverse=False, yInverse=False, start=0, stop=300, xLog=True, yLog=True
    ):
        plt.figure(figsize=(15, 8))
        plt.grid(True)
        if xLog:
            plt.xlabel(
                "Logarithmus der {x}Temperatur".format(
                    x="inversen " if xInverse else ""
                )
            )
        else:
            plt.xlabel("Die {x}Temperatur".format(x="inverse " if xInverse else ""))
        plt.ylabel("Log(1/R)" if yInverse else "Log(R)")
        plt.title(self.material)
        if yLog:
            plt.yscale("log")
        if xLog:
            plt.xscale("log")

        x, y, yerr, xerr = self.getPlotVariables("platinum", "helium", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " helium with errors",
            alpha=0.5,
        )

        x, y, yerr, xerr = self.getPlotVariables("platinum", "nitrogen", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " nitrogen with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "helium", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " carbon of helium with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "nitrogen", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " carbon of nitrogen with errors",
            alpha=0.5,
        )

        # x = self.df["temperatures"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # y = self.df[self.material][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # yerr = self.df[self.material + " error"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # xerr = self.df["temperatures error"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # plt.errorbar(1/x if xInverse else x, 1/y if yInverse else y, yerr=1/y**2 * yerr if yInverse else yerr, xerr=1/x**2 * xerr if xInverse else xerr, label=self.material + " Logarithmic data of carbon with errors")

        # T_plat =self.platinumTemperatureTail
        # T_carb =self.carbonTemperatureHead
        # plt.axvline(1/T_plat if xInverse else T_plat, ymin=min(1/y) if yInverse else min(y), ymax = max(1/y) if yInverse else max(y), label="End of Platinum Data")
        # plt.axvline(1/T_carb if xInverse else T_carb, ymin=min(1/y) if yInverse else min(y), ymax = max(1/y) if yInverse else max(y), label="begin of Carbon Data")
        plt.legend()
        plt.savefig(
            dir_path
            + "/../protokoll/images/"
            + self.material
            + "_log{}{}{}{}_start{}_stop{}.pdf".format(
                "_xInverse" if xInverse else "",
                "_yInverse" if yInverse else "",
                "_xLog" if xLog else "",
                "_yLog" if yLog else "",
                start,
                stop,
            )
        )
        plt.show()

    def convert_platinum(self, df):
        A, B = resistance_platinum.getFitValues()
        sigma_A, sigma_B = resistance_platinum.getFitErrors()
        sigma_R = resistance_platinum.getResistanceError()
        temp = []
        temp_err = []
        # temp_sys = []
        for R in df["platinum"]:
            T = R / A - B / A
            T_err = np.sqrt(
                (sigma_B / A) ** 2
                + (sigma_R / A) ** 2
                + (sigma_A * (R - B) / (A ** 2)) ** 2
            )
            # T_sys = A*sigma_R_sys
            temp.append(T)
            temp_err.append(T_err)
            # temp_sys.append(T_sys)
        df["temperatures"] = pd.Series(temp)
        df["temperatures error"] = pd.Series(temp_err)
        # df["temperatures systematic"] = pd.Series(temp_sys)
        df["material"] = "platinum"
        return df

    def convert_carbon(self, df):
        A, B, C = resistance_carbon.getFitValues()
        sigma_A, sigma_B, sigma_C = resistance_carbon.getFitErrors()
        sigma_R = resistance_carbon.getResistanceError()
        temp = []
        temp_err = []
        # temp_sys = []
        for R in df["carbon"]:
            T = -np.log(R / A - C / A) / (B)
            T_err = np.sqrt(
                (sigma_B * np.log((R - C) / A) / (B ** 2)) ** 2
                + (sigma_A / (B * A)) ** 2
                + (1 / (B * (R - C))) ** 2 * (sigma_R ** 2 + sigma_C ** 2)
            )
            # T_sys = np.abs(A*B*np.exp(-B*R)*sigma_R_sys)
            temp.append(T)
            temp_err.append(T_err)
            # temp_sys.append(T_sys)
        df["temperatures"] = pd.Series(temp)
        df["temperatures error"] = pd.Series(temp_err)
        # df["temperatures systematic"] = pd.Series(temp_sys)
        df["material"] = "carbon"
        return df

    def cutToLinear(self, df):
        pass

    def cutToNonLinear(self, df):
        pass

    def jumpTemperature(self):
        pass


if __name__ == "__main__":
    analysis_cu = AnalysisOfMaterial("copper")
    analysis_ta = AnalysisOfMaterial("tantal")
    analysis_si = AnalysisOfMaterial("silicon")
    print("Tasks: ")
    print(
        "a)  R (T)-Kurve und ln(R) über ln(T) nur im nichtlinearen Bereich für Cu und Ta. "
    )
    print(
        "b) ln(1/R) über ln(1/T) und ln(1/R) über ln(T) im Bereich der Erschöpfung für SI."
    )
    print("Weiterhin folgende Größen")
    print(
        "c) den linearen Widerstandskoeffizienten von Cu und Ta mit dem Ansatz: R(T) = R0 (1+ α T)wobei T in °C und R in Ω einzusetzen sind. Dabei ist R0 = R(T=0°C) ."
    )
    print(
        "d) für den nichtlinearen Bereich den Exponenten β mit dem Ansatz R ∝ T^β(Restwiderstand berücksichtigen!)"
    )
    print("e) die Sprungtemperatur von Ta bzw. von Nb3Sn ")
    print(
        "f) für den Bereich der Erschöpfung bei Si die Beweglichkeit µ (T) mit dem Ansatz 1/R ~ sigma ~ mü(T) ~ T^gamma"
    )

    print(
        "g) unter Vernachlässigung des Temperaturganges der Beweglichkeit die Aktivierungsenergieder Akzeptoren mit dem Ansatz: , wobei p(T) die Löcherkonzentration darstellt. "
    )
    print("sigma(T) ~ p(T) ~ exp(-(E_A - E_V)/2kT")


def task_a():
    print(
        "a)  R (T)-Kurve und ln(R) über ln(T) nur im nichtlinearen Bereich für Cu und Ta. "
    )
    analysis_cu.plotRaw()
    analysis_cu.plotLogarithmic(start=0, stop=60)
    analysis_ta.plotRaw()
    analysis_ta.plotLogarithmic(start=0, stop=60)


def task_b():
    print(
        "b) ln(1/R) über ln(1/T) und ln(1/R) über ln(T) im Bereich der Erschöpfung für SI."
    )
    analysis_si.plotLogarithmic(True, True)
    analysis_si.plotLogarithmic(False, True, 220, 400)
    analysis_si.plotRaw()


def task_e():
    print("e) die Sprungtemperatur von Ta bzw. von Nb3Sn ")
    analysis_ta.plotLogarithmic(start=20, stop=30, xLog=False)
    analysis_ta.plotLogarithmic(start=24, stop=25, xLog=False)
    print(
        "Sprungtemperatur von Tantal: zwischen 24.3 und 27 Kelvin. Nicht genauer bestimmbar aufgrund Messungen (springend)."
    )


def task_f():
    print(
        "f) für den Bereich der Erschöpfung bei Si die Beweglichkeit µ (T) mit dem Ansatz 1/R ~ sigma ~ mü(T) ~ T^gamma"
    )
    print("Eindeutiger Fehler ab 250 Kelvin, daher wird alles größer ignoriert")
    print(
        "Bereich der Erschöpfung von 160 bis 250 Kelvin: Amplitude (150), offset(100), exponent(1.5) / pcov"
    )

    def model(param, T):
        # return param[0] * (T-param[1])**(param[2]) + param[3] * (T-param[1])
        return param[0] * T + param[1]

    analysis_si.plotLogarithmic(
        start=150, stop=250, xLog=False, yLog=False, xInverse=False, yInverse=True
    )
    x, y, yerr, xerr = analysis_si.getPlotVariables("platinum", "helium", 0, 200)
    reg = nice_plots.nice_regression_plot_odr(
        model,
        x=np.array(np.log(x)),
        y=np.array(np.log(1 / y)),
        xerror=np.array(np.abs(xerr / x)),
        yerror=np.array(np.abs(yerr / y)),
        figsize=(15, 8),
        xlabel="Logarithmus der Temperatur",
        ylabel="Logarithmus des inversen Widerstand",
        ylabelresidue="Residuum",
        title="Beweglichkeit in der Erschöpfung",
        save=dir_path + "/../protokoll/images/" + "beweglichkeit" + "_fit.pdf",
        colors=("red", "green"),
        beta0=[1, 3],
    )
    latextable.simple_draw(
        save="/../protokoll/tables/" + "beweglichkeit" + "_fit.tex",
        content=[["A", reg[0][0], reg[1][0][0]], ["B", reg[0][1], reg[1][1][1]]],
        header=["Fit Parameter", "Wert", "Fehler"],
        caption="Fit Parameter für die Beweglichkeit im Bereich der Erschöpfung von Si. A ist die exponentielle abhängigkeit von T",
        label="beweglichkeit_fit",
        precision=2,
        small=False,
    )
    print(reg)


def task_g():
    print(
        "g) unter Vernachlässigung des Temperaturganges der Beweglichkeit die Aktivierungsenergieder Akzeptoren mit dem Ansatz: , wobei p(T) die Löcherkonzentration darstellt. "
    )
    print("sigma(T) ~ p(T) ~ exp(-(E_A - E_V)/2kT")

    def model(param, T):
        return (-(2 * const.Boltzmann / const.eV) / (param[0])) * T + param[1]

    print("1/ln(1/R) gegen -2kT/e*(E_A-E_V in eV)")
    print("E_A-E_V in eV und offset der geraden:")
    analysis_si.plotLogarithmic(
        start=150, stop=250, xLog=False, yLog=False, xInverse=False, yInverse=True
    )
    x, y, yerr, xerr = analysis_si.getPlotVariables("platinum", "helium", 0, 160)
    reg = nice_plots.nice_regression_plot_odr(
        model,
        x=np.array(x),
        y=np.array(1 / np.log(1 / y)),
        xerror=np.array(xerr),
        yerror=np.array(np.abs(yerr / (y * np.log(1 / y) ** 2))),
        figsize=(15, 8),
        xlabel="Temperatur",
        ylabel="1/Log(1/R)",
        ylabelresidue="Residuum",
        title="Aktivierungsenergie in der Reserve",
        save=dir_path + "/../protokoll/images/" + "aktivierungsenergie" + "_fit.pdf",
        colors=("red", "green"),
        beta0=[0.1, 0],
    )
    latextable.simple_draw(
        save="/../protokoll/tables/" + "aktivierungsenergie" + "_fit.tex",
        content=[["A", reg[0][0], reg[1][0][0]], ["B", reg[0][1], reg[1][1][1]]],
        header=["Fit Parameter", "Wert", "Fehler"],
        caption="Fit Parameter für die Aktivierungsenergie vor dem Bereich der Erschöpfung von Si. A ist die Energie in eV",
        label="aktivierungsenergie_fit",
        precision=7,
        small=False,
    )
    print(reg)
    yerr = yerr * 200
    reg = nice_plots.nice_regression_plot_odr(
        model,
        x=np.array(x),
        y=np.array(1 / np.log(1 / y)),
        xerror=np.array(xerr),
        yerror=np.array(np.abs(yerr / (y * np.log(1 / y) ** 2))),
        figsize=(15, 8),
        xlabel="Temperatur",
        ylabel="1/Log(1/R)",
        ylabelresidue="Residuum",
        title="Aktivierungsenergie in der Reserve",
        save=dir_path
        + "/../protokoll/images/"
        + "aktivierungsenergie_angepasst"
        + "_fit.pdf",
        colors=("red", "green"),
        beta0=[0.1, 0],
    )
    latextable.simple_draw(
        save="/../protokoll/tables/" + "aktivierungsenergie_angepasst" + "_fit.tex",
        content=[["A", reg[0][0], reg[1][0][0]], ["B", reg[0][1], reg[1][1][1]]],
        header=["Fit Parameter", "Wert", "Fehler"],
        caption="Fit Parameter für die Aktivierungsenergie vor dem Bereich der Erschöpfung von Si. A ist die Energie in eV, angepasster Fehler",
        label="aktivierungsenergie_fit_angepasst",
        precision=7,
        small=False,
    )
    print(reg)


def table_error():
    latextable.simple_draw(
        save="/../protokoll/tables/" + "cu_ta_si_errors.tex",
        content=np.array(resistance_error_materials),
        header=[
            r"Kupfer: $\sigma_{cu}$",
            r"Tantal: $\sigma_{ta}$",
            r"Silizium: $\sigma_{si}$",
        ],
        caption="Fehler für Kupfer, Tantal und Silizium",
        label="sigma_resistance",
        precision=4,
        small=False,
    )


def task_all():
    task_a()
    task_b()
    task_e()
    task_f()
    task_g()


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from ResistanceToTemperature import ResistanceToTemperature
import nice_plots
from scipy import constants as const
import latextable

dir_path = os.path.dirname(os.path.realpath(__file__))

pd.set_option("display.max_rows", None)
pd.set_option("display.max_columns", None)

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

os.chdir(dir_path + "/../")
resistance_platinum = ResistanceToTemperature("platinum")
resistance_carbon = ResistanceToTemperature("carbon")
resistance_platinum.fit_data()
resistance_carbon.fit_data()
resistance_error_materials = pd.DataFrame(
    [resistance_platinum.get_calibration_cu_ta_si()],
    columns=["copper", "tantal", "silicon"],
)
os.chdir(dir_path)


def lin_fit(x, A, B):
    return A * x + B


def KelvinToCelsius(K):
    return K - 273.15


def lin_res_coefficient(T, R_0, alpha):
    return R_0 * (1 + alpha * T)


class AnalysisOfMaterial:
    def __init__(self, material):
        self.material = material
        df_nit = pd.read_csv(
            "../data/Stickstoff",
            decimal=",",
            delimiter=" ",
            skiprows=1,
            engine="python",
            index_col=False,
            header=None,
            names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"],
        )
        df_hel = pd.read_csv(
            "../data/Helium",
            decimal=",",
            delimiter=" ",
            skiprows=1,
            engine="python",
            index_col=False,
            header=None,
            names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"],
        )
        df_nit["measurement"] = "nitrogen"
        df_hel["measurement"] = "helium"

        df_plat = self.convert_platinum(
            pd.concat(
                [
                    df_nit[["platinum", material, "measurement"]],
                    df_hel[["platinum", material, "measurement"]],
                ],
                ignore_index=True,
            )
        )
        df_carb = self.convert_carbon(
            pd.concat(
                [
                    df_hel[["carbon", material, "measurement"]],
                    df_nit[["carbon", material, "measurement"]],
                ],
                ignore_index=True,
            )
        )

        # df2 = pd.read_csv("../data/Helium", decimal=",", delimiter=" ", skiprows=1, engine='python', index_col=False, header=None, names=["platinum", "carbon", "copper", "tantal", "silicon", "a", "b"])
        # df2 = df2[["carbon", material]]
        # df2 = self.convert_carbon(df2)
        # self.beginHelium = df2["temperatures"].head(1)

        self.df = pd.concat([df_plat, df_carb], ignore_index=True)

        self.df[material + " error"] = float(resistance_error_materials[material])
        # self.df[material + " error"] =

        if material == "silicon":
            self.df = self.df.drop(
                self.df[np.array(self.df[self.material]) > 10 ** 10].index
            )
        self.df = self.df.sort_values("temperatures", axis=0)
        self.df = self.df.reset_index(drop=True)
        # print(self.df[self.df[material] < 0.1])
        # print(self.df[self.df["measurement"] == "helium"])

    def getPlotVariables(self, material, measurement, start, stop):
        x = self.df["temperatures"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        y = self.df[self.material][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        yerr = self.df[self.material + " error"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        xerr = self.df["temperatures error"][
            (self.df["measurement"] == measurement)
            & (self.df["material"] == material)
            & (np.array(self.df["temperatures"]) <= stop)
            & (np.array(self.df["temperatures"]) >= start)
        ].reset_index(drop=True)
        return x, y, yerr, xerr

    def plotRaw(self):
        plt.figure(figsize=(15, 8))
        plt.grid(True)
        plt.xlabel("Temperatur [K]")
        plt.title("Rohdaten für " + self.material)
        plt.ylabel("Widerstand [Ω]")
        x, y, yerr, xerr = self.getPlotVariables(
            "platinum", "helium", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of platinum of helium with errors",
            alpha=0.5,
            linestyle="-",
        )

        x, y, yerr, xerr = self.getPlotVariables(
            "platinum", "nitrogen", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of platinum of nitrogen with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "helium", start=0, stop=400)
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of carbon of helium with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables(
            "carbon", "nitrogen", start=0, stop=400
        )
        plt.errorbar(
            x,
            y,
            yerr,
            xerr,
            label=self.material + " Raw Data of carbon of nitrogen with errors",
            alpha=0.5,
            linestyle="-",
        )
        # plt.errorbar(self.df[self.df["material"] == "platinum"]["temperatures"], self.df[self.df["material"] == "platinum"][self.material], yerr=self.df[self.df["material"] == "platinum"][self.material + " error"], xerr = self.df[self.df["material"] == "platinum"]["temperatures error"], label=self.material + " Raw Data of platinum with errors")
        # plt.errorbar(self.df[self.df["material"] == "carbon"]["temperatures"], self.df[self.df["material"] == "carbon"][self.material], yerr=self.df[self.df["material"] == "carbon"][self.material + " error"], xerr = self.df[self.df["material"] == "carbon"]["temperatures error"], label=self.material + " Raw Data of carbon with errors")
        plt.legend()
        plt.savefig(dir_path + "/../protokoll/images/" + self.material + "_raw.pdf")
        plt.show()

    def plotLogarithmic(
        self, xInverse=False, yInverse=False, start=0, stop=300, xLog=True, yLog=True
    ):
        plt.figure(figsize=(15, 8))
        plt.grid(True)
        if xLog:
            plt.xlabel(
                "Logarithmus der {x}Temperatur".format(
                    x="inversen " if xInverse else ""
                )
            )
        else:
            plt.xlabel("Die {x}Temperatur".format(x="inverse " if xInverse else ""))
        plt.ylabel("Log(1/R)" if yInverse else "Log(R)")
        plt.title(self.material)
        if yLog:
            plt.yscale("log")
        if xLog:
            plt.xscale("log")

        x, y, yerr, xerr = self.getPlotVariables("platinum", "helium", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " helium with errors",
            alpha=0.5,
        )

        x, y, yerr, xerr = self.getPlotVariables("platinum", "nitrogen", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " nitrogen with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "helium", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " carbon of helium with errors",
        )

        x, y, yerr, xerr = self.getPlotVariables("carbon", "nitrogen", start, stop)
        plt.errorbar(
            1 / x if xInverse else x,
            1 / y if yInverse else y,
            yerr=1 / y ** 2 * yerr if yInverse else yerr,
            xerr=1 / x ** 2 * xerr if xInverse else xerr,
            label=self.material + " carbon of nitrogen with errors",
            alpha=0.5,
        )

        # x = self.df["temperatures"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # y = self.df[self.material][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # yerr = self.df[self.material + " error"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # xerr = self.df["temperatures error"][self.df["material"] == "carbon"][(np.array(self.df["temperatures"][self.df["material"] == "carbon"]) < stop) & (np.array(self.df["temperatures"][self.df["material"] == "carbon"]) >= start)]
        # plt.errorbar(1/x if xInverse else x, 1/y if yInverse else y, yerr=1/y**2 * yerr if yInverse else yerr, xerr=1/x**2 * xerr if xInverse else xerr, label=self.material + " Logarithmic data of carbon with errors")

        # T_plat =self.platinumTemperatureTail
        # T_carb =self.carbonTemperatureHead
        # plt.axvline(1/T_plat if xInverse else T_plat, ymin=min(1/y) if yInverse else min(y), ymax = max(1/y) if yInverse else max(y), label="End of Platinum Data")
        # plt.axvline(1/T_carb if xInverse else T_carb, ymin=min(1/y) if yInverse else min(y), ymax = max(1/y) if yInverse else max(y), label="begin of Carbon Data")
        plt.legend()
        plt.savefig(
            dir_path
            + "/../protokoll/images/"
            + self.material
            + "_log{}{}{}{}_start{}_stop{}.pdf".format(
                "_xInverse" if xInverse else "",
                "_yInverse" if yInverse else "",
                "_xLog" if xLog else "",
                "_yLog" if yLog else "",
                start,
                stop,
            )
        )
        plt.show()

    def convert_platinum(self, df):
        A, B = resistance_platinum.getFitValues()
        sigma_A, sigma_B = resistance_platinum.getFitErrors()
        sigma_R = resistance_platinum.getResistanceError()
        temp = []
        temp_err = []
        # temp_sys = []
        for R in df["platinum"]:
            T = R / A - B / A
            T_err = np.sqrt(
                (sigma_B / A) ** 2
                + (sigma_R / A) ** 2
                + (sigma_A * (R - B) / (A ** 2)) ** 2
            )
            # T_sys = A*sigma_R_sys
            temp.append(T)
            temp_err.append(T_err)
            # temp_sys.append(T_sys)
        df["temperatures"] = pd.Series(temp)
        df["temperatures error"] = pd.Series(temp_err)
        # df["temperatures systematic"] = pd.Series(temp_sys)
        df["material"] = "platinum"
        return df

    def convert_carbon(self, df):
        A, B, C = resistance_carbon.getFitValues()
        sigma_A, sigma_B, sigma_C = resistance_carbon.getFitErrors()
        sigma_R = resistance_carbon.getResistanceError()
        temp = []
        temp_err = []
        # temp_sys = []
        for R in df["carbon"]:
            T = -np.log(R / A - C / A) / (B)
            T_err = np.sqrt(
                (sigma_B * np.log((R - C) / A) / (B ** 2)) ** 2
                + (sigma_A / (B * A)) ** 2
                + (1 / (B * (R - C))) ** 2 * (sigma_R ** 2 + sigma_C ** 2)
            )
            # T_sys = np.abs(A*B*np.exp(-B*R)*sigma_R_sys)
            temp.append(T)
            temp_err.append(T_err)
            # temp_sys.append(T_sys)
        df["temperatures"] = pd.Series(temp)
        df["temperatures error"] = pd.Series(temp_err)
        # df["temperatures systematic"] = pd.Series(temp_sys)
        df["material"] = "carbon"
        return df

    def cutToLinear(self, df):
        pass

    def cutToNonLinear(self, df):
        pass

    def jumpTemperature(self):
        pass


if __name__ == "__main__":
    analysis_cu = AnalysisOfMaterial("copper")
    analysis_ta = AnalysisOfMaterial("tantal")
    analysis_si = AnalysisOfMaterial("silicon")
    print("Tasks: ")
    print(
        "a)  R (T)-Kurve und ln(R) über ln(T) nur im nichtlinearen Bereich für Cu und Ta. "
    )
    print(
        "b) ln(1/R) über ln(1/T) und ln(1/R) über ln(T) im Bereich der Erschöpfung für SI."
    )
    print("Weiterhin folgende Größen")
    print(
        "c) den linearen Widerstandskoeffizienten von Cu und Ta mit dem Ansatz: R(T) = R0 (1+ α T)wobei T in °C und R in Ω einzusetzen sind. Dabei ist R0 = R(T=0°C) ."
    )
    print(
        "d) für den nichtlinearen Bereich den Exponenten β mit dem Ansatz R ∝ T^β(Restwiderstand berücksichtigen!)"
    )
    print("e) die Sprungtemperatur von Ta bzw. von Nb3Sn ")
    print(
        "f) für den Bereich der Erschöpfung bei Si die Beweglichkeit µ (T) mit dem Ansatz 1/R ~ sigma ~ mü(T) ~ T^gamma"
    )

    print(
        "g) unter Vernachlässigung des Temperaturganges der Beweglichkeit die Aktivierungsenergieder Akzeptoren mit dem Ansatz: , wobei p(T) die Löcherkonzentration darstellt. "
    )
    print("sigma(T) ~ p(T) ~ exp(-(E_A - E_V)/2kT")


def task_a():
    print(
        "a)  R (T)-Kurve und ln(R) über ln(T) nur im nichtlinearen Bereich für Cu und Ta. "
    )
    analysis_cu.plotRaw()
    analysis_cu.plotLogarithmic(start=0, stop=60)
    analysis_ta.plotRaw()
    analysis_ta.plotLogarithmic(start=0, stop=60)


def task_b():
    print(
        "b) ln(1/R) über ln(1/T) und ln(1/R) über ln(T) im Bereich der Erschöpfung für SI."
    )
    analysis_si.plotLogarithmic(True, True)
    analysis_si.plotLogarithmic(False, True, 220, 400)
    analysis_si.plotRaw()


def task_e():
    print("e) die Sprungtemperatur von Ta bzw. von Nb3Sn ")
    analysis_ta.plotLogarithmic(start=20, stop=30, xLog=False)
    analysis_ta.plotLogarithmic(start=24, stop=25, xLog=False)
    print(
        "Sprungtemperatur von Tantal: zwischen 24.3 und 27 Kelvin. Nicht genauer bestimmbar aufgrund Messungen (springend)."
    )


def task_f():
    print(
        "f) für den Bereich der Erschöpfung bei Si die Beweglichkeit µ (T) mit dem Ansatz 1/R ~ sigma ~ mü(T) ~ T^gamma"
    )
    print("Eindeutiger Fehler ab 250 Kelvin, daher wird alles größer ignoriert")
    print(
        "Bereich der Erschöpfung von 160 bis 250 Kelvin: Amplitude (150), offset(100), exponent(1.5) / pcov"
    )

    def model(param, T):
        # return param[0] * (T-param[1])**(param[2]) + param[3] * (T-param[1])
        return param[0] * T + param[1]

    analysis_si.plotLogarithmic(
        start=150, stop=250, xLog=False, yLog=False, xInverse=False, yInverse=True
    )
    x, y, yerr, xerr = analysis_si.getPlotVariables("platinum", "helium", 0, 200)
    reg = nice_plots.nice_regression_plot_odr(
        model,
        x=np.array(np.log(x)),
        y=np.array(np.log(1 / y)),
        xerror=np.array(np.abs(xerr / x)),
        yerror=np.array(np.abs(yerr / y)),
        figsize=(15, 8),
        xlabel="Logarithmus der Temperatur",
        ylabel="Log(1/R)",
        ylabelresidue="Residuum",
        title="Beweglichkeit in der Erschöpfung",
        save=dir_path + "/../protokoll/images/" + "beweglichkeit" + "_fit.pdf",
        colors=("red", "green"),
        beta0=[1, 3],
    )
    latextable.simple_draw(
        save="/../protokoll/tables/" + "beweglichkeit" + "_fit.tex",
        content=[["A", reg[0][0], reg[1][0][0]], ["B", reg[0][1], reg[1][1][1]]],
        header=["Fit Parameter", "Wert", "Fehler"],
        caption="Fit Parameter für die Beweglichkeit im Bereich der Erschöpfung von Si. A ist die exponentielle abhängigkeit von T",
        label="beweglichkeit_fit",
        precision=2,
        small=False,
    )
    print(reg)


def task_g():
    print(
        "g) unter Vernachlässigung des Temperaturganges der Beweglichkeit die Aktivierungsenergieder Akzeptoren mit dem Ansatz: , wobei p(T) die Löcherkonzentration darstellt. "
    )
    print("sigma(T) ~ p(T) ~ exp(-(E_A - E_V)/2kT")

    def model(param, T):
        return (-(2 * const.Boltzmann / const.eV) / (param[0])) * T + param[1]

    print("1/ln(1/R) gegen -2kT/e*(E_A-E_V in eV)")
    print("E_A-E_V in eV und offset der geraden:")
    analysis_si.plotLogarithmic(
        start=150, stop=250, xLog=False, yLog=False, xInverse=False, yInverse=True
    )
    x, y, yerr, xerr = analysis_si.getPlotVariables("platinum", "helium", 0, 160)
    reg = nice_plots.nice_regression_plot_odr(
        model,
        x=np.array(x),
        y=np.array(1 / np.log(1 / y)),
        xerror=np.array(xerr),
        yerror=np.array(np.abs(yerr / (y * np.log(1 / y) ** 2))),
        figsize=(15, 8),
        xlabel="Temperatur",
        ylabel="1/Log(1/R)",
        ylabelresidue="Residuum",
        title="Aktivierungsenergie in der Reserve",
        save=dir_path + "/../protokoll/images/" + "aktivierungsenergie" + "_fit.pdf",
        colors=("red", "green"),
        beta0=[0.1, 0],
    )
    latextable.simple_draw(
        save="/../protokoll/tables/" + "aktivierungsenergie" + "_fit.tex",
        content=[["A", reg[0][0], reg[1][0][0]], ["B", reg[0][1], reg[1][1][1]]],
        header=["Fit Parameter", "Wert", "Fehler"],
        caption="Fit Parameter für die Aktivierungsenergie vor dem Bereich der Erschöpfung von Si. A ist die Energie in eV",
        label="aktivierungsenergie_fit",
        precision=7,
        small=False,
    )
    print(reg)
    yerr = yerr * 200
    reg = nice_plots.nice_regression_plot_odr(
        model,
        x=np.array(x),
        y=np.array(1 / np.log(1 / y)),
        xerror=np.array(xerr),
        yerror=np.array(np.abs(yerr / (y * np.log(1 / y) ** 2))),
        figsize=(15, 8),
        xlabel="Temperatur",
        ylabel="1/Log(1/R)",
        ylabelresidue="Residuum",
        title="Aktivierungsenergie in der Reserve",
        save=dir_path
        + "/../protokoll/images/"
        + "aktivierungsenergie_angepasst"
        + "_fit.pdf",
        colors=("red", "green"),
        beta0=[0.1, 0],
    )
    latextable.simple_draw(
        save="/../protokoll/tables/" + "aktivierungsenergie_angepasst" + "_fit.tex",
        content=[["A", reg[0][0], reg[1][0][0]], ["B", reg[0][1], reg[1][1][1]]],
        header=["Fit Parameter", "Wert", "Fehler"],
        caption="Fit Parameter für die Aktivierungsenergie vor dem Bereich der Erschöpfung von Si. A ist die Energie in eV, angepasster Fehler",
        label="aktivierungsenergie_fit_angepasst",
        precision=7,
        small=False,
    )
    print(reg)


def table_error():
    latextable.simple_draw(
        save="/../protokoll/tables/" + "cu_ta_si_errors.tex",
        content=np.array(resistance_error_materials),
        header=[
            r"Kupfer: $\sigma_{cu}$",
            r"Tantal: $\sigma_{ta}$",
            r"Silizium: $\sigma_{si}$",
        ],
        caption="Fehler für Kupfer, Tantal und Silizium",
        label="sigma_resistance",
        precision=4,
        small=False,
    )


def task_all():
    task_a()
    task_b()
    task_e()
    task_f()
    task_g()
