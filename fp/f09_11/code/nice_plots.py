# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:59:22 2020

@author: Max_b
heavily amended by lewinb
even more heavily amended by Max_b
"""

from praktikum import analyse
from scipy.optimize import curve_fit
import numpy as np
import matplotlib.pyplot as plt
from itertools import repeat
from scipy import odr

import math

plt.rcParams['font.size'] = 24.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['figure.autolayout'] = True


# def nice_lin_shift_method(x, y, xerr=None, yerr=None):
#     """Returns systematic errors on m and b for given data."""
#     sys_m = []
#     sys_b = []

#     (m, em, b, eb, chiq, corr) = nice_regression_parameters(x, y, xerr, yerr)

#     if xerr is not None:
#         x_p, x_m = x + xerr, x - xerr
#         m_p, _, b_p, _, _, _ = nice_regression_parameters(x_p, y, xerr, yerr)
#         m_m, _, b_m, _, _, _ = nice_regression_parameters(x_m, y, xerr, yerr)
#         sys_m.append((m_p - m_m) / 2)
#     if yerr is not None:
#         y_p, y_m = y + yerr, y - yerr
#         m_p, _, b_p, _, _, _ = nice_regression_parameters(x, y_p, xerr, yerr)
#         m_m, _, b_m, _, _, _ = nice_regression_parameters(x, y_m, xerr, yerr)
#         sys_m.append((m_p - m_m) / 2)

#     return math.sqrt((np.array(sys_m) ** 2).sum()), math.sqrt(
#         (np.array(sys_b) ** 2).sum()
#     )


# def nice_regression_parameters(x, y, xerr=None, yerr=None):
#     """Calculate and print a linear regression in a nice way."""
#     if xerr is not None and xerr.all():
#         (m, em, b, eb, chiq, corr) = analyse.lineare_regression_xy(x, y, xerr, yerr)
#     else:
#         (m, em, b, eb, chiq, corr) = analyse.lineare_regression(x, y, yerr)

#     print(
#         "y = m x + b :: m = {:.5e} +/- {:.5e}; b = {:.5e} +/- {:.5e}; X^2/DoF = {:.3f} / {:d} = {:.3f}; Corr_m,b = {:.3f}".format(
#             m, em, b, eb, chiq, x.size - 2, chiq / (x.size - 2), corr
#         )
#     )

#     return (m, em, b, eb, chiq, corr)


# def nice_quadratic_regression_plot(
#     x,
#     y,
#     yerror,
#     xerror=np.zeros(1),
#     xlabel="",
#     ylabel="",
#     ylabelresidue="",
#     title="",
#     save=None,
# ):
#     plt.tight_layout()
#     fig, axarray = plt.subplots(
#         2, 1, figsize=(20, 10), sharex=True, gridspec_kw={"height_ratios": [5, 2]}
#     )
#     fig.tight_layout()
#     axarray[1].grid(True)
#     axarray[0].grid(True)
#     # axarray[0].set_xlabel(xlabel)
#     axarray[0].set_ylabel(ylabel)
#     axarray[1].set_xlabel(xlabel)
#     axarray[1].set_ylabel(ylabelresidue)

#     if title:
#         axarray[0].set_title(title)

#     if xerror.all():
#         axarray[0].errorbar(
#             x,
#             y,
#             xerr=xerror,
#             yerr=yerror,
#             color="red",
#             fmt=".",
#             marker="o",
#             markeredgecolor="red",
#         )
#     else:
#         axarray[0].errorbar(
#             x, y, yerr=yerror, color="red", fmt=".", marker="o", markeredgecolor="red"
#         )

#     if xerror is not None and xerror.all():
#         (a, ea, b, eb, c, ec, chiq, corrs) = analyse.quadratische_regression_xy(
#             x, y, xerror, yerror
#         )
#     else:
#         (a, ea, b, eb, c, ec, chiq, corrs) = analyse.quadratische_regression(
#             x, y, yerror
#         )

#     axarray[0].plot(x, a * x ** 2 + b * x + c, color="green")
#     axarray[0].text(
#         0.03,
#         0.9,
#         "$\\chi^2$/ndof = %g / %g = %g" % (chiq, x.size - 3, chiq / (x.size - 3)),
#         transform=axarray[0].transAxes,
#     )

#     residuals = y - (a * x ** 2 + b * x + c)
#     sigmaRes = np.sqrt(((2 * a * x + b) * xerror) ** 2 + yerror ** 2)
#     axarray[1].axhline(y=0.0, color="black", linestyle="--")
#     axarray[1].errorbar(
#         x,
#         residuals,
#         yerr=sigmaRes,
#         color="red",
#         fmt=".",
#         marker="o",
#         markeredgecolor="red",
#     )

#     ymax = max([abs(x) for x in axarray[1].get_ylim()])
#     axarray[1].set_ylim(-ymax, ymax)

#     fig.subplots_adjust(hspace=0.0)
#     plt.show()
#     if save:
#         fig.savefig(save)

#     return (a, ea, b, eb, c, ec, chiq, corrs)


# def nice_linear_regression_plot(
#     x,
#     y,
#     yerror,
#     xerror=np.zeros(1),
#     xlabel="",
#     ylabel="",
#     ylabelresidue="",
#     title="",
#     regrange=[0, -1],
#     figsize=(10, 6),
#     legend_x=None,
#     save=None,
#     colors=("red", "green"),
#     return_axes=False,
#     use_axes=None,
#     ix=None,
# ):
#     """Does a linear Regression with x (and y errors).
#     x = array with x-coordinates,
#     y = array with y-coordinates,
#     xerror = array with errors of x,
#     yerror = array with errors of y,
#     xlabel = name on x-axis,
#     ylabel = name on y-axis,
#     ylabelresidue = name on residue
#     save = filename to save figure in"""
#     plt.tight_layout()
#     if use_axes is None:
#         fig, axarray = plt.subplots(
#             2,
#             1,
#             figsize=figsize,
#             sharex=True,
#             tight_layout=True,
#             gridspec_kw={"height_ratios": [5, 2]},
#         )
#         # fig.tight_layout()
#     else:
#         axarray = use_axes
#         fig = None
#     axarray[1].grid(True)
#     axarray[0].grid(True)
#     # axarray[0].set_xlabel(xlabel)
#     axarray[0].set_ylabel(ylabel)
#     if xlabel:
#         axarray[1].set_xlabel(xlabel)
#     axarray[1].set_ylabel(
#         ylabelresidue if ylabelresidue else "{} - (mx + b)".format(ylabel)
#     )

#     if title:
#         axarray[0].set_title(title)

#     if xerror.all():
#         axarray[0].errorbar(
#             x,
#             y,
#             xerr=xerror,
#             yerr=yerror,
#             color=colors[0],
#             fmt=".",
#             marker=".",
#             markeredgecolor=colors[0],
#         )
#     else:
#         axarray[0].errorbar(
#             x,
#             y,
#             yerr=yerror,
#             color=colors[0],
#             fmt=".",
#             marker=".",
#             markeredgecolor=colors[0],
#         )

#     if regrange[1] == -1:
#         regrange = (regrange[0], x.size)
#     (m, em, b, eb, chiq, corr) = nice_regression_parameters(
#         x[regrange[0] : regrange[1]],
#         y[regrange[0] : regrange[1]],
#         xerror[regrange[0] : regrange[1]],
#         yerror[regrange[0] : regrange[1]],
#     )

#     axarray[0].plot(x, m * x + b, color=colors[1])

#     text_x = legend_x if legend_x is not None else 0.03
#     text_y = 0.8 if use_axes is None else 0.6
#     axarray[0].text(
#         text_x,
#         text_y + 0.08,
#         "{} $\\chi^2$/ndof = {:.1f} / {} = {:.1f}".format(
#             str(ix) + ":" if ix is not None else "",
#             chiq,
#             x.size - 2,
#             chiq / (x.size - 2),
#         ),
#         transform=axarray[0].transAxes,
#         color=colors[0],
#     )
#     axarray[0].text(
#         text_x,
#         text_y,
#         "$m x + b = ${:.3e} $x +$ {:.3e}".format(m, b),
#         transform=axarray[0].transAxes,
#         color=colors[0],
#     )

#     residuals = y - (m * x + b)
#     sigmaRes = np.sqrt((m * xerror) ** 2 + yerror ** 2)
#     axarray[1].axhline(y=0.0, color="black", linestyle="--")
#     axarray[1].errorbar(
#         x[regrange[0] : regrange[1]],
#         residuals[regrange[0] : regrange[1]],
#         yerr=sigmaRes[regrange[0] : regrange[1]],
#         color=colors[0],
#         fmt=".",
#         marker=".",
#         markeredgecolor=colors[0],
#     )

#     ymax = max([abs(x) for x in axarray[1].get_ylim()])
#     axarray[1].set_ylim(-ymax, ymax)

#     if fig:
#         # fig.subplots_adjust(hspace=0.0)
#         if save:
#             fig.savefig(save)

#     if return_axes:
#         return (m, em, b, eb, chiq, corr), fig, axarray
#     return (m, em, b, eb, chiq, corr)


def nice_regression_plot(
    func,
    x,
    y,
    yerror,
    xlabel="",
    ylabel="",
    ylabelresidue="",
    title="",
    regrange=[0, -1],
    figsize=(10, 6),
    legend_x=None,
    save=None,
    colors=("red", "green"),
    return_axes=False,
    use_axes=None,
    ix=None,
    **kwargs
):

    """Does a fit to func with y errors.
    func = function to fit
    x = array with x-coordinates,
    y = array with y-coordinates,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue
    save = filename to save figure in"""
    if use_axes is None:
        fig, axarray = plt.subplots(
            2,
            1,
            figsize=figsize,
            sharex=True,
            tight_layout=True,
            gridspec_kw={"height_ratios": [5, 2]},
        )
    else:
        axarray = use_axes
        fig = None
    axarray[1].grid(True)
    axarray[0].grid(True)
    axarray[0].set_ylabel(ylabel)
    if xlabel:
        axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue if ylabelresidue else "Residue")

    if title:
        axarray[0].set_title(title)

    axarray[0].errorbar(
        x,
        y,
        yerr=yerror,
        color=colors[0],
        fmt=".",
        marker=".",
        markeredgecolor=colors[0],
    )

    if regrange[1] == -1:
        regrange = (regrange[0], len(x))
    popt, pcov = curve_fit(func, x, y, sigma=yerror, **kwargs)

    popt_list = [[*popt] for _ in range(len(x))]
    Nexp = []
    for i in range(len(x)):
        Nexp.append(func(x[i], *(popt_list[i])))

    residuals = [y[i] - Nexp[i] for i in range(len(x))]
    residuals = np.array(residuals)
    yerror = np.array(yerror)
    chiq = np.sum((residuals / yerror) ** 2)
    df = len(x) - len(popt)
    x_temp = np.linspace(x[0], x[-1], 1000)
    Nexp_temp = []
    for i in x_temp:
        Nexp_temp.append(func(i, *popt))

    axarray[0].plot(x_temp, Nexp_temp, color=colors[1])

    text_x = legend_x if legend_x is not None else 0.03
    text_y = 0.8 if use_axes is None else 0.6
    axarray[0].text(
        text_x,
        text_y + 0.08,
        "{} $\\chi^2$/ndof = {:.1f} / {} = {:.1f}".format(
            str(ix) + ":" if ix is not None else "",
            chiq,
            df,
            chiq / df,
        ),
        transform=axarray[0].transAxes,
        color=colors[0],
    )

    axarray[1].axhline(y=0.0, color="black", linestyle="--")
    axarray[1].errorbar(
        x[regrange[0] : regrange[1]],
        residuals[regrange[0] : regrange[1]],
        yerr=yerror,
        color=colors[0],
        fmt=".",
        marker=".",
        markeredgecolor=colors[0],
    )

    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)

    if fig:
        if save:
            fig.savefig(save)
        else:
            plt.show()
    return popt, pcov, chiq, df


def nice_regression_plot_odr(
    func,
    x,
    y,
    xerror,
    yerror,
    xlabel="",
    ylabel="",
    ylabelresidue="",
    title="",
    regrange=[0, -1],
    figsize=(10, 6),
    legend_x=None,
    save=None,
    colors=("red", "green"),
    return_axes=False,
    use_axes=None,
    ix=None,
    **kwargs
):

    """Does a fit to func with y errors.
    func = function to fit
    x = array with x-coordinates,
    y = array with y-coordinates,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue
    save = filename to save figure in"""
    if use_axes is None:
        fig, axarray = plt.subplots(
            2,
            1,
            figsize=figsize,
            sharex=True,
            tight_layout=True,
            gridspec_kw={"height_ratios": [5, 2]},
        )
    else:
        axarray = use_axes
        fig = None
    axarray[1].grid(True)
    axarray[0].grid(True)
    axarray[0].set_ylabel(ylabel)
    if xlabel:
        axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue if ylabelresidue else "Residue")

    if title:
        axarray[0].set_title(title)

    axarray[0].errorbar(
        x,
        y,
        yerr=yerror,
        color=colors[0],
        fmt=".",
        marker=".",
        markeredgecolor=colors[0],
    )

    if regrange[1] == -1:
        regrange = (regrange[0], len(x))

    model = odr.Model(func)
    mydata = odr.RealData(x, y, sx=xerror, sy=yerror)
    myodr = odr.ODR(mydata, model, **kwargs)
    myoutput = myodr.run()
    popt = myoutput.beta
    pcov = myoutput.cov_beta
    # print(popt)
    # print(pcov)
    # popt, pcov = curve_fit(func, x, y, sigma=yerror, **kwargs)

    popt_list = [[*popt] for _ in range(len(x))]
    Nexp = []

    for i in range(len(x)):
        Nexp.append(func(popt_list[i], x[i]))

    residuals = [y[i] - Nexp[i] for i in range(len(x))]
    residuals = np.array(residuals)
    yerror = np.array(yerror)
    chiq = np.sum((residuals / yerror) ** 2)
    df = len(x) - len(popt)
    x_temp = np.linspace(x[0], x[-1], 1000)
    Nexp_temp = []
    for i in x_temp:
        Nexp_temp.append(func(popt, i))

    axarray[0].plot(x_temp, Nexp_temp, color=colors[1])

    text_x = legend_x if legend_x is not None else 0.03
    text_y = 0.8 if use_axes is None else 0.6
    axarray[0].text(
        text_x,
        text_y + 0.08,
        "{} $\\chi^2$/ndof = {:.1f} / {} = {:.1f}".format(
            str(ix) + ":" if ix is not None else "",
            chiq,
            df,
            chiq / df,
        ),
        transform=axarray[0].transAxes,
        color=colors[0],
    )

    axarray[1].axhline(y=0.0, color="black", linestyle="--")
    axarray[1].errorbar(
        x[regrange[0] : regrange[1]],
        residuals[regrange[0] : regrange[1]],
        yerr=yerror,
        color=colors[0],
        fmt=".",
        marker=".",
        markeredgecolor=colors[0],
    )

    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)

    if fig:
        if save:
            fig.savefig(save)
        else:
            plt.show()
    return popt, pcov, chiq, df
