### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# ╔═╡ e6129662-8d98-11eb-1aa7-1d81065bd20e
# Changing utility code
begin
	using Revise
	push!(LOAD_PATH, pwd())
	using analysis
end

# ╔═╡ d2f1d098-8d98-11eb-09fc-fd66808c6bf3
begin
	import CSV
	import DataFrames
	import Measurements
	import StatsPlots
	import LsqFit
	import Statistics
	
	using Plots
	
	const mea = Measurements
	const DF = DataFrames

	Plots.gr()
	Plots.default(tickfont = ("sans-serif", 10), legendfontsize=10, html_output_format=:png)
end

# ╔═╡ e8f38b20-8d98-11eb-1b38-fb6306463607
begin
	BASE_PATH = "../data/"
	
	function to_path(fn::String)::String
		return string(BASE_PATH, fn)
	end
end

# ╔═╡ ebb2b502-8d98-11eb-038f-c113db7716b5
function read_csv(file::String)
	CSV.File(to_path(file), normalizenames=true) |> DataFrames.DataFrame
end

# ╔═╡ d2130e1c-912b-11eb-30fd-0df33de96c00
begin
	VOLTAGE_RANGE = 20  # V
	VOLTAGE_BINS = 2^16
	VOLTAGE_UNCERTAINTY = VOLTAGE_RANGE / VOLTAGE_BINS / sqrt(12)
end

# ╔═╡ eedfc774-8d98-11eb-199e-bb08abb3a3a0
TRANSISTOR_OUT_TYPES = [
	("0625VU1", 470, 10000, 0.625),
	("0650VU1", 470, 10000, 0.650),
	("0675VU1", 470, 10000, 0.675),
	("0700VU1", 470, 10000, 0.700),
	("0720VU1", 470, 10000, 0.720),
];

# ╔═╡ 9eb63468-9126-11eb-0b4a-ff6e9cbc050b
TRANSISTOR_IN_TYPES = [220];

# ╔═╡ abc71cbc-9126-11eb-1e54-7d06e0b8e8f0
TRANSISTOR_CONTROL_TYPES = [
	(100, 10000),
	(470, 10000),
	(1000, 10000),
];

# ╔═╡ f0e1667c-9149-11eb-16f8-8fcbfd3d3613
TRANSISTOR_CONTROL_RESISTANCES = [t[1] for t = TRANSISTOR_CONTROL_TYPES]

# ╔═╡ bab2acdc-9126-11eb-0ad8-3bef85d16ff7
TRANSISTOR_OUT_FILENAMES = [
	"Transistor_Ausgangslinie_$(i)_$(j)OhmRC_$(k)OhmRB.csv"
	for (i, j, k) = TRANSISTOR_OUT_TYPES];

# ╔═╡ d928a266-9126-11eb-3533-f7ebb2eb86e4
TRANSISTOR_IN_FILENAMES = [
	"Transistor_Eingangslinie_$(r)OhmRB.csv"
	for r = TRANSISTOR_IN_TYPES];

# ╔═╡ eceb5c30-9126-11eb-2980-7334d2049300
TRANSISTOR_CONTROL_FILENAMES = [
	"Transistor_Steuerungslinie_$(rc)OhmRC_$(rb)OhmRB.csv"
	for (rc, rb) = TRANSISTOR_CONTROL_TYPES];

# ╔═╡ 1850b202-912b-11eb-1166-2bc8af93ac86
md"""## Eingangskennlinie"""

# ╔═╡ 430b90ac-912b-11eb-0fac-8b6ab89f58a4
TRANSISTOR_IN_FILES = [read_csv(f) for f = TRANSISTOR_IN_FILENAMES];

# ╔═╡ 530c6e74-912b-11eb-180b-d1ff3b32faeb
gb = DF.groupby(TRANSISTOR_IN_FILES[1], :U_BE_V_)

# ╔═╡ 6a5d9ca0-923f-11eb-05d0-c350ba6b402c
for df_ = TRANSISTOR_IN_FILES
	df_.UNCERTAINTY = zeros(size(df_)[1])
	sort!(df_, :U_BE_V_)
	grouped = DF.groupby(df_, :U_BE_V_)
	unc_by_ibrb = [
		(g[1, :U_BE_V_], Statistics.std(g[!, :I_B_A_])) for g = grouped]
	max_unc = maximum(
		filter(x -> !isnan(x), [Statistics.std(g[!, :I_B_A_]) for g = grouped]))
	for (v, u) = unc_by_ibrb
		df_[df_.U_BE_V_ .== v, :UNCERTAINTY] .= isnan(u) ? 0 : u
	end
end

# ╔═╡ da42ba6e-923f-11eb-3da9-7154fa055b81
# Mean uncertainty seems too high?
TRANSISTOR_IN_CURRENT_UNCERTAINTY = 1e-5 #Statistics.mean(TRANSISTOR_IN_FILES[1][TRANSISTOR_IN_FILES[1].UNCERTAINTY .> 0,:UNCERTAINTY])

# ╔═╡ 333c30cc-67b6-4841-8d1c-fd65e84193bb
@StatsPlots.df TRANSISTOR_IN_FILES[1] histogram(:UNCERTAINTY, bins=100)

# ╔═╡ 8bac6cd4-912b-11eb-111d-5bb74319707e
begin
	p = @StatsPlots.df TRANSISTOR_IN_FILES[1] plot(:U_BE_V_, 1e3 .* :I_B_A_,
	title="Eingangskennlinie", xlabel="\$U_{BE} / V\$", size=(600,300),
	ylabel="\$I_B / mA\$", label="$(TRANSISTOR_IN_TYPES[1]) Ω", legend=:topleft,
	bottom_margin=20*Plots.px)
	Plots.savefig("../img/transistor_eingangskennlinie_plot.pdf")
	p
end

# ╔═╡ cea791e8-9186-11eb-35de-6d0ef8e658ff
from, to = 146, 160

# ╔═╡ 1f131b8e-9187-11eb-1a9d-cf53ae362cd9
transistor_in_df = TRANSISTOR_IN_FILES[1][from:to, :];

# ╔═╡ b4d428ee-9186-11eb-0ce6-27365a5c7918
@StatsPlots.df transistor_in_df plot(:U_BE_V_, :I_B_A_)

# ╔═╡ 7d2a73de-9187-11eb-0595-99a61c5d32e0
begin
	linreg_residual_plot(transistor_in_df.U_BE_V_, mfl.(transistor_in_df.I_B_A_, 1e-5), figsize=(800, 400), grid=2,
	xlabel="\$U_{BE} / V\$",
	ylabel="\$I_B / mA\$", legend=:topleft,
	bottom_margin=20*Plots.px)
	Plots.savefig("../img/transistor_eingangskennlinie_ud.pdf")
	Plots.current()
end

# ╔═╡ 08f65f64-9189-11eb-332a-ef7613ae074c
transistor_in_fit = linreg(transistor_in_df.U_BE_V_,  mfl.(transistor_in_df.I_B_A_, TRANSISTOR_IN_CURRENT_UNCERTAINTY))

# ╔═╡ 134d0286-9189-11eb-2105-779852bc70fc
md"""Spannung ($U_{BE}$), ab der der Transistor leitet:"""

# ╔═╡ f163354e-9188-11eb-1d1d-797a3bdc58a5
transistor_in_threshold = -transistor_in_fit.b/transistor_in_fit.m

# ╔═╡ 2b217ad8-02e5-4653-b8f1-cba770fcb710
md"""### Messprogramm"""

# ╔═╡ 182e10cc-6c7e-4b5f-83aa-b0bf01fc77ab
begin
	struct InState
		U_BE::Float64
	end
	
	function initialize_system()::InState
		InState(0)
	end
	
	function update_voltage(s, step=0.01)::InState
		InState(s.U_BE + step)
	end
	
	function measure_voltage(s)::Float64
		s.U_BE
	end
	
	function measure_current(s)::Float64
		0.1
	end
	
	function measure_input_response()
		system = initialize_system()
		U_BE_max = 0.8
		step = 0.01
		steps = round(Int, div(U_BE_max, step))
		data = DF.DataFrame(U_BE_V_=zeros(steps), I_B_A_=zeros(steps))
		i = 1
		
		while U_BE_max > (v = measure_voltage(system))
			system = update_voltage(system, step)
			data[i, :] .= [v, measure_current(system)]
			i += 1
		end
		
		data
	end
end

# ╔═╡ 685ece55-1f2d-4297-93ef-352428f8128a
measure_input_response()

# ╔═╡ 02f12004-912d-11eb-3bf5-5d37e49f1ceb
md"""## Stromsteuerkennlinie"""

# ╔═╡ 09c53846-912d-11eb-2ff4-eb577dd43657
TRANSISTOR_CTRL_FILES = map(f -> f |> (read_csv ∘ to_path), TRANSISTOR_CONTROL_FILENAMES);

# ╔═╡ 5290d8f6-923c-11eb-0af3-7db63bbe1adc
TRANSISTOR_CTRL_FILES[1]

# ╔═╡ da1051f8-9138-11eb-2571-b5fa567099b5
# Calculate and assign statistical uncertainty from measurement fluctuations
begin
	for df_ = TRANSISTOR_CTRL_FILES
		df_.UNCERTAINTY = zeros(size(df_)[1])
		sort!(df_, :I_B_R_B_V_)
		grouped = DF.groupby(df_, :I_B_R_B_V_)
		unc_by_ibrb = [
			(g[1, :I_B_R_B_V_], Statistics.std(g[!, :I_C_R_C_V_])) for g = grouped]
		max_unc = maximum(
			filter(x -> !isnan(x), [Statistics.std(g[!, :I_C_R_C_V_]) for g = grouped]))
		for (v, u) = unc_by_ibrb
			df_[df_.I_B_R_B_V_ .== v, :UNCERTAINTY] .= isnan(u) ? 0 : u
		end
	end
end

# ╔═╡ 5ee6b67a-923c-11eb-03dd-b51eea99744e
VOLTAGE_UNCERTAINTIES = [
		Statistics.mean(TRANSISTOR_CTRL_FILES[i][TRANSISTOR_CTRL_FILES[i].UNCERTAINTY .> 0, :UNCERTAINTY])
		for i = 1:length(TRANSISTOR_CTRL_FILES)]

# ╔═╡ bc5813e6-9230-11eb-37f0-63831fb820a6
md"""TODO: What's up with the voltage intervals? Smallest bin is 20/2^16 wide"""

# ╔═╡ f96356e0-9142-11eb-311d-d53f443f69c0
begin
	filtered_hist_errors = [
		TRANSISTOR_CTRL_FILES[i][TRANSISTOR_CTRL_FILES[i].UNCERTAINTY .> 0, :UNCERTAINTY]
		for i = 1:length(TRANSISTOR_CTRL_FILES)]
	unchist = Plots.histogram(filtered_hist_errors, t=:stephist, title="Verteilung Unsicherheit (Steuerkennlinie)", xlabel="\$\\sigma(I_C R_C) / V\$", ylabel="n", labels=permutedims(TRANSISTOR_CONTROL_TYPES), xlim=(0, 1.2e-5))
	Plots.savefig(unchist, "../img/transistor_fehler_verteilung.pdf")
	unchist
end

# ╔═╡ 92eab876-913e-11eb-3c37-814c188f02ab
begin
	grouped = DF.groupby(TRANSISTOR_CTRL_FILES[1], :I_B_R_B_V_)
	counts_by_ibrb = vcat([[g[1, :I_B_R_B_V_]  size(g)[1]] for g = grouped] ...)
	plot(counts_by_ibrb .+ 1, scale=:log)
end

# ╔═╡ 4c1203f6-9142-11eb-2069-41830828ec8d
plot(hcat([df.I_C_R_C_V_ for df = TRANSISTOR_CTRL_FILES]...), title="Überblick Messpunkte", labels=permutedims(TRANSISTOR_CONTROL_TYPES),
xlabel="Messung #", ylabel="U / V", legend=:topleft)

# ╔═╡ 2735d847-b165-42bd-bed7-7c197153ad2f
plot(hcat([df.UNCERTAINTY for df = TRANSISTOR_CTRL_FILES]...))

# ╔═╡ e72698bc-9141-11eb-3dce-85b2e8e6982f
@StatsPlots.df TRANSISTOR_CTRL_FILES[3] plot(:I_B_R_B_V_, :UNCERTAINTY)

# ╔═╡ f4033be0-913b-11eb-0364-470007772723
DATA_FRACTION = 10

# ╔═╡ c908a340-913f-11eb-2f3e-1982ed3bcb7f
SKIP_START = 3500

# ╔═╡ 491c97c0-913c-11eb-3da6-89868029ebe4
SKIP_LAST = 300

# ╔═╡ 5c7a94c0-912d-11eb-11df-3d388aebbe8f
# Plot Steuerlinien
begin
	Plots.plot()
	for (i, t) = enumerate(TRANSISTOR_CONTROL_TYPES)
		RC, RB = 1, 1
		@StatsPlots.df TRANSISTOR_CTRL_FILES[i] plot!(
			:I_B_R_B_V_[1:DATA_FRACTION:end] ./ RB,
			:I_C_R_C_V_[1:DATA_FRACTION:end] ./ RC,
		title="Stromsteuerkennlinie", xlabel="\$I_{B} / A\$",
		ylabel="\$I_C / A\$",
		label="$(t[1])Ω", size=(700, 350),
		bottom_margin=20*Plots.px, left_margin=20*Plots.px,
		legend=:topleft, grid=2, show=false)
		
	end
	Plots.savefig(Plots.current(), "../img/transistor_steuerlinien.pdf")
end

# ╔═╡ e2550e34-9148-11eb-2ec1-9f4cfd94327f
AMPLIFICATION_FACTORS = zeros(M64, length(TRANSISTOR_CONTROL_TYPES))

# ╔═╡ 328e2ecc-9134-11eb-1109-85df0c0c06c5
function analyse_current_control()
	for (i, t) = enumerate(TRANSISTOR_CONTROL_TYPES)
		df = TRANSISTOR_CTRL_FILES[i]
		RC, RB = 1, 1
		ibrb = df.I_B_R_B_V_[SKIP_START:DATA_FRACTION:end-SKIP_LAST]
		icrc = mfl.(
				df.I_C_R_C_V_[SKIP_START:DATA_FRACTION:end-SKIP_LAST], 
				df.UNCERTAINTY[SKIP_START:DATA_FRACTION:end-SKIP_LAST])
		
		xs = ibrb ./ RB
		ys = icrc ./ RC
		lr = linreg(xs, ys)
		AMPLIFICATION_FACTORS[i] = lr.m
		
		# Shift RB
		lr_rb_low, lr_rb_up = linreg(ibrb ./ (.999*RB), ys), linreg(ibrb ./ (1.001*RB), ys)
		println("$i $t :: RB :: m = $(lr.m), sys. unc. $((lr_rb_up.m-lr_rb_low.m)/2), b = $(lr.b), sys.unc. $((lr_rb_up.b-lr_rb_low.b)/2)")
		
		# Shift RC
		lr_rc_low, lr_rc_up = linreg(xs, icrc ./ (.999*RC)), linreg(xs, icrc ./ (1.001*RC))
		println("$i $t :: RC :: m = $(lr.m), sys. unc. $((lr_rc_up.m-lr_rc_low.m)/2), b = $(lr.b), sys.unc. $((lr_rc_up.b-lr_rc_low.b)/2)")
		
		println("$i $t :: total uncertainties :: m = $(sqrt((lr_rb_up.m-lr_rb_low.m)^2 + (lr_rc_up.m-lr_rc_low.m)^2)/2)")
		
		Plots.savefig(
			linreg_residual_plot(
				1e6 .* xs, 
				1e6 .* ys,
				figsize=(800, 400), legend=:topleft, grid=2,
				xlabel="\$I_{B} / \\mu A\$",
				ylabel="\$I_C / \\mu A\$",
				left_margin=20*Plots.px,
				title="Verstärkungsfaktor RC = $(t[1])Ω, RB = $(t[2])Ω"),
			"../img/transistor_linreg_$(t[1])_$(t[2]).pdf")
	end
end

# ╔═╡ 3d5139ea-9135-11eb-1b76-db090ff10e39
analyse_current_control()

# ╔═╡ 8bcac2dc-cd3d-4a22-9f5b-a6814809c8b7
AMPLIFICATION_FACTORS

# ╔═╡ ee78d1a0-914d-11eb-076f-3fc6886ff34e
begin
	linreg_residual_plot(TRANSISTOR_CONTROL_RESISTANCES, AMPLIFICATION_FACTORS,
	legend=:bottomleft, grid=2, figsize=(700, 400))
end

# ╔═╡ fb7e507c-9135-11eb-1d4e-5d62697f2e46
function polynomic_model(x, p)
	r = zeros(Float64, size(x))
	for (i, p_) = enumerate(p[2:end])
		r .+= p_ .* x.^i
	end
	r
end

# ╔═╡ 5a0a2374-9149-11eb-1412-cd1bde737f4e
function antiproportional_model(x, p)
	p[1] .* x .^ (-p[2])
end

# ╔═╡ 6adb9912-9149-11eb-0421-e74c3f108628
amplification_fit = LsqFit.curve_fit(antiproportional_model, [t[1] for t = TRANSISTOR_CONTROL_TYPES], nv(AMPLIFICATION_FACTORS), [2e4, 1.])

# ╔═╡ 2946f8c4-914a-11eb-14c5-07308e288710
sqrt.(LsqFit.estimate_covar(amplification_fit))

# ╔═╡ 215ef856-9149-11eb-384e-9d2195e80083
begin
	Plots.scatter(TRANSISTOR_CONTROL_RESISTANCES, AMPLIFICATION_FACTORS, grid=2, label="Verstärkungsfaktor", xlabel="\$R_C / \\Omega\$", ylabel="\$B\$")
	xs = range(minimum(TRANSISTOR_CONTROL_RESISTANCES), maximum(TRANSISTOR_CONTROL_RESISTANCES), length=100)
	Plots.plot!(xs, antiproportional_model(xs, amplification_fit.param),
	label="\$B = $(round(amplification_fit.param[1]/1e6, digits=1)) \\cdot 10^6 R^{-$(round(amplification_fit.param[2], digits=3))} \$")
	Plots.savefig(current(), "../img/transistor_verstärkung_antiproportional.pdf")
end

# ╔═╡ a37827e0-9149-11eb-340b-0de6ffef262f
md"""## Ausgangskennlinie"""

# ╔═╡ 40e377c8-914f-11eb-191c-d5b2ff2c8b00
TRANSISTOR_OUT_FILES = map(read_csv ∘ to_path, TRANSISTOR_OUT_FILENAMES);

# ╔═╡ 508c7134-914f-11eb-1fcd-bfa86f93adf3
TRANSISTOR_OUT_FILES[1]

# ╔═╡ 8e22019e-914f-11eb-1d07-af445686bd45
@StatsPlots.df TRANSISTOR_OUT_FILES[1] plot(:U_CE_V_, :I_C_R_C_V_)

# ╔═╡ 532a44fe-9152-11eb-3489-d702e18da18e
function plot_all_out_responses()
	UB = [t[1] for t = TRANSISTOR_OUT_TYPES]
	RC = [t[2] for t = TRANSISTOR_OUT_TYPES]
	RB = [t[3] for t = TRANSISTOR_OUT_TYPES]
	Is = hcat([f.I_C_R_C_V_./RC[i] for (i,f) = enumerate(TRANSISTOR_OUT_FILES)]...)
	Us = hcat([f.U_CE_V_ for f = TRANSISTOR_OUT_FILES]...)
	plot(Us, Is, title="Ausgangskennlinien",
		label=permutedims(map(t -> "\$$(t[4])V, I_B = $(round(1e6 * t[4]/t[3], digits=3)) \\mu A\$", TRANSISTOR_OUT_TYPES)),
		xscale=:linear, xlim=(0,0.5), legend=:topleft,
	xlabel="\$U_{CE} / V\$", ylabel="\$I_C / A\$", grid=2, size=(800, 400),
	bottom_margin=10*Plots.px, left_margin=10*Plots.px)
end

# ╔═╡ 861b3cec-9152-11eb-0a64-95e725596741
Plots.savefig(plot_all_out_responses(), "../img/transistor_ausgangskennlinie_raw.pdf")

# ╔═╡ 96c9b94c-9157-11eb-238a-c1f23f8156c9
current()

# ╔═╡ ef8a37fd-e393-42ba-8692-bde1efca7d2a
for df_ = TRANSISTOR_OUT_FILES
	df_.UNCERTAINTY = zeros(size(df_)[1])
	sort!(df_, :U_CE_V_)
	grouped = DF.groupby(df_, :U_CE_V_)
	unc_by_ibrb = [
		(g[1, :U_CE_V_], Statistics.std(g[!, :I_C_R_C_V_])) for g = grouped]
	max_unc = maximum(
		filter(x -> !isnan(x), [Statistics.std(g[!, :I_C_R_C_V_]) for g = grouped]))
	for (v, u) = unc_by_ibrb
		df_[df_.U_CE_V_ .== v, :UNCERTAINTY] .= isnan(u) ? 0 : u
	end
end

# ╔═╡ bf7c8652-4c4e-4066-9b0a-44edc626f12a
TRANSISTOR_OUT_RESISTANCES = zeros(M64, size(TRANSISTOR_OUT_FILES))

# ╔═╡ 9aa4b631-39dd-4e63-bfb1-48734dccd478
for (i, (ub, rc, rb)) = enumerate(TRANSISTOR_OUT_TYPES)
	df = TRANSISTOR_OUT_FILES[i]
	min_uce_ix = argmin(abs.(df.U_CE_V_ .- 0.04))
	max_uce_ix = argmin(abs.(df.U_CE_V_ .- 0.11))
	step = round(Int, (max_uce_ix-min_uce_ix)/100)
	df = df[min_uce_ix:step:max_uce_ix, :]
	
	lr = linreg(TRANSISTOR_OUT_FILES[i][min_uce_ix:max_uce_ix, :U_CE_V_], mfl.(TRANSISTOR_OUT_FILES[i][min_uce_ix:max_uce_ix, :I_C_R_C_V_], TRANSISTOR_OUT_FILES[i][min_uce_ix:max_uce_ix, :UNCERTAINTY]))
	println(lr)
	TRANSISTOR_OUT_RESISTANCES[i] = 1/lr.m
	
	Plots.savefig(
		linreg_residual_plot(df.U_CE_V_, mfl.(df.I_C_R_C_V_, df.UNCERTAINTY),
			xlabel="\$U_{CE} / V\$", ylabel="\$I_C / A\$", legend=:bottomright,
			figsize=(800,400)),
		"../img/transistor_ausgang_linreg_$(ub)V.pdf")
end

# ╔═╡ 77b44755-6c8c-4dbe-9212-30ba2de95398
current()

# ╔═╡ e7a40f9f-8926-453d-ab1f-7e0742625029
begin
	scatter([t[4] for t = TRANSISTOR_OUT_TYPES], log.(TRANSISTOR_OUT_RESISTANCES),
	label="\$R(I_B)\$", xlabel="\$I_B / A\$", ylabel="\$R / \\Omega\$",
	size=(600,300), bottom_margin=10*Plots.px)
	Plots.savefig("../img/transistor_ausgang_widerstand_strom.pdf")
	Plots.current()
end

# ╔═╡ f4e808f9-a796-4924-b203-b4efbaf647fb
linreg_residual_plot([t[4] for t = TRANSISTOR_OUT_TYPES], log.(TRANSISTOR_OUT_RESISTANCES),
	label="\$R(I_B)\$", xlabel="\$I_B / A\$", ylabel="\$R / \\Omega\$",
	size=(600,300), bottom_margin=10*Plots.px)

# ╔═╡ dea891c6-0f3f-4d52-95d1-afd67b852113
TRANSISTOR_OUT_RESISTANCES

# ╔═╡ Cell order:
# ╠═d2f1d098-8d98-11eb-09fc-fd66808c6bf3
# ╠═e6129662-8d98-11eb-1aa7-1d81065bd20e
# ╠═e8f38b20-8d98-11eb-1b38-fb6306463607
# ╠═ebb2b502-8d98-11eb-038f-c113db7716b5
# ╠═d2130e1c-912b-11eb-30fd-0df33de96c00
# ╠═eedfc774-8d98-11eb-199e-bb08abb3a3a0
# ╠═9eb63468-9126-11eb-0b4a-ff6e9cbc050b
# ╠═abc71cbc-9126-11eb-1e54-7d06e0b8e8f0
# ╠═f0e1667c-9149-11eb-16f8-8fcbfd3d3613
# ╠═bab2acdc-9126-11eb-0ad8-3bef85d16ff7
# ╠═d928a266-9126-11eb-3533-f7ebb2eb86e4
# ╠═eceb5c30-9126-11eb-2980-7334d2049300
# ╠═1850b202-912b-11eb-1166-2bc8af93ac86
# ╠═430b90ac-912b-11eb-0fac-8b6ab89f58a4
# ╠═530c6e74-912b-11eb-180b-d1ff3b32faeb
# ╠═6a5d9ca0-923f-11eb-05d0-c350ba6b402c
# ╠═da42ba6e-923f-11eb-3da9-7154fa055b81
# ╠═333c30cc-67b6-4841-8d1c-fd65e84193bb
# ╠═8bac6cd4-912b-11eb-111d-5bb74319707e
# ╠═cea791e8-9186-11eb-35de-6d0ef8e658ff
# ╠═1f131b8e-9187-11eb-1a9d-cf53ae362cd9
# ╠═b4d428ee-9186-11eb-0ce6-27365a5c7918
# ╠═7d2a73de-9187-11eb-0595-99a61c5d32e0
# ╠═08f65f64-9189-11eb-332a-ef7613ae074c
# ╠═134d0286-9189-11eb-2105-779852bc70fc
# ╠═f163354e-9188-11eb-1d1d-797a3bdc58a5
# ╠═2b217ad8-02e5-4653-b8f1-cba770fcb710
# ╠═182e10cc-6c7e-4b5f-83aa-b0bf01fc77ab
# ╠═685ece55-1f2d-4297-93ef-352428f8128a
# ╠═02f12004-912d-11eb-3bf5-5d37e49f1ceb
# ╠═09c53846-912d-11eb-2ff4-eb577dd43657
# ╠═5290d8f6-923c-11eb-0af3-7db63bbe1adc
# ╠═da1051f8-9138-11eb-2571-b5fa567099b5
# ╠═5ee6b67a-923c-11eb-03dd-b51eea99744e
# ╠═bc5813e6-9230-11eb-37f0-63831fb820a6
# ╠═f96356e0-9142-11eb-311d-d53f443f69c0
# ╠═92eab876-913e-11eb-3c37-814c188f02ab
# ╠═4c1203f6-9142-11eb-2069-41830828ec8d
# ╠═2735d847-b165-42bd-bed7-7c197153ad2f
# ╠═e72698bc-9141-11eb-3dce-85b2e8e6982f
# ╠═f4033be0-913b-11eb-0364-470007772723
# ╠═c908a340-913f-11eb-2f3e-1982ed3bcb7f
# ╠═491c97c0-913c-11eb-3da6-89868029ebe4
# ╠═5c7a94c0-912d-11eb-11df-3d388aebbe8f
# ╠═e2550e34-9148-11eb-2ec1-9f4cfd94327f
# ╠═328e2ecc-9134-11eb-1109-85df0c0c06c5
# ╠═3d5139ea-9135-11eb-1b76-db090ff10e39
# ╠═8bcac2dc-cd3d-4a22-9f5b-a6814809c8b7
# ╠═6adb9912-9149-11eb-0421-e74c3f108628
# ╠═2946f8c4-914a-11eb-14c5-07308e288710
# ╠═215ef856-9149-11eb-384e-9d2195e80083
# ╠═ee78d1a0-914d-11eb-076f-3fc6886ff34e
# ╠═fb7e507c-9135-11eb-1d4e-5d62697f2e46
# ╠═5a0a2374-9149-11eb-1412-cd1bde737f4e
# ╠═a37827e0-9149-11eb-340b-0de6ffef262f
# ╠═40e377c8-914f-11eb-191c-d5b2ff2c8b00
# ╠═508c7134-914f-11eb-1fcd-bfa86f93adf3
# ╠═8e22019e-914f-11eb-1d07-af445686bd45
# ╠═532a44fe-9152-11eb-3489-d702e18da18e
# ╠═861b3cec-9152-11eb-0a64-95e725596741
# ╠═96c9b94c-9157-11eb-238a-c1f23f8156c9
# ╠═ef8a37fd-e393-42ba-8692-bde1efca7d2a
# ╠═bf7c8652-4c4e-4066-9b0a-44edc626f12a
# ╠═9aa4b631-39dd-4e63-bfb1-48734dccd478
# ╠═77b44755-6c8c-4dbe-9212-30ba2de95398
# ╠═e7a40f9f-8926-453d-ab1f-7e0742625029
# ╠═f4e808f9-a796-4924-b203-b4efbaf647fb
# ╠═dea891c6-0f3f-4d52-95d1-afd67b852113
