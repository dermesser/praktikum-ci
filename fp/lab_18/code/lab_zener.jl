### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# ╔═╡ e6129662-8d98-11eb-1aa7-1d81065bd20e
# Changing utility code
begin
	using Revise
	push!(LOAD_PATH, pwd())
	using analysis
end

# ╔═╡ d2f1d098-8d98-11eb-09fc-fd66808c6bf3
begin
	import CSV
	import DataFrames
	import Measurements
	import LsqFit
	
	using Plots

	const mea = Measurements
	const DF = DataFrames

	Plots.gr(html_output_format=:png)
	Plots.default(tickfont = ("sans-serif", 10), legendfontsize=10)
end

# ╔═╡ e8f38b20-8d98-11eb-1b38-fb6306463607
begin
	BASE_PATH = "../data/"
	
	function to_path(fn::String)::String
		return string(BASE_PATH, fn)
	end
end

# ╔═╡ ebb2b502-8d98-11eb-038f-c113db7716b5
function read_csv(file::String)
	CSV.File(to_path(file), normalizenames=true) |> DataFrames.DataFrame
end

# ╔═╡ eedfc774-8d98-11eb-199e-bb08abb3a3a0
md"""## Zener diode

"""

# ╔═╡ 19a9750c-8daa-11eb-376a-45b39c3c9fda
begin
	VOLTAGE_RANGE = 20  # V
	VOLTAGE_BINS = 2^16
	VOLTAGE_UNCERTAINTY = VOLTAGE_RANGE / VOLTAGE_BINS / sqrt(12)
end

# ╔═╡ 0fe539ca-8d99-11eb-0d7b-1fbdfce4ad6e
const RVs = [47, 100, 220, 470, 1000, 4700, 10000]  # Ohm
#const RVs = [47, 100, 1000] # Ohm

# ╔═╡ 84c5aec2-8da1-11eb-3fea-6fdccc5622db
ZENER_DATA_FILES = ["Diode_$(r)Ohm.csv" for r = RVs]

# ╔═╡ a40c4340-8da1-11eb-1d62-dfad02868f3b
ZENER_DATA = map(read_csv, ZENER_DATA_FILES)

# ╔═╡ 72cdccc4-8da4-11eb-0ba2-ed2152f8ad58
md"""

The response curve of the Zener diode is: $I_Z(U_Z)$.

Here, $U_Z = U_E - U_A$ and $I_Z = U_Z / R_V$, as there is no current flowing between the output poles.

"""

# ╔═╡ c7a9ce0e-8da5-11eb-3a47-81b62743f1bb
begin
	df_ = ZENER_DATA[1]
	Umax = maximum(df_.U_A_V_)

	plot(df_.U_E_V_, df_.U_A_V_, label="$(RVs[1])Ω",
		xlabel="\$U_E~/~V\$", ylabel="\$U_A~/~V\$",
		title="Zener-Diode: Spannungsstabilisierung. U_max = $(round(Umax, digits=2))V", legend=:bottomright, size=(800, 400),
	left_margin=20*Plots.px, bottom_margin=20*Plots.px)
	
	# Plot remaining plots
	[plot!(df__.U_E_V_, df__.U_A_V_, label="$(l)Ω") for (df__, l) = zip(ZENER_DATA[2:end], RVs[2:end])]
	
	U_e = df_.U_E_V_[1:800]
	plot!(U_e, U_e, label="Vergleich")
	
	savefig(current(), "../img/zener_voltage_stabilization.pdf")
	current()
end

# ╔═╡ 4418af5e-8daa-11eb-0437-71d255f96f02
begin
	df = ZENER_DATA[1]
	U_z = df.U_A_V_
	I_z = (df.U_E_V_ - df.U_A_V_) / RVs[1]
	plot(U_z, I_z, label="R = $(RVs[1])Ω",
		xlabel="\$U_Z~/~V\$", ylabel="\$I_Z= U_Z~/~R_V~~(A)\$",
		title="Zener Kennlinie", legend=:topleft, size=(800, 400),
		left_margin=20*Plots.px, bottom_margin=20*Plots.px)
	
	for i = 2:length(ZENER_DATA)
		df = ZENER_DATA[i]
		U_z = df.U_A_V_
		I_z = (df.U_E_V_ - df.U_A_V_) / RVs[i]
		plot!(U_z, I_z, label="R = $(RVs[i])Ω")
	end
	savefig(current(), "../img/zener_response_curve.pdf")
	current()
end

# ╔═╡ df314fce-8dab-11eb-3d26-ff4537ebef77
	struct Smoothing
		G::M64
		S::M64
	end

# ╔═╡ 28829df4-8da2-11eb-0622-1945685d8424
function relative_smoothing_at(df; Ua_work=2.6, Ua_area=0.05)
	ix = argmin(abs.(df.U_A_V_ .- Ua_work))
	minix, maxix = (argmin(abs.(df.U_A_V_ .- (Ua_work - Ua_area))),
		argmin(abs.(df.U_A_V_ .- (Ua_work + Ua_area))))
	
	lr = linreg(mfl.(df.U_A_V_[minix:maxix], VOLTAGE_UNCERTAINTY), mfl.(df.U_E_V_[minix:maxix], VOLTAGE_UNCERTAINTY))
	G = lr.m
	S = G * df.U_A_V_[ix] / df.U_E_V_[ix]
	
	Smoothing(G, S)
	
	#linreg_residual_plot(mfl.(df.U_A_V_[minix:maxix], VOLTAGE_UNCERTAINTY), mfl.(df.U_E_V_[minix:maxix], VOLTAGE_UNCERTAINTY), xlabel="\$U_A~/~V\$",
	#	ylabel="\$U_E~/~V\$", legend=:topleft)
end

# ╔═╡ c6b1f630-8da2-11eb-3bc6-119e9b4594cb
#[Plots.savefig(p, "../img/zener_smoothing_$(r).pdf") for (p, r) = zip(relative_smoothing_at.(ZENER_DATA), RVs)]

# ╔═╡ 34114e36-9f81-4667-8ebc-397be397c2fc
smooths = relative_smoothing_at.(ZENER_DATA)

# ╔═╡ ac7c16d8-34b0-41a7-8be6-1e46cde5ddcf
begin
	xs = RVs
	ys_g = nv([y.G for y = smooths])
	ys_s = nv([y.S for y = smooths])
	
	lr = linreg(xs, ys_g)
	
	Plots.scatter(xs, ys_g, label="G", xlabel="R / Ω", ylabel="G, S",
		legend=:topleft, size=(600,300))
	Plots.scatter!(xs, ys_s, label="S")
	Plots.plot!(xs, nv(lr.m) .* xs .+ nv(lr.b), color=:gray, label="G ≈ $(round(1e3 * nv(lr.m), digits=2))e-3 R + $(round(nv(lr.b), digits=3))")
	Plots.savefig("../img/zener_smoothing_factors.pdf")
end

# ╔═╡ 563c7b7d-b33d-4c50-bc6d-13d6b0658c2a
md"""### Messprogramm"""

# ╔═╡ 6a9022f6-f058-4675-be83-c7362baa8516
begin
	struct ZenerState
		U_set::Float64
	end
	
	function initialize_system()::ZenerState
		ZenerState(0)
	end
	
	function update_voltage(s, step=0.01)::ZenerState
		ZenerState(s.U_set + step)
	end
	
	function measure_set_voltage(s)::Float64
		s.U_set
	end
	
	function measure_r_voltage(s)::Float64
		0.1
	end
	
	function measure_input_response()
		system = initialize_system()
		U_set_max = 10
		interval = 0.005 # 5 ms
		step = 0.005
		steps = round(Int, div(U_set_max, step)) + 1
		data = DF.DataFrame(U_E_V_=zeros(steps), U_A_V_=zeros(steps))
		i = 1
		
		while U_set_max > (v = measure_set_voltage(system))
			data[i, :] .= (v, measure_r_voltage(system))
			i += 1
			system = update_voltage(system, step)
			sleep(interval)
		end
		
		nothing
	end
end

# ╔═╡ e9889d22-f4c5-4e27-aba9-cc3ca68dd3e1
@time measure_input_response()

# ╔═╡ Cell order:
# ╠═d2f1d098-8d98-11eb-09fc-fd66808c6bf3
# ╠═e6129662-8d98-11eb-1aa7-1d81065bd20e
# ╠═e8f38b20-8d98-11eb-1b38-fb6306463607
# ╠═ebb2b502-8d98-11eb-038f-c113db7716b5
# ╠═eedfc774-8d98-11eb-199e-bb08abb3a3a0
# ╠═19a9750c-8daa-11eb-376a-45b39c3c9fda
# ╠═0fe539ca-8d99-11eb-0d7b-1fbdfce4ad6e
# ╠═84c5aec2-8da1-11eb-3fea-6fdccc5622db
# ╠═a40c4340-8da1-11eb-1d62-dfad02868f3b
# ╠═72cdccc4-8da4-11eb-0ba2-ed2152f8ad58
# ╠═c7a9ce0e-8da5-11eb-3a47-81b62743f1bb
# ╠═4418af5e-8daa-11eb-0437-71d255f96f02
# ╠═df314fce-8dab-11eb-3d26-ff4537ebef77
# ╠═28829df4-8da2-11eb-0622-1945685d8424
# ╠═c6b1f630-8da2-11eb-3bc6-119e9b4594cb
# ╠═34114e36-9f81-4667-8ebc-397be397c2fc
# ╠═ac7c16d8-34b0-41a7-8be6-1e46cde5ddcf
# ╠═563c7b7d-b33d-4c50-bc6d-13d6b0658c2a
# ╠═6a9022f6-f058-4675-be83-c7362baa8516
# ╠═e9889d22-f4c5-4e27-aba9-cc3ca68dd3e1
