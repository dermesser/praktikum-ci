# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 11:31:28 2021

@author: 394493: Alexander Kohlgraf
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import progressbar
import os

plt.rcParams["font.size"] = 15.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.weight"] = "bold"


class Heat_Map_Plotter:
    def __init__(self, sensor_number, tile_number):
        self.path = os.path.join(
            os.getcwd(),
            "data",
            "dcmTransfer",
            "spu{}_{}.dcmTileCoords".format(sensor_number, tile_number),
        )
        self.data = pd.read_csv(
            self.path,
            names=["x", "y", "intensity"],
            sep="  | ",
            index_col=False,
            engine="python",
        )
        self.sensor_number = sensor_number
        self.tile_number = tile_number
        self.data_max = self.data.max()
        self.data_min = self.data.min()

    def plot_image(self, cutoff=np.inf, title="title", filename=None):
        spu_x_length = int(self.data_max["x"] / 2)
        spu_y_length = int(self.data_max["y"])

        image = np.zeros((spu_x_length, spu_y_length))

        for entry in self.data.itertuples():
            index_x = int(entry[1] / 2) - 1
            index_y = int(entry[2]) - 1

            if entry[3] > cutoff:
                continue
            image[index_x, index_y] = entry[3]
        plt.figure(figsize=(6.4, 4.8))
        c = plt.imshow(
            image,
            cmap="viridis",
            interpolation="nearest",
        )
        plt.title(
            title + " cutoff percentage {:.2f}".format(self.cutoff_percentage(cutoff))
        )

        plt.colorbar(c, fraction=0.046, pad=0.04)
        self.save_plot(filename)

    def plot_histogramm(self, cutoff=np.inf, title="title", filename=None):
        data_clean = self.data.drop(self.data[self.data.intensity > cutoff].index)
        plt.figure()
        data_clean["intensity"].plot(
            figsize=(8, 6),
            kind="hist",
            title=title
            + " cutoff percentage {:.2f}".format(self.cutoff_percentage(cutoff)),
            bins=100,
        )
        plt.xlabel("intensity in 1/s")
        plt.ylabel("countrate")
        plt.grid(True)
        self.save_plot(filename)

    def cutoff_percentage(self, cutoff=np.inf):
        data_clean = self.data.drop(self.data[self.data.intensity > cutoff].index)
        return 1 - data_clean.shape[0] / self.data.shape[0]

    def plot_percentile(self, filename=None):
        p = np.linspace(0, 1, 100)
        plot_p = np.array([0.8, 0.90, 0.95, 0.99])
        temp_data = self.data.drop(self.data[self.data.intensity == -1].index)
        i = temp_data["intensity"].quantile(p)
        plot_i = temp_data["intensity"].quantile(plot_p)
        fig, axs = plt.subplots(figsize=(12, 8))
        plt.title(
            "quantile plot of sensor {} tile number {}".format(
                self.sensor_number, self.tile_number
            )
        )
        plt.ylim(0, 1.1)
        plt.plot(i, p)
        for counter, i in enumerate(plot_i):
            axs.axvline(i, alpha=0.8, linestyle=":", color="red")
            axs.text(
                i,
                plot_p[counter] - 0.05,
                "{}%".format(plot_p[counter]),
                size=10,
                alpha=0.8,
            )
            axs.text(
                i,
                0,
                "{:.1f}".format(i),
                size=10,
                alpha=0.8,
            )
        plt.grid(True)
        plt.xlabel("intensity")
        plt.ylabel("percentile")
        self.save_plot(filename)

    def save_plot(self, filename):
        if filename:
            path = os.path.join(
                os.getcwd(),
                "protokoll",
                "images",
                filename,
            )

            plt.savefig(path)
        else:
            plt.show()

    def plot_disabled_spad(self, filename=None):
        ordered_data = self.data.sort_values(by="intensity", ascending=False)

        ordered_data = ordered_data.drop(
            ordered_data[ordered_data.intensity == -1].index
        )
        spad_array = np.linspace(0, len(ordered_data), num=len(ordered_data))
        spad_array_sum = np.zeros(len(spad_array))
        array_sum = ordered_data["intensity"].sum()
        for i, entry in progressbar.progressbar(enumerate(ordered_data.itertuples())):
            array_sum -= entry[3]
            spad_array_sum[i] = array_sum

        plt.figure(figsize=(10, 6))

        plt.plot(spad_array / spad_array[-1], spad_array_sum / spad_array_sum[0])
        plt.grid(True)
        plt.title("Intensity sum for numbers of SPADs")
        plt.xlabel("number of disabled SPADs")
        plt.ylabel("intensity sum over all enabled SPADs")
        self.save_plot(filename)


if __name__ == "__main__":

    heat_map_1_3 = Heat_Map_Plotter(1, 3)
    heat_map_1_3.plot_image(title="dark count map", filename="1_3_heatmap.pdf")
    heat_map_1_3.plot_image(
        title="darc count map", filename="1_3_cutoff_2_heatmap.pdf", cutoff=2
    )
    heat_map_1_3.plot_disabled_spad(filename="1_3_disabled_spad.pdf")
    heat_map_1_3.plot_percentile(filename="1_3_percentile.pdf")
    heat_map_1_6 = Heat_Map_Plotter(1, 6)
    heat_map_1_6.plot_image(title="dark count map", filename="1_6_heatmap.pdf")
    heat_map_1_6.plot_image(
        title="darc count map", filename="1_6_cutoff_2_heatmap.pdf", cutoff=2
    )
    heat_map_1_6.plot_disabled_spad(filename="1_6_disabled_spad.pdf")
    heat_map_1_6.plot_percentile(filename="1_6_percentile.pdf")
    heat_map_2_3 = Heat_Map_Plotter(2, 3)
    heat_map_2_3.plot_image(title="dark count map", filename="2_3_heatmap.pdf")
    heat_map_2_3.plot_image(
        title="darc count map", filename="2_3_cutoff_2_heatmap.pdf", cutoff=2
    )
    heat_map_2_3.plot_disabled_spad(filename="2_3_disabled_spad.pdf")
    heat_map_2_3.plot_percentile(filename="2_3_percentile.pdf")
    heat_map_2_6 = Heat_Map_Plotter(2, 6)
    heat_map_2_6.plot_image(title="darc count map", filename="2_6_heatmap.pdf")
    heat_map_2_6.plot_image(
        title="darc count map", filename="2_6_cutoff_2_heatmap.pdf", cutoff=2
    )
    heat_map_2_6.plot_disabled_spad(filename="2_6_disabled_spad.pdf")
    heat_map_2_6.plot_percentile(filename="2_6_percentile.pdf")

    """
    heat_map_1_3.plot_disabled_spad()
    heat_map_1_3.plot_percentile(filename="percentile_1_3.png")
    heat_map_1_3.plot_image(cutoff=2)
    heat_map_1_3.plot_histogramm()
    heat_map_1_3.plot_histogramm(2, filename="cutoff_2_histo_of_heatmap.png")
    """