# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 11:31:28 2021

@author: 394493: Alexander Kohlgraf
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import optimize
from matplotlib.patches import Rectangle
import scipy as sc
import os

plt.rcParams["font.size"] = 15.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.weight"] = "bold"
# plt.rcParams["text.usetex"] = True


def gauss(x, mu, sigma, alpha):
    return (
        alpha
        / ((2 * np.pi) ** (1 / 2))
        * (1 / sigma)
        * np.exp(-((x - mu) ** 2) / (2 * sigma ** 2))
    )


class Energy_Plot:
    def __init__(self, single: bool, empty=False):
        if single and empty:
            self.path = os.path.join(
                os.getcwd(),
                "data",
                "Energy_single_empty.log",
            )
        elif single:
            self.path = os.path.join(
                os.getcwd(),
                "data",
                "Energy_single.log",
            )
        elif not single:
            self.path = os.path.join(
                os.getcwd(),
                "data",
                "Energy_coinc.log",
            )
        self.data = pd.read_csv(
            self.path,
            delimiter=";",
            header=None,
            engine="python",
            skiprows=3,
        )
        self.data = self.data.T
        self.data.drop(self.data.tail(1).index, inplace=True)
        self.data = self.data[0].str.split(",", expand=True).astype(int)
        self.data.columns = ["energy", "entries"]

    def plot_raw_data(self, filename=None, lines_array=None):
        fig, axs = plt.subplots(figsize=(10, 6))
        plt.plot(self.data["energy"], self.data["entries"])
        plt.title("Energy spectrum")
        plt.xlabel("energy [keV]")
        plt.ylabel("entries")
        self.add_lines(axs, lines_array)
        plt.grid(True)
        self.save_plot(filename)

    def add_lines(self, axs, lines_array):
        if lines_array is None:
            return
        for counter, i in enumerate(lines_array):
            axs.axvline(i, alpha=0.8, linestyle=":", color="red")
            axs.text(
                i,
                0,
                "",
                size=10,
                alpha=0.8,
            )
            axs.text(
                i,
                0,
                "{:.1f}".format(i),
                size=10,
                alpha=0.8,
            )

    def fit_gauss(
        self, value, bottomborder, topborder, alpha, plot_in_border=False, filename=None
    ):
        fit_data = self.data.drop(self.data[self.data.energy < bottomborder].index)
        fit_data = fit_data.drop(fit_data[fit_data.energy > topborder].index)
        plt.figure(figsize=(10, 8))
        if plot_in_border:
            plt.plot(fit_data["energy"], fit_data["entries"])
        else:
            plt.plot(self.data["energy"], self.data["entries"])
        popt, pcov = sc.optimize.curve_fit(
            gauss,
            fit_data["energy"],
            fit_data["entries"],
            p0=[value, 10, alpha],
            maxfev=1000,
        )
        mu, sigma, alpha = popt
        plt.plot(fit_data["energy"], gauss(fit_data["energy"], mu, sigma, alpha))

        plt.title("energy resolution with gauss fit")
        plt.xlabel("energy [keV]")
        plt.ylabel("entries")
        extra1 = Rectangle(
            (0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0
        )
        extra2 = Rectangle(
            (0, 0), 1, 1, fc="w", fill=False, edgecolor="none", linewidth=0
        )

        plt.legend(
            [extra1, extra2],
            ("$\mu$ {:.0f} keV".format(mu), "$\sigma$ {:.0f} keV".format(sigma)),
        )
        plt.grid(True)
        self.save_plot(filename)

    def save_plot(self, filename):
        if filename:
            path = os.path.join(
                os.getcwd(),
                "protokoll",
                "images",
                filename,
            )

            plt.savefig(path)
        else:
            plt.show()


if __name__ == "__main__":
    energy_data_coinc = Energy_Plot(False, False)
    energy_data_coinc.plot_raw_data(
        filename="energy_data_coinc.pdf", lines_array=[511, 307, 202]
    )
    energy_data_coinc.fit_gauss(
        510, 470, 570, 10000, True, "energy_data_coinc_gauss_cut.pdf"
    )
    energy_data_coinc.fit_gauss(
        510, 470, 570, 10000, False, "energy_data_coinc_gauss.pdf"
    )
    energy_data_single_empty = Energy_Plot(True, True)
    energy_data_single_empty.plot_raw_data(
        filename="energy_data_single_empty.pdf", lines_array=[628, 401, 307, 202]
    )
    energy_data_single = Energy_Plot(True, False)
    energy_data_single.plot_raw_data(
        filename="energy_data_single.pdf", lines_array=[1275, 628, 511, 307, 202]
    )
    energy_data_single.fit_gauss(
        1200, 1170, 1350, 5000, True, "energy_data_single_gauss_cut.pdf"
    )
    energy_data_single.fit_gauss(
        1200, 1170, 1350, 5000, False, "energy_data_single_gauss.pdf"
    )
    """
    energy_data_1275.plot_raw_data()
    """