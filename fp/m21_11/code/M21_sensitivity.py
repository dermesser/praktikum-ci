# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 11:31:28 2021

@author: 394493: Alexander Kohlgraf
"""

import os
import codecs
import numpy as np
import pandas as pd
import latextable
import texttable
import nice_plots
import matplotlib.pyplot as plt
import scipy as sp
import scipy.optimize as opt

dir_path = os.path.dirname(os.path.realpath(__file__))


plt.rcParams['font.size'] = 24.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['figure.autolayout'] = True
plt.rcParams["text.usetex"] = False

def drawer(name, caption, label, content, header=None):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(len(content[0])))
        if header:
            table.header(header)
            table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        else:
            table.set_deco(texttable.Texttable.VLINES)
        table.set_precision(3)
        
        for i in content:
            table.add_row(i)
        
        text += latextable.draw_latex(table, caption, label = "tab:"+label) +'\n'
    
        text_file = codecs.open(dir_path + "\\..\\protokoll\\tables\\" + "Tabelle_" + name + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

def hist(xArrayArray, xLabel="", yLabel="", title="", labelArray = None, savename = None):
        plt.figure(figsize=(19,9))
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.title(title)
        if labelArray:
            for (x, label) in zip(xArrayArray, labelArray):
                plt.hist(x,label=label)
                plt.legend()
        else:
            for x in xArrayArray:
                plt.hist(x)
        plt.grid(True)
        if savename:
            plt.savefig(dir_path + "/../protokoll/images/" + savename +".pdf")
        plt.show()
        
def plot(xArrayArray, yArrayArray, xLabel="", yLabel="", title="", labelArray = None, savename = None):
        plt.figure(figsize=(19,9))
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.title(title)
        if labelArray:
            if len(xArrayArray) != len(yArrayArray) or len(xArrayArray) != len(labelArray):
                raise ValueError('A Not same size') 
            for (x, y, label) in zip(xArrayArray, yArrayArray, labelArray):
                plt.plot(x,y,label=label)
                plt.legend()
        else:
            if len(xArrayArray) != len(yArrayArray):
                raise ValueError('A Not same size at') 
            for (x, y) in zip(xArrayArray, yArrayArray):
                plt.plot(x,y)
        plt.grid(True)
        if savename:
            plt.savefig(dir_path + "/../protokoll/images/" + savename +".pdf")
        plt.show()

def readLog(name):
    #reads the Log format and formats time into seconds.
    df = pd.read_csv("../data/" + name + ".log", names=["time", "ignore", "counts"],
                         delimiter=": 	", 
                         skiprows=3, engine='python')
    df = df.drop("ignore",1)
    #Time
    df[["hours", "minutes", "seconds", "?"]] = df.time.str.split(":", expand=True).astype(float)
    df[["hours","minutes","seconds"]] = df[["hours","minutes","seconds"]] - df[["hours","minutes","seconds"]][:1].squeeze()
    df["time"] = df["seconds"] + df["minutes"] * 60 + df["hours"] * 3600
    
    #deltatime
    df["deltatime"] = df["time"].diff()
    #Counts per time: deltacounts
    df["deltacounts"] = df["counts"].diff()
    return df

def calcCountsMax(df):
    return df["counts"].iloc[-1]

def calcRate(df, save=None):
    #std = df[["time","counts"]].std(skipna=True)
    savefile = dir_path + "\\..\\protokoll\\images\\"  + save + ".pdf"
    m, em, b, eb, chiq, corr = nice_plots.nice_regression_plot(df["time"], df["counts"], np.ones(len(df["counts"])), np.ones(len(df["time"])), xlabel=r'Zeit [s]',
                         ylabel='Events', ylabelresidue='Residuum', title='Lineare Regression der Datei ' + save + ".log", save=savefile)
    
    #print((m, em, b, eb, chiq, corr))

    
    #old
    #std = df[["deltatime","deltacounts"]].std(skipna=True)
    #mean = df[["deltatime","deltacounts"]].mean(skipna=True)
    #r = n/t => (sigma_r/r)^2 = (sigma_n/n)^2 + (sigma_t/t)^2
    #rate = mean["deltacounts"] / mean["deltatime"]
    #sigma_rate = rate * np.sqrt( (std["deltacounts"]/mean["deltacounts"])**2 + (std["deltatime"]/mean["deltatime"])**2 )

    return m, em
def calcRateClean(name):
    df = readLog(name)
    df_back = readLog(name+"_empty")
    rate, sigma_rate = calcRate(df, name)
    rate_back, sigma_rate_back = calcRate(df_back, name+"_empty")
    #r_c = r - r_back => sigma_r_c = sigma_r + sigma_r_back
    r_clean = rate - rate_back
    sigma_r_clean = sigma_rate + sigma_rate_back
    
    #Save Results in Table
    drawer(name, "Gemessene Events für " + name.replace("_", r"\_"), "rate_"+name, [["Quelle mit Hintergrund", rate, sigma_rate],["Hintergrund",rate_back, sigma_rate_back],["Quelle Ohne Hintergrund",r_clean, sigma_r_clean]], ["Messung", r"Rate [\SI{}{\becquerel}]", r"Unsicherheit [\SI{}{\becquerel}]"])
    
    return r_clean, sigma_r_clean

    

no_singles = readLog("no_singles")
plot([no_singles["time"]], [no_singles["counts"]], "Vergangene Zeit [s]", "Events", "Number of Singles", savename="singles_example")
no_coinc = readLog("no_coinc")
plot([no_coinc["time"]], [no_coinc["counts"]], "Vergangene Zeit [s]", "Events", "Number of Coincidents")



singles_rate, singles_rate_sigma = calcRateClean("no_singles")
coinc_rate, coinc_rate_sigma = calcRateClean("no_coinc")
singles_rate_expected = 3463
coinc_rate_expected = 859
print("----------------------------------------------------")
print("Clean Rate Coincidents")
print(coinc_rate, coinc_rate_sigma)
print("expected: {}".format(coinc_rate_expected))
print("ratio: {}% +- {}%".format(coinc_rate / coinc_rate_expected, coinc_rate_sigma / coinc_rate_expected))
print("----------------------------------------------------")
print("Clean Rate Singles")
print(singles_rate, singles_rate_sigma)
print("expected: {}".format(singles_rate_expected))
print("ratio: {}% +- {}%".format(singles_rate / singles_rate_expected, singles_rate_sigma / singles_rate_expected))
print("----------------------------------------------------")
print("Sensitivity")
beta_rate = 134.707*10**3 * 0.899
print("Beta Rate {}".format(beta_rate))
print("Coinc Rate {}".format(coinc_rate))
print("Sensitivity: {}%".format(coinc_rate * 100 / beta_rate))
drawer("sensitivity", "Sensitivität", "sensitivity", [[r"Beta Zerfallsrate [$\SI{}{\becquerel}$]", beta_rate], [r"Koinzidenz Rate [$\frac{1}{\SI{}{\second}}$]", coinc_rate], ["Sensitivität", "{:.3f}\%".format(coinc_rate * 100 / beta_rate)]])