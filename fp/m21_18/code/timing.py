#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 16:51:51 2021

@author: lbo
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
from scipy import optimize

from os import path

import uncertainties as unc

FIGSIZE = (9,6)

BUCKET_WIDTH = 10  # ps

plt.rcParams['font.size'] = 12.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'

SOL = 299792458


def ChiSq(xdata, ydata, fitdata, ddof=3, details=False):
    # Assuming poisson distribution: Variance is expected value
    diffsq = (ydata-fitdata)**2/(ydata+1)
    if details:
        return diffsq.sum()/(xdata.size-ddof), xdata.size-ddof, diffsq.sum()
    return diffsq.sum()/(xdata.size-ddof)

def time_files():
    return ['../data/preproc/time_{:d}.log'.format(i) for i in range(0, 4)]

def parse_time_log(filename):
    """Parse time diff log and return (time, count) tuple of arrays.

    Times are in ps.
    """
    lines = []
    with open(filename, 'r') as f:
        lines = f.readlines()
    assert len(lines) == 1
    datapoints = lines[0].split(';')
    pairs = [(int(ps), int(count)) for (ps, count) in map(lambda s: s.split(','), datapoints[:-1])]
    times = np.array([p[0] for p in pairs])
    counts = np.array([p[1] for p in pairs])
    return times, counts


def fit_peak(times, counts, min_count=1500, use_offset=True):
    """Fits a gauss peak to the given data points and returns (µ, σ)."""

    peakix = np.argmax(counts)
    left_limit = np.argmin(np.abs(counts[:peakix]-min_count))
    right_limit = peakix + np.argmin(np.abs(counts[peakix:]-min_count))

    times = times[left_limit:right_limit]
    counts = counts[left_limit:right_limit]

    totalcount = counts.sum()
    if use_offset:
        (mu, sigma, off), pcov = optimize.curve_fit(lambda x, mu, sigma, off: BUCKET_WIDTH*stats.norm.pdf(x, mu, sigma) - off,
                                                 times, counts/totalcount, [0, 400, 0], sigma=np.sqrt(counts)/totalcount)
        sigma_mu, sigma_sigma, sigma_off = np.sqrt(pcov[0,0]), np.sqrt(pcov[1,1]), np.sqrt(pcov[2,2])
        c, sigma_c = 1, 1e-6
    else:
        (c, mu, sigma), pcov = optimize.curve_fit(lambda x, c, mu, sigma: c*BUCKET_WIDTH*stats.norm.pdf(x, mu, sigma),
                                                 times, counts/totalcount, [1, 0, 400], sigma=np.sqrt(counts)/totalcount)
        sigma_c, sigma_mu, sigma_sigma = np.sqrt(pcov[0,0]), np.sqrt(pcov[1,1]), np.sqrt(pcov[2,2])
        off, sigma_off = 0, 1e-6

    print('c, mu, sigma, off:', c, mu, sigma, off)
    fit_counts = totalcount*(c*stats.norm.pdf(times, mu, sigma)*BUCKET_WIDTH - off)
    chisq, chisq_dof, chisq_sum = ChiSq(times, counts, fit_counts, details=True)
    print('ChiSq/DoF =', chisq)

    return c, mu, sigma, off, totalcount, (chisq, chisq_dof, chisq_sum), (sigma_c, sigma_mu, sigma_sigma, sigma_off)


def fit_all_peaks(filenames):
    for f in filenames:
        t, c = parse_time_log(f)
        c, mu, sigma, off, totalcount, _, _ = fit_peak(t, c)
        print('{}: Peak bei {}, σ = {}'.format(path.basename(f), mu, sigma))


def paint_time_histogram(titles, times, counts, savefig=None, fit=False, use_offset=False, **plotargs):
    """Paint histogram, calculate peak location. Returns (mu, fwhm) of measured time peak if `fit`."""
    if type(titles) is not list:
        return paint_time_histogram([titles], [times], [counts], savefig, fit, use_offset, **plotargs)

    figsize = FIGSIZE

    f, p, res = None, None, None
    if fit:
        f, (p, res) = plt.subplots(2, 1, figsize=figsize, sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    else:
        f, p = plt.subplots(1,1, figsize=figsize)

    result = None

    for i, t in enumerate(titles):
        p.plot(times[i], counts[i], label=t)
        if fit:
            min_count = 1250
            peakix = np.argmax(counts[i])
            peak = np.max(counts[i])
            leftix, rightix = (
                np.argmin(np.abs(counts[i][:peakix]-min_count)),
                peakix + np.argmin(np.abs(counts[i][peakix:]-min_count)))
            left_limit = times[i][leftix]
            right_limit = times[i][rightix]
            c, mu, sigma, off, tc, chisq, (sigma_c, sigma_mu, sigma_sigma, sigma_off) = fit_peak(
                times[i], counts[i], min_count=min_count, use_offset=use_offset)

            fit_ys = (stats.norm.pdf(times[i], mu, sigma)*BUCKET_WIDTH - off)*tc*c
            fwhm = 2.35482*sigma
            fwhm_y = [(fit_ys.max()+off*tc)/2 - off*tc]*2
            fwhm_x = [mu-fwhm/2, mu+fwhm/2]
            p.plot(times[i], fit_ys, label='Peak: {:.0f} ps, σ = {:.0f} ps\n$\chi^2 /$ndof = {:.1f}/{} = {:.1f}'.format(mu, sigma, chisq[2], chisq[1], chisq[0]), color='grey')
            p.plot(fwhm_x, fwhm_y, label='FWHM = {:.0f} ps'.format(fwhm), color='#a000a0')
            # p.plot([times[i].min(), times[i].max()], [min_count]*2, label='Fit-Cutoff', color='#00a000')
            p.plot([left_limit, left_limit], [0, peak], color='#00a000')
            p.plot([right_limit, right_limit], [0, peak], color='#00a000')
            res.errorbar(times[i][leftix:rightix], (fit_ys-counts[i])[leftix:rightix], np.sqrt(counts[i][leftix:rightix]), fmt='.')

            # mean = (times[i]*counts[i]).sum()/counts[i].sum()
            # print('Mean +/- std: {} +/- {} ps'.format(mean, np.sqrt((counts[i]*(times[i]-mean)**2).sum()/counts[i].sum())/np.sqrt(counts[i].sum())))
            # print('Peak uncertainty:', sigma/np.sqrt(tc))
            # print('Determined uncertainties mu/sigma/off:', sigma_mu, sigma_sigma, sigma_off)

            # Determine location
            mu_, fwhm_ = 1e-12*mu, 1e-12*fwhm
            print('Peak at: {}, FWHM: {}, Delta-x at: {}, x-FWHM: {}'.format(mu_, fwhm_, SOL/2*mu_, SOL/2*fwhm_))

            result = (mu, fwhm)

    p.set_xlabel('$\\Delta t$ / ps')
    p.set_ylabel('Anzahl (abs)')
    p.set_title('Time of Flight')

    if res:
        res.set_ylabel('Modell - Daten')
        res.grid()

    if len(titles) == 1:
        p.set_title('Time of Flight: {}'.format(titles[0]))
    p.grid()
    p.legend()
    if savefig:
        f.savefig(savefig)

    return result


def calculate_corrected_peaks(peaks):
    """Calculate corrected peaks from paint_time_histogram()'s result."""
    uncs = [unc.ufloat(a, b) for (a,b) in peaks]
    zero = uncs[0]
    meas = uncs[1:]

    print('Zero-measure time:', repr(zero), 'zero-measure location:', repr(SOL/2*zero*1e-12))

    for i, m in enumerate(meas):
        corrected = m - zero
        corrected_loc = SOL/2 * corrected * 1e-12
        print(i+1, 'Original time:', repr(m), 'Original location:', repr(SOL/2*m*1e-12), '\n\tCorrected time:', repr(corrected), 'Corrected location:', repr(corrected_loc))

    # Plot location distributions
    f = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = f.add_subplot(111)
    xpoints = np.linspace(-125, 125, 250)
    for i, m in enumerate(uncs):
        loc = SOL/2 * (m-zero)*1e-12 * 1000
        loc_fwhm = loc.std_dev if loc.nominal_value else SOL/2*1e-12*1000*uncs[i].std_dev
        sigma = loc_fwhm/(2*np.sqrt(2*np.log(2)))
        ypoints = stats.norm.pdf(xpoints, loc.nominal_value, sigma)
        p.plot(xpoints, ypoints, label='Messung {}'.format(i))
    p.legend()
    p.grid()
    p.set_xlabel('$\Delta x$/mm')
    p.set_ylabel('Wahrscheinlichkeit (P)')
    p.set_title('Ortswahrscheinlichkeit')
    f.savefig('artifacts/location_prob.pdf')


def paint_all_time_histograms(filenames):
    titles = []
    times = []
    counts = []
    peaks = []
    for f in filenames:
        t, c = parse_time_log(f)
        times.append(t)
        counts.append(c)
        titles.append(path.basename(f))
        peaks.append(paint_time_histogram(path.basename(f), t, c, fit=True, use_offset=True,
                                          savefig='artifacts/{}_plot.pdf'.format(path.basename(f))))

    paint_time_histogram(titles, times, counts, fit=False, savefig='artifacts/all_time_diff.pdf')
    calculate_corrected_peaks(peaks)
