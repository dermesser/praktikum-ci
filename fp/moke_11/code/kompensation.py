# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 16:14:29 2021

@author: 394493: Alexander Kohlgraf
"""
import hysterese
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import nice_plots
import latextable

dir_path = os.path.dirname(os.path.realpath(__file__))
plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


def lin_fit(param, x):
    return param[0] * x + param[1]


CoPt_name = "CoPt-Raumtemp..dat"
CoPt2_name = "Co-Raumtemp.(2).dat"
Co_name = "Co-Raumtemp..dat"
GdFe_names = [
    # "GdFe -32.0Degree.dat",
    "GdFe -32.0Degree(2).dat",
    "GdFe -31.4--31.7Degree.dat",
    "GdFe -30.2--30.0Degree.dat",
    "GdFe -28.8--28.8Degree.dat",
    "GdFe -27.8--27.8Degree.dat",
    "GdFe -26.0--25.9Degree.dat",
    "GdFe -21.8--22.0Degree.dat",
    "GdFe -20.0--19.8Degree.dat",
    # "GdFe -17.1--16.9Degree.dat",
    # "GdFe -14.9--14.6Degree.dat",
    "GdFe 18.4Degree.dat",
    "GdFe 19.5-19.6Degree.dat",
    "GdFe 25.6-25.7Degree.dat",
    "GdFe 31,2-31,2Degree.dat",
    # "GdFe 35.5-35.6Degree.dat",
    "GdFe 39.5-39.3Degree.dat",
    "GdFe 43.6-43.5Degree.dat",
    "GdFe 47.5-47.5Degree.dat",
    # "GdFe 53.3-54.0Degree.dat",
    "GdFe 57.0-56.8Degree.dat",
    # "GdFe 62.5-62.3Degree.dat",
    "GdFe 64.4-64.6Degree.dat",
    # "GdFe 65.7-65.9Degree.dat",
    "GdFe 71.6-71.7Degree.dat",
    # "GdFe 75,5-75,6Degree.dat",
    "GdFe 78.8-78.8Degree.dat",
    "GdFe 84.7-84.3Degree.dat",
]

temperatures = [
    [-32, -32],
    [-32, -32],
    [-31.4, -31.7],
    [-30.2, -30.0],
    [-28.8, -28.8],
    [-27.8, -27.8],
    [-26, -25.9],
    [-21.8, -22.0],
    [-20.0, -19.8],
    # [-17.1, -16.9],
    # [-14.9, -14.6],
    [18.4, 18.4],
    [19.5, 19.6],
    [25.6, 25.7],
    [31.2, 31.2],
    # [35.5, 35.6],
    [39.5, 39.3],
    [43.6, 43.5],
    [47.5, 47.5],
    # [53.3, 54.0],
    [57.0, 56.8],
    [62.5, 62.3],
    [64.4, 64.6],
    # [65.7, 65.9],
    [71.6, 71.7],
    # [75.5, 75.6],
    [78.8, 78.8],
    [84.7, 84.3],
]

cut_above_left_array = [
    1.5,
    1,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1,
    # 1.3,
    1.3,
    0.8,
    1.3,
    1.5,
]

cut_above_right_array = [
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    1.8,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    2,
    # 2,
    2,
    0.3,
    0.35,
    0.5,
]

cut_below_left_array = [
    1.9,
    1.9,
    1.9,
    1.9,
    1.9,
    1.9,
    1.9,
    2,
    0.8,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    # 1.5,
    0.3,
    0.15,
    1.5,
    1,
]

cut_below_right_array = [
    1,
    1,
    1,
    1,
    1,
    1.2,
    1,
    1.5,
    1,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    1.5,
    # 1.5,
    1.5,
    1.5,
    1.2,
    1.3,
]


def plot_GdFe_all_raw():
    for (x, y) in zip(GdFe_names, temperatures):
        z = hysterese.Hysterese(x, y)
        z.scatter()


class Compensation:
    def __init__(self, GdFe_names=GdFe_names, temperatures=temperatures):
        # =============================================================================
        # Make an array out of all GdFe Temperatures, errors and 1/H_c, errors
        # =============================================================================
        self.GdFe_objects = []
        T = []
        T_error = []
        H_c_inverse = []
        H_c_inverse_error = []
        for (
            name,
            temperature,
            cut_above_left,
            cut_above_right,
            cut_below_left,
            cut_below_right,
        ) in zip(
            GdFe_names,
            temperatures,
            cut_above_left_array,
            cut_above_right_array,
            cut_below_left_array,
            cut_below_right_array,
        ):
            print(name)
            tmp = hysterese.Hysterese(name, temperature)
            tmp.compensation(
                cut_above_left=cut_above_left,
                cut_above_right=cut_above_right,
                cut_below_left=cut_below_left,
                cut_below_right=cut_below_right,
            )
            self.GdFe_objects.append(tmp)
            T.append(tmp.temperature)
            T_error.append(tmp.temperature_error)
            H_c_inverse.append(tmp.compensation)
            H_c_inverse_error.append(tmp.compensation_error)
            tmp.plot_all()
        self.df = pd.DataFrame(T, columns=["temperatures"])
        self.df["temperatures error"] = pd.DataFrame(T_error)
        self.df["compensation"] = pd.DataFrame(H_c_inverse)
        self.df["compensation error"] = pd.DataFrame(H_c_inverse_error)

    def plot_raw(self):
        """
        plots the errorbars on call
        """
        plt.figure(figsize=(15, 8))
        plt.xlabel("Temperatur [°C]")
        plt.ylabel("1/H_c [m/A]")
        plt.title(
            "Scatter mit Fehler der GdFe Daten für die Bestimmung des Kompensationspunktes"
        )
        plt.errorbar(
            self.df["temperatures"],
            self.df["compensation"],
            yerr=self.df["compensation error"],
            xerr=self.df["temperatures error"],
            fmt="o",
        )
        plt.grid(True)
        plt.savefig(dir_path + "/../protokoll/images/" + "GdFe_errorbars" + ".pdf")

    def fit_compensation(self, df, temperature, p0):
        reg = nice_plots.nice_regression_plot_odr(
            lin_fit,
            x=np.array(df["temperatures"]),
            y=np.array(df["compensation"]),
            xerror=np.array(df["temperatures error"]),
            yerror=np.array(df["compensation error"]),
            figsize=(15, 8),
            xlabel="Temperatur [°C]",
            ylabel="1/H_c [m/A]",
            ylabelresidue="Residuum",
            title="Anpassung für die Kompensation bei "
            + temperature
            + "en Temperaturen",
            save=dir_path
            + "/../protokoll/images/"
            + "compensation_"
            + temperature
            + "_fit.pdf",
            colors=("red", "green"),
            beta0=p0,
        )

        latextable.simple_draw(
            save="/../protokoll/tables/" + "compensation_" + temperature + "_fit.tex",
            content=[["a", reg[0][0], reg[1][0][0]], ["b", reg[0][1], reg[1][1][1]]],
            header=["Fit Parameter", "Wert", "Fehler"],
            caption="Anpassungsparameter für die Kompensationstemperatur bei "
            + temperature
            + "en Temperaturen",
            label="compensation_" + temperature + "fit",
            precision=20,
            small=False,
        )
        return reg

    def fit_parameter_to_intersection(self, reg):
        T_k = -reg[0][1] / reg[0][0]
        T_k_error = np.sqrt(
            (reg[1][1][1] / reg[0][0]) ** 2
            + ((reg[0][1] * reg[1][0][0]) / reg[0][0] ** 2) ** 2
        )
        return T_k, T_k_error

    def fit_both_compensation(self, p0_cold=[0, 0], p0_warm=[0, 0]):
        self.reg_cold = self.fit_compensation(
            self.df[self.df["temperatures"] < 0], "kalt", p0_cold
        )
        self.reg_warm = self.fit_compensation(
            self.df[self.df["temperatures"] > 0], "warm", p0_warm
        )

        self.T_k_cold = self.fit_parameter_to_intersection(self.reg_cold)
        self.T_k_warm = self.fit_parameter_to_intersection(self.reg_warm)

        print("cold")
        print(self.reg_cold)
        print(self.T_k_cold)
        # print(str(self.reg_cold[0][0]))
        print("")
        print("warm")
        print(self.reg_warm)
        print(self.T_k_warm)

        latextable.simple_draw(
            save="/../protokoll/tables/" + "compensation_" + "temperature" + "_fit.tex",
            content=[
                ["Kalt, T<0", self.T_k_cold[0], self.T_k_cold[1]],
                ["Warm, T>0", self.T_k_warm[0], self.T_k_warm[1]],
            ],
            header=["Temperaturbereich", "Kompensationstemperatur [°C]", "Fehler [°C]"],
            caption="Kompensationstemperaturen",
            label="compensation_" + "temperature" + "fit",
            precision=20,
            small=False,
        )


compensation = Compensation()
CoPt = hysterese.Hysterese(CoPt_name)
CoPt2 = hysterese.Hysterese(CoPt2_name)
Co = hysterese.Hysterese(Co_name)
CoPt.scatter(save=True)
CoPt2.scatter(save=True)
Co.scatter(save=True)
