module analyze

include("colormap.jl")
include("loading.jl")
include("display.jl")
include("filter.jl")
include("lr.jl")

end # module
