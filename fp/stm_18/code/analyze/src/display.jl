module display

using ..colormap
using ..loading

using Images
import MosaicViews
import FileIO

using PyPlot

export display_viridis, crosssection, draw_crosssection, draw_window, display_mpl, draw_crosssection_mpl
export draw_contour_image, elevation_distribution

const RGB = Images.RGB{Images.N0f8}

# Display

function display_grey(sti::STImage; scale=identity)
    img = sti.data
    return Images.Gray.(img/maximum(img))
end

function display_mpl(sti::STImage; save=true, prefix="zscan")::PyPlot.Figure
    fig = figure(figsize=(6,5), tight_layout=true)
    p = fig.add_subplot(111)
    img = p.imshow(sti.data, extent=(0, sti.x, sti.y, 0))
    p.set_title(short_desc(sti.desc))
    p.set_xlabel("x / nm")
    p.set_ylabel("y / nm")
    fig.colorbar(img)

    if save
        fig.savefig(plot_path(sti.desc, prefix))
    end
    return fig
end

function display_viridis(sti::STImage; relative=true, scale=identity, save="")

    function viridis(f::Float64)::RGB
        if f < 0 || f > 1
            return RGB(0,0,0)
        end
        return RGB(VIRIDIS[1+round(Int32, 255*f)]...)
    end

    img = sti.data
    offset = img.-minimum(img)
    scaled = scale.(offset)
    normalized = offset/maximum(scaled)
    rgb = map(f -> RGB(VIRIDIS[1+round.(Int32, 255 * f)]...), normalized)

    if save ≠ ""
        FileIO.save(save, rgb)
    end

    return rgb
end

# Crosssections

# x/y are indices into each img.
function crosssection(imgs::Vector{STImage}, backward::Union{Nothing, Vector{STImage}}=nothing;
        x=nothing, y=nothing, relative=false, colorscale=false, save=false)
    fig = figure(figsize=(10,5))
    p = fig.add_subplot(111)
    maxid = maximum(img.desc.id for img in imgs)

    for (i, img) = enumerate(imgs)
        fn = to_filename(img.desc; base="")
        ordinate = img.desc.ityp == ZOff ? "z / nm" : "I / nA"
        if relative == :abcyxxzxf
            ordinate = img.desc.ityp == ZOff ? "\$z~/~z_{-1}\$" : "\$I~/~I_{-1}\$"
        end

        color = nothing
        if colorscale
            color = VIRIDIS[round(Int32, img.desc.id/maxid * 256)]
        end

        if y ≠ nothing
            xs = range(0, img.x, length=size(img.data)[1])
            off = 0
            if relative == :first
                off = img.data[y+1, 4]
            end

            l2d = p.plot(xs, img.data[y+1, :] .- off, label="$(short_desc(img.desc))",
                 color=color)

            if backward !== nothing
                if relative == :first
                    off = img.data[y+1, end-4]
                end
                p.plot(xs, backward[i].data[y+1, :] .- off, linestyle="dashed",# label="$(short_desc(backward[i].desc))",
                       color=l2d[1].get_color())
            end
            p.set_xlabel("x / nm")
            p.set_ylabel(ordinate)
            p.legend()
            p.set_title("Horiz. crosssection @ y = $(round(xs[y], digits=1)) nm")
        elseif x ≠ nothing
            ys = range(0, img.y, length=size(img.data)[1])
            off = 0
            if relative == :first
                off = img.data[1, x+1]
            end

            l2d = p.plot(ys, img.data[:, x+1] .- off, label="$(short_desc(img.desc))",
                 color=color)
            if backward !== nothing
                p.plot(xs, backward[i].data[:, x+1] .- off, label="$(short_desc(backward[i].desc))",
                       color=l2d[1].get_color())
            end
            p.set_xlabel("y / nm")
            p.set_ylabel(ordinate)
            p.legend()
            p.set_title("Vert. crosssection @ x = $(round(ys[x], digits=1)) nm")
        end
    end

    if save
        fig.savefig("../img/crosssection_$(imgs[1].desc.typ)_$(imgs[1].desc.ityp)_$(convert(Int32, y))px.pdf")
    end
    fig
end

function draw_crosssection(imgs::Union{STImage, Vector{STImage}};
        x=nothing, y=nothing, save="")::Array{RGB, 2}
    if !isa(imgs, Vector)
        imgs = [imgs]
    end

    imgs_ = display_viridis.(imgs)
    for img = imgs_
        if y ≠ nothing
            img[y+1, :] .= RGB(1,0,0)
        end
        if x ≠ nothing
            img[:, x+1] .= RGB(1,0,0)
        end
    end

    mv = MosaicViews.mosaicview(imgs_, nrow=1)
    if save ≠ ""
        FileIO.save(save, mv)
    end
    mv
end

function draw_crosssection_mpl(img::STImage;
        x=nothing, y=nothing, save=true)
    fig = figure(figsize=(7,4), tight_layout=true)
    p = fig.add_subplot(111)
    pimg = p.imshow(img.data, extent=(0, img.x, img.y, 0))

    if x !== nothing
        p.plot([x, x], [0, img.y], color=:red)
    end
    if y !== nothing
        p.plot([0, img.x], [y, y], color=:red)
    end

    p.set_title(short_desc(img.desc))
    p.set_xlabel("x / nm")
    p.set_ylabel("y / nm")
    fig.colorbar(pimg)

    if save
        fig.savefig(plot_path(img.desc, "cross_$(x)_$(y)"))
    end
    fig
end

function draw_window(imgs::Union{STImage, Vector{STImage}};
        xlim=nothing, ylim=nothing)::Vector{Array{RGB, 2}}
    if !isa(imgs, Vector)
        imgs = [imgs]
    end

    imgs_ = display_viridis.(imgs)
    for (i, img) = enumerate(imgs_)
        steps = range(0, imgs[i].desc.dim, length=size(imgs[i].data)[1])
        xlim = xlim === nothing ? (0, imgs[i].desc.dim) : xlim
        ylim = ylim === nothing ? (0, imgs[i].desc.dim) : ylim

        xmin, xmax = argmin(abs.(steps.-xlim[1])), argmin(abs.(steps.-xlim[2]))
        ymin, ymax = argmin(abs.(steps.-ylim[1])), argmin(abs.(steps.-ylim[2]))
        print((xmin,xmax,ymin,ymax))
        img[ymin, :] .= RGB(1,0,0)
        img[ymax, :] .= RGB(1,0,0)
        img[:, xmin] .= RGB(1,0,0)
        img[:, xmax] .= RGB(1,0,0)
    end
    imgs_
end

function draw_contour_image(forward::STImage, backward=nothing; save=false, prefix="zscan", png=false, title=nothing)
    img = forward
    img2 = backward

    dim = size(img.data)[1]
    data = reshape(img.data, :)
    fig = figure(figsize=(6,5), tight_layout=true)
    p = fig.add_subplot(111)

    levels = range(minimum(img.data), maximum(img.data), length=10)

    c1 = p.contour(
                 range(0, img.desc.dim, length=dim),
                 range(0, img.desc.dim, length=dim),
                 img.data, cmap="Blues", label=short_desc(img.desc), levels=levels)
    p.set_ylim(img.desc.dim, 0)
    if img2 !== nothing
        c2 = p.contour(
                     range(0, img.desc.dim, length=dim),
                     range(0, img.desc.dim, length=dim),
                     img2.data, cmap="Oranges", label=short_desc(img2.desc), levels=levels)
        if title === nothing
            p.set_title("Contour $(forward.desc.typ).$(forward.desc.ityp) Forward $(forward.desc.id)/Backward $(backward.desc.id)")
        else
            p.set_title(string(title, " ", short_desc(forward.desc)))
        end
        fig.colorbar(c1, pad=-0.11)
        fig.colorbar(c2, pad=0, format="")
    else
        if title === nothing
            p.set_title("Contour $(short_desc(forward.desc))")
        else
            p.set_title(string(title, " ", short_desc(forward.desc)))
        end
        fig.colorbar(c1)
    end
    p.set_xlabel("x / nm")
    p.set_ylabel("y / nm")
    if save
        fig.savefig(plot_path(forward.desc, prefix, png=png))
    end
    fig
end

# Show surface distribution histogram for roughness.
function elevation_distribution(img::STImage)
	fig = figure(figsize=(7,5), tight_layout=true)
	p = fig.add_subplot(111)
	p.hist(reshape(img.data, :), bins=200)
	p.set_xlabel("z / nm")
	p.set_ylabel("n")
	p.set_title("Elevation distribution ($(short_desc(img.desc)))")
	fig.savefig(plot_path(img.desc, "elevation_dist_z"))
	fig
end

end
