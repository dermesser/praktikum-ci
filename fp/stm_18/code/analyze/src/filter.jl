module filter

using ..loading

import Statistics
import DSP.Filters

using Setfield

export drop_peaks, img_line_bandpass, line_bandpass

# Filter

function drop_peaks(img::STImage, quantile::Float64=98.)
    qhigh = Statistics.quantile(reshape(img.data, :), quantile/100)
    qlow = Statistics.quantile(reshape(img.data, :), 1 - quantile/100)
    highmask, lowmask = img.data .> qhigh, img.data .< qlow
    img_ = deepcopy(img)
    img_.data[highmask] .= qhigh
    img_.data[lowmask] .= qlow
    return img_
end

function line_bandpass(line::Vector{<:Real}, low=0, high=1)::Vector{<:Real}
    if high <= 0
        filt = Filters.Highpass(low)
    elseif low <= 0
        filt = Filters.Lowpass(high)
    else
        filt = Filters.Bandpass(low, high)
    end
    dm = Filters.Chebyshev1(1, 1)
    filter = Filters.digitalfilter(filt, dm)
    app = line -> Filters.filt(filter, line)
    return Filters.filt(filter, line)
end

function img_line_bandpass(img::STImage, low=0, high=1)::STImage
    imgd = deepcopy(img.data)
    if high <= 0
        filt = Filters.Highpass(low)
    elseif low <= 0
        filt = Filters.Lowpass(high)
    else
        filt = Filters.Bandpass(low, high)
    end
    dm = Filters.Chebyshev1(1, 1)
    filter = Filters.digitalfilter(filt, dm)
    app = line -> Filters.filt(filter, line)
    img_ = @set img.data = mapslices(app, imgd, dims=2)
    return img_
end
end
