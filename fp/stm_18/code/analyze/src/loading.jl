module loading

import Statistics
using PyPlot

using ..colormap

export load_image, to_filename, short_desc, plot_path
export rms_roughness
export STImage, MDescriptor, RGB
export MType, ImgType, IGain, Time, Current, ZOff

export MD_IGAIN_ZOFF, MD_IGAIN_CURRENT, MD_T_CURRENT, MD_T_ZOFF, T_VALUES, I_VALUES

import DelimitedFiles
import Images
	
@enum MType IGain Time
@enum ImgType Current ZOff

struct MDescriptor
    typ::MType
    ityp::ImgType

    id::Int64  # Gain or time/line
    suff::String
    forward::Bool
    dim::Float64  # quadratic side length in nm
end

struct STImage
    data::Array{Float64, 2}
    x::Float64
    y::Float64
    desc::MDescriptor
end

function to_filename(md::MDescriptor; base="../data/preproc/")::String
    typ = md.typ == IGain ? "igain" : "time"
    idpre = md.typ == IGain ? "i" : "t"
    ityp = md.ityp == Current ? "current" : "zoff"
    dir = md.forward ? "fw" : "bw"
    serial = md.id ≠ 0 ? "_$(idpre)$(md.id)" : ""
    return "$(base)gold_$(typ)_$(ityp)$(serial)$(md.suff)_$(dir).txt"
end

function short_desc(md::MDescriptor)::String
    direction = md.forward ? "fw" : "bw"
    return "$(md.typ).$(md.ityp) @ $(md.id)_$(direction)$(md.suff)"
end

function plot_path(md::MDescriptor, typ::String; png=false)::String
    direction = md.forward ? "fw" : "bw"
    filetype = png ? "png" : "pdf"
    return "../img/$(typ)_$(md.typ)_$(md.ityp)_$(md.id)_$(md.suff)_$(direction).$(filetype)"
end

### Measurements configuration
#
I_VALUES = [200, 500, 1000, 2000, 3000, 4000, 8000]

MD_IGAIN_ZOFF = [
                 [MDescriptor(IGain, ZOff, i, "", dir, 98.8) for dir = [true, false]]
                 for i = I_VALUES]

push!(MD_IGAIN_ZOFF, [MDescriptor(IGain, ZOff, 0::Int64, "_overview", d, 574) for d = [true, false]])
MD_IGAIN_ZOFF = permutedims(hcat(MD_IGAIN_ZOFF...))

##

MD_IGAIN_CURRENT = [
                    [MDescriptor(IGain, Current, i, "", dir, 98.8) for dir = [true, false]]
                    for i = I_VALUES]

push!(MD_IGAIN_CURRENT, [MDescriptor(IGain, Current, 0::Int64, "_overview", d, 574) for d = [true, false]])
MD_IGAIN_CURRENT = permutedims(hcat(MD_IGAIN_CURRENT...))

##

T_VALUES = [50, 100, 200, 300, 500]

MD_T_ZOFF = [
             [MDescriptor(Time, ZOff, i, "", dir, 98.8) for dir = [true, false]]
             for i = T_VALUES]
MD_T_ZOFF = permutedims(hcat(MD_T_ZOFF...))

##

MD_T_CURRENT = [
                [MDescriptor(Time, Current, i, "", dir, 98.8) for dir = [true, false]]
                for i = T_VALUES]

MD_T_CURRENT = permutedims(hcat(MD_T_CURRENT...))

function load_image(file::MDescriptor; to_nm=true)::STImage
    img = DelimitedFiles.readdlm(to_filename(file), '\t',  Float32) .* (to_nm ? 1e9 : 1)
    return STImage(img, file.dim, file.dim, file)
end

function rms_roughness(img::STImage)::Float64
	m = Statistics.mean(img.data)
	diffs = img.data .- m
	return sqrt(sum(diffs .^ 2)/length(img.data))
end

end
