### A Pluto.jl notebook ###
# v0.14.0

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ bf9adf7c-8c8e-11eb-1f75-fd7ba58b9724
begin
	import Statistics
	import Plots
	using PyPlot
	PyPlot.svg()
	
	using Revise
	using MosaicViews
	
	push!(LOAD_PATH, pwd())
	using analyze.loading
	using analyze.colormap
	using analyze.display
	using analyze.filter
	using analyze.lr
end

# ╔═╡ dfdc596e-8c9d-11eb-305f-3f84439457ab
md"""## Analysis of Gold scans by integration interval
"""

# ╔═╡ 8222ef6e-8e45-11eb-0a58-31f17f4071b6
md"""#### Given conditions
Image size	98,8nm


Scan direction	Idle


Time/Line	200ms


Setpoint	1nA


P-Gain	1k


I-Gain	200


D-Gain	0


Tip Voltage	1 V
"""

# ╔═╡ d052df8c-8e3b-11eb-25a9-898481dd4a4e
IGAIN_SELECTION = [1, 3, 5, 6, 7]

# ╔═╡ 0a088a5a-8c9e-11eb-3dc7-879094adcd36
begin
	zscans = MD_IGAIN_ZOFF[IGAIN_SELECTION,:]
	currentscans = MD_IGAIN_CURRENT[IGAIN_SELECTION,:]
end

# ╔═╡ 71c629c0-8ca3-11eb-0f6d-9900c9b2df46
function load_gold_scan(md)
	img = drop_peaks(load_image(md), 99.5)
	return img
end

# ╔═╡ 229e4d5c-8ca1-11eb-250e-3d4b8dbff95a
md"""### Show images with different scan times
"""

# ╔═╡ 4eee62f4-8ca1-11eb-3500-cdaa54a555f0
begin
	img = load_gold_scan.(zscans)
	display_mpl.(img, save=true, prefix="zscan")
end

# ╔═╡ 70f154ce-8e36-11eb-23f0-f9efc10c6877
begin
	cimg = load_gold_scan.(currentscans)
	display_mpl.(cimg, save=true, prefix="currentscan")
end

# ╔═╡ ea7bd546-8ecf-11eb-29df-976a80281229
md"""Also show overview images."""

# ╔═╡ eca3a894-8ecf-11eb-3164-bb36425371cf
begin
	ov_z = load_gold_scan.(MD_IGAIN_ZOFF[end,:])
	ov_c = load_gold_scan.(MD_IGAIN_CURRENT[end,:])
	
	display_mpl.(ov_z, save=true, prefix="zscan")
	display_mpl.(ov_c, save=true, prefix="currentscan")
	draw_contour_image(ov_z..., save=true,
	prefix="contour_overview",
	title="Contour Overview")
end

# ╔═╡ 3fd04900-8ee4-11eb-2b3b-253e6d36e9fa
elevation_distribution.(img)

# ╔═╡ 814ea66a-8ee4-11eb-20ff-5b09329dec5e
elevation_distribution.(ov_z)

# ╔═╡ 3fb36108-8f9f-11eb-3cad-99758e26b041
rms_roughness.(img)

# ╔═╡ ffb4df62-8ca3-11eb-0ecd-03402d56ea1d
md"""### Show crosssections with different scan times
"""

# ╔═╡ 76a66c2e-8ca6-11eb-01c0-cf3c4466b92c
@bind eycross html"""Y-Crosssection: <input type=range value=15.>"""

# ╔═╡ 06e47f68-8ca4-11eb-017e-59da3bd8a376
crosssection(img[:,1], img[:,2],
	y=round(Int32, eycross/100 * size(img[1].data)[1]), 
	relative=nothing, save=true)

# ╔═╡ df011596-8ca4-11eb-0d3e-89841a5ee30b
draw_crosssection(img[:,1], y=round(Int32, eycross/100 * size(img[1].data)[1]), save="../img/gold_crosssec_overlay_igain_$(convert(Int32, eycross))_fw.png")

# ╔═╡ 866811a0-9073-11eb-1714-9373516983e7
draw_crosssection(cimg[:,1],
	y=round(Int32, eycross/100 * size(cimg[1].data)[1]),
	save="../img/gold_crosssec_overlay_time_current_$(convert(Int32, eycross))_fw.png")

# ╔═╡ 83af9476-8e23-11eb-394c-9393512fae01
begin
	for i = img
		csfig = crosssection([i, img_line_bandpass(i, .0001, .2)],
			y=round(Int32, eycross/100 * size(i.data)[1]))
		csfig.savefig(plot_path(i.desc, "crossection_lowpass"))
	end
end

# ╔═╡ 2bf24af2-8e36-11eb-326d-9dfce1191ac1
draw_contour_image.(
	img[:,1],
	img_line_bandpass.(img[:,1], .0001, .2),
	save=true,
	prefix="contour_bandpass_20pct",
	title="Contour Bandpass")

# ╔═╡ 6c610fe0-8f9d-11eb-000c-9fe3a7e3edad
display_mpl.(img_line_bandpass.(img[:,1], 0.0001, 0.2), save=false)

# ╔═╡ 5c7e440e-8e25-11eb-0db2-5d50d23f5028
# This function estimates the measurement uncertainty of the Z offset by only
# considering high-frequency parts (> 10% of nyquist frequency).
# Here, this means everything bigger than 20nm, with a Chebyshev filter form.
function noise_stddev_img(img::STImage, bandpass=(.2, .999); x=nothing, y=nothing)
	if x !== nothing
		series = img.data[:, x+1]
	elseif y !== nothing
		series = img.data[y+1, :]
	end
	off = 10
	filtered = line_bandpass(series, bandpass[1], bandpass[2])
	stddev = Statistics.std(filtered[off:end])
	clf()
	figure(figsize=(10,5), tight_layout=true)
		
	gca().plot(
		range(1, img.desc.dim, length=length(series)-off+1),
		filtered[off:end], alpha=1)
	gca().set_xlabel("x / nm")
	gca().set_ylabel("Δz / nm")
	gca().set_ylim(-1.5, 1.5)
	
	ax2 = gca().twiny()
	ax2.hist(filtered[off:end], bins=range(-1.5, 1.5, step=0.1),
		orientation=:horizontal, color=:red, alpha=0.5)
	title("Highpass (> 20%) $(short_desc(img.desc)): σ = $(round(stddev, digits=3)) nm")
	ax2.set_xlabel("n")
	ax2.set_ylabel("Δz / nm")


	gcf().savefig(plot_path(img.desc, "noise_bandpass_gt20pct"))
	stddev
end

# ╔═╡ 801a8476-8e26-11eb-1be3-f182c23c4707
# Calculate uncertainty from high-frequency noise.
zoff_uncertainties = noise_stddev_img.(img, y=round(Int32, eycross/100 * size(img[1].data)[1]))

# ╔═╡ 64898c3f-c80e-4823-be35-77478ff59019
begin
	noise_stddev_img(img[4,1], y=round(Int32, eycross/100 * size(img[1].data)[1]))
	gcf()
end

# ╔═╡ a5d0af72-8e37-11eb-208b-47f46610cc43
draw_contour_image.(img[:,1], img[:,2], save=true, prefix="contour_fw_bw")

# ╔═╡ d8feda30-8fcd-11eb-13e7-0d486f61bc18
function measure_steepness(imgs::Vector{STImage}, xmin, xmax;
		y=eycross, uncertainties::Vector{Float64}=[])
	img = imgs[1]
	if length(uncertainties) < length(imgs)
		push!(uncertainties, zeros(length(imgs)-length(uncertainties))...)
	end

	yix = round(Int32, eycross/100 * size(img.data)[1])
	
	to_x_ix = x -> round(Int32, x * size(img.data)[2] / img.desc.dim)
	xminix, xmaxix = to_x_ix(xmin), to_x_ix(xmax)
	
	xs_ = collect(range(0, img.desc.dim, length=size(img.data)[1]))
	xs = xs_[xminix:xmaxix]
	allxs = hcat(repeat([xs], length(imgs))...)
	allys = hcat([lr.mfl.(i.data[yix+1,xminix:xmaxix], uncertainties[ix])
			for (ix, i) in enumerate(imgs)]...)
	
	fits = mapslices(y -> linreg(xs[:,1], y), allys, dims=1)
	println(fits)
	allfitys = lr.nv.(hcat([(xs .* f.m .+ f.b) for f in fits]...))
	
	resids = allys .- allfitys

	labels = permutedims(["$(split(short_desc(i.desc), '@')[2][2:end]): m = $(round(fits[ix].m, digits=3))"
		for (ix, i) in enumerate(imgs)])
	
	lay = Plots.grid(2, 1, heights=[0.7, 0.3])

	Plots.scatter(allxs, allys, t=:scatter, xlabel="x / nm", ylabel="z / nm",
               	label=labels, color=[:blue :orange :green :red],  legend=:bottomright,
				legendfontsize=7,
				title="Flank steepness (IGain) @ y = $(round(xs_[yix], digits=1))")
    dataplot = Plots.plot!(allxs, allfitys, color=:black, label="")   
    
    Plots.plot(xs, resids, legend=false, t=:scatter,    
              ylabel="Daten - (mx + b)", color=[:blue :orange :green :red])    
    residplot = Plots.plot!((xs), zeros(size(xs)), color="gray")    
    Plots.plot(dataplot, residplot, layout=lay, size=(700,700), link=:x)
end

# ╔═╡ da03e93e-8fcd-11eb-02b4-51a72434e97e
begin
	p =	measure_steepness(img[:,1], 60, 80, uncertainties=zoff_uncertainties[:,1])
	Plots.savefig(p,
		"../img/steepness_igain_$(eycross)px.pdf")
	p
end

# ╔═╡ 126313ca-8e45-11eb-3be6-9150cf648896
md"""### Current images

Now we look at the current images instead of Z offset images. They are in `currentscans`:
"""

# ╔═╡ 16106b3c-8e45-11eb-0cd1-eb8270b9d1d3
cs = crosssection(cimg[:,1], cimg[:,2],
	y=round(Int32, eycross/100 * size(cimg[1].data)[1]), 
	save=true)

# ╔═╡ 2acc0324-8e45-11eb-06c9-879b2aa34ce8
cs.savefig("../img/igain_current_crosssection.pdf")

# ╔═╡ d8a1d6be-8e4c-11eb-002f-b9fe790ce80f
draw_contour_image.(cimg[:,1], cimg[:,2], save=true, prefix="contour_fw_bw", png=true)

# ╔═╡ f3407dd6-9077-11eb-18e3-f52c88d1a7a6
begin
	for i = cimg
		csfig = crosssection([i, img_line_bandpass(i, .0001, .2)],
			y=round(Int32, eycross/100 * size(i.data)[1]))
		csfig.savefig(plot_path(i.desc, "current_lowpass"))
	end
end

# ╔═╡ Cell order:
# ╠═bf9adf7c-8c8e-11eb-1f75-fd7ba58b9724
# ╠═dfdc596e-8c9d-11eb-305f-3f84439457ab
# ╠═8222ef6e-8e45-11eb-0a58-31f17f4071b6
# ╠═d052df8c-8e3b-11eb-25a9-898481dd4a4e
# ╠═0a088a5a-8c9e-11eb-3dc7-879094adcd36
# ╠═71c629c0-8ca3-11eb-0f6d-9900c9b2df46
# ╠═229e4d5c-8ca1-11eb-250e-3d4b8dbff95a
# ╠═4eee62f4-8ca1-11eb-3500-cdaa54a555f0
# ╠═70f154ce-8e36-11eb-23f0-f9efc10c6877
# ╠═ea7bd546-8ecf-11eb-29df-976a80281229
# ╠═eca3a894-8ecf-11eb-3164-bb36425371cf
# ╠═3fd04900-8ee4-11eb-2b3b-253e6d36e9fa
# ╠═814ea66a-8ee4-11eb-20ff-5b09329dec5e
# ╠═3fb36108-8f9f-11eb-3cad-99758e26b041
# ╠═ffb4df62-8ca3-11eb-0ecd-03402d56ea1d
# ╠═76a66c2e-8ca6-11eb-01c0-cf3c4466b92c
# ╠═06e47f68-8ca4-11eb-017e-59da3bd8a376
# ╠═df011596-8ca4-11eb-0d3e-89841a5ee30b
# ╠═866811a0-9073-11eb-1714-9373516983e7
# ╠═83af9476-8e23-11eb-394c-9393512fae01
# ╠═2bf24af2-8e36-11eb-326d-9dfce1191ac1
# ╠═6c610fe0-8f9d-11eb-000c-9fe3a7e3edad
# ╠═5c7e440e-8e25-11eb-0db2-5d50d23f5028
# ╠═801a8476-8e26-11eb-1be3-f182c23c4707
# ╠═64898c3f-c80e-4823-be35-77478ff59019
# ╠═a5d0af72-8e37-11eb-208b-47f46610cc43
# ╠═d8feda30-8fcd-11eb-13e7-0d486f61bc18
# ╠═da03e93e-8fcd-11eb-02b4-51a72434e97e
# ╠═126313ca-8e45-11eb-3be6-9150cf648896
# ╠═16106b3c-8e45-11eb-0cd1-eb8270b9d1d3
# ╠═2acc0324-8e45-11eb-06c9-879b2aa34ce8
# ╠═d8a1d6be-8e4c-11eb-002f-b9fe790ce80f
# ╠═f3407dd6-9077-11eb-18e3-f52c88d1a7a6
