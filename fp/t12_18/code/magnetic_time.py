#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 12 15:12:40 2021

@author: lbo
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import pandas
import uncertainties as unc
from uncertainties import umath, unumpy

import nice_plots as npl

from scipy import stats
from scipy import optimize

from os import path

nv = lambda x: unumpy.nominal_values(x) # if type(x) is np.ndarray else x.nominal_value
sd = lambda x: unumpy.std_devs(x) # if type(x) is np.ndarray else x.std_dev

MAGNETIC_TIME_FILES = [
    '../data/momentum/temporal_meas1.csv',
    '../data/momentum/temporal_meas2.csv']

B_SYSTEMATIC_ERROR = math.sqrt(0.002**2+0.003**2)*2e3
B_UNCERTAINTY = 1

def parse_magnetic_time_file(filename):
    df = pandas.read_csv(filename, sep=';', decimal=',', usecols=[0,1],
                         names=['N', 'B'], skiprows=1)
    df['B'] *= 1000
    df['B'] = unumpy.uarray(df['B'], B_UNCERTAINTY)
    return df

def plot_magnetic_time(dfs):
    # m, em, b, eb, chiq, corr = npl.nice_linear_regression_plot(
    #     df['N'], nv(df['B']), sd(df['B']))
    if type(dfs) is not list:
        dfs = [dfs]

    fig = plt.figure(figsize=(8,5), tight_layout=True)
    p = fig.add_subplot(111)

    for i, df in enumerate(dfs):
        B = nv(df['B']) - nv(df['B']).mean()
        p.scatter(df['N']*0.2, B, marker='.', label='$B_{}$'.format(i+1))

    p.set_title('Temporal variability of $B$')
    p.set_xlabel('t / s ($\\Delta t = 0.2$s)')
    p.set_ylabel('$B_i - \\overline{B_i} / mT$')
    p.grid()
    p.legend()
    fig.savefig('../img/magnetic_over_time.pdf')
