#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 16:59:30 2021

@author: lbo
"""

import math
import numpy as np
import matplotlib.pyplot as plt
import pandas
import uncertainties as unc
from uncertainties import umath, unumpy

import nice_plots as npl

from scipy import stats, odr
from scipy import optimize

from os import path

FIGSIZE = (8, 5)

USE_OFFSET = False

BASEPATH = '../data/multiscatter/'
ELECTRON_MASS = 511.0

PATH = lambda p: path.join(BASEPATH, p)

BACKGROUND_FILE = (PATH('background_10m.csv'), 600)
NOABSORBER_FILES = [
    PATH('noabsorber_60s.csv'),
    PATH('back-60s.csv'),
    ]

FRONT_FILES = [PATH('front-60s.csv')]
BACK_FILES = [PATH('back-60s.csv'), PATH('noabsorber-60s.csv')]

AL_FILES = {
    '0.3': (PATH('al-0,3-60s.csv'), 60),
    '0.6': (PATH('al-0,6-90s.csv'), 90),
    '0.8': (PATH('al-0,8-90s.csv'), 90),
    '1.0': (PATH('al-1,0-90s.csv'), 90),
    }

MATERIAL_FILES = {
    'cardboard': (PATH('cardboard-1,0-60s.csv'), 60),
    'fe': (PATH('fe-1,0-90s.csv'), 90),
    'plexi': (PATH('plexi-1,0-60s.csv'), 60),
    'acryl': (PATH('acryl-1,0-60s.csv'), 60)}

NUMBER_COUNTERS = 4

COLORS = ['blue', 'green', 'red', 'purple']
MARKERS = ['s-', 'o-', 'x-', '^-']

COUNTERS = ['GM{}'.format(i) for i in range(1, NUMBER_COUNTERS+1)]
COUNTERS_CORRECTED = [c+'_corr' for c in COUNTERS]

COUNTER_RAW_CALIBRATION_COUNTS = np.array([17392, 18077, 18203, 18679])
COUNTER_CALIBRATION_COUNTS = unumpy.uarray(COUNTER_RAW_CALIBRATION_COUNTS,
                                           np.sqrt(COUNTER_RAW_CALIBRATION_COUNTS))
COUNTER_CORRECTIONS = COUNTER_CALIBRATION_COUNTS.max() / COUNTER_CALIBRATION_COUNTS

# Background noise in Hz. Mean for all counters, as stddev is too large
# compared to differences.
COUNTER_MEAN_BACKGROUND_NOISE = None  # Lazily initialized

# Automatically calibrate positions of counters.
COUNTER_POSITIONS = None  # lazily initialized below!

ANGLE_UNCERTAINTY = 0.3  # statistical uncertainty; degrees from reading/parallax error

# STATISTICAL UTILITIES

def ChiSq(xdata, ydata, fitdata, yerr=None, ddof=3, details=False):
    # Assuming poisson distribution: Variance is expected vallue
    yerr = yerr if yerr is not None else np.sqrt(ydata)
    diffsq = (fitdata-ydata)**2/(yerr**2)
    if details:
        return diffsq.sum()/(xdata.size-ddof), xdata.size-ddof, diffsq.sum()
    return diffsq.sum()/(xdata.size-ddof)

def normal_fit(ang, mu, sigma, scale, off=0):
    """Normal distribution with scale and offset."""
    return scale*stats.norm.pdf(ang, mu, sigma)-off

def normal_fit_deriv(ang, mu, sigma, scale, off=0):
    return (mu-ang)/sigma**2 * normal_fit(ang, mu, sigma, scale, off)

nv = lambda x: unumpy.nominal_values(x) # if type(x) is np.ndarray else x.nominal_value
sd = lambda x: unumpy.std_devs(x) # if type(x) is np.ndarray else x.std_dev
weighted_mean = lambda unparr: (nv(unparr)/sd(unparr)**2).sum()/(1/sd(unparr)**2).sum()
weighted_mean = lambda unparr: (unparr/sd(unparr)**2).sum()/(1/sd(unparr)**2).sum()
# PHYSICAL CALCULATIONS

def angle_rms(p, thick, X0=89):
    """Calculate RMS angle of scattering for particles with p, thickness in mm and X0 in keV cm^2/g"""
    gamma = np.sqrt((p**2+ELECTRON_MASS**2))/ELECTRON_MASS
    beta = np.sqrt(1-1/gamma**2)
    c = 1
    z = 1
    return 13.6e3/(beta*c*p) * z * np.sqrt(thick/X0) * (1+0.038*np.log(thick/X0))

# PARSING

def parse_scatter_file(filename, duration=0, calibrate=True):
    """Parse multiscatter counting data in file."""
    COLS = [*COUNTERS, 'angle', 't', '_']
    df = pandas.read_csv(filename,
                         names=COLS,
                         skiprows=1)
    df = df.drop([df.columns[-1]], axis=1)

    df['angle'] = unumpy.uarray(df['angle'],
                                np.ones_like(df['angle'])* ANGLE_UNCERTAINTY)

    for c in COUNTERS:
        if duration:
            # print('parse_scatter_file: Corrected background by', duration*COUNTER_MEAN_BACKGROUND_NOISE)
            df[c] -= duration*COUNTER_MEAN_BACKGROUND_NOISE
        df[c] = unumpy.uarray(df[c], np.sqrt(df[c]))
    if calibrate:
        return calibrate_scatter_data(df)
    return df

# CALIBRATION AND CORRECTION

def calibrate_scatter_data(df):
    """Apply calibration factors to data in dataframe."""
    new = pandas.DataFrame({'angle': df['angle'], 't': df['t']})
    for i, c in enumerate(COUNTERS):
        new[COUNTERS[i]] = df[COUNTERS[i]] * COUNTER_CORRECTIONS[i]
    new['corrected'] = [True]*len(new)
    return new

def determine_background_rate(bgdf, duration):
    bgs = []
    for c in COUNTERS:
        mean = bgdf[c].sum()/(duration*bgdf[c].size)
        bgs.append(mean)
    return weighted_mean(np.array(bgs))

COUNTER_MEAN_BACKGROUND_NOISE = nv(determine_background_rate(
    parse_scatter_file(BACKGROUND_FILE[0], calibrate=True), BACKGROUND_FILE[1]))

# FITTING AND MATCHING

def find_scatter_peak(angles, counts, plot=False, use_offset=USE_OFFSET):
    """For a set of angles and counts (a single sensor), find where is the peak."""
    if use_offset:
        (mu, sigma, scale, off), pcov = optimize.curve_fit(
            normal_fit,
            nv(angles),
            nv(counts),
            [nv(angles[np.argmax(nv(counts))]), 10, nv(counts).sum(), 0],
            sigma=sd(counts), absolute_sigma=True)
        s_mu, s_sigma, s_scale, s_off = list(np.sqrt(pcov.diagonal()))
    else:
        (mu, sigma, scale), pcov = optimize.curve_fit(
            normal_fit,
            nv(angles),
            nv(counts),
            [nv(angles[np.argmax(nv(counts))]), 10, nv(counts).sum()],
            )#sigma=sd(counts), absolute_sigma=True)
        off, s_off = 0, 1e-6

        model = odr.Model(lambda param, x: normal_fit(x, *param))
        data = odr.Data(nv(angles), nv(counts), wd=1/sd(angles)**2, we=1/sd(counts)**2)
        fit = odr.ODR(data, model, beta0=[nv(angles[np.argmax(nv(counts))]), 10, nv(counts).sum()])
        out = fit.run()
        out.pprint()
        [mu, sigma, scale] = out.beta
        [s_mu, s_sigma, s_scale] = out.sd_beta

    if plot:
        plt.errorbar(nv(angles), nv(counts), sd(counts),
                     xerr=sd(angles), fmt='.')
        intpol = np.linspace(nv(angles).min(),
                             nv(angles).max(), 100)
        plt.plot(intpol,
                 [normal_fit(a, mu, sigma, scale, off) for a in intpol])

    fit = np.array([normal_fit(a, mu, sigma, scale, off)
                    for a in nv(angles)])
    chisq = ChiSq(nv(angles), nv(counts), fit, yerr=sd(counts),
                  ddof=4 if use_offset else 3, details=True)
    print('find_scatter_peak: ChiSq = ', chisq,
          '; other params:', (mu, sigma, scale, off))
    return (mu, sigma, scale, off), (s_mu, s_sigma, s_scale, s_off)

def find_sensor_positions(df):
    """Find sensor positions for all sensors in df."""
    pos = []
    width = []
    for i, counter in enumerate(COUNTERS):
        (mu, sigma, scale, off), pstd = find_scatter_peak(
            df['angle'], df[counter])
        print('find_sensor_positions: Counter', counter, 'at offset', mu, '+/-', sigma)
        pos.append(unc.ufloat(mu, pstd[0]))
        width.append(unc.ufloat(sigma, pstd[1]))
    print("find_sensor_positions: Weighted mean of width: ", weighted_mean(np.array(width)))
    return np.array(pos)

def find_average_sensor_positions(source=BACK_FILES):
    """Calculate sensor positions from no-absorb, back-position files."""
    positions = []

    for f in BACK_FILES:
        df = parse_scatter_file(f)
        pos = find_sensor_positions(df)
        positions.append(pos)

    positions = np.array(positions)
    # print('find_average_sensor_positions:', positions)
    # mean = (nv(positions)/sd(positions)**2).sum(axis=0)/(1/sd(positions)**2).sum(axis=0)
    # mean_sigma = 1/(1/sd(positions)**2).sum(axis=0)
    # return unumpy.uarray(mean, mean_sigma)
    return (positions.sum(axis=0))/positions.shape[0]

# With systematic uncertainties!
COUNTER_POSITIONS = find_average_sensor_positions()

def reduce_angles(df, positions=COUNTER_POSITIONS):
    """Reduce measurements from 4 counters to one count per angle.

    If positions is not None, use the given positions to calibrate.
    Otherwise, automatically guess center."""
    assert df['corrected'][0]  # We work on corrected data

    # For each counter, determine angular distance from center by fitting
    # Gauss distribution, then shift appropriately.

    angles, angles_sys, counts = [], [], []

    for i, counter in enumerate(COUNTERS):
        if positions is None:
            (mu, sigma, scale, off), (s_mu, s_sigma, s_scale, s_off) = find_scatter_peak(
                df['angle'], df[counter])
            print('reduce_angles: Counter', counter, 'at offset', mu, '+/-', sigma)
            shifted_angles = df['angle'] - mu
            shifted_angles_sys_unc = np.ones_like(df['angle'])*s_mu
        else:
            shifted_angles = df['angle'] - nv(positions[i])
            shifted_angles_sys_unc = np.ones_like(df['angle'])*sd(positions[i])
        angles.extend(shifted_angles)
        angles_sys.extend(shifted_angles_sys_unc)
        counts.extend(df[counter])

    reduced = pandas.DataFrame({'angle': angles, 'count': counts, 'angles_sys': angles_sys})
    sorted_values = reduced.sort_values(by='angle')
    return sorted_values

# PLOTTING AND GRAPHING

def plot_scatter_raw(df, series=None, title='', save=None):
    """Plot the raw collected data (or calibrated)"""
    if type(df) is not list:
        df = [df]

    f = plt.figure(figsize=FIGSIZE, tight_layout=True)
    p = f.add_subplot(111)

    for m, measurement in enumerate(df):
        for i, counter in enumerate(COUNTERS):
            p.errorbar(nv(measurement['angle']), nv(measurement[counter]),
                       sd(measurement[counter]),
                       color=COLORS[i], fmt=MARKERS[m],
                       label=counter + (' ' + series[m] if series else ''))
    p.set_xlabel('Angle / degrees')
    p.set_ylabel('Count (calibrated)')
    p.legend()
    p.grid()

    if save:
        f.savefig(save)

def plot_scatter_reduced(dfr, title='', save=None, oldcanvas=None,
                         regr_angle=None):
    """Plot the reduced (angle-adjusted) data frame df."""
    if oldcanvas is None:
        f, (p, res) = plt.subplots(2, 1, figsize=FIGSIZE, tight_layout=True,
                               sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    else:
        f, p, res = oldcanvas
    angle, count, angles_sys = list(np.array(dfr).T)
    rangle, rcount, rangles_sys = angle, count, angles_sys

    if regr_angle:
        leftix, rightix = np.argmin(np.abs(nv([dfr['angle']])+20)), np.argmin(np.abs(nv([dfr['angle']])-20))
        rangle, rcount, rangles_sys = angle[leftix:rightix], count[leftix:rightix], angles_sys[leftix:rightix]

    (mu, sigma, scale, off), pcov = find_scatter_peak(
        rangle, rcount)
    fitdata = np.array([normal_fit(a, mu, sigma, scale, off) for a in nv(rangle)])


    residuals_propagated_x_err = normal_fit_deriv(nv(rangle), mu, sigma, scale, off)*sd(rangle)
    residuals = nv(rcount) - fitdata
    residuals_err = np.sqrt(sd(rcount)**2+residuals_propagated_x_err**2)

    chisq, chisq_sz, chisq_sum = ChiSq(nv(rangle), nv(rcount), fitdata,
                                       yerr=residuals_err, details=True)

    print('plot_scatter_reduced: Fit parameters (mu, sigma, scale) = ',
          (mu, sigma, scale, off), 'ChiSq:', chisq)

    intpol = np.linspace(nv(angle).min(), nv(angle).max(), 100)
    p.errorbar(nv(angle), nv(count),
               yerr=sd(count), xerr=sd(angle),
               fmt='.', label=title+': Count')
    p.plot(intpol, [normal_fit(a, mu, sigma, scale, off) for a in intpol],
           label=title+' fit, $\\sigma = {:.1f}$\n$\\chi^2$/ndof = {:.1f}/{} = {:.1f}'.format(sigma, chisq_sum, chisq_sz, chisq),
           color='gray')
    res.errorbar(nv(rangle), residuals, residuals_err, fmt='.')

    res.set_xlabel('Angle (centered) / degrees')
    res.set_ylabel('count - fit')
    p.set_ylabel('Count (calibrated)')
    p.set_title('Reduced Scatter Counts')
    p.legend()

    return f, p, res

def analyze_all_scatter_noabsorb():
    analyze_scatter_noabsorb(BACK_FILES, save='../img/beam_profile_position_a.pdf')
    analyze_scatter_noabsorb(FRONT_FILES, save='../img/beam_profile_position_b.pdf')

def analyze_scatter_noabsorb(files=(BACK_FILES+FRONT_FILES), save='',
                             duration=60):
    """Plot and analyze scatter processes with no absorber."""
    fig, p, res = None, None, None
    for f in files:
        print('analyze_scatter_noabsorb: ======== {} ========'.format(f))
        df = parse_scatter_file(f, duration=duration)
        dfr = reduce_angles(df)

        rms = unumpy.sqrt((dfr['count'] * dfr['angle']**2).sum()/dfr['count'].sum())
        print('analyze_scatter_noabsorb: RMS angle for {:.1f}mm is {}'.format(0, rms))

        title = path.basename(f)
        if fig is None:
            fig, p, res = plot_scatter_reduced(
                dfr, title=title.split('-')[0], regr_angle=20)
            p.grid()
            res.grid()
        else:
            plot_scatter_reduced(
                dfr, title=title, save='../img/{}.pdf'.format(title.replace(' ', '_')),
                oldcanvas=(fig,p,res), regr_angle=20)
    if save:
        fig.savefig(save)

def plot_aluminium_scatter(data=AL_FILES):
    al_dfs = []
    thicknesses = []
    fit_params = []

    for thick, (file, dur) in data.items():
        df = parse_scatter_file(file, duration=dur)
        dfr = reduce_angles(df)
        dfr['count'] /= dur
        al_dfs.append((float(thick), dfr))
        thicknesses.append(float(thick))

    f, (p, res) = plt.subplots(2, 1, figsize=(FIGSIZE[0]+2, FIGSIZE[1]),
                               tight_layout=True,
                               sharex=True, gridspec_kw={'height_ratios': [5, 2]})

    for thick, df in al_dfs:
        leftix, rightix = np.argmin(np.abs(nv([df['angle']])+20)), np.argmin(np.abs(nv([df['angle']])-20))
        angle, count = df['angle'].to_numpy()[leftix:rightix], df['count'].to_numpy()[leftix:rightix]

        (mu, sigma, scale, off), pcov = find_scatter_peak(angle, count)
        fit_params.append((mu, sigma, scale, off, pcov))
        fitdata = normal_fit(nv(angle), mu, sigma, scale, off)

        print('plot_aluminium_scatter: {} has peak at {}'.format(
                thick, stats.norm.pdf(0, 0, sigma)*scale))

        # Propagate angle error to residuals
        residuals_propagated_x_err = normal_fit_deriv(nv(angle), mu, sigma, scale, off)*sd(angle)
        residuals = nv(count) - fitdata
        residuals_err = np.sqrt(sd(count)**2+residuals_propagated_x_err**2)

        chisq, chisq_dof, chisq_sum = ChiSq(
            nv(angle), nv(count), fitdata, yerr=residuals_err,
            details=True, ddof=3 if USE_OFFSET else 4)

        fitx = np.linspace(nv(df['angle']).min(), nv(df['angle']).max(), 100)
        fity = normal_fit(fitx, mu, sigma, scale, off)

        p.errorbar(nv(df['angle']), nv(df['count']),
                   xerr=sd(df['angle']), yerr=sd(df['count']),
                   fmt=',',
                   label='{:.1f} mm, $\\chi^2$ / ndof = {:.1f}/{} = {:.1f}'.format(
                       thick, chisq_sum, chisq_dof, chisq))
        p.plot(fitx, fity, color='gray')
        res.errorbar(nv(angle), nv(residuals),
                     yerr=residuals_err, fmt='.')

        # Calculate RMS angle.
        rms = unumpy.sqrt((df['count'] * df['angle']**2).sum()/df['count'].sum())
        print('plot_aluminium_scatter: RMS angle for {:.1f}mm is {}'.format(thick, rms))

    p.legend()
    p.grid()
    res.grid()
    p.set_xlabel('Angle / degree')
    p.set_ylabel('Rate / Hz')
    p.set_title('Aluminium multi scattering profile')
    res.set_ylabel('Data - Fit')
    f.savefig('../img/aluminium_multi_scattering.pdf')

    # Plot change of sigma (peak width) as function of thickness
    thicknesses = np.array(thicknesses)
    sigmas = np.array([fp[1] for fp in fit_params])
    sigma_sigmas = np.array([fp[4][1] for fp in fit_params])
    (a, ea, b, eb, chiq, corrs) = npl.nice_linear_regression_plot(
        thicknesses, sigmas, sigma_sigmas, figsize=FIGSIZE,
        title='Al peak widths for varying thickness',
        ylabel='Beam width / degree', xlabel='Al thickness / mm',
        ylabelresidue='beam width - (mx + b)',
        save='../img/aluminium_peak_widths.pdf')

    return

    # Less sophisticated plot
    fig = plt.figure(figsize=(5,3), tight_layout=True)
    p = fig.add_subplot(111)

    p.errorbar(thicknesses, [fp[1] for fp in fit_params], [fp[4][1] for fp in fit_params],
               fmt='.', label='$\sigma$')
    p.set_xlabel('Al Thickness / mm')
    p.set_ylabel('$\\sigma \\pm \\sigma_\\sigma$')
    p.set_ylim(0,40)
    p.grid()
    p.legend()
    fig.savefig('../img/aluminium_peak_widths.pdf')

def plot_material_scatter(data=MATERIAL_FILES):
    dfs = []
    for material, (file, dur) in data.items():
        df = parse_scatter_file(file, duration=dur)
        dfr = reduce_angles(df)
        dfr['count'] /= dur
        dfs.append((material, dfr))

    f, (p, res) = plt.subplots(2, 1, figsize=(FIGSIZE[0]+2, FIGSIZE[1]),
                               tight_layout=True,
                               sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    for material, df in dfs:
        print('plot_material_scatter: ======= {} ======='.format(material))
        if material not in ('fe',):
            leftix, rightix = np.argmin(np.abs(nv([df['angle']])+20)), np.argmin(np.abs(nv([df['angle']])-20))
            angle, count = df['angle'].to_numpy()[leftix:rightix], df['count'].to_numpy()[leftix:rightix]

            (mu, sigma, scale, off), pcov = find_scatter_peak(angle, count)
            print('plot_material_scatter: {} has peak at {}'.format(
                material, stats.norm.pdf(0, 0, sigma)*scale))
            rms = unumpy.sqrt((df['count'] * df['angle']**2).sum()/df['count'].sum())
            print('plot_material_scatter: RMS angle for {} is {}'.format(material, rms))
            fitdata = normal_fit(nv(angle), mu, sigma, scale, off)

            # Propagate angle error to residuals
            residuals_propagated_x_err = normal_fit_deriv(nv(angle), mu, sigma, scale, off)*sd(angle)
            residuals = nv(count) - fitdata
            residuals_err = np.sqrt(sd(count)**2+residuals_propagated_x_err**2)

            chisq, chisq_dof, chisq_sum = ChiSq(
                nv(angle), nv(count), nv(fitdata), yerr=residuals_err,
                details=True)

            fitx = np.linspace(nv(df['angle']).min(), nv(df['angle']).max(), 100)
            fity = normal_fit(fitx, mu, sigma, scale, off)
            residuals = nv(count) - fitdata
            p.plot(fitx, fity, color='gray')
            res.errorbar(nv(angle), nv(residuals),
                 yerr=residuals_err, fmt='.')
            p.errorbar(nv(df['angle']), nv(df['count']),
                       xerr=sd(df['angle']), yerr=sd(df['count']),
                       fmt=',',
                       label='{}, $\\chi^2$ / ndof = {:.1f}/{} = {:.1f}'.format(material, chisq_sum, chisq_dof, chisq))
        else:
            p.errorbar(nv(df['angle']), nv(df['count']),
                   xerr=sd(df['angle']), yerr=sd(df['count']),
                   fmt=',',
                   label=material)
            res.plot([0], [0], ',')
    p.legend()
    p.grid()
    res.grid()
    p.set_xlabel('Angle / degree')
    p.set_ylabel('Rate / Hz')
    p.set_title('Various materials: multi scattering profile')
    res.set_ylabel('Data - Fit')
    f.savefig('../img/materials_multi_scattering.pdf')
