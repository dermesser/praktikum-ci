\documentclass[DIV=15]{scrartcl}
\usepackage[english]{babel}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{hyperref}

\usepackage[notext]{stix2}
\usepackage[no-math]{fontspec}
\setmainfont{STIX Two Text}

\usepackage{siunitx}

\title{T12: Preparation}
\author{Lewin Bormann}
\date{March 2021}

\newcommand{\e}[1]{\emph{#1}}
\newcommand{\bta}{$\beta$~}
\newcommand{\bigO}{\ensuremath{\mathcal{O}}}

\begin{document}

\maketitle

\section{Introduction}

Goals of the experiment: Teach basic knowledge about how particle detectors work:

\begin{enumerate}
    \item Measure momentum of charged particles.
    \item Measure multiple scattering deflection.
    \item Measure dE/dx in matter.
\end{enumerate}

There are two fundamental ways of measuring properties of particles:
\begin{enumerate}
    \item Measure curvature of trajectory in B field to obtain momentum. Detector type: \e{tracker}, with high spatial resolution. Limited by multi scattering in material.
    \item \e{Calorimeter} measures deposited energy, depending on particle velocity.
    \begin{enumerate}
        \item \e{Electromagnetic calorimeter}: Detect photons, $e^\pm$.
        \item \e{Hadronic calorimeter}: Detect protons/kaons.
    \end{enumerate}
\end{enumerate}

From energy and momentum, \e{all} properties of particles can be derived (e.g. mass, $p_T$).

This experiment will have electrons and positrons colliding with each other.

\section{\bta Decays}

\begin{itemize}
    \item Beta+ decay: $p \rightarrow n + e^+ + \nu_e$; occurs in proton-rich nuclei.
    \item Beta- decay: $n \rightarrow p + e^- + \overline \nu_e$; occurs in neutron-rich nuclei.
\end{itemize}

Energy release for \bta decays is usually on the order of \SI{1}{\mega\eV}.

\subsection{\bta spectrum}

Because in \bta decays, energy and momentum is transferred to \e{three} particles, the energy spectrum of the particles is continuous, and can be calculated from Fermi's \e{Golden Rule}:

\[w = \frac{1}{h} \left| \langle \psi_f | H_S | \psi_i \rangle \right|^2 \dv{N}{E_0}\]

with

\[\psi_i = e^{i \vec p \cdot \vec r / \hbar} / \sqrt{V}\]

A \bta decay is modelled as transition from the initial nucleus to the final nucleus in addition to electron and anti-neutrino (or vice versa; see equations above) within an infinitesimal momentum range $p \pm \dd p$. Due to the short range of the weak force, wave functions can be approximated by plane waves.
$\dv{N}{E_0}$ represents the density of final states.

Assuming a nearly constant wave function within the nucleus, we can determine \[\left| \langle \psi_f | H_S | \psi_i \rangle \right|^2 = \frac{g^2 |M_{fi}|^2}{V^2}\] with the matrix element $M_{fi}$ and the normalization factor $V$. Because we can see in this equation that neither positron nor neutrino carry angular momentum, this transition is allowed.

The energy of the final state is the sum of the positron's and the neutrino's energies. The possible number of states is composed of the number of possible neutrino and positron states ($\dd N = \dd n_e \dd n_\nu$). Because each state occupies a volume of $\hbar^3$, the total density of states is due to the sort range of interaction

\[\dv{N}{E_0} = \frac{16\pi^2V^2}{(2\pi\hbar)^6} p_e^2 p_\nu^2 \dv{p}{E_0} \dd p_e\]

If we additionally neglect the neutrino's mass, we can write

\[p^2_\nu \dv p_\nu = \frac{(E_0-E_e)^2}{c^3} \dv E_0\]

so that we arrive at an equation for $w$

\[\dv w(p_e) = K |M_{fi}|^2 p_e^2 (E_0-E_e)^2 \dv p_e\]

in which we can substitute $E_e$ and $\dv p_e$ with the electron's kinetic energy $T = E_e - m_e$.

\subsubsection{Fermi correction}

Due to charged nucleus and electrons/positrons, the energy spectrum is distorted by the Coulomb force. The distortion depends on the nucleon's $Z$ and the $e$'s momentum.

The Fermi function $F(Z, \eta)$ describes the ratio of wave functions with and without the Coulomb effect being taken into account:

\[F(Z, \eta) = \frac{2\pi\eta}{1-e^{-2\pi\eta}},\qquad\eta = \pm \frac{Z \alpha E}{p_e}\]

The \e{Fermi function} $F(Z, \eta)$ determines the distortion and is the ratio of $e$ wave functions with respectively without the effect:

\[\dv w(p_e) = K |M_{fi}|^2 F(Z, \eta) p_e^2 (E_0-E_e)^2 \dv p_e\]

\subsubsection{Allowed/prohibited transitions}

Matrix elements are only constant for "allowed" transitions; those are transitions with a high probability $w$ and where the resulting particles don't carry angular momentum. There are two kinds of allowed transitions:

\begin{enumerate}
    \item \e{Fermi transition}: Electron and neutrino spins are pointing in opposite directions. A singlet state is formed, nuclear spin unchanged.
    \item \e{Gamow-Teller transition}: $e, \nu_e$ form a triplet state (because $s_e + s_\nu = 1$). The nuclear spin may change by $\Delta J \in \{+1, -1, 0\}$
\end{enumerate}

All other transitions are prohibited. The more prohibited $\rightarrow$ the lower the probability of a transition, making the wave function non-constant across the nucleus. Higher orders have to be taken into account for the calculation of the matrix element. The decay constant $\lambda$ (in $N = N_0 \exp(-t/\lambda)$) is calculated from the \e{Fermi integral} $f(Z, W_{max})$: 

\[\lambda = K' |M_{fi}|^2 \int_1^{W_{max}} F(Z, W_e) \sqrt{W_e^2-1} (W_{max} - W_e)^2 \dd W_e\]

with $W_e = \frac{E_e}{m_e c^2}$. From this we can calculate the half life $t_{1/2} = \ln 2/\lambda = K' |M_{fi}|^2 f(Z,W_{max})$. Finally, the \e{$ft$ value} gives an estimation of whether a transition is allowed:

\[f(Z,W_{max})\,t_{1/2} = \frac{\ln 2}{K' |M_{fi}|^2}\]

Values of $ft$ up to \num{1e6} are allowed, higher values indicate a prohibited transition.

\begin{itemize}
    \item \e{What kind of transitions are found in $^{90}$Sr and $^{90}$Y?} ($^{90}$Sr will be used in the experiment)
\end{itemize}


\subsubsection{Kurie diagram}

We obtain the \e{Kurie diagram} when plotting $\sqrt{\frac{\dd w / \dd p_e}{F(Z, p_e) p_e^2}}$. A linear behavior is expected if $|M_{fi}|^2$ is constant; the maximum energy is found by considering the intersection with the x-axis.

Multiple decay components in the spectrum manifest as kinks in the line. This diagram is directly derived from the p-N diagram shown in the T12 slides.

\subsection{Bending charged particles in B fields}

A charged particle flying through a magnetic field experiences the Lorentz force $\vec F = q \vec v \times \vec B$. This doesn't change the particle's energy.

The Frequency \[\omega = \frac{q B}{m}\] is called \e{Larmor frequency}. It is the circular frequency of the particle circling through the field. In a constant magnetic field, only particles with a specific momentum can go around a circular path with radius $R_0$. We can see that \[m v = p = q B R_0\] which leads to the following equation in natural units: \[p / \si{\kilo\eV} = 0.3~ B/\si{\milli\tesla}~ R_0/\si{\milli\meter}\]

\subsubsection{Momentum measurement in CMS \& tracking detectors}

Charged particles in CMS leave an ionization signal that can be reconstructed as hits. From these, a helix line can be fit, which is in the shape of an arc with length $L$ and radius $R$. However, we actually measure the \e{sagitta}, which is the straight line connecting the first and last hits. With a small angle $\theta$, the sagitta is \[s = R(1-\cos \theta) \approx \frac{L^2}{8R}\]

From this, the momentum equation becomes \[p = 0.3 B R \approx 0.3 B \frac{L^2}{8s}\].

\subsection{Energy loss of electrons in matter}

Particles travelling through matter lose energy, by interaction with shell electrons (excitation/ionization), with nuclei (Coulomb scattering) through emission of Bremsstrahlung. Cerenkov radiation is emitted if the speed of a particle exceeds the speed of light in the medium.

For this experiment, \e{ionization} and \e{bremsstrahlung} are important.

\subsubsection{Ionization}

The energy loss for electrons is different than the one for heavy particles. However, we achieve results by calculating first for heavy particles and then applying the results to electrons. We assume that the incoming particle's energy is much larger than a shell electron's ionization energy. If relative momentum transfer $\Delta p/p$ is small, the electron can then be considered small.

The momentum transferred to a shell electron by a passing-by particle of charge $Z_1 e$ is \[\Delta \vec p = \int_{-\infty}^\infty \vec F_C \dd{t} = \int_{-\infty}^{\infty} \frac{e}{v} \vec E_\perp \dd{x}\]

(the longitudinal component is irrelevant here). With Gauss' divergence theorem, we obtain $\Delta p = \frac{Z_1 e^2}{2\pi \varepsilon_0} \frac{1}{bv}$ with the \e{impact parameter} $b$ determining the minimum distance between electron and particle.

We calculate the total energy transfer to all electrons by integrating across all electrons in $\dd{V}$: \[\Delta E = \frac{(\Delta p)^2}{2 m_e} n_e \dd{V} = \frac{Z_2^2 e^4 n_e b}{8m_e\pi^2 \varepsilon_0^2 \beta^2 c^2 b^2} \dd \phi \dd b \dd x\]

with which we arrive at the energy transfer per unit length of \[\dv{E}{x} = \frac{e^4 Z_1^2 n_e}{4\pi \varepsilon_0^2 m_e \beta^2 c^2} \ln \frac{b_{max}}{b_{min}}\] according to which the maximum momentum transfer occurs for central collisions with $\Delta p_{max} = 2 m_e c \beta$, with $b_{max} = \frac{Z_1 e^2}{4\pi \varepsilon_0 m_e c^2 \beta^2}$. The minimum energy transfer is equal to the electron's ionization energy, at $b_{min} = \frac{z_1 e^2}{2\pi \varepsilon_0 \beta c \sqrt{2 m_e I}}$.

For heavy elements, average ionization energy can be approximated by \[I \approx 9.73 Z + 58.8 Z^{-0.19} \si{\eV}\] and for media composed of several elements \[\ln I = \sum_k g_k \ln I_k\] with the ratio of electrons $g_k$ for the $k$th element.

The modified Bethe-Bloch formula for electrons (which are indistinguishable from shell electrons) is then given by \[\qty(\dv{E}{x})_{ion} = \frac{2\pi N_A r_0^2 m_e c^2}{\beta^2} \frac{Z \rho}{A} \qty(\ln(\frac{\tau^2 (\tau+2)}{2(I/m_e c^2)^2}) + \frac{\tau^2/8 - (2\tau +1 )\ln 2}{(\tau+1)^2} + (1-\beta^2) - \delta)\]

In this formula, $r_0 = \frac{e^2}{4\pi\varepsilon_0 m_e c^2}$ is the classical electron radius, and $\tau$ is the kinetic energy divided by $m_e c^2$ ($\tau = \gamma - 1$). $\delta$ is a correction factor accounting for the polarisation caused by the travelling electrons, which is important at large energies.

\subsubsection{Bremsstrahlung}

Acceleration of particles in a nucleus Coulomb field triggers Bremsstrahlung radiation. The magnitude of radiation is suppressed by the particle's mass, and is therefore only relevant for light particles (like $e^\pm$). The $\dv{E}{x}$ caused by Bremsstrahlung is given by

\[\qty(\dv{E}{x})_{brems} = 4\alpha N_A \frac{Z^2}{A} r_e^2 \ln(\frac{183}{Z^{1/3}}) E := E / X_0\]

$X_0$ is the distance after which the particle's energy has fallen to $1/e = 37\%$.

Ionization losses rise with $\bigO(\ln E)$, whereas Bremsstrahlung rises with $\bigO(E)$. At $E = E_C$, energy losses are dominated by Bremsstrahlung. In heavier materials (than Al), the critical energy is roughly given by \[E_C = \frac{\SI{800}{\mega\eV}}{Z+1.2}\]

For composite media, the combined energy loss can be calculated by weighting the various $\dv{E}{x}$ with the relative concentrations $w_i$.

\subsection{Multi scattering}

Scattering processes can change the direction of a particle traversing a medium. The main factor are Coulomb scattering events with nuclear fields. As a consequence, the collimated beam of particles will expand as it travels through the medium, affecting resolution of any detectors.

Measuring the momentum of a particle requires us knowing the bending radius of the charged particle, the measurement of which depends on the spatial resolution. Because scattering also prolongs the path of the particle through the material, we also obtain distorted $\dv{E}{x}$ measurements.

The scattering process dominates for electrons due to their low mass (causing large transfers of momentum). The combined effects of multiple scattering is described by Molière theory, which states that the angular distribution for small scattering angles follows a gaussian distribution. This result is expected, as multiple scattering can be modelled as the result of many small changes of direction combined with the Central Limit Theorem.

The root mean square of the deflection anlge $\Theta$ can be calculated as \[\Theta_{RMS} = \frac{\SI{13.6}{\mega\eV}}{\beta c p} z \sqrt{x/X_0} (1+0.038 \ln(x/X_0))\]

This angle importantly depends on: \[\Theta \propto \frac{1}{p} \sqrt{x} \sqrt{Z \rho}\] (because $X_0 \propto Z \rho$)

\section{Experimental setup}

We have three setups for this experiment:

\begin{enumerate}
    \item Momentum resolution
    \item Energy loss in matter
    \item Multiple scattering
\end{enumerate}

according to the three measurement goals outlined at the beginning.

\subsection{Detecting nuclear radiation}

We will use two different kinds of detectors to detect electrons emitted by the $^{90}$Sr source:

\begin{enumerate}
    \item A Geiger-Müller counter
    \item A scintillator
\end{enumerate}

\subsubsection{Geiger-Müller counter}

A metal cylinder containing a thin anode wire filled with a counting gas (mix of a noble gas and a quenching gas). Ionizing particles traversing the chamber will ionize gas particles, and the freed electrons will move along the electric fields towards the anode, causing a measurable current.

There are several different modes in which a GM counter can work:

\begin{enumerate}
    \item Recombination: the voltage is not high enough; ions recombine with electrons before anything happens.
    \item Ionisation chamber: Single electrons are accelerated to the anode.
    \item Proportional counter: Accelerated electrons cause more ionization, causing an avalanche proportional to the initial excitement.
    \item Geiger Müller counter: An exponential avalanche is triggered, covering the entire tube volume. The gas ions travel towards the cathode, and the quenching gas prevents further activation. The measured signal is independent of the energy deposition and number of particles.
\end{enumerate}

In this experiment, a GM counter is used and read out by an \e{Cobra3} system through BNC cables. The voltage applied is \SI{500}{\volt}.

\subsubsection{Scintillators}

Scintillators are frequently used as detectors for nuclear radiation. Ionizing radiation causes light flashes within the scintillator, which can be detected by a photomultiplier. A photomultiplying tube utilizes a series of dynodes, initiating emission avalanches of electrons. The signal is proportional to the energy deposition. The scintillator should absorb as much energy of traversing particles as possible, and convert it to photons. The ratio of generated photons to deposited energy is the \e{light yield}. A linear light yield is desirable for accurate measurement of deposited energy.

We can differentiate between two important types of scintillators: Organic scintillators (plastic, organic crystals), and anorganic crystals. Organic scintillators are faster and thus often used as triggers for slower oscillators. However, due to their nonlinear response, they cannot be used for accurate energy detection.

Anorganic oscillators are slower, but have a linear energy response and often a good light yield. Examples are \e{NaI(Tl)} (used here) or \e{CsI}. However, those crystals are often brittle and hygroscopic, so they cannot be exposed to air. In such crystals, scintillation processes can be described using band structures.

Here, the valence band contains electrons in molecules, whereas the conduction band contains free electrons (that can move in the material). Energy deposition results in valence electrons being pushed into the conduction band, later falling back to the valence band and emitting a specific energy. Because this energy is equal to the energy difference between valence and conduction band, those photons will be reabsorbed, making the material opaque to these photons. Activator atoms that the scintillator crystal is doped with create additional energy levels close to the bands. Excited electrons can now fall into activator states, causing the emission of a phonon and later on a photon with a different energy. The scintillator is transparent to photons of that second energy.

\subsubsection{Momentum resolution}

This is a momentum spectroscope: Electrons produced by $S$ pass through an opening $\Delta x_1$ into a B field where they are subject to Lorentz force. A detector $D$ is covered with a slit $\Delta x_2$.

The momentum resolution is then according to Fig. 3.8 in the script the difference between the lowest and highest momentum that can cause electrons to pass from the opening to the detector. We know that $p = 0.3 B R$ with $p$ in GeV, $R$ in m, and $B$ in T.

 For the resolution, consider \autoref{fig:momentum_circles}. Because \[p / \si{\giga\eV} = 0.3\,B/\si{\tesla}\,R/\si{\meter}\] we are interested in the extreme radii $R_{max}$ and $R_{min}$ that particles can take. The smallest radius corresponds to the lowest momentum and the largest radius to the highest momentum. The resolution is then \[\Delta p = 0.3 B \Delta R\] in the units described above.

\begin{figure}
    \centering
    \includegraphics[scale=0.5]{momentum_circles.png}
    \caption{Minimum and maximum radius for given apertures.}
    \label{fig:momentum_circles}
\end{figure}

To find the extreme radii, we take a look at our geometry. The source $S$ is limited by points $I$, $K$; and the detector $D$ is limited by points $F$, $C$. Geometrically, the smallest circle through these slots will cross $K\rightarrow F$, whereas the largest will go through $I\rightarrow C$. To calculate the radii, we are interested in the center point of each circle.

The centerpoint $J$/$G$ (let this be $P$ in general) must lie on the y axis (because electrons go into the chamber with only a momentum in x direction), and the distance $GF = GK$ for the small circle and $JC = JI$. This can be expressed as \[y_P - y_{I/K} = \sqrt{x_{F/C}^2 + (y_P - y_{F/C})^2}\] where $x$ and $y$ are the respective coordinates of $F, C, I, K$.

Solving this, we find that \[y_P = \frac{x_{F/C}^2 + y_{F/C}^2 - y_{I/K}^2}{2(y_{F/C}-y_{I/K})} \Rightarrow R_{max/min} = y_P - y_{I/K}\]

Finally, \[\Delta R = R_{max} - R_{min} = \frac{x_{C}^2 + y_{C}^2 - y_{I}^2}{2(y_{C}-y_{I})} - \frac{x_{F}^2 + y_{F}^2 - y_{K}^2}{2(y_{F}-y_{K})} - y_I + y_K\]

\section{Execution}
\subsection{Measuring momentum in a magnetic field}

Important aspects to consider:

\begin{enumerate}
    \item The coil current must be changed slowly as to not end up with a residual field at $I = 0$. The current must always be below \SI{5}{\ampere}. The aperture between the magnet's poles must be less than \SIrange{0.5}{1}{\cm}. Otherwise B field lines escape the enclosed area and field is not homogenous anymore (?)
    \item Magnetic field can be measured using Hall probe; the drawn grid is the reference. Check where B field is homogenous and how coil currents affect temporal behavior of field: Perform 60s measurements for different currents (sampling interval 0.2s e.g.). Hall probe to USB port on the right. \textbf{$\rightarrow$ Check that field is homogenous in space and time}
    \item Construct setup for measurement of $^{90}$Sr momentum. Sensible and convenient position for source and GM counter! Ask supervisor before operating. \e{Guess: Source/detector in 90 degree angle around poles, as per \autoref{fig:momentum_path}}
    \item Measure event count in dependence of B field. Vary field appropriately. Power supply may be too coarse; use pole piece screw to adjust field. Keep hall probe between poles, fixed position, outside electron path! Rescale if measurement different from center of magnet: compare with measurement 2.
    \item Measure background count rate without magnetic field; correct remaining measurements.
\end{enumerate}

The radius here is the radius of the pole shoes of the electromagnet, as can be seen in \autoref{fig:momentum_path} (electrons enter centered and orthogonal to the outer shape).

\begin{figure}
    \centering
    \includegraphics[scale=0.4]{momentum_path.png}
    \caption{Path of electrons through homogenous magnetic field has radius of that field (red)}
    \label{fig:momentum_path}
\end{figure}

\subsection{Energy loss in matter}

This experiment sends radioactive ionizing particles through an absorber, measuring the penetrating radiation on the other side of the absorber using a scintillator and photomultiplier.

Important aspects:

\begin{enumerate}
    \item Supply voltage must remain at \SI{500}{\volt}. Voltage supply to PM should be turned on after the program has been started, and turned off before closing it.
    \item Collect enough data for good statistics. Save as TKA file, which is readable text.
    \item Measure spectrum of Sr without absorber. Adjust amplification so that complete spectrum is recorded!
    \item Measure spectra of Na-22 (\bta +), Cs-137, Co-60, Eu-152. Do energy channel calibration with these. These are gamma emitters and have a well defined energy peak.
    \item Measure background, use it! (subtract from measurements)
    \item Measure spectra of Sr with Al absorbers, thickness \SIrange{4}{0}{\mm}.
\end{enumerate}

\subsection{Multiple scattering}

Setup has 3 to 4 GM counters read out by Arduino+Raspberry Pi. Take appropriate angular steps for this setup! $\Rightarrow$ e.g. angle between counters divided by number of counters: provides high resolution, repeated measurements of same points. Or angle between counters, provides fast measurement.

GM counters must be calibrated, as they don't exhibit the same counts when exposed to the same source. Tutors have calibration factors.

Measurement of multi scattering: the source can be moved within the collimator, but cannot be removed from it.

\begin{enumerate}
    \item Remove radioactive source inside collimator as far as possible away from GM counters opening (Position A)
    \item Measure angular distribution of electrons for Sr source with/without absorbers. Choose appropriate steps based on expected distribution (center-heavy! Vorkurs slides: \SI{5.3}{\degree} between steps). Use 4 different thicknesses of Al and 4 different materials with same thickness. Use sensible measurement times. Consider background.
\end{enumerate}

Measurement of beam profile: Intensity and collimation depend on source positioning. The source can be put either towards the exit of the source enclosure or away from it. The beam is most collimated when the source is removed from the aperture.

\begin{enumerate}
    \item Move radioactive source close to opening towards Geiger Müller counter. (Position B)
    \item Measure angular distribution of electrons without absorbers. Choose reasonable angular steps!
    \item Restore source to Position A. Measure beam profile without absorbers, is reproducibility given?
\end{enumerate}

\end{document}
