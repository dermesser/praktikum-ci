# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 09:38:56 2021

@author: 394493: Alexander Kohlgraf
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import os
import nice_plots
from mpl_toolkits.mplot3d import Axes3D

# import vg
# from pytransform3d.rotations import matrix_from_axis_angle

# from scipy.interpolate import UnivariateSpline

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class elevator:
    def __init__(self):
        self.df_hoch = pd.read_csv(
            "data/3_acceleration/acceleration/accelerationhoch.txt",
            delimiter=", ",
            engine="python",
        )
        self.df_runter = pd.read_csv(
            "data/3_acceleration/acceleration/accelerationrunter.txt",
            delimiter=", ",
            engine="python",
        )

        self.df_hoch["time"] = self.df_hoch["time"] * 10 ** (-6)
        self.df_runter["time"] = self.df_runter["time"] * 10 ** (-6)

        self.df_hoch["acc"] = np.sqrt(
            self.df_hoch["x_acc"] ** 2
            + self.df_hoch["y_acc"] ** 2
            + self.df_hoch["z_acc"] ** 2
        )
        self.df_runter["acc"] = np.sqrt(
            self.df_runter["x_acc"] ** 2
            + self.df_runter["y_acc"] ** 2
            + self.df_runter["z_acc"] ** 2
        )

    def plot_raw(self):
        plt.figure(figsize=(20, 10))
        plt.grid(True)
        plt.ylabel("acceleration [m/(s^2*g)]")
        plt.xlabel("time [s]")
        plt.title("Rohdaten der Beschleunigungsmessung für hoch und runter")
        plt.plot(
            self.df_hoch["time"],
            self.df_hoch["acc"],
            label="nach oben fahrend",
            alpha=0.8,
        )
        plt.plot(
            self.df_runter["time"],
            self.df_runter["acc"],
            label="nach unten fahrend",
            alpha=0.8,
        )
        plt.legend()
        plt.savefig(r"protokoll/images/raw_elevator.pdf")


class fields:
    def __init__(self, all_numbers):
        self.numbers = all_numbers
        self.names = ["rotation" + str(x) + ".txt" for x in all_numbers]

        self.df_list = [
            pd.read_csv(
                "data/3_acceleration/rotation/" + x, delimiter=", ", engine="python"
            )
            for x in self.names
        ]
        for x in self.df_list:

            x["acc"] = np.sqrt(x["x_acc"] ** 2 + x["y_acc"] ** 2 + x["z_acc"] ** 2)
            x["mag"] = np.sqrt(x["x_mag"] ** 2 + x["y_mag"] ** 2 + x["z_mag"] ** 2)

            x["angle"] = (
                180
                / np.pi
                * np.arccos(
                    (
                        x["x_acc"] * x["x_mag"]
                        + x["y_acc"] * x["y_mag"]
                        + x["z_acc"] * x["z_mag"]
                    )
                    / (x["acc"] * x["mag"])
                )
            )
            x["phi_acc"] = np.arctan2(x["y_acc"], x["x_acc"]) * 180 / np.pi
            x["theta_acc"] = np.arcsin(x["z_acc"] / x["acc"]) * 180 / np.pi
            x["phi_mag"] = np.arctan2(x["y_mag"], x["x_mag"]) * 180 / np.pi
            x["theta_mag"] = np.arcsin(x["z_mag"] / x["mag"]) * 180 / np.pi

        self.quiver()
        self.angle()
        self.amplitude_acc()
        self.amplitude_mag()

    def angle(self):
        angle_list = [x["angle"].mean() for x in self.df_list]
        plt.figure(figsize=(20, 10))
        plt.grid(True)
        plt.ylabel("count")
        plt.xlabel("Angle between g and B")
        plt.title(
            "Winkel zwischen Schwerefeld und Magnetfeld für verschiedene Richtungen"
        )
        plt.hist(angle_list, bins=30)
        plt.savefig("protokoll/images/" + "angle_hist.pdf")
        plt.show()

    def quiver(self, angle=45):
        fig = plt.figure(figsize=(20, 10))
        ax = fig.gca(projection="3d")
        ax.set_xlim3d([-1.0, 1.0])
        ax.set_xlabel("X")
        ax.set_ylim3d([-1.0, 1.0])
        ax.set_ylabel("Y")
        ax.set_zlim3d([-1, 1])
        ax.set_zlabel("Z")
        ax.set_title(
            "Entgegengesetzte Richtung von Schwerkraft im verhältnis zum Sensor"
        )
        for df in self.df_list:
            u = df["x_acc"].mean()
            v = df["y_acc"].mean()
            w = df["z_acc"].mean()
            ax.quiver(0, 0, 0, u, v, w, length=1, normalize=True, color="b", alpha=0.5)
        ax.view_init(None, angle)
        fig.savefig("protokoll/images/" + "quiver_acceleration.pdf")

        fig = plt.figure(figsize=(20, 10))
        ax = fig.gca(projection="3d")
        ax.set_xlim3d([-1.0, 1.0])
        ax.set_xlabel("X")
        ax.set_ylim3d([-1.0, 1.0])
        ax.set_ylabel("Y")
        ax.set_zlim3d([-1, 1])
        ax.set_zlabel("Z")
        ax.set_title("Richtung von Magnetfeld im verhältnis zum Sensor")
        for df in self.df_list:
            u = df["x_mag"].mean()
            v = df["y_mag"].mean()
            w = df["z_mag"].mean()
            ax.quiver(0, 0, 0, u, v, w, length=1, normalize=True, color="r", alpha=0.5)
        ax.view_init(None, angle)
        fig.savefig("protokoll/images/" + "quiver_magnetic.pdf")

        fig = plt.figure(figsize=(20, 10))
        ax = fig.gca(projection="3d")
        ax.set_xlim3d([-1.0, 1.0])
        ax.set_xlabel("X")
        ax.set_ylim3d([-1.0, 1.0])
        ax.set_ylabel("Y")
        ax.set_zlim3d([-1, 1])
        ax.set_zlabel("Z")
        ax.set_title("Magnetfeld und umgekehrte Beschleunigung")
        for df in self.df_list:
            u = df["x_acc"].mean()
            v = df["y_acc"].mean()
            w = df["z_acc"].mean()
            ax.quiver(0, 0, 0, u, v, w, length=1, normalize=True, color="b", alpha=0.5)
            u = df["x_mag"].mean()
            v = df["y_mag"].mean()
            w = df["z_mag"].mean()
            ax.quiver(0, 0, 0, u, v, w, length=1, normalize=True, color="r", alpha=0.5)
        ax.view_init(None, angle)
        fig.savefig("protokoll/images/" + "quiver_both.pdf")

    def amplitude_acc(self, angle=45):
        phi = np.array([df["phi_acc"].mean() for df in self.df_list])
        theta = np.array([df["theta_acc"].mean() for df in self.df_list])
        acc = np.array([df["acc"].mean() for df in self.df_list])
        fig = plt.figure(figsize=(20, 10))
        ax = fig.gca(projection="3d")
        ax.set_xlim3d([-180.0, 180.0])
        ax.set_xlabel("Phi")
        ax.set_ylim3d([-90.0, 90.0])
        ax.set_ylabel("Theta")
        ax.set_zlim3d([0.97, 1.03])
        ax.set_zlabel("Z")
        ax.set_title("")
        ax.scatter(phi, theta, acc)
        ax.view_init(None, angle)
        fig.savefig("protokoll/images/" + "Amplitude_2D_acceleration.pdf")

    def amplitude_mag(self, angle=45):
        phi = np.array([df["phi_mag"].mean() for df in self.df_list])
        theta = np.array([df["theta_mag"].mean() for df in self.df_list])
        mag = np.array([df["mag"].mean() for df in self.df_list])
        fig = plt.figure(figsize=(20, 10))
        ax = fig.gca(projection="3d")
        ax.set_xlim3d([-180.0, 180.0])
        ax.set_xlabel("Phi")
        ax.set_ylim3d([-90.0, 90.0])
        ax.set_ylabel("Theta")
        # ax.set_zlim3d([0.97, 1.03])
        ax.set_zlabel("Z")
        ax.set_title("")
        ax.scatter(phi, theta, mag)
        ax.view_init(None, angle)
        fig.savefig("protokoll/images/" + "Amplitude_2D_magnet_.pdf")


x = fields(range(68))
# x = fields(range())
# x = fields([7])

##
