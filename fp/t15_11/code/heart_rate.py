# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 17:25:22 2021

@author: 394493: Alexander Kohlgraf

This code should find out the heart rate through a given set of time and voltage Values
and return it
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import os
from scipy.interpolate import UnivariateSpline

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class heart_rate:
    def __init__(self, measurement):
        self.measurement = "heart_rate_measurement_" + str(measurement)
        self.df = pd.read_csv(
            r"data\6_heartrate\heart_rate_raw_test" + str(measurement) + ".txt",
            delimiter=", ",
            engine="python",
        )
        # micros to seconds
        self.df["time"] = self.df["time"] * 10 ** (-6)
        # 5 volt max, 10 bit adc
        self.df["voltage"] = self.df["raw"] * 5 / 2 ** 10
        # relative intensity
        self.df["intensity"] = self.df["raw"] / 2 ** 10
        self.time_step = self.df["time"][1] - self.df["time"][0]
        self.sample_rate = 1 / self.time_step

        self.plot_raw()
        self.fft()

    def plot_raw(self):
        plt.figure(figsize=(20, 5))
        plt.grid(True)
        plt.ylabel("intensity")
        plt.xlabel("time [s]")
        plt.title("Rohdaten der " + str(self.measurement) + " Messung")
        plt.plot(self.df["time"], self.df["intensity"])
        plt.savefig(
            r"protokoll/images/heart_rate_raw_" + str(self.measurement) + ".pdf"
        )

    def fft(self):
        x = np.array(
            self.df["intensity"][(self.df["time"] < 100) & (self.df["time"] > 1)]
        )
        x = x - x.mean()
        x = np.append(x, np.zeros(1000))
        X = fftpack.fft(x)
        freqs = fftpack.fftfreq(len(x)) * self.sample_rate

        fig, ax = plt.subplots(figsize=(15, 8))

        ax.stem(freqs * 60, np.abs(X))
        ax.set_xlabel("Frequency [BPM]")
        ax.set_ylabel("Magnitude")
        ax.set_yscale("log")
        ax.set_xlim(0, 180)
        ax.set_ylim(0.05, 100)
        ax.set_title(self.measurement + " Fast Fourier Transformation")
        fig.savefig(
            r"protokoll/images/heart_rate_fft_" + str(self.measurement) + ".pdf"
        )


class heart_rate_home:
    def __init__(self, measurement):
        self.measurement = "heart_rate_measurement_" + str(measurement)
        self.df = pd.read_csv(
            r"data\6_heartrate\heart_rate_raw_" + str(measurement) + ".txt",
            delimiter=", ",
            engine="python",
        )
        # micros to seconds
        self.df["time"] = self.df["time"] * 10 ** (-6)
        # 5 volt max, 10 bit adc
        self.df["voltage"] = self.df["raw"] * 5 / 2 ** 10
        # relative intensity
        self.df["intensity"] = self.df["raw"] / 2 ** 10
        self.time_step = self.df["time"][1] - self.df["time"][0]
        self.sample_rate = 1 / self.time_step

        self.interpolate()
        self.plot_raw()
        self.fft()

    def interpolate(self, factor=0.0006):
        spl = UnivariateSpline(self.df["time"], self.df["intensity"])
        spl.set_smoothing_factor(factor)
        new_time = pd.DataFrame(
            np.linspace(
                min(self.df["time"]),
                max(self.df["time"]),
                1 * len(self.df["time"]),
                endpoint=True,
            )
        )
        self.df["time_smooth"] = new_time
        self.df["intensity_smooth"] = spl(self.df["time_smooth"])

    def plot_raw(self):
        plt.figure(figsize=(20, 5))
        plt.grid(True)
        plt.ylabel("intensity")
        plt.xlabel("time [s]")
        plt.title("Rohdaten der " + str(self.measurement) + " Messung")
        plt.plot(self.df["time"], self.df["intensity"], label="Rohdaten", alpha=0.5)
        plt.plot(
            self.df["time_smooth"],
            self.df["intensity_smooth"],
            label="Gesmoothte Interpolierte Daten",
            alpha=0.5,
        )
        plt.legend()
        plt.savefig(
            r"protokoll/images/heart_rate_raw_" + str(self.measurement) + ".pdf"
        )

    def fft(self):
        x = np.array(
            self.df["intensity_smooth"][
                (self.df["time_smooth"] < 100) & (self.df["time_smooth"] > 1)
            ]
        )
        x = x - x.mean()
        x = np.append(x, np.zeros(1000))
        X = fftpack.fft(x)
        freqs = fftpack.fftfreq(len(x)) * self.sample_rate

        plt.subplots(figsize=(15, 8))

        plt.stem(freqs * 60, np.abs(X))
        plt.xlabel("Frequency [BPM]")
        plt.ylabel("Magnitude")
        plt.yscale("log")
        plt.xlim(0, 180)
        plt.ylim(0.05, 100)
        plt.title(self.measurement + " Fast Fourier Transformation")
        plt.savefig(
            r"protokoll/images/heart_rate_fft_"
            + str(self.measurement)
            + "_interpolation.pdf"
        )


##

##

##
