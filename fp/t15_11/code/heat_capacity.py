import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import os
from scipy.interpolate import UnivariateSpline
import matplotlib.dates as mdates
from datetime import datetime

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class Heat_capacity:
    def __init__(self, measurement):
        self.measurement = str(measurement)
        self.data = pd.read_csv(
            r"data/4_heat_capacity/data/" + str(measurement) + ".csv",
            delimiter=", ",
            engine="python",
        )
        self.data["time"] = pd.to_datetime(self.data["time"], format="%H:%M:%S")

    def check_import(self):
        print(self.data.head())

    def plot_raw(self):
        plt.figure(figsize=(10, 8))
        plt.scatter(self.data["time"], self.data["temp"])

        plt.gcf().autofmt_xdate()
        myFmt = mdates.DateFormatter("%H:%M:%S")
        plt.gca().xaxis.set_major_formatter(myFmt)
        plt.grid(True)
        plt.ylabel("Temperatur [°C]")
        plt.xlabel("Uhrzeit [h:min:s]")
        plt.title("Temperatur gegen Zeit")
        plt.savefig("protokoll/images/heat_capacity/heat_capacity.pdf")


if __name__ == "__main__":
    heat_capacity = Heat_capacity("temperatures")
    heat_capacity.plot_raw()