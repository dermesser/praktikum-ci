# -*- coding: utf-8 -*-
"""
Created on Sun Mar 28 12:53:21 2021

@author: 394493: Alexander Kohlgraf
For one, this code should take in the kinematics data, output it as a diagram, as well as the speed and acceleration
for seconds, it should find the speed of sound as a callibration for the first
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import os
import nice_plots
import latextable

from scipy.interpolate import UnivariateSpline

# from scipy.interpolate import interp1d
from scipy.misc import derivative

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


distance_error_geodreieck = 0.15  # in cm
distance_error_zollstock = 0.5  # in cm


class speed_of_sound:
    def speed_fit(self, param, t):
        return param[0] * t / 2 + param[1]

    def __init__(self, distances, place):
        self.df = pd.DataFrame()
        self.distances = distances
        self.place = place
        for distance in distances:
            tmp_df = pd.read_csv(
                r"data/2_kinematics/"
                + place
                + "/speed_of_sound_"
                + str(distance)
                + "_cm.txt",
                delimiter=", ",
                engine="python",
            )
            self.df["duration_" + str(distance)] = tmp_df["duration"]
        self.plot_raw()
        self.calc_speed()

    def calc_speed(self):
        tmp_df_all = pd.DataFrame(
            columns=["duration", "duration_error", "distance", "distance_error"]
        )
        for column, distance in zip(self.df, self.distances):
            tmp_df = pd.DataFrame(
                np.array(
                    [
                        (self.df[column] * 10 ** (-6)),
                        (self.df[column] * 0 + self.df[column].std() * 10 ** (-6)),
                        (self.df[column] * 0 + distance * 10 ** (-2)),
                        (
                            self.df[column] * 0
                            + (
                                distance_error_geodreieck
                                if distance != 100
                                else distance_error_zollstock
                            )
                            * 10 ** (-2)
                        ),
                    ]
                ).T,
                columns=["duration", "duration_error", "distance", "distance_error"],
                index=None,
            )

            tmp_df_all = pd.concat([tmp_df_all, tmp_df], ignore_index=True).dropna()
        self.df_all_SI = tmp_df_all

        reg = nice_plots.nice_regression_plot_odr(
            self.speed_fit,
            x=np.array(self.df_all_SI["duration"]),
            y=np.array(self.df_all_SI["distance"]),
            xerror=np.array(self.df_all_SI["duration_error"]),
            yerror=np.array(self.df_all_SI["distance_error"]),
            figsize=(20, 10),
            xlabel="Tau [s]",
            ylabel="distance [m]",
            ylabelresidue="Residuum",
            title="Anpassung der Schallgeschwindigkeit " + self.place,
            save="protokoll/images/"
            + "Schallgeschwindigkeit_"
            + self.place
            + "_fit.pdf",
            colors=("red", "green"),
            beta0=[300, 0],
        )
        latextable.simple_draw(
            save="/../protokoll/tables/"
            + "schallgeschwindigkeit_angepasst_"
            + self.place
            + "_fit.tex",
            content=[["a", reg[0][0], reg[1][0][0]], ["b", reg[0][1], reg[1][1][1]]],
            header=["Fit Parameter", "Wert [m/s]", "Fehler [m/s]"],
            caption="Fit Parameter für die Schallgeschwindigkeit " + self.place,
            label="schallgeschwindigkeit_fit_angepasst_" + self.place,
            precision=3,
            small=False,
        )
        print(reg)
        self.speed_of_sound = reg[0][0]
        self.speed_of_sound_error = reg[1][0][0]

    def plot_raw(self):
        plt.figure(figsize=(20, 10))
        plt.grid(True)
        plt.ylabel("count")
        plt.xlabel("Tau [μs]")
        plt.title("Rohdaten der Schallgeschwindigkeitsmessung " + self.place)
        for x in self.distances:
            plt.hist(
                self.df["duration_" + str(x)],
                bins=5,
                label="duration_" + str(x) + "_cm",
                alpha=0.5,
            )
        plt.legend()
        # plt.ylim(0, 600)
        plt.savefig(r"protokoll/images/raw_speed_of_sound_" + self.place + ".pdf")


class kinematics:
    def __init__(self, speed_of_sound, speed_of_sound_error):
        self.df = pd.read_csv(
            r"data/2_kinematics/" + "kinematics" + "/kinematics" + ".txt",
            delimiter=", ",
            engine="python",
        )
        self.df["time"] = self.df["time"] * 10 ** (-6)
        self.df["duration"] = self.df["duration"] * 10 ** (-6)
        self.df["distance"] = self.df["duration"] * speed_of_sound / 2
        self.df["distance_error"] = np.sqrt(
            (self.df["duration"].std() * speed_of_sound_error / 2) ** 2
            + (self.df["duration"] * speed_of_sound_error / 2) ** 2
        )

        # clean errors
        self.df = self.df[self.df["distance"] < 2.2]

    def interpolate(self, factor=0.5):
        spl = UnivariateSpline(self.df["time"], self.df["distance"])
        spl.set_smoothing_factor(factor)
        newX = np.linspace(
            min(self.df["time"]), max(self.df["time"]), 500, endpoint=True
        )
        # dx = (max(self.df["time"]) - min(self.df["time"])) / 500
        plt.figure(figsize=(20, 10))
        plt.grid(True)
        plt.plot(self.df["time"], self.df["distance"], label="Rohdaten für Abstand")
        plt.plot(newX, spl(newX), label="Gesmoothter Interpolierter Abstand")
        plt.plot(
            newX,
            derivative(spl, newX, n=1),
            label="Gesmoothte Interpolierte Geschwindigkeit",
        )
        plt.plot(
            newX,
            derivative(spl, newX, n=2),
            label="Gesmoothte Interpolierte Beschleunigung",
        )
        plt.hlines(0, min(self.df["time"]), max(self.df["time"]), colors="black")
        plt.title("Kinematik Plot")
        plt.ylabel("Abstand / Geschwindigkeit / Beschleunigung [m] / [m/s] / [m/s^2]")
        plt.xlabel("Zeit [s]")
        plt.legend()
        plt.savefig(r"protokoll/images/kinematics_interpolation.pdf")

    def plot(self):
        pass


inside = speed_of_sound([5, 10, 20, 100], "inside")
outside = speed_of_sound([5, 10, 20, 100], "outside")

x = kinematics(inside.speed_of_sound, inside.speed_of_sound_error)
