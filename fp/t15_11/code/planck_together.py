import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
import nice_plots
import scipy
import os
from scipy.interpolate import UnivariateSpline
import matplotlib.dates as mdates
from datetime import datetime
import planck_one

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

wavelengths = (
    np.array(
        [
            687,
            # 940,
            568,
            498,
            687,
            479,
            702,
            722,
            # 413,
            431,
        ]
    )
    * 1e-9
)
wavelength_nm = np.array(
    [
        687,
        940,
        568,
        498,
        687,
        479,
        702,
        722,
        413,
        431,
    ]
)


def lin_model(wave, A, B):
    return A * wave + B


class Planck_constant:
    def __init__(self):
        self.u_i = []
        self.parameter = []
        for i in range(10):
            self.u_i.append(planck_one.U_I_Diagramm(i + 1))
            # self.u_i[-1].plot_fit_without_xerror_data()
            # parameter, _, _, _, _ = self.u_i[-1].get_variables()
            # self.parameter.append(parameter[1])
            if i == 8 or i == 1:
                # self.parameter.append(0)
                continue
            self.parameter.append(self.u_i[-1].get_activation_voltage().to_numpy()[0])
        self.parameter = np.array(self.parameter)

    def plot_all(self):
        plt.figure(figsize=(12, 8))
        plt.scatter(scipy.constants.c / wavelengths, self.parameter)
        x_axis = np.linspace(3e14, 7.4e14, 1000)
        plt.plot(x_axis, scipy.constants.h / scipy.constants.e * x_axis)
        plt.grid(True)
        plt.savefig("protokoll/images/led/planck_constant.pdf")

    def fit_plot(self):
        self.y_error = [0.01 / np.sqrt(3)] * 8
        (self.popt, self.pcov, self.chiq, self.ndof,) = nice_plots.nice_regression_plot(
            lin_model,
            scipy.constants.c / wavelengths,
            self.parameter,
            self.y_error,
            xlabel=r"frequency $\nu_0$ [Hz]",
            ylabel="Energy [eV]",
            title="Linfit for planck constant",
            save="protokoll/images/led/planck_regression.pdf",
        )
        print(self.popt, np.sqrt(np.diag(self.pcov)))

    def plot_all_together(self):
        plt.figure(figsize=(12, 10))
        for i, data in enumerate(self.u_i):
            plt.errorbar(
                data.data["voltage_on_led"],
                data.data["voltage_on_resistor"] / data.resistor_ohm,
                xerr=data.digital_error,
                yerr=data.data["current_error"],
                label="{} nm".format(wavelength_nm[i]),
                alpha=0.8,
            )
        plt.grid(True)
        plt.legend()
        plt.xlabel("Voltage on LED [V]")
        plt.ylabel("Current through LED [A]")
        plt.title("I-V for all wavelengths")
        plt.savefig("protokoll/images/led/led_all.pdf")


if __name__ == "__main__":
    constant = Planck_constant()
    constant.fit_plot()