"""
Created on Fri Mar 26.03.2021

@author: 394386: Maximilian Bartel
let's the led blink and returns value
"""

import serial
import time

ser = serial.Serial("/dev/tty.usbmodem1401", 9600)

outName = input("textfile name: ") + ".csv"
outFile = open(outName, "w")
outFile.write("time, duration, state \n")

time.sleep(4.0)
currentAddedTime = 0
while True:
    ser.write(bytes("1", "utf-8"))
    time.sleep(4.0)

    ser.write(bytes("0", "utf-8"))
    time.sleep(2.0)
    while ser.in_waiting:
        line = ser.readline().decode("utf-8")
        systemtime, duration, state = line.split(",")
        outFile.write(line)
        currentAddedTime += float(duration)
    print(currentAddedTime)
    if currentAddedTime > 60:
        break

ser.close()
outFile.close()