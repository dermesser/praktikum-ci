"""
Created on Mon Mar 26 14:14:29 2021

@author: 394493: Alexander Kohlgraf

This code saves data
"""

import serial as serial
import time


def kinematics():
    ser = serial.Serial("COM5", 9600)
    print(ser.name)
    outName = "test" + ".txt"
    outFile = open(outName, "w")
    outFile.write("duration, time, distance_guess \n")
    counter = 200
    while True:
        line = str(ser.readline())
        print(line)
        line = line.replace("b'", "")
        line = line.replace(r"\r\n'", "")

        list_of_data = str(line).split(", ")
        if len(list_of_data) == 2:

            duration = list_of_data[0]
            time = list_of_data[1]
            distance = float(duration) / 58

            outFile.write(
                str(duration) + ", " + str(time) + ", " + str(distance) + "\n"
            )
            print(str(duration) + ", " + str(time) + ", " + str(distance) + "\n")

            counter = counter - 1
            print(counter)
            if counter <= 0:
                ser.close()
                outFile.close()
                break


def speed_of_sound(distance_in_cm):
    ser = serial.Serial("COM5", 9600)
    print(ser.name)
    outName = "speed_of_sound_" + str(distance_in_cm) + "_cm.txt"
    outFile = open(outName, "w")
    outFile.write("duration, time, speed \n")

    counter = 1000
    while True:
        line = str(ser.readline())
        print(line)
        line = line.replace("b'", "")
        line = line.replace(r"\r\n'", "")

        list_of_data = str(line).split(", ")
        if len(list_of_data) == 2:

            duration = list_of_data[0]
            time = list_of_data[1]
            speed = 10 ** 4 * float(distance_in_cm) / (float(duration) / 2)

            outFile.write(str(duration) + ", " + str(time) + ", " + str(speed) + "\n")
            print(str(duration) + ", " + str(time) + ", " + str(speed) + "\n")

            counter = counter - 1
            print(counter)
            if counter <= 0:
                ser.close()
                outFile.close()
                break
