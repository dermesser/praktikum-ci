"""
Created on Mon Mar 26 14:14:29 2021

@author: 394493: Alexander Kohlgraf

This code saves data
"""

import serial as serial
import time


def rotation(name):
    ser = serial.Serial("COM6", 9600)
    print(ser.name)
    outName = "rotation" + str(name) + ".txt"
    outFile = open(outName, "w")
    outFile.write("x_acc, y_acc, z_acc, x_mag, y_mag, z_mag \n")

    counter = 200
    while True:
        try:
            line = ser.readline().decode("UTF-8")
            print(line)
        
            list_of_data = str(line).split(", ")
            
            if len(list_of_data) == 6:
                # x = float(list_of_data[0])
                # y = float(list_of_data[1])
                # z = float(list_of_data[2])
                # x1 = float(list_of_data[3])
                # y1 = float(list_of_data[4])
                # z1 = float(list_of_data[5])
    
                outFile.write(line)
                print(line)
    
                counter = counter - 1
                print(counter)
                if counter <= 0:
                    ser.close()
                    outFile.close()
                    break
        except:
            print("conversion error with UTF-8")
        
def acceleration(name):
    ser = serial.Serial("COM6", 9600)
    print(ser.name)
    outName = "acceleration" + str(name) + ".txt"
    outFile = open(outName, "w")
    outFile.write("x_acc, y_acc, z_acc, time\n")

    counter = 600
    while True:
        try:
            line = ser.readline().decode("UTF-8")
            print(line)
        
            list_of_data = str(line).split(", ")
            
            if len(list_of_data) == 4:
                # x = float(list_of_data[0])
                # y = float(list_of_data[1])
                # z = float(list_of_data[2])
                # x1 = float(list_of_data[3])
                # y1 = float(list_of_data[4])
                # z1 = float(list_of_data[5])
    
                outFile.write(line)
                print(line)
    
                counter = counter - 1
                print(counter)
                if counter <= 0:
                    ser.close()
                    outFile.close()
                    break
        except:
            print("conversion error with UTF-8")
        
