/*
   Author: Alexander Kohlgraf

   Date: 26.03.2021

   This code sends magentic field and acceleration. Changed it to send timestamp instead

*/
#include <Arduino_LSM9DS1.h>

float x, y, z, x1, y1, z1;

void setup() {
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Started");

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (1);
  }
  Serial.print("Accelerometer sample rate = ");
  Serial.print(IMU.accelerationSampleRate());
  Serial.print("Magnetic field sample rate = Hz ");
  Serial.print(IMU.magneticFieldSampleRate());

  Serial.print("Gyroscope sample rate = ");
  Serial.print(IMU.magneticFieldSampleRate());
  Serial.println("Acceleration in G's");
  Serial.println("X\tY\tZ");
  Serial.println("Magnetic Field in uT");
  Serial.println("X\tY\tZ");
}

void loop() {
  if (IMU.accelerationAvailable() && IMU.magneticFieldAvailable()) {
    IMU.readAcceleration(x, y, z);
    //IMU.readMagneticField(x1, y1, z1);
    Serial.println(String(x) + ", " + String(y) + ", " + String(z) + ", " + String(micros()));
  }
}
