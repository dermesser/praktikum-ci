/*
   Author: Alexander Kohlgraf

   Date: 25.03.2021

   This Code sends the time difference recorded by an ultrasonic
   sensor to the serial

*/

#define TXpin 12
#define RXpin 13

const int SENSOR_MAX_RANGE = 300; // in cm
unsigned long duration;

void setup() {
  Serial.begin(9600);
  pinMode(TXpin, OUTPUT);
  pinMode(RXpin, INPUT);
}

void loop() {
  digitalWrite(TXpin, LOW);
  delayMicroseconds(2);

  digitalWrite(TXpin, HIGH);
  delayMicroseconds(10);

  duration = pulseIn(RXpin, HIGH);

  Serial.println(str(duration) + ", " + str(micros()));

  delay(1000);
}
