"""
Created on Fri Mar 26.03.2021

@author: 394386: Maximilian Bartel
gets the temperature adds time and saves it
"""

import serial
import time

ser = serial.Serial("/dev/tty.usbmodem1401", 9600)

outName = input("textfile name: ") + ".csv"
outFile = open(outName, "w")
outFile.write("time, temp \n")

time.sleep(4.0)
tic = time.perf_counter()
while True:
    while ser.in_waiting:
        line = ser.readline().decode("utf-8")
        toc = time.perf_counter()
        t = time.localtime()
        current_time = time.strftime("%H:%M:%S", t)
        outFile.write(current_time + ", " + line)
        print(current_time + " " + line)
        print("{:.3f} seconds running".format(toc - tic))
    if toc - tic > 60 * 60:
        break

ser.close()
outFile.close()