/*
   Author: Alexander Kohlgraf

   Date: 25.03.2021

   This Code sends the heart rate raw data
*/
 

int rawValue;

  
////////////////////////////////////////////////////////////////////////
// Arduino main code
////////////////////////////////////////////////////////////////////////
int analogPin=A0;
  
void setup()
{
  // Serielle Ausgabe Initialisierung
  Serial.begin(9600);
  //pinMode(IRSensorPin, INPUT);
  //Serial.println("Heartbeat Detektion Beispielcode.");
}
  
const int delayMsec = 60; // 100msec per sample

  
// Das Hauptprogramm hat zwei Aufgaben: 
// - Wird ein Herzschlag erkannt, so blinkt die LED kurz aufgesetzt
// - Der Puls wird errechnet und auf der serriellen Ausgabe ausgegeben.
  
void loop()
{
  rawValue = analogRead(analogPin);
  Serial.println(String(rawValue) + ", " + String(micros()));
  //Serial.println(String(rawValue));

  delay(delayMsec);
}
