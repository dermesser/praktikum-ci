\clearpage
\section{Grundlegende Kinematik und Schallgeschwindigkeit}
\label{sec:kinematics}

\subsection{Versuchziel}
In diesem Mini-Versuch wollen wir zum einen die Schallgeschwindigkeit im Raum des Versuches und draußen bestimmen. Zum Anderen möchten wir grundlegende Kinematik näher bringen mit einem Diagramm von Ort, Geschwindigkeit und Beschleunigung gegen die Zeit.

\subsection{Theorie}
Die Schallgeschwindigkeit setzt sich aus der Strecke zusammen die der Schall in einer bestimmten Zeit zurücklegt. Sie ist primär von der kinetischen Energie der Teilchen abhängig und somit von der Temperatur ($v\propto \sqrt{T}$). Sie lässt sich durch eine Laufzeitmessung bei fester Strecke und Temperatur bestimmen:
\begin{equation}
    v_s = \frac{s}{t} = \frac{2\cdot s}{\tau}
\end{equation}
Dabei ist $s$ die feste Strecke und $t$ die Zeit die der Schall für die Strecke braucht. $\tau$ ist die tatsächlich gemessene Zeit, welche doppelt so groß ist wie die Laufzeit, da der Schall hin und zurück muss.\\

Für den rein kinematischen Teil dieses Versuches, ist es wichtig den Zusammenhang zwischen Ort, Geschwindigkeit und Zeit zu kennen. Dieser lautet:
\begin{align}
    x(t) &= x(t)\\
    v(t) &= \frac{dx(t)}{dt}\\
    a(t) &= \frac{d^2x(t)}{dt^2}
\end{align}

Dabei ist $x$ der Ort, $v$ die Geschwindigkeit des bewegten Objektes und $a$ die Beschleunigung.

\subsection{Aufbau}
Für die Aufnahme der Daten wurden ein sr-hc04 Ultraschallsensor sowie ein Arduino Uno verwendet. Der Aufbau findet sich in \autoref{fig:aufbau_kinematik}.\\

Der verwendete Arduino Code sendete die Zeitdifferenz von Start des Schallpulses, bis zur Rückkehr aus, sowie die totale Laufzeit des Microcontrollers. Für schnelle Tests gab er auch eine Schätzung der Entfernung aus, welche für die Auswertung jedoch nicht verwendet wurde.\\

Es wurde zudem mithilfe eines Geodreiecks eine Entfernungsreferenz aufgezeichnet. Für die Entfernung von \SI{1}{\meter} wurde ein Zollstock verwendet.\\

Für die Messung der Bewegung wurde der Sensor in den leeren Raum gerichtet. 

\begin{figure}
    \centering
    \includegraphics[width = \linewidth]{images/3_kinematics/aufbau_schall_innen_5_10_20.jpg}
    \caption{Aufbau für beide Kinematik Teilversuche}
    \label{fig:aufbau_kinematik}
\end{figure}

\subsection{Durchführung}
Zuerst wurde die Bewegungskurve aufgenommen. Der Sensor wurde in den Raum gerichtet und eine Person lief auf ihn zu und wieder zurück. Dies wurde mit höherer Geschwindigkeit wiederholt.\\

Die Schallgeschwindigkeit (welche zur Kalibrierung der ersten Messung verwendet wurde), wurde danach gemessen. Es wurde im Abstand von \SIlist{5;10;20;100}{\centi\meter} gemessen. Dies wurde sowohl im Versuchsraum als auch draußen gemacht.\\

Die Messungen draußen sollten ein anderes Ergebnis aufgrund der abweichenden Temperatur erzielen. Leider war die Messung durch starken Wind geplagt der alle Versuchsmaterialien bewegte.

\subsection{Analyse}
Für die Analyse werden wir die zwei Teilversuche Separat behandeln.

\subsubsection{Schallgeschwindigkeit}
Jede Messung bei fester Entfernung bestand aus 1000 Messungen. Jede dieser Messungen wurde als einzelner Punkt in der linearen Anpassung verwendet. Es gab fehlerhafte Datenpunkte in denen der erste Wert der seriellen Verbindung drastisch erhöht war, diese wurden manuell entfernt.\\
Die Fehler auf die Entfernung ergaben sich aus dem Ablesefehler von \SI{0.15}{\centi\meter} beim Geodreieck und \SI{0.5}{\centi\meter} beim Zollstock. Da der Zollstock sich durch bog haben wir einen Fehler größer als den Ablesefehler angenommen.

Der Fehler auf die Zeit wurde als Standardabweichung der jeweiligen Entfernung bestimmt und für alle aus dieser Entfernung verwendet. Dessen Größenordnung war größer als die Digitalisierung des Arduino.

Mithilfe von \autoref{eq:speed_fit} ergibt sich \autoref{fig:inside_speed_fit} und \autoref{fig:outside_speed_fit}. \\
Die Ergebnisse der Regression finden sich in \autoref{tab:schallgeschwindigkeit_fit_angepasst_inside} und \autoref{fig:outside_speed_fit}.\\
Die Geschwindigkeiten sind etwas hoch aber durchaus realistisch. Die Güte der Messung ist wie erwartet draußen schlechter als innen. In beiden Fällen allerdings hinreichend.\\

\begin{equation}
    s = \frac{a}{2} \cdot \tau + b
    \label{eq:speed_fit}
\end{equation}
Hier wird $a$ die Schallgeschwindigkeit sein. $b$ verkörpert einen systematischen Abstand zwischen Sensor und Messabstand (z.B. \SI{5}{\milli\meter} bei jeder Messung durch falsche Abschätzung der Sensorposition). Es spielt jedoch keine Rolle, da nur die Steigung relevant ist. Um dies zu kompensieren wurden Messungen bei verschiedenen Abständen durchgeführt.

\begin{figure}[H]
\begin{subfigure}{1\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{images/3_kinematics/raw_speed_of_sound_inside.pdf}
    \caption{Rohdaten der Messung}
\end{subfigure}
\begin{subfigure}{1\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{images/3_kinematics/Schallgeschwindigkeit_inside_fit.pdf}
    \caption{lineare Anpassung innen nach \autoref{eq:speed_fit}}
    \label{fig:inside_speed_fit}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\begin{subfigure}{1\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{images/3_kinematics/raw_speed_of_sound_outside.pdf}
    \caption{Rohdaten der Messung}
\end{subfigure}
\begin{subfigure}{1\textwidth}
    \centering
    \includegraphics[width=.8\linewidth]{images/3_kinematics/Schallgeschwindigkeit_outside_fit.pdf}
    \caption{lineare Anpassung außen nach \autoref{eq:speed_fit}}
    \label{fig:outside_speed_fit}
\end{subfigure}
\end{figure}

\input{tables/3_kinematics/schallgeschwindigkeit_angepasst_inside_fit}
\input{tables/3_kinematics/schallgeschwindigkeit_angepasst_outside_fit}

Die Residuen sind stets diagonal, da die Entfernung mit der gemessenen Zeit direkt zusammenhängt. Die unterschiedlichen Messungen hingegen sind zwar kompatibel, allerdings insgesamt Unterschiedlich. So entsteht die Diagonale.

\subsubsection{Kinematik}
In dieser Messung wurde eine kontinuierliche Messreihe mit 200 Werten aufgenommen. Da das Ziel eine qualitative Erklärung ist, wird eine Fehlerrechnung hier nicht durchgeführt.\\

Der Datensatz war durch einzelne Datenpunkte in denen die Wand hinter der laufenden Person gemessen wurde, von einigen Fehlern geplagt. Diese wurden durch einen einfachen cutoff von allen Werten über \SI{2.2}{\meter} bereinigt.\\

An den Datensatz wurde dann mithilfe der Scipy Funktion \\ \textbf{scipy.interpolate.UnivariateSpline} eine Kurve angepasst. Diese Anpassung hat zudem den Vorteil, dass sie geglättet werden kann. Somit konnten die Stellen wo die Wand gemessen wurde, interpoliert werden. Dies ist insbesondere bei 16 Sekunden der Fall.\\

Diese Funktion des Ortes, wurde wiederum mit der \textbf{scipy.misc.derivative} Packet abgeleitet. So erhielt man die Funktion von Geschwindigkeit und Beschleunigung. Die resultierenden Kurven finden sich in \autoref{fig:kinematik_interpolation}.\\

\begin{figure}
    \centering
    \includegraphics[width = \linewidth]{images/3_kinematics/kinematics_interpolation.pdf}
    \caption{Kurve des Ortes, der Geschwindigkeit und der Beschleunigung}
    \label{fig:kinematik_interpolation}
\end{figure}

Wie zu erwarten ist in den Stellen des Wendepunktes des Ortes, die Geschwindigkeit maximal, während die Beschleunigung 0 ist. Ebenso ist an den Punkten an dem die Person die Richtung wechselte die Geschwindigkeit 0, während die Beschleunigung maximal ist. Bei schnelleren Richtungswechseln ist die bestimmte Beschleunigung ebenso größer im Ausschlag. 