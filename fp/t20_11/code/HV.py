# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 12:52:33 2021

@author: 394493: Alexander Kohlgraf

Grafische Darstellung der HV PMT Messungen
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

# import nice_plots
# import latextable

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True


class PMT:
    def __init__(self):
        df = pd.read_csv(
            r"data/HV/HV_Messung.txt",
            delimiter="\t",
            engine="python",
        )

        self.df_A = df.drop(["Voltage B", "Events B"], axis=1)
        self.df_B = df.drop(["Voltage A", "Events A"], axis=1)
        self.df_A["Rate A"] = self.df_A["Events A"] / self.df_A["Zeit [s]"]
        self.df_B["Rate B"] = self.df_B["Events B"] / self.df_B["Zeit [s]"]
        self.df_B.sort_values(by="Voltage B", inplace=True, axis=0)
        self.df_A.sort_values(by="Voltage A", inplace=True, axis=0)

    def plot(self):
        plt.figure(figsize=(15, 8))
        plt.yscale("log")
        plt.errorbar(
            self.df_A["Voltage A"],
            self.df_A["Events A"],
            xerr=1 / np.sqrt(3),
            yerr=np.sqrt(self.df_A["Events A"].to_numpy()),
            label="A channel",
            color="b",
            marker="o",
        )
        plt.errorbar(
            self.df_B["Voltage B"],
            self.df_B["Events B"],
            xerr=1 / np.sqrt(3),
            yerr=np.sqrt(self.df_B["Events B"].to_numpy()),
            label="B channel",
            color="r",
            marker="o",
        )
        plt.ylabel("Rate per 5 Minutes")
        plt.xlabel("Voltage of PMT [V]")
        plt.legend()
        plt.grid(True)
        plt.title("Measurement Rate against High VOltage for PMTs")
        plt.savefig("protokoll/images/HV/HV_PMT.pdf")


a = PMT()
a.plot()
