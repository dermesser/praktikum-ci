module gain

import StatsBase
import Statistics
import PyPlot
import ..fit

using ..util

function find_peak_scatter(df; limit=nothing)
    figsize = (6, 3)
    fig, axarr = PyPlot.subplots(2, 1, figsize=figsize, sharex=true, gridspec_kw=Dict("height_ratios" => [5,2]),
                                tight_layout=true)

    dataplot = axarr[1]
    residplot = axarr[2]

    nbins=150
    hist = StatsBase.fit(StatsBase.Histogram, df.Max_Voltage_V_, nbins=nbins)
    xs = collect(hist.edges[1])
    xs = (xs .+ (xs[2]-xs[1])/2)[1:end-1]
    ys = hist.weights
    yerr = sqrt.(ys)

    lo = limit === nothing ? 1 : argmin(abs.(xs .- limit[1]))
    mid = limit === nothing ? 0.235 : (limit[2]+limit[1])/2
    drop_end = limit === nothing ? 1 : (length(xs) - argmin(abs.(xs .- limit[2])))
    gf = fit.fit_partial_gauss(xs[lo:end-drop_end], ys[lo:end-drop_end], ps=[30, 0.04, mid])
    fitys = fit.gauss_model(xs[lo:end-drop_end], gf.param) #[150, 0.14, 0.245])

    residys = ys[lo:end-drop_end] - fitys
    residerr = yerr[lo:end-drop_end]

    chisq_n = length(xs) - 3
    chisq_all = round(sum((ys[lo:end-drop_end] - fitys).^2 / yerr[lo:end-drop_end].^2), digits=1)
    chisq_per = round(chisq_all / chisq_n, digits=1)

    dataplot.errorbar(xs, ys, yerr, fmt=".", label="χ²/ndof = $chisq_all / $chisq_n = $chisq_per", color=:blue, ecolor=:black)
    dataplot.plot(xs[lo:end-drop_end], fitys, "-", label="μ = $(round(gf.param[3], digits=3)), σ = $(round(gf.param[2], digits=3))", color=:red)
    residplot.errorbar(xs[lo:end-drop_end], residys, residerr, fmt=".", color=:blue, ecolor=:black)
    residplot.plot(xs[lo:end-drop_end], zeros(length(xs)-lo-drop_end+1), color=:red)

    dataplot.legend()
    dataplot.set_xlabel("U / V")
    dataplot.set_ylabel("n")
    residplot.set_ylabel("data - model")

    fig
end

function find_peak(df; limit=nothing)
    f = PyPlot.figure(figsize=(5, 3), tight_layout=true)
    p = f.add_subplot(111)
    counts, bins, _ = p.hist(df.Max_Voltage_V_, bins=100);
    width = bins[2]-bins[1]

    lo = limit === nothing ? 3 : argmin(abs.(bins .- limit[1]))
    drop_end = limit === nothing ? 1 : (length(bins) - argmin(abs.(bins .- limit[2])))
    println(size(bins), " ",lo, " ", drop_end)
    xs = (bins .+ width/2)[lo:end-1-drop_end]
    ys = counts[lo:end-drop_end]

    mid = limit === nothing ? 0.235 : (limit[2]+limit[1])/2
    gf = fit.fit_partial_gauss(xs, ys, ps=[30, 0.04, mid])
    fitys = fit.gauss_model(xs, gf.param) #[150, 0.14, 0.245])
    println(gf.param)

    weights = StatsBase.Weights(ys)
    println(Statistics.mean(xs, weights), "  ",
            Statistics.std(xs, weights))

    mu = round(gf.param[3], digits=3)
    sigma = round(gf.param[2], digits=3)
    p.plot(xs, fitys, label="μ = $mu, σ = $sigma")
    p.set_xlabel("U / V")
    p.set_ylabel("n")
    p.set_title("Events by voltage, N = $(size(df)[1])")
    p.legend()
    f
end

end
