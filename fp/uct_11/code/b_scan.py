# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 15:39:49 2021

@author: 394493: Alexander Kohlgraf
"""
import numpy as np

# import pandas as pd
import matplotlib.pyplot as plt
import os
import nice_plots
import latextable

# import helper
# import file_reader
import a_scan

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

global_x = 170  # mm
global_y = 130  # mus

a_scan_1_reg = a_scan.a_scan(1).reg  # in mm/mus
speed_1 = a_scan_1_reg[0][0]
speed_1_e = a_scan_1_reg[1][0][0]
g_x_1 = np.array([25, 31, 41, 61, 77, 91, 105, 120, 132, 150])[::-1]  # in mm
g_y_1 = np.array([7.5, 44, 13, 19, 26, 30, 36, 42, 50, 16]) * speed_1 / 2  # in mm
g_x_e_1 = 5
g_y_e_1 = np.sqrt((speed_1_e * g_y_1 / speed_1) ** 2 + (1.5 * speed_1 / 2) ** 2)

ug_x_1 = np.array([28, 48, 60, 75, 88, 102, 118, 130, 150])  # in mm
ug_y_1 = np.array([14, 50, 42, 38, 30, 25, 19, 13, 46]) * speed_1 / 2  # in mm
ug_x_e_1 = 5
ug_y_e_1 = np.sqrt((speed_1_e * ug_y_1 / speed_1) ** 2 + (1.5 * speed_1 / 2) ** 2)


a_scan_2_reg = a_scan.a_scan(2).reg  # in mm/mus
speed_2 = a_scan_2_reg[0][0]
speed_2_e = a_scan_2_reg[1][0][0]
g_x_2 = np.array([25, 25, 41, 56, 71, 87, 101, 115, 141, 147])[::-1]  # in mm
g_y_2 = np.array([7, 43, 13, 18, 24, 30, 35, 41, 46, 14]) * speed_2 / 2  # in mm
g_x_e_2 = 5
g_y_e_2 = np.sqrt((speed_2_e * g_y_2 / speed_2) ** 2 + (1.5 * speed_2 / 2) ** 2)

ug_x_2 = np.array([35, 50, 65, 80, 94, 109, 123, 140, 155])  # in mm
ug_y_2 = np.array([13, 47, 42, 35, 30, 22, 15, 10, 45]) * speed_2 / 2  # in mm
ug_x_e_2 = 5
ug_y_e_2 = np.sqrt((speed_2_e * ug_y_2 / speed_2) ** 2 + (1.5 * speed_2 / 2) ** 2)


a_scan_4_reg = a_scan.a_scan(4).reg  # in mm/mus
speed_4 = a_scan_4_reg[0][0]
speed_4_e = a_scan_4_reg[1][0][0]
g_x_4 = np.array([26, 27, 41, 56, 72, 88, 102, 120, 135, 148])[::-1]  # in mm
g_y_4 = np.array([6, 42, 12, 18, 24, 29, 35, 40, 46, 8]) * speed_4 / 2  # in mm
g_x_e_4 = 5
g_y_e_4 = np.sqrt((speed_4_e * g_y_2 / speed_4) ** 2 + (1.5 * speed_4 / 2) ** 2)

ug_x_4 = np.array([30, 45, 61, 78, 91, 105, 120, 135])  # in mm
ug_y_4 = np.array([12, 46, 41, 35, 29, 22, 16, 10]) * speed_4 / 2  # in mm
ug_x_e_4 = 5
ug_y_e_4 = np.sqrt((speed_4_e * ug_y_4 / speed_4) ** 2 + (1.5 * speed_4 / 2) ** 2)


av_x_ug = np.average(
    np.array([ug_x_1[0:-1], ug_x_2[0:-1], ug_x_4]).T,
    axis=1,
    weights=[1 / ug_x_e_1 ** 2, 1 / ug_x_e_2 ** 2, 1 / ug_x_e_4 ** 2],
)
av_y_ug = np.average(
    np.array([ug_y_1[0:-1], ug_y_2[0:-1], ug_y_4]).T,
    axis=1,
    weights=np.array(
        [1 / ug_y_e_1[0:-1] ** 2, 1 / ug_y_e_2[0:-1] ** 2, 1 / ug_y_e_4 ** 2]
    ).T,
)
av_x_ug_e = 1 / sum([1 / ug_x_e_1 ** 2, 1 / ug_x_e_2 ** 2, 1 / ug_x_e_4 ** 2])
av_y_ug_e = 1 / sum(
    [1 / ug_y_e_1[0:-1] ** 2, 1 / ug_y_e_2[0:-1] ** 2, 1 / ug_y_e_4 ** 2]
)


av_x_g = np.average(
    np.array([g_x_1, g_x_2, g_x_4]).T,
    axis=1,
    weights=[1 / g_x_e_1 ** 2, 1 / g_x_e_2 ** 2, 1 / g_x_e_4 ** 2],
)
av_y_g = np.average(
    np.array([g_y_1, g_y_2, g_y_4]).T,
    axis=1,
    weights=np.array([1 / g_y_e_1 ** 2, 1 / g_y_e_2 ** 2, 1 / g_y_e_4 ** 2]).T,
)
av_x_g_e = 1 / sum([1 / g_x_e_1 ** 2, 1 / g_x_e_2 ** 2, 1 / g_x_e_4 ** 2])
av_y_g_e = 1 / sum([1 / g_y_e_1 ** 2, 1 / g_y_e_2 ** 2, 1 / g_y_e_4 ** 2])


def plot_g():
    plt.figure(figsize=(15, 8))
    plt.title("Störungskarte Gedreht")
    plt.grid(True)
    plt.errorbar(
        g_x_1, g_y_1, xerr=g_x_e_1, yerr=g_y_e_1, marker="o", fmt="none", label="1 MHz"
    )
    plt.errorbar(
        g_x_2, g_y_2, xerr=g_x_e_2, yerr=g_y_e_2, marker="o", fmt="none", label="2 MHz"
    )
    plt.errorbar(
        g_x_4, g_y_4, xerr=g_x_e_4, yerr=g_y_e_4, marker="o", fmt="none", label="4 MHz"
    )
    plt.legend()
    plt.legend(loc=6, fontsize=20)
    plt.xlabel("Breite [mm]")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/gedreht.pdf")


def plot_av():
    plt.figure(figsize=(15, 8))
    plt.title("Störungskarte im ungedrehten System gewichtetes Mittel")
    plt.grid(True)
    plt.errorbar(
        av_x_g,
        av_y_g,
        xerr=av_x_g_e,
        yerr=av_y_g_e,
        marker="o",
        fmt="none",
        label="gedreht",
    )
    plt.errorbar(
        av_x_ug,
        av_y_ug,
        xerr=av_x_ug_e,
        yerr=av_y_ug_e,
        marker="o",
        fmt="none",
        label="ungedreht",
        color="r",
    )
    plt.errorbar(
        np.average(
            [ug_x_1[-1], ug_x_2[-1]],
            weights=[1 / g_x_e_1 ** 2, 1 / g_x_e_2 ** 2],
        ),
        np.average(
            [ug_y_1[-1], ug_y_2[-1]],
            weights=[1 / g_y_e_1[-1] ** 2, 1 / g_y_e_2[-1] ** 2],
        ),
        xerr=av_x_ug_e,
        yerr=av_y_ug_e[-1],
        marker="o",
        fmt="none",
        color="r",
    )
    plt.legend(loc=6, fontsize=20)
    plt.xlabel("Breite [mm]")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/average.pdf")


def plot_ug():
    plt.figure(figsize=(15, 8))
    plt.title("Störungskarte Ungedreht")
    plt.grid(True)
    plt.errorbar(
        ug_x_1,
        ug_y_1,
        xerr=ug_x_e_1,
        yerr=ug_y_e_1,
        marker="o",
        fmt="none",
        label="1 MHz",
    )
    plt.errorbar(
        ug_x_2,
        ug_y_2,
        xerr=ug_x_e_2,
        yerr=ug_y_e_2,
        marker="o",
        fmt="none",
        label="2 MHz",
    )
    plt.errorbar(
        ug_x_4,
        ug_y_4,
        xerr=ug_x_e_4,
        yerr=ug_y_e_4,
        marker="o",
        fmt="none",
        label="4 MHz",
    )
    plt.legend()
    plt.legend(loc=6, fontsize=20)
    plt.xlabel("Breite [mm]")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/ungedreht.pdf")


def plot_both():
    plt.figure(figsize=(15, 8))
    plt.title("Störungskarte Gesamt im ungedrehten System")
    plt.grid(True)
    plt.errorbar(
        g_x_1,
        g_y_1,
        xerr=g_x_e_1,
        yerr=g_y_e_1,
        marker="o",
        fmt="none",
        label="1 MHz gedreht",
    )
    plt.errorbar(
        g_x_2,
        g_y_2,
        xerr=g_x_e_2,
        yerr=g_y_e_2,
        marker="o",
        fmt="none",
        label="2 MHz gedreht",
    )
    plt.errorbar(
        g_x_4,
        g_y_4,
        xerr=g_x_e_4,
        yerr=g_y_e_4,
        marker="o",
        fmt="none",
        label="4 MHz gedreht",
    )
    plt.errorbar(
        ug_x_1,
        ug_y_1,
        xerr=ug_x_e_1,
        yerr=ug_y_e_1,
        marker="o",
        fmt="none",
        label="1 MHz ungedreht",
    )
    plt.errorbar(
        ug_x_2,
        ug_y_2,
        xerr=ug_x_e_2,
        yerr=ug_y_e_2,
        marker="o",
        fmt="none",
        label="2 MHz ungedreht",
    )
    plt.errorbar(
        ug_x_4,
        ug_y_4,
        xerr=ug_x_e_4,
        yerr=ug_y_e_4,
        marker="o",
        fmt="none",
        label="4 MHz ungedreht",
    )
    plt.legend(loc=6, fontsize=20)
    plt.xlabel("Breite [mm]")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/beide.pdf")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/beide.pdf")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/beide.pdf")
    plt.ylabel("Tiefe [mm]")
    plt.savefig("protokoll/images/b_scan/beide.pdf")
