# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 15:39:55 2021

@author: 394493: Alexander Kohlgraf
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import nice_plots
from numpy import fft
from scipy.signal import hilbert, find_peaks
import progressbar
from skimage.transform import iradon


import helper
import file_reader

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path + "/../")

plt.rcParams["font.size"] = 24.0
plt.rcParams["font.family"] = "sans-serif"
plt.rcParams["font.sans-serif"] = "Arial"
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelsize"] = "medium"
plt.rcParams["axes.labelweight"] = "bold"
plt.rcParams["axes.linewidth"] = 1.2
plt.rcParams["lines.linewidth"] = 2.0
plt.rcParams["figure.autolayout"] = True

cut_low_array = [0, 0, 1e6, 0, 1e6]
threshold_array = [0, 0.00, 0.00, 0, 0.00]


class CT:
    def __init__(
        self, degree_step: int, MHz: int, ram_lak=True, shep_logan=False, velocity=False
    ):
        self.degree_step = degree_step
        self.degrees = np.arange(0, 360, degree_step, dtype=int)
        self.positions = np.arange(
            -45,
            45.5,
            0.5,
            dtype=float,
        )  # pos in mm
        self.MHz = MHz
        self.MHz_name = str(MHz) + "MHz"
        path = (
            "data/ct_scan/"
            + self.MHz_name
            + "/{}.bin".format("duration" if MHz == 4 else "both")
        )
        self.MHz_name = str(MHz) + " MHz"
        self.df = file_reader.read_org_file(path).T

        # self.plot_raw(angle=30, pos=20)
        self.sample = self.df[(0, 0)].index[1] - self.df[(0, 0)].index[0]
        # self.reduce_time_axis(velocity)
        # print("Reduction done")
        # self.filter_ct(ram_lak, shep_logan)
        # print("Filtering done")
        # self.image(velocity)
        # print("Image done")

    def plot_raw(self, angle=0, pos=-50):
        plt.figure(figsize=(10, 7))

        plt.plot(self.df[(angle, pos)], label="Rohdaten")
        sample = self.df[(angle, pos)].index[1] - self.df[(angle, pos)].index[0]
        new_data = self.fourier_filter(self.df[(angle, pos)], sample=sample)
        new_data_einhullende = self.einhullende(new_data)
        peaks = self.peak_finder(new_data_einhullende)
        plt.plot(self.df[(angle, pos)].index, new_data, label="Fourierfilter")
        plt.plot(self.df[(angle, pos)].index, new_data_einhullende, label="Einhüllende")
        plt.plot(self.df[(angle, pos)].index[peaks], new_data_einhullende[peaks], "x")
        plt.grid(True)
        plt.title("Rohdaten für {} MHz {} mm {}°".format(self.MHz, pos, angle))
        plt.ylabel("Schalldruck [$V$]")
        plt.xlabel("Zeit [$\mu s$]")
        plt.legend()
        plt.savefig("plot_of_velocity_one_measurement.pdf")

    def skiimage_radon(self):

        reconstruction_fbp = iradon(
            self.reduced.T, theta=self.degrees, filter_name="hamming"
        )
        plt.figure()
        plt.imshow(reconstruction_fbp)
        plt.show()

    def sinogramm(self):
        plt.figure(figsize=(4, 12))
        a = plt.imshow(self.reduced, extent=[-45, 45, 0, 360])

        plt.xlabel("Position")
        plt.ylabel("Angle")
        plt.title(self.MHz_name)
        plt.savefig("sinogramm_{}mhz.pdf".format(self.MHz))

    def fourier_filter(self, data, sample=1, cut_high=5e6, cut_low=7e5):
        if type(data) is not np.ndarray:
            array = data.to_numpy()
        else:
            array = data
        fourier = fft.rfft(array)
        frequencies = fft.rfftfreq(array.size) / sample * 1e6
        cut_index_high = np.where(np.abs(frequencies) > (self.MHz + 3) * 1e6)
        fourier[cut_index_high] = 0
        cut_index_low = np.where(np.abs(frequencies) < cut_low_array[self.MHz])
        fourier[cut_index_low] = 0
        new_data = fft.irfft(fourier)
        return new_data

    def einhullende(self, data):
        return np.abs(hilbert(data))

    def peak_finder(self, data):
        return find_peaks(data, height=threshold_array[self.MHz])[0]

    def reduce_time_axis(self, velocity):
        self.reduced = np.zeros((len(self.degrees), len(self.positions) + 1))

        data = self.df  # .to_numpy()
        for angle in progressbar.progressbar(range(len(self.degrees))):
            for pos in range(len(self.positions)):
                new_data = self.fourier_filter(
                    data[(self.degrees[angle], self.positions[pos])],
                    sample=self.sample,
                )
                new_data_einhullende = self.einhullende(new_data)
                peaks = self.peak_finder(new_data_einhullende)
                if peaks.size == 0:

                    self.reduced[angle, pos] = 0
                    continue
                if velocity:

                    self.reduced[angle, pos] = peaks[
                        new_data_einhullende[peaks]
                        >= (max(new_data_einhullende[peaks]) / 10)
                    ][0]
                else:
                    self.reduced[angle, pos] = new_data_einhullende[max(peaks)]

            # self.reduced = 1 / (self.df.T.iloc[0::1, 0::1] / 100 + 1)
            # self.reduced = self.reduced.T

    def func_angle(self, angle):
        for pos in self.positions:
            new_data = self.fourier_filter(data[(angle, pos)], sample=self.sample)
            new_data_einhullende = self.einhullende(new_data)
            peaks = self.peak_finder(new_data_einhullende)
            if peaks.size == 0:
                self.reduced[(angle, pos)] = 0
                continue
            self.reduced[(angle, pos)] = peaks[0]

    def filter_ct(self, Ram_Lak=True, Shep_Logan=False, shep_logan_factor=0.1):
        self.ram_lak = Ram_Lak
        self.shep_logan = Shep_Logan
        if Ram_Lak and not Shep_Logan:

            def filter_function(frequencies, fourier):
                return np.abs(frequencies) * fourier

        elif Shep_Logan and not Ram_Lak:

            def filter_function(frequencies, fourier):
                factor = np.maximum(np.pi * frequencies / (2 * shep_logan_factor), 1e-9)
                return fourier * np.sin(factor) / factor

        elif Shep_Logan and Ram_Lak:

            def filter_function(frequencies, fourier):
                factor = np.maximum(np.pi * frequencies / (2 * shep_logan_factor), 1e-9)
                return fourier * np.abs(frequencies) * np.sin(factor) / factor

        else:

            def filter_function(frequencies, fourier):
                return fourier

        self.filtered = np.zeros(self.reduced.shape)
        for angle in self.degrees:
            projections = self.reduced[np.where(self.degrees == angle)[0]]
            fourier = fft.rfft(projections)
            frequencies = fft.rfftfreq(projections.size)
            frequencies[0] = (frequencies[1] - frequencies[0]) / 4
            filtered_frequencies = filter_function(frequencies, fourier)
            filtered_projection = np.real(fft.irfft(filtered_frequencies))

            normalize = True
            if normalize:
                filtered_projection += projections.mean()

            self.filtered[np.where(self.degrees == angle)[0], :] = filtered_projection
            (
                projections,
                fourier,
                frequencies,
                filtered_frequencies,
                filtered_projection,
            ) = (0, 0, 0, 0, 0)

    def image(self, velocity=False):
        dim = self.filtered[0].size
        img = np.zeros((dim, dim))

        norm = 1 / (2 * len(self.degrees))
        for angle in self.degrees:
            img += (
                helper.project(
                    self.filtered[np.where(self.degrees == angle)[0]][0],
                    plot=False,
                    angle=angle,
                )
                * norm
            )
        img = img.flatten()
        if velocity:
            img = 1 / img
        # img = np.log(img * 1e6)
        # img = np.log(abs(img))
        # img[abs(img) > 10] = 0
        img = img.reshape((dim, dim))
        # print(img)
        print("angle projection done")

        # img = imgmap(img)
        fig = plt.figure(figsize=(8, 8), tight_layout=True)
        sub = fig.add_subplot(111)
        image = sub.imshow(img, origin="lower", extent=[-45, 45, -45, 45])
        filters = "{}{}".format(
            "Ram-Lak" if self.ram_lak else "", ", Shep-Logan" if self.shep_logan else ""
        )
        plt.title(
            "{} ".format("Speed of Sound" if velocity else "Absorption")
            + filters
            + " "
            + self.MHz_name
        )
        # fig.colorbar(image, ax=sub, shrink=0.7)
        if self.ram_lak and not self.shep_logan:
            plt.savefig(
                "img_velocity_{}Mhz_{}step_ram_lak.pdf".format(
                    self.MHz, self.degree_step
                )
            )
        elif self.ram_lak and self.shep_logan:
            plt.savefig(
                "img_velocity_{}Mhz_{}step_ram_lak_shep_logan.pdf".format(
                    self.MHz, self.degree_step
                )
            )
        else:
            plt.savefig(
                "img_velocity_{}Mhz_{}step.pdf".format(self.MHz, self.degree_step)
            )
        # plt.show()


array_mhz = [1, 2, 4]
# for i in array_mhz:
#     a = CT(1, i, ram_lak=True, shep_logan=False, velocity=True)
# a.sinogramm()
# b = CT(1, i, ram_lak=True, velocity=True)
# for i in range(-45, 45, 2):
#     a.plot_raw(angle=0, pos=i)
a = CT(1, 2, ram_lak=True, shep_logan=False, velocity=True)
a.plot_raw(0, -20)
# a.skiimage_radon()
