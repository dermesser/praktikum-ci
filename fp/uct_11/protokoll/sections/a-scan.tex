\clearpage
\section{A-Scan}

\subsection{Theorie}
Eine longitudinale Schallwelle ist eine Druckschwingung entlang der Ausbreitungsausrichtung. Diese lässt sich also mit der Wellengleichung beschreiben:
\begin{equation}
    \frac{d^2S}{dt^2} = v^2 \cdot \frac{d^2S}{dx^2}
\end{equation}
Wobei $v$ die Schallgeschwindigkeit von Longitudinalwellen ist und $S$ ist die Schnelle. $t$ und $x$ sind Entsprechend Ort und Zeit.\\

Trifft eine Schallwelle auf ein Medium, so entscheidet die Differenz der Wellenwiderstände $Z$ wie stark die Transmission ist:
\begin{equation}
    I_2 = I_1 \cdot \frac{4Z_1Z_2}{(Z_1+Z_2)^2}
    \label{eq:Wellenwiderstand}
\end{equation}
Wobei $I_2$ die Schallintensität nach dem Auftreffen ist. Somit steigt die Transmission an, je näher die Wellenwiderstände der Medien sind.\\

Die Absorption eines Mediums mit Absorptionskoeffizienten $\alpha$ lautet:
\begin{equation}
    A = A_0e^{-\alpha \cdot s} \approx a \cdot e^{-b\cdot s} + c
    \label{eq:absorption_fit}
\end{equation}
Dabei ist $A_0$ die Anfangsamplitude und $s$ die im Medium zurückgelegte Strecke. $a$, $b$ und $c$ sind entsprechende Anpassungsparameter.\\

Die Schallgeschwindigkeit kann beschrieben werden über das Modell:
\begin{equation}
    x \approx a \cdot t + b
    \label{eq:schallgeschwindigkeit_fit}
\end{equation}
Dabei ist $a$ die Schallgeschwindigkeit, x die zurückgelegte Strecke, t die dafür benötigte Zeit und b die nicht im Medium zurückgelegte Strecke bei angenommen gleicher Schallgeschwindigkeit.


\subsection{Aufbau}
Für die Messung der Schallgeschwindigkeit und des Absorptionskoeffizienten in Acrylglas wurde der selbe Aufbau verwendet. Es wurden drei verschieden lange Acrylglas-Zylinder zwischen einen Ultraschall Transducer und Receiver platziert. \\
Um eine gute Verbindung zu schaffen wurde Ultraschallgel verwendet. Dies sorgt auch für eine geringere Reflektion im Übergangsmaterial von Transducer und Acrylglas. Luft hat einen sehr geringen Wellenwiderstand, somit erschließt sich aus \autoref{eq:Wellenwiderstand}, dass ein Gel mit ähnlichem Wellenwiderstand verwendet wird.\\

\begin{figure}[H]
    \centering
    \begin{subfigure}{.5\textwidth}
    \includegraphics[width=\linewidth]{images/aufbau/A-Scan.jpg}
    \caption{Kurzer Zylinder}
    \end{subfigure}
    
    \begin{subfigure}{.5\textwidth}
    \includegraphics[width=\linewidth]{images/aufbau/A-Scan 2.jpg}
    \caption{langer Zylinder}
    \end{subfigure}
\end{figure}

\subsection{Durchführung}
Es wurde das Transmissionssignal vermessen. Dafür wurden Zeitpunkt des eintreffenden Signals (für Schallgeschwindigkeit) und dessen Intensität (für Absorption) gemessen.\\
Mithilfe der zur Verfügung gestellten Amplituden wurde der höchste Peak jedes Signals sowie dessen Zeitpunkt bestimmt. Ein Beispiel für das gemessene Signal und dessen Amplitude finden sich in \autoref{fig:raw_HF_1MHz} und \autoref{fig:raw_amplitudes_1MHz}. Man sieht ebenfalls die doppelte Reflektion der kürzesten Länge an der Stelle der längsten Länge. Die dreifache Länge des Kürzesten entspricht der des Längsten, somit ist dies zu erwarten gewesen.

\begin{figure}[H]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/1_MHz_HF.pdf}
        \caption{Rohsignal Bei 1MHz}
        \label{fig:raw_HF_1MHz}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/1_MHz_amplitudes.pdf}
        \caption{Amplituden Bei 1MHz}
        \label{fig:raw_amplitudes_1MHz}
    \end{subfigure}
\end{figure}

\subsection{Analyse}

Die Längen der Zylinder wurden mit einer Messschieber-Unsicherheit von \SI{0.05}{\milli\meter} gegeben als:
\begin{center}
    \begin{tabular}{c|c}
    	Zylinder & Länge \\
    	\hline
    	kurz & \SI{40}{\milli\meter}  \\
    	mittel & \SI{79.9}{\milli\meter}  \\
    	lang & \SI{120.1}{\milli\meter}  \\
    \end{tabular}
\end{center}

Auf die Position der Peaks wurde ein Fehler von \SI{0.05}{\micro\second} angenommen, da nachlaufende Peaks die Position verzerren.\\
Mit den bestimmten Positionen der Peaks, wurden die Modelle in \autoref{eq:schallgeschwindigkeit_fit} und \autoref{eq:absorption_fit} verwendet, um Anpassungen durchzuführen.

\begin{figure}[H]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/1_MHz_speed_of_sound_fit.pdf}
        \caption{Schallgeschwindigkeit Anpassung 1 MHz}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/2_MHz_speed_of_sound_fit.pdf}
        \caption{Schallgeschwindigkeit Anpassung 2 MHz}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/4_MHz_speed_of_sound_fit.pdf}
        \caption{Schallgeschwindigkeit Anpassung 4 MHz}
    \end{subfigure}
\end{figure}

Die Güte der Anpassungen ist akzeptabel. Bei der 2 MHz Messung wurde der Fehler etwas Überschätzt. Es lassen sich bei 3 Messpunkten natürlich keine Systematiken erkennen.\\
Die resultierenden Parameter befinden sich in \autoref{tab:schallgeschwindigkeit_fit_1}, \autoref{tab:schallgeschwindigkeit_fit_2} und \autoref{tab:schallgeschwindigkeit_fit_4}. Diese sind in der Einheit \SI{}{\milli\meter\per\micro\second} und \SI{}{\milli\meter} respektiv.\\

\input{tables/ascan/schallgeschwindigkeit_1_fit}
\input{tables/ascan/schallgeschwindigkeit_2_fit}
\input{tables/ascan/schallgeschwindigkeit_4_fit}

Das gewichtete Mittel beträgt hier \SI{2.737}{\milli\meter\per\micro\second} mit einem Fehler von \SI{0.153}{\milli\meter\per\micro\second}. Literaturwerte$^{\cite{plexiglas}}$ betragen \SI{2.670}{\milli\meter\per\micro\second}, was in einem $0.44\sigma$ Intervall liegt.\\

Die Fehler auf $b$ (zurückgelegten Strecke außerhalb des Mediums) sind zu groß um eine sinnvolle Aussage zu treffen. Auch müsste von einer gleichen Schallgeschwindigkeit ausgegangen werden, was natürlich nicht der Fall ist außerhalb des Acrylglases. Die Luftblasen im Gel könnten eine der Fehlerquellen sein. Auch Unreinheiten und Störquellen im Acrylyglas können zu Laufzeitunterschieden, sowie Amplitudenunterschieden führen.\\

Im Rahmen der Unsicherheit sind die Schallgeschwindigkeiten miteinander kompatibel. Wir folgern somit, dass im untersuchten Bereich die Schallgeschwindigkeit nicht von der Frequenz abhängig ist.\\

Die Güte der Anpassung des Absorptionskoeffizienten ist nicht definiert, da man 3 Punkte benötigt um eine Exponentialfunktion zu definieren.

\begin{figure}[H]
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/1_MHz_dampening_fit.pdf}
        \caption{Absorptionskoeffizient Anpassung 1 MHz}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/2_MHz_dampening_fit.pdf}
        \caption{Absorptionskoeffizient Anpassung 2 MHz}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/ascan/4_MHz_dampening_fit.pdf}
        \caption{Absorptionskoeffizient Anpassung 4 MHz}
    \end{subfigure}
\end{figure}

Die Einheit des Absorptionskoeffizienten $b$ ist hier \SI{}{\per\milli\meter}.

\input{tables/ascan/absorption_1_fit}
\input{tables/ascan/absorption_2_fit}
\input{tables/ascan/absorption_4_fit}

Auch wenn hier die Absorptionskoeffizienten im Bereich ihrer Fehler miteinander kompatibel wären, beobachten wir einen eindeutigen Trend, welcher auch in den Grafiken sichtbar ist. Je höher die Frequenz, desto steiler fällt die Exponentialkurve. Somit folgern wir, dass die Absorption im untersuchten Bereich von der Frequenz des Schalls abhängig ist.
