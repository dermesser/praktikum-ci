
mutable struct FrontPanel
    mode::String
    output::Float64
    gain::Float64
    probe1::Float64
    probe2::Float64
end

mutable struct Measure
    selection::Int32
    samplerate::Float64
    begin_::Float64
    end_::Float64
    sound_speed::Float64
    units::Float64
end

mutable struct AScanPar
    fft_filter::Int32
    fft_middle::Float64
    fft_width::Float64
    fft_gate::Int32
    cepstrum_filter::Int32
    cepstrum_middle::Float64
    cepstrum_width::Float64
    cepstrum_gate::Int32
end

const MeasureParams = Dict{String, Dict{String, String}}

mutable struct AScan
    params::MeasureParams

    depth::Vector{Float64}
    hf::Vector{Float64}
    amplitude::Vector{Float64}
end

function parse_ascan_dat(filename, endmarker="Depth")::Tuple{MeasureParams, Array{Float64, 2}}
    # Returns measurement parameters as well as Nx3 array with data points (time, depth, amplitude)
    f = open(filename, "r")
    secs = []

    (START_SEC, PARAMS, DATA) = (1,2,3)
    state = START_SEC

    allparams = Dict{String, Dict{String,String}}()

    data = Array{Float64, 2}

    current_param = ""
    keepline = false
    line = ""

    while !eof(f) && state != DATA
        if !keepline
            line = chomp(readline(f))
        else
            keepline = false
        end

        if state == START_SEC
            if startswith(line, '[')
                # Handle start of section.
                current_param = strip(line, collect("[]"))
                par = Dict{String, String}()
                allparams[current_param] = par
                state = PARAMS
            else
                continue
            end
        elseif state == PARAMS
            if startswith(line, '[')
                state = START_SEC
                keepline = true
                continue
            elseif startswith(line, endmarker)
                state = DATA
                keepline = false
                continue
            end
            if !contains(line, '=')
                continue
            end
            k, v = split(line, '=')
            allparams[current_param][k] = v
        end
    end

    # State: DATA. Read remaining stuff
    all_lines = readlines(f, keep=false)
    data = zeros(Float64, (length(all_lines), 3))
    for (i, line) in enumerate(all_lines)
        cols = split(replace(line, ',' => '.'))
        if length(cols) != 3
            print("Irregular line: ", cols)
            continue
        end
        data[i, :] = [parse(Float64, c) for c = cols]
    end

    return (allparams, data)
end

function parameters_to_title(p::MeasureParams)::String
    fp = p["front_panel"]
    mode = fp["mode"]
    output, gain = fp["output"], fp["gain"]

    return "$mode, o/g = $output/$gain"
end
