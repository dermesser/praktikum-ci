import Measurements
import Plots

using Printf

function fwhm(sigma::Real)::Float64
    # Calculate FWHM from sigma.
    2sqrt(2log(2))*sigma
end

const M64 = Measurements.Measurement{Float64}


mfl = (v,e) -> Measurements.measurement(v, e)
mean = a -> sum(a)/length(a)

function nv(x)
    Measurements.value.(x)
end

function sd(x)
    Measurements.uncertainty.(x)
end

struct LinReg
    m::Measurements.Measurement
    b::Measurements.Measurement
    chisq::Float64
    samplesize::Float64
    chisq_dof::Float64
end

function linreg(xs::Vector{<:Real}, ys::Vector{<:Real}; allow_offset=true)
    N = length(xs)
    if length(ys) != N
        return nothing
    end
    if allow_offset
        m = (mean(xs.*ys)-mean(xs)*mean(ys))/(mean(xs.^2)-mean(xs)^2)
        b = mean(ys)-m*mean(xs)
    else
        m = mean(xs.*ys)/mean(xs.^2)
        b = 0
    end

    chisq = sum( (nv(ys) .- (nv(m) .*nv(xs) .+ nv(b))).^2 /
                (sd(ys).^2 .+ (nv(m) .* sd(xs)).^2) )
    samplesize = length(xs)
    chisq_dof = samplesize-2
    return LinReg(m, b, nv(chisq), samplesize, chisq_dof)
end

function linreg_residual_plot(xs, ys; figsize=(600,350), allow_offset=true, expo=false, kwargs...)
    lr = linreg(xs, ys)
    fitys = nv(lr.m .* xs .+ lr.b)
    resids = nv(ys .- fitys)
    residerrs = sqrt.((sd(ys)).^2 + (nv(lr.m) .* sd(xs)).^2)
    resids = mfl.(resids, residerrs)

    lay = Plots.grid(2, 1, heights=[0.7, 0.3])
    m, b = lr.m, lr.b

    if expo
        fmt = n -> @sprintf("%.2e", n)
    else
        fmt = n -> round(n, digits=3)
    end

    Plots.plot(xs, ys, t=:scatter,
               label="Daten, χ²/ndof = $(fmt(lr.chisq))/$(lr.chisq_dof) = $(fmt(lr.chisq/lr.chisq_dof))";
               kwargs...)
    dataplot = Plots.plot!(xs, fitys, label="mx + b = ($(fmt(nv(m))) ± $(fmt(sd(m)))) x + ($(fmt(nv(b))) ± $(fmt(sd(b))))")

    Plots.plot(nv(xs), resids, legend=false, t=:scatter,
               ylabel="Daten - (mx + b)")
    residplot = Plots.plot!(nv(xs), zeros(size(xs)), color="gray")
    Plots.plot(dataplot, residplot, layout=lay, size=figsize, link=:x)
end
