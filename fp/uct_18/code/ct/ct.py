#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 12:01:49 2021

@author: lbo
"""

import re
import math
import numpy as np
from numpy import fft
import matplotlib.pyplot as plt
import pandas as pd
from scipy import signal

import ctfilereader as ctf
import cthelper as cth

# Files for testing
MHZ1_CT_BIN = '../../data/DatensatzA/ct_scan/1MHz/both.bin'
MHZ2_CT_BIN = '../../data/DatensatzA/ct_scan/2MHz/both.bin'


def drop_data(df, drop=(1,1)):
    """Drops measurements from a pandas series according to the given
    "schedule"."""
    lines, cols = drop
    return df.iloc[::lines, ::cols]

def reduce_time_axis(df):
    """For intensity tomography: Reduce time axis by taking peak value."""
    ampls = df.abs().max(axis=1)
    proj = np.log(ampls.max()/ampls)
    return proj
    # Alternatively, use mean. Makes it more blurry.
    # return df.abs().mean(axis=1)

def reduce_time_find_peaks(df, filterfft=None, absolute=True):
    """Reduce the raw data time axis by finding the signal's arrival time.

    if filterfft: Filter the frequencies with an exp(-filterfft * f) lowpass function
    if absolute: don't convert the resulting peak times to the (t/t0 - 1) format
    """
    ap = find_all_peaks(df, filterfft=filterfft)
    # Return time taken (in µs)
    if absolute:
        return ap
    else:
        return (ap/ap.iloc[0] - 1)*100

def determine_sample_rate(df):
    """Determine sample rate of a dataframe."""
    diff = df.iloc[0].index[-1] - df.iloc[0].index[0]
    count = df.iloc[0].index.size
    return count/(diff*1e-6)

def determine_t0(df, freq):
    """Determine runtime through water."""
    sample_rate = determine_sample_rate(df)
    peakix = find_peak(df.loc[0,0], sample_rate, freq)
    return df.columns[peakix]

def find_all_peaks(df, filterfft=20, sample_rate=None):
    """For velocity tomography: Find signal arrival for all timeseries in df.

    The time is stored as microseconds and returned as series.
    """
    if sample_rate is None:
        sample_rate = determine_sample_rate(df)

    index = df.index

    all_angles, all_lens = [], []
    peaktimes = []

    for (angle, leng) in index:
        peakix = find_peak(df.loc[angle, leng], sample_rate, filterfft=filterfft)
        peakt = df.columns[peakix]
        all_angles.append(angle)
        all_lens.append(leng)
        peaktimes.append(peakt)

    df = pd.DataFrame({ctf.ANGLE_LABEL: all_angles,
                       ctf.POS_LABEL: all_lens,
                       'peaktime': peaktimes})
    df.set_index([ctf.ANGLE_LABEL, ctf.POS_LABEL], inplace=True)
    df.sort_index(axis=0, inplace=True)

    return df['peaktime']

def fft_filter_signal(sig, filterfft=None):
    """Run sig through a lowpass filter."""
    return fft.irfft(np.exp(-filterfft*fft.rfftfreq(sig.size))*fft.rfft(sig))

def find_peak(sig, sample_rate, filterfft=None, plot=False, rise_frac=4):
    """Return index of rise or peak within sig. Current algorithm:
        Determine rise time, t(sig >= max(sig)/rise_frac).

    filterfft will filter the input signal using an exp(-a f) lowpass filter.
    """
    if filterfft is not None:
        sig = fft_filter_signal(sig, filterfft)
    thresh = sig.max()/rise_frac
    if plot:
        plt.plot(sig)

    off = 25
    for i, sig in enumerate(sig[off:]):
        if sig >= thresh:
            return off+i
    return -1

def reconstruct_fast(fname: str, ftype="bin", preproc=None, cut180=False,
                     ramlak=False, shepplo=False,
                     reconstruct_dim=100, shepplo_C=0.3,
                     reduce_time_axis=reduce_time_axis,
                     is_vel=False, drop=(1,1), imgmap=lambda x: x):
    """Main entry point: Reconstruct CT image from raw data in fname.

    General procedure from here on:
        - Read original file, drop data if desired.
        - Reduce timeseries to single value, either max. amplitude
            or arrival time.
        - Apply ram-lak or shepp-logan filter.
        - Calculate norm factor to arrive at useful output values.
        - Project all measurements onto CT image.
        - Postprocess and render image.
    """
    if preproc is None:
        df = ctf.read_org_file(fname, ftype)
        df = drop_data(df, drop)

        if cut180:
            df = df.loc[0:180.]

        mean_ampl = reduce_time_axis(df)
    else:
        mean_ampl = preproc

    dim = mean_ampl.loc[0,:].size
    angles = list(mean_ampl.loc[:,0].index)

    if ramlak and shepplo:
        raise Exception("please select either ramlak or shepplo filtering")
    if ramlak:
        mean_ampl = filter_ct_data(mean_ampl, ramlak=True, shepplo=False,
                                   is_raw=False, cut180=cut180)
    elif shepplo:
        mean_ampl = filter_ct_data(mean_ampl, ramlak=False, shepplo=True,
                                   shepplo_C=shepplo_C,
                                   is_raw=False, cut180=cut180)

    N = len(angles)
    # Distance between sensors is ~13 cm.
    if is_vel:
        s = .13e6  # µm, so that with µs we get m/s
        norm_factor = 1/(2*N*s)
    else: # Absorption
        s = 130 # mm, so we get absorption per mm
        norm_factor = 1/(2*N*s)

    img = np.zeros((dim, dim))

    for angle in angles:
        # constant factor: 1/(2 pi s) * pi/N = 1/(2 N s)
        img += cth.project(mean_ampl.loc[angle,:], plot=False, angle=angle) * norm_factor

    if is_vel:
        img = 1/img
    img = imgmap(img)

    f = plt.figure(figsize=(10,10), tight_layout=True)
    p = f.add_subplot(111)
    s = p.imshow(img, origin='lower')
    f.colorbar(s, ax=p, shrink=0.7)

    filtertype = {
        (True, False): 'Ram-Lak',
        (False, True): 'Shepp-Logan, C = {}'.format(shepplo_C),
        (False, False): 'kein Filter'}[(ramlak, shepplo)]
    measure_type = 'Amplitude / 1V' if not is_vel else 'Geschwindigkeit / (m/s)'
    freq = (re.search('/(\\dMHz)', fname) or ['', 'unknown'])[1]

    p.set_title('{} ({}) \n {} -- drop {}'.format(freq, filtertype, measure_type, drop))

    return f

def filter_ct_data(df, ramlak=True, shepplo=False, shepplo_C=1/2,
                   is_raw=False, cut180=False, normalize=True):
    """Takes a dataframe read from file or mean amplitude series (is_raw==False)
    and applies Ram-Lak on it.

    This function applies the actual frequency filter by fourier-transforming
    the measured projections into the spatial frequency domain, applying the
    desired filter, and transforming it back into the spatial domain again.
    """

    # Mean over time
    if is_raw:
        mean_ampl = reduce_time_axis(df)
    else:
        mean_ampl = df

    if cut180:
        mean_ampl = mean_ampl[0:180.]

    transformed = df.copy()

    positions = mean_ampl.index.to_numpy()
    angles = np.unique(np.array([e[0] for e in positions]))
    angles.sort()

    filterfunc = lambda x, y: y
    if ramlak and not shepplo:
        def filterfunc(freqs, fourier):
            return np.abs(freqs)*fourier
    elif shepplo and not ramlak:
        def filterfunc(freqs, fourier):
            k = freqs
            factor = np.maximum(np.pi*k/(2*shepplo_C), 1e-6)
            return fourier * np.abs(k) * np.sin(factor)/factor
    else:
        raise Exception("filter_ct_data: no filter selected!")

    for (i, angle) in enumerate(angles):
        # Projections contains all projections for a fixed angle.
        projections = mean_ampl[angle].to_numpy()
        fourier = fft.rfft(projections)
        freqs = fft.rfftfreq(projections.size)
        filtered_freqs = filterfunc(freqs, fourier)
        filtered_projection = fft.irfft(filtered_freqs)

        if normalize:
            filtered_projection -= filtered_projection.min()
            # filtered_projection = filtered_projection/filtered_projection.max() * projections.max()

        # print(transformed[angle].size, filtered_projection.size)
        transformed[angle].iloc[0:filtered_projection.size] = filtered_projection
        if (transformed[angle].size % 2 == 1):
            transformed[angle].iloc[-1] = 0
        del filtered_freqs
        del freqs
        del fourier
        del projections

    return transformed


# Test our data first

def plot_sinogram(fname, ftype="bin",
                  **kwargs):
    """Draw an absorption sinogram from the raw data in the given file."""
    df = ctf.read_org_file(fname, ftype, **kwargs)
    mean_ampl = reduce_time_axis(df)

    positions = mean_ampl.index.to_numpy()
    angles = np.array([e[0] for e in positions])
    lens = np.array([e[1] for e in positions])

    loff = int(lens.min())
    woff = int(angles.min())

    width = int((angles.max()-angles.min()))
    height = int(lens.max()-lens.min())

    img = np.zeros((height+1, width+1))

    for (ang, l) in positions:
        img[int(l)-loff, int(ang)-2*woff] += mean_ampl[ang, l]

    f = plt.figure(figsize=(4,16), tight_layout=True)
    p = f.add_subplot(111)
    p.imshow(img.T)

    del(mean_ampl)
    del(positions)
    del(angles)
    del(lens)
    del(p)

    return f

def plot_pulse(fname, locs=[(0., -45.), (0., 0.), (0., 20.)]):
    df = ctf.read_org_file(fname, 'bin')
    freq = (re.search('/(\\dMHz)', fname) or ['', 'unknown'])[1]
    for loc in locs:
        f = plt.figure(figsize=(7,5), tight_layout=True)
        p = f.add_subplot(111)
        sig = df.loc[loc[0],loc[1]]
        signp = sig.to_numpy()
        filtered = fft_filter_signal(signp, filterfft=20)
        p.plot(sig, label="Signal")
        p.plot(sig.index, filtered, label="Signal, FFT Lowpass")
        p.legend()
        p.set_xlabel("t / µs")
        p.set_ylabel("Amplitude / roh")
        p.set_title("Ultraschallpuls, {}, {:.1f}mm / {:.1f}°".format(freq, loc[1], loc[0]))

        f.savefig('../../img/ct/pulse_{}_{}mm_{}g.pdf'.format(freq, loc[1], loc[0]))


def plot_crosssections(fname, amplitude=True, peaktime=True, angles=range(0, 360, 90),
                       add_filter=None, filtername='', peakfindargs={},
                       savepattern='crosssection_{}_{}_degree.pdf'):
    df = ctf.read_org_file(fname, 'bin')

    if amplitude:
        reda = reduce_time_axis(df)
    if peaktime:
        redp = reduce_time_find_peaks(df, **peakfindargs)

    if add_filter:
        if amplitude:
            filtereda = add_filter(reda)
        if peaktime:
            filteredp = add_filter(redp)

    for angle in angles:
        fig = plt.figure(figsize=(7,5), tight_layout=True)
        p = fig.add_subplot(111)
        p.set_xlabel("s / mm")
        p.set_ylabel("Amplitude/Zeit (rel. zu max.)")

        if amplitude:
            p.plot(reda[angle]/reda[angle].max(), label="Amplitude - {} Gr.".format(angle))
        if peaktime:
            p.plot(redp[angle]/redp[angle].max(), label="Zeit - {} Gr.".format(angle))

        if add_filter:
            if amplitude:
                p.plot(filtereda[angle]/filtereda[angle].max(), label="{} - {} Gr.".format(filtername, angle))
            if peaktime:
                p.plot(filteredp[angle]/filteredp[angle].max(), label="{} - {} Gr.".format(filtername, angle))

        p.legend()
        fig.savefig(savepattern.format(filtername, angle))


def ct_render_all_plots():
    # Unfiltered back-projections
    FREQS = [1,2,4]
    FILE_PAT_ATT = lambda f: '../../data/DatensatzA/ct_scan/{}MHz/attenuation.bin'.format(f)
    FILE_PAT_DUR = lambda f: '../../data/DatensatzA/ct_scan/{}MHz/duration.bin'.format(f)
    SAVE_PATH = lambda s: '../../img/ct/{}.pdf'.format(s)

    configs = {
        'raw': {},
        'ramlak': {'ramlak': True, 'shepplo': False},
        'shepplo': {'shepplo': True, 'ramlak': False, 'shepplo_C': .3},
    }

    drops = [(1,1), (2, 2), (5, 5)]
    lowpass = [None, 10, 20]
    absolute = [(lambda x: x, True)]#, (logmap_neg, False)]

    for fr in FREQS:
        # plot_sinogram(FILE_PAT_ATT(fr)).savefig(SAVE_PATH('sinogramm_ampl_{}MHz'.format(fr)))
        for title, cfg in configs.items():
            for drop in drops:
                fig = reconstruct_fast(
                        FILE_PAT_ATT(fr), reduce_time_axis=reduce_time_axis, drop=drop, **cfg)
                fig.savefig(
                        SAVE_PATH('orig_amplitude_{}MHz_{}_drop_{}_{}'.format(fr, title, *drop)))
                for lp in lowpass:
                    for (imap, ab) in absolute:
                        # Log plots only for velocity plots
                        print('Rendering plot:', fr, title, drop, ab)
                        fig = reconstruct_fast(
                            FILE_PAT_DUR(fr),
                            reduce_time_axis=lambda x: reduce_time_find_peaks(
                                x, absolute=ab, filterfft=lp),
                            drop=drop, is_vel=True, imgmap=imap, **cfg)
                        fig.savefig(
                            SAVE_PATH('orig_velocity_{}MHz_{}_drop_{}_{}_abs_{}_{}_lowpass_{}'.format(
                                fr, title, *drop, ab, 'lin' if ab else 'log',
                                lp)))

### Lewin's old code.

def lts_to_xy(l, theta, s):
    """Convert an l-theta-s coordinate to x-y."""
    theta = theta/180 * math.pi
    xs = l*np.cos(theta) - s*np.sin(theta)
    ys = l*np.sin(theta) + s*np.cos(theta)
    return np.array([xs, ys]).T

def generate_scan_xy(l, theta, dim, nmax):
    """Generate valid s values along the line of measurement for given l, t."""
    ss = np.linspace(-dim/math.sqrt(2), dim/math.sqrt(2)-1, nmax)
    points = lts_to_xy(l, theta, ss)
    mask = [-dim/2 <= x < dim/2 and -dim/2<= y < dim/2 for [x,y] in points]
    return points[mask]

def reconstruct_sample(fname: str, ftype="bin", cut180=False, ramlak=False, shepplo=False,
                       reconstruct_dim=100, shepplo_C=1/2,
                       **kwargs):
    df = ctf.read_org_file(fname, ftype, **kwargs)

    DIM = reconstruct_dim
    img = np.zeros((DIM+1,DIM+1))

    mean_ampl = reduce_time_axis(df)

    if cut180:
        mean_ampl = mean_ampl[0:180.]

    if ramlak and shepplo:
        raise Exception("please select either ramlak or shepplo filtering")
    if ramlak:
        mean_ampl = filter_ct_data(mean_ampl, ramlak=True, shepplo=False,
                                   is_raw=False, cut180=cut180)
    elif shepplo:
        mean_ampl = filter_ct_data(mean_ampl, ramlak=False, shepplo=True,
                                   shepplo_C=shepplo_C,
                                   is_raw=False, cut180=cut180)

    positions = mean_ampl.index.to_numpy()
    angles = np.array([e[0] for e in positions])
    lens = np.array([e[1] for e in positions])

    lmax = int(lens.max())

    npos, last = positions.shape[0], 0
    for (i, (ang, l)) in enumerate(positions):
        if (i/npos) - last > 0.05:
            print("reconstruct: progress {:.1f}%".format(100*i/npos))
            last = i/npos

        l_ = l/lmax * DIM/2
        # Generate samples along measured line.
        # xys is an array of [x,y] lines.
        samples = generate_scan_xy(l_, ang, dim=DIM, nmax=2*DIM)
        # XY coordinates go from -DIM/2 to DIM/2; convert to image index here.
        samples += DIM/2

        for (x,y) in samples:
            xix, yix = round(x), round(y)
            img[xix, yix] += mean_ampl[ang, l]

    f = plt.figure(figsize=(10,10), tight_layout=True)
    p = f.add_subplot(111)
    s = p.imshow(img, origin='lower')
    f.colorbar(s, ax=p, shrink=0.7)

    del(mean_ampl)
    del(positions)
    del(angles)
    del(lens)
    del(p)
    del(f)

def plot_auxiliary_plots():
    # High frequency jitter at 4 MHz
    rdf = ctf.read_org_file('../../data/DatensatzA/ct_scan/4MHz/duration.bin')
    fig = plt.figure(figsize=(7,5), tight_layout=True)
    p = fig.add_subplot(111)
    p.set_xlabel("t / µs")
    p.set_ylabel("Amplitude")
    p.set_title("Tiefpassfilter 4 MHz")

    lbl = "40° 0mm"
    sig = rdf.loc[40,0]
    filtdata = fft_filter_signal(sig, filterfft=10)
    filt = sig.copy()
    filt.iloc[:] = filtdata
    p.plot(sig, label=lbl)
    p.plot(filt, label=lbl + " Tiefpass")

    fig.savefig("../../img/tiefpass_signal.pdf")
