\section{2O1 Prismenspektrometer}

\begin{aufgabe}{Grundlagen}
  Knappe Beschreibung der theoretischen Grundlagen, Angabe der
  benötigten Formel(n), ohne Herleitung. Definition der verwendeten
  Formelzeichen.
\end{aufgabe}

Das Phänomen der Brechung wird vom Snellius´schen Brechungsgesetz beschrieben:
\begin{equation}
n_1 \cdot \sin(\alpha_1) = n_2 \cdot \sin(\alpha_2)
\label{eq:snellius}
\end{equation}
Dabei sind $n_1$ und $n_2$ Brechungsindizes oder Brechzahlen, sie beschreiben das Verhältnis zwischen der Lichtgeschwindigkeit im Vakuum und im Medium, $n = \frac{c}{v}$. Die Winkel $\alpha_1$ und $\alpha_2$ sind die Einfalls-, beziehungsweise Ausfallswinkel zum Lot der Fläche.

Lichtwellen sind elektromagnetische Wellen. Sie bestehen unter anderem aus einem oszillierendem elektrischem Feld. Die äußeren Elektronen eines Materials interagieren mit diesem elektrischen Feld. In transparenten Materialien lässt sich diese Interaktion gut mit einem Dipol-Modell beschreiben. Die Phasenverschiebung die sich zwischen der einfallenden Welle und der von den Elektronen wieder emittierten Welle ergibt, ist verantwortlich für das Phänomen der Brechung. Somit ist der Brechungsindex von der Frequenz und somit Wellenlänge des Lichtes abhängig.

Im Dipol-Modell werden die äußeren Elektronen vom elektrischen Feld in Schwingung versetzt. Wenn die Frequenz des Feldes viel kleiner als die Eigenschwingfrequenz von Atom und äußerem Elektron ist, ist $n>1$. Sobald sie viel größer als die Eigenschwingfrequenz ist, ist $n<1$. Dieser Übergang erfolgt schnell. Mit der Sellmeier Formel kann eine solche Dispersionskurve mit zwei Eigenschwingfrequenzen, je bei $\omega_{0_i}=2\pi c / \sqrt{b_i}$, $i=1,2$, modelliert werden:
\begin{equation}
n^2 -1 = a + \frac{a_1}{1-\frac{b_1}{\lambda^2}} + \frac{a_2}{1-\frac{b_2}{\lambda^2}}
\end{equation}
Dabei ist $\lambda$ die Wellenlänge und $a$, $a_1$ und $a_2$ sind Faktoren die sich aus der Modellierung ergeben.

Wir nutzen eine Näherung dieser Formel, die im Bereich des sichtbaren Lichts gültig ist:
\begin{equation}
n(\lambda) = \frac{a}{\lambda^4} + \frac{b}{\lambda^2} + c
\label{eq:sellmeier}
\end{equation}

Zur Bestimmung des Berechungsindex werden einige geometrische Überlegungen am Prisma gemacht. Dabei wird vereinfacht angenommen, dass der Brechungsindex von Luft gleich dem vom Vakuum ist ($n_L = 1$). Der Winkel $\alpha_1$ beschreibt den Winkel des einfallenden Strahls zum Lot der ersten Seite des Prismas mit Öffnungswinkel $\epsilon$. Dieser wird nach \autoref{eq:snellius} zum Lot des optisch dichterem Glas gebrochen mit dem Winkel $\alpha_2$ zum Lot. Dieser Strahl durchquert das Prisma und kommt im Winkel $\beta_1$ zum Lot der zweiten Seite auf die Grenzfläche. Dort wird er im optisch dünneren Medium der Luft vom Lot weg gebrochen im Winkel $\beta_2$.

Somit wurde der Strahl insgesamt um den Winkel $\delta$ verändert. Die minimale Winkeländerung $\delta_{min}$ erfährt der Strahl wenn er sich parallel zur dritten Seite durch das Prisma bewegt. In diesem Fall gilt: 
\begin{equation}
\alpha_1 = \beta_2 = \frac{\delta_{min} + \epsilon}{2} $$ und $$\alpha_2 = \beta_1 = \frac{\epsilon}{2}
\end{equation}
Damit folgt mit \autoref{eq:snellius}:
\begin{equation}
n = \frac{\sin(\frac{\delta_{min} + \epsilon}{2})}{\sin(\frac{\epsilon}{2})}
\label{eq:n}
\end{equation}
Der minimale Ablenkwinkel lässt sich experimentell aus zwei Winkelmessungen, die später erläutert werden, bestimmen:
\begin{equation}
\delta_{min} = \frac{\psi_2 - \psi_1}{2}
\label{eq:delta}
\end{equation}

Das Auflösungsvermögen $A$ eines Prismas beschreibt, bei welcher Wellenlängendifferenz $\Delta\lambda$ zwei spektrale Wellenlängen $\lambda$ und $\lambda + \Delta\lambda$ noch trennbar sind:
\begin{equation}
A = \frac{\lambda}{\Delta\lambda} = S\frac{dn}{d\lambda} = 2\cdot l \cdot \frac{\sin(\frac{\epsilon}{2})}{\cos(\frac{\delta_{min} + \epsilon}{2})} \cdot \frac{dn}{d\lambda}
\label{eq:A}
\end{equation}
Dabei ist S die Basisbreite des genutzten Teil des Prismas und ergibt sich wie oben mit $l$, der Breite des Lichtstrahls.
$\frac{dn}{d\lambda}$ ist die Ableitung der Dispersionskurve und ist bei \autoref{eq:sellmeier}:
\begin{equation}
\frac{dn}{d\lambda} = -4 \cdot \frac{a}{\lambda^5} - 2 \cdot \frac{b}{\lambda^3}
\label{eq:dn/dlamb}
\end{equation}
\\
Es wurden Gasdampflampen verwendet, da diese fest definierte Wellenlängen emittieren, welche bereits bekannt sind und für unsere Auswertung verwendet werden.

\FloatBarrier
\clearpage
\begin{aufgabe}{Versuchsaufbau und Versuchsdurchführung}
  Beschreibung des Versuchsaufbaus einschließlich beschrifteter Skizze
  oder Foto. Beschreibung der Versuchsdurchführung einschließlich
  aller Handgriffe an der Apparatur.
\end{aufgabe}

In \autoref{fig:aufbauSchraeg} sieht man den Aufbau dieses Versuches. Eine Spektrallampe ist auf den Spalt einer Kollimatorlinse gerichtet. Diese hat ihren Brennpunkt im Schlitz und parallelisiert somit den Strahlengang. Dieser durchquert das Prisma, welches mit der Winkelhalbierenden des Öffnungswinkels auf der Drehachse steht. Der Strahlengang erreicht daraufhin das Teleskop, welches mit einem Fadenkreuz zusammen den Strahlengang in das Auge wirft. Das Fadenkreuz wird mit Streulicht von der Tischlampe sichtbar gemacht.
In \autoref{fig:aufbauVogel} ist der Strahlengang noch einmal eingezeichnet.
\\
\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{../Bilder/394493_2O1_Aufbau.jpg}
  \caption{Aufbau aus Perspektive des Experimentators}
  \label{fig:aufbauSchraeg}
\end{figure}
\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{../Bilder/394493_2O1_Aufbau_Vogel.jpg}
  \caption{Strahlengang, Vogelperspektive}
  \label{fig:aufbauVogel}
\end{figure}

Das Teleskop sowie der Drehteller waren drehbar. Die Rotation des Teleskops war mit einem Winkelmaß welches Halbwinkelschritte umfasste, sowie einem Nonius ablesbar.
\\

Zu Beginn des Experimentes haben wir den Drehteller mit Hilfe der 3 Verstellschrauben und einer zweidimensionalen Wasserwaage geebnet. Daraufhin haben wir eine Spektrallampe an den Kollimatorspalt geschoben. Über die Drossel haben wir die HgCd-Gasdampflampe mit Strom versorgt, da diese durch eine Netzspannung von $\SI{230}{\volt}$ zerstört werden würde. 
\\

Das Prisma mit dem Öffnungswinkel $\epsilon = 60°$ befand sich zu diesem Zeitpunkt noch nicht auf dem Drehteller. Wir haben das Teleskop und den Kollimator um 180° zueinander gedreht um die Höhenwinkel der beiden anzupassen, bis der Spalt sichtbar war.
\\

Nachdem das Prisma wie oben genannt platziert wurde, haben wir es so rotiert, dass es einen steilen Winkel zwischen dem Lot der bestrahlten Seite und dem einfallenden Strahlenbündel hatte. Das Teleskop wurde ebenfalls in einem steilen Winkel zum entsprechendem Lot der ausfallenden Seite platziert.
\\

Nachdem das Spektrum der HgCd-Lampe sichtbar wurde, haben wir mithilfe der Verstellschraube für die Spaltbreite am Kollimatorspalt, eine möglichst kleine Breite gewählt. So klein, dass der Spalt gerade so sichtbar war. Mithilfe der Fokusschraube am Teleskop wurde das Bild scharfgestellt. Scharfstellen und andere Justierungen werden im Weiteren nicht mehr erwähnt.
\\

Für unseren ersten Teilversuch, der Bestimmung der Dispersionskurve unseres Prismas, haben wir den Drehteller mit Prisma und das Teleskop gleichsinnig rotiert. Die Spektrallinien haben sich dabei verschoben, was durch die Verschiebung des Teleskops im Sichtfeld kompensiert wurde. Wir haben zuerst die  prominente rote Spektrallinie beobachtet.  Bei $\delta_{min}$ schlägt die Richtung in die sich die Spektrallinie bewegt um. Dieser Punkt wurde so genau es ging bestimmt, indem wir das Fadenkreuz auf die Linie gesetzt haben, um dann vorsichtig den Drehteller in beide Richtungen zu bewegen. Wenn die Linie sich über das Fadenkreuz hinaus bewegt hat, wurde nachjustiert. Danach wurde der Winkel in dem das Teleskop stand in Winkel und Bogenminute notiert und Drehteller und Teleskop wurden leicht verstellt.
\\

Dieser Vorgang wurde 10 mal wiederholt um eine Rauschmessung für den Ablesefehler durchzuführen. Dafür wurde die rote Cd-Linie verwendet während das Teleskop auf die rechte Seite gedreht worden war. Es hätte auch jede andere Linie verwendet werden können. Jedoch würden wir aufgrund unserer Erfahrungen die Linie mit der größten Wellenlänge wählen aus Gründen die später erläutert werden.
\\

Falls der Nonius oder der Winkelmesser einen systematischen Fehler aufwies, kann dieser in der Berechnung ignoriert werden, da eine relative Messung zwischen zwei Punkten auf demselben Messapparat durchgeführt wurde.
\\

Als nächstes wurde der Drehteller so gedreht, dass der Strahlengang durch die Prismenseite einfällt, aus der er zuvor ausgefallen ist. Das Teleskop wurde entsprechend verstellt und die Schritte von zuvor wurden wiederholt, um die \textit{linke} Seite zu vermessen. Dieses mal haben wir nur 2 Messungen gemacht. Diese waren zum Zweck einer Plausibilitätsmessung. Die Winkel und Bogenminuten wurden wieder notiert.
\\

Dieses Verfahren wurde für alle gut erkennbaren Spektrallinien (\autoref{fig:HgCdSpektrum}) auf beiden Seiten wiederholt, wir haben nur eine der gelben Doppellinien vermessen.
\\

Für den zweiten Teilversuch, der Bestimmung der Auflösung des Prismas, haben wir eine Spaltbreitenbegrenzung an den Ausgang des Kollimators angebracht. Diese haben wir auf die breiteste Einstellung, \SI{6}{\milli\meter} eingestellt. Nachdem die gelbe Doppellinie in den Fokus gestellt wurde, haben wir den Spalt in \SI{0.5}{\milli\meter} Schritten verkleinert, bis man die Doppellinie nicht mehr trennen konnte. Die entsprechenden Schritte wurden notiert.
\\

Für den letzten Teilversuch haben wir die HgCd-Lampe gegen eine He-Lampe ausgetauscht. Das Ziel war es, die Wellenlänge einer Spektrallinie der He-Lampe zu bestimmen, mithilfe der zuvor bestimmten Dispersionskurve. Es wurde die prominente gelbe Linie (\autoref{fig:HeSpektrum}) gewählt. Die Messung verlief wie im ersten Teilversuch.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{../Bilder/394493_2O1_Spektrum_HgCd.jpg}
  \caption{Spektrum Quecksilber-Cadmium, Aufnahme vor Ort}
  \label{fig:HgCdSpektrum}
\end{figure}
\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{../Bilder/394493_2O1_Spektrum_He.jpg}
  \caption{Spektrum Helium, Aufnahme vor Ort}
  \label{fig:HeSpektrum}
\end{figure}

\FloatBarrier
\clearpage
\begin{aufgabe}{Bestimmung der Winkelgenauigkeit}
  Vermessen Sie zunächst eine Spektrallinie auf einer Seite und
  wiederholen Sie den gesamten Messvorgang mindestens
  zehnmal. Tabellieren Sie Ihre Messwerte (in Grad und Bogenminuten)
  und stellen Sie sie als Häufigkeitsverteilung dar. Berechnen Sie die
  Standardabweichung als Maß für Ihre Winkelgenauigkeit.
\end{aufgabe}

Für die Berechnung des Fehlers unserer Messung haben wir angenommen, dass wir immer den gleichen Messfehler haben. Um diesen abzuschätzen haben wir eine Rauschmessung durchgeführt. 10 Werte wurden genommen. Der Fehler auf unsere Winkelmittelwerte $\sigma_{\overline{\psi_1}}$ und $\sigma_{\overline{\psi_2}}$ lässt sich dann für alle Messreihen mit Messungsanzahl N wie folgt bestimmen
\begin{equation}
\sigma_{\overline{\psi}} = \frac{\sigma_\psi}{\sqrt{N}}
\end{equation}
Dabei ist $\sigma_\psi$ die Standardabweichung der Rauschmessung.


\input{tabellen/Rausch.tex}
\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{grafiken/RauschGrafik.pdf}
  \caption{Rauschmessung Histogramm}
  \label{fig:Rausch}
\end{figure}

\FloatBarrier
\clearpage
\begin{aufgabe}{Vermessung der Dispersionskurve eines Prismas}
  Vermessen Sie die Dispersionskurve des vorhandenen Prismas mit einer
  HgCd-Lampe. Messen Sie jede Linie $2\times$ auf jeder Seite aus,
  bzw.~messen Sie abwechselnd unabhängig voneinander, um grobe
  Messfehler auszuschließen. Tabellieren Sie zunächst die jeweils
  gemessenen Ablenkwinkel (in Grad und Bogenminuten) und ordnen Sie
  die jeweiligen Wellenlängen zu. Zeigen Sie die sich ergebende
  Dispersionskurve $n(\lambda)$. Führen Sie in geeigneter Weise eine
  Anpassungsrechnung mit der vereinfachten Sell\-mei\-er-For\-mel
  durch und vergleichen Sie Ihre Dispersionskurve mit einschlägigen
  Herstellerangaben.
\end{aufgabe}

Die Rohdaten der Messwerte finden sich in \autoref{tab:Raw}. Dort sind auch die gemittelten Werte mitsamt ihrer Fehler eingezeichnet. Die in \autoref{fig:HgCdSpektrum} gesehene Farbe steht ganz links, daneben die entsprechende Wellenlänge aus Tabellen.

Die daraus bestimmten $\delta_{min}$, sowie Brechzahlen mit Fehlern finden sich in \autoref{tab:nValues}

Die Fehler wurden per Fehlerfortpflanzung bestimmt. Dabei war der Fehler auf $\delta_{min}$ wegen \autoref{eq:delta}:
\begin{equation}
\sigma_{\delta_{min}} = \frac{1}{2} \cdot \sqrt{ \sigma_{\overline{\psi_2}}^2 + \sigma_{\overline{\psi_1}}^2 } 
\end{equation}
Und der Fehler auf die Brechzahl wegen \autoref{eq:n}:
\begin{equation}
\sigma_n = \frac{1}{2} \cdot \frac{\cos(\frac{\delta_{min} + \epsilon}{2})}{\sin(\frac{\epsilon}{2})} \cdot \sigma_{\delta_{min}}
\end{equation}
\input{tabellen/Raw.tex}
\input{tabellen/nValues.tex}

In \autoref{tab:nValues} sieht man den Vergleich zwischen unserer gemessenen Brechzahl und der von Schott Glas F2 (Flintglas) aus der Praktikumsbibliothek. Die Werte stimmen bis auf kleine Abweichungen mit den Literaturwerten überein. Diese lassen sich durch Fehler wie dem Parallax Effekt der Teleskop Linse (welcher bei Violett besonders stark war, Anmerkung oben) erklären.

\FloatBarrier
Für unsere Regression haben wir die $\chi^2$ Methode der Kurvenanpassung verwendet, nach der $\chi^2 = \sum_{i}(\frac{n_i - f(\lambda_i)}{\sigma_{n_i}})$ minimiert wird. Dafür haben wir zunächst in \autoref{eq:sellmeier} angenommen, dass $a=0$ sei. Die Auftragung sowie, das $\chi^2 / nDof$, wobei $nDof$ die freien Freiheitsgrade sind (hier 8 Messpunkte - 2 Parameter = 6), finden sich in \autoref{fig:LinReg}.
Für die Berechnung wurde $x = \frac{1}{\lambda^2}$ verwendet, damit sich die Funktionen der Praktikumsbibliothek nutzen lassen konnten. Die Wellenlängen $\lambda$ sind als Spektrallinien fest bestimmt und weisen keinen Fehler auf.
\\
\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{grafiken/LinReg.pdf}
  \caption{Lineare Regression: $n(\lambda) = m \cdot x + b$}
  \label{fig:LinReg}
\end{figure}
\input{tabellen/LinReg.tex}
\\
Wie man in \autoref{fig:LinReg} sehen kann ist die Anpassungsgüte in diesem Modell unzureichend. Sie ist weit über 1. Desweiteren kann man eine klare Systematik in den Residuen erkennen. Sie hat die Form einer Parabel. Somit wurde die Vereinfachung, dass $a=0$ sei, verworfen und es wurde mit \autoref{eq:sellmeier} die Regression erneut durchgeführt. 

Bei dieser Regression war $nDof = 5$, da ein weiterer Freiheitsgrad durch den Parameter $c$ besetzt wird.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.95\textwidth]{grafiken/QuadReg.pdf}
  \caption{Quadratische Regression: $n(\lambda) = a \cdot x^2 + b \cdot x + c$}
  \label{fig:QuadReg}
\end{figure}
\input{tabellen/QuadReg.tex}

In \autoref{fig:QuadReg} erkennen wir, dass die Anpassungsgüte fast bei 1 liegt. Die Systematik hat sich auch aufgelöst. Die Residuen sind gleichmäßig und mit passendem Fehler über und unter der Nulllinie verteilt. Die Ergebnisse aus diesem Modell sind für die weitere Auswertung verwendet worden.

\FloatBarrier
\clearpage
\begin{aufgabe}{Bestimmung des Auflösungsvermögens}
  Bestimmen Sie mit der gelben Hg-Doppellinie das Auflösungsvermögen
  des Prismas und vergleichen Sie es mit Ihrer Erwartung anhand der
  Dispersionskurve.
\end{aufgabe}

Für die Bestimmung des Auflösungsvermögens wurde die gelbe Doppellinie der HgCd-Lampe betrachtet. Damit die beiden Linien unterschieden werden können braucht nach \autoref{eq:A} eine Auflösung von:
\begin{equation}
A = \frac{\lambda}{\Delta\lambda} = \frac{\SI{576.96}{\nano\meter}}{\SI{2.11}{\nano\meter}} \approx 273,44
\end{equation}

In der Versuchsdurchführung erhielten wir eine Strahlenbreite $l$ in der die Linien unterschieden werden konnten von \SI{1.5}{\milli\meter} bis \SI{2}{\milli\meter}. Genauer, konnte sie bei \SI{2}{\milli\meter} aufgelöst werden und bei \SI{1.5}{\milli\meter} nicht mehr. Die entsprechenden Auflösungen samt Fehler stehen in \autoref{tab:Resolution}. Es wurde der Betrag der errechneten Werte genommen.
\\
Wie man erkennt liegt die benötigte Auflösung in diesem Intervall. Die Auflösung unseres Prismas liegt zwischen diesen beiden Werten.

Für die Fehler von A wurde die Fehlerfortpflanzung auf \autoref{eq:A} angewandt:
\begin{equation}
\sigma_A = \sqrt{ (2\cdot l \cdot \frac{\sin(\frac{\epsilon}{2})}{\cos(\frac{\delta_{min} + \epsilon}{2})} \cdot \sigma_{\frac{dn}{d\lambda}})^2 + (\frac{dn}{d\lambda} \cdot l \cdot \frac{\sin(\frac{\epsilon}{2}) \cdot \sin(\frac{\delta_{min} + \epsilon}{2})}{\cos(\frac{\delta_{min} + \epsilon}{2})^2} \cdot \sigma_{\delta_{min}})^2 }
\end{equation}
Wobei der Fehler auf die Ableitung der Dispersionskurve mithilfe der Quadratischen Regression und \autoref{eq:dn/dlamb} bestimmt wurde:
\begin{equation}
\sigma_{\frac{dn}{d\lambda}} = \sqrt{\sigma_c^2 + (\frac{\sigma_b}{\lambda^2})^2 + (\frac{\sigma_a}{\lambda^4})^2}
\end{equation}
\input{tabellen/Resolution.tex}

\FloatBarrier
\clearpage
\begin{aufgabe}{Vermessung einer unbekannten Spektrallinie}
  Tauschen Sie die Spektrallinie gegen eine Na-, Zn-, He- oder
  Ne-Lampe aus, und vermessen Sie eine prominente Linie dieser
  Lampe. Bestimmen Sie über die Invertierung der Dispersionskurve die
  Wellenlänge der Spektrallinie und vergleichen Sie sie mit dem
  Literaturwert.
\end{aufgabe}

Für diese Aufgabe musste zuerst wie in der Vermessung der Dispersionskurve der Brechungsindex bestimmt werden. Daher wird hier nicht weiter darauf eingegangen, die Ergebnisse und Messdaten finden sich in \autoref{tab:RawHe} und \autoref{tab:nValuesHe}.


\input{tabellen/RawHe.tex}
\input{tabellen/nValuesHe}


Für die Bestimmung der Wellenlänge aus diesem Ergebnis wird \autoref{eq:sellmeier} invertiert. Dafür werden die Ergebnisse der Quadratischen Regression verwendet. Es werden nur physikalisch sinnvolle Ergebnisse verwendet, also keine negativen Wellenlängen, oder komplexe Wellenlängen, in diesem Fall:
\begin{equation}
\lambda = \sqrt{ \frac{b}{2\cdot(n-c)} + \sqrt{ \frac{b^2}{4} \cdot \frac{1}{(n-c)^2} + \frac{a}{n-c} }}
\end{equation} 
Die Fehler darauf werden abgeschätzt, indem der Wert erneut für $n \pm \sigma_n$ berechnet wird.
Es Ergeben sich die Werte in \autoref{tab:lambdaByN}.

\input{tabellen/lambdaByN}

Die anvisierte Wellenlänge ist im Rahmen der Fehler enthalten.