# -*- coding: utf-8 -*-
"""
Created on Fri Sep  4 16:34:09 2020

@author: 394493: Alexander Kohlgraf
"""

import os
import codecs
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import nice_plots
import texttable
import latextable
from praktikum import analyse, literaturwerte


def degreeToMinute(degree):
    return degree * 60
def minuteToDegree(minute):
    return minute/60
def degreeAndMinute(degree):
    deg = degree - np.floor(degree)
    return np.floor(degree), degreeToMinute(deg)

class tableDrawer:
    def drawLambdaByN(self, color, lamb, lambP, lambM):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(4))
        header = ["$\lambda(n)$", "$ \lambda(n) - \lambda(n+\sigma_n)$", "$\lambda(n-\sigma_n) - \lambda(n)$", "$\lambda_{Literatur}$ in nm"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        print(lamb)
        print(lambP)
        print(lambM)
        contents = ["{:.2f}".format(lamb),"{:.2f}".format(lambP), "{:.2f}".format(lambM), "{:.2f}".format(color.wavelength)]
        table.add_row(contents)
        
            
        text += latextable.draw_latex(table, caption='Bestimmung $\lambda$', label = "tab:" + "lambdaByN") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "lambdaByN.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawResolution(self, color, dn_dlambda, A_lower, lower, A_upper, upper):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["Spaltbreite l in mm", "|A|"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        contents = [lower,"${:.2f} \pm {:.2f} $".format(A_lower[0],A_lower[1])]
        table.add_row(contents)
        contents = [upper, "${:.2f} \pm {:.2f} $".format(A_upper[0],A_upper[1])]
        table.add_row(contents)
        
            
        text += latextable.draw_latex(table, caption='Auflösung: ${}$, {}nm'.format(color.name, color.wavelength), label = "tab:" + "Resolution") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "Resolution.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

    def drawLinReg(self,m, em, b, eb, chiq, nDof):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["Parameter", "Wert"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        contents = ["m", "${:.3e} \pm {:.3e} $".format(m,em) + " $[m^2]$"]
        table.add_row(contents)
        contents = ["b", "${:.3e} \pm {:.3e} $".format(b,eb) + " $[ ]$"]
        table.add_row(contents)
        contents = ["$\chi^2$", "${:.3f}$".format(chiq)]
        table.add_row(contents)
        contents = ["$nDof$", "${:}$".format(nDof)]
        table.add_row(contents)
        contents = ["$\chi^2 / nDof$", "${:.3f}$".format(chiq/nDof)]
        table.add_row(contents)
        
            
        text += latextable.draw_latex_small(table, caption='Lineare Regression: $n(\lambda) = m \cdot x + b$', label = "tab:" + "LinReg") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "LinReg.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
        
    def drawQuadReg(self,a, ea, b, eb, c, ec, chiq, nDof):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["Parameter", "Wert"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        contents = ["a", "${:.3e} \pm {:.3e} $".format(a,ea) + " $[m^4]$"]
        table.add_row(contents)
        contents = ["b", "${:.3e} \pm {:.3e} $".format(b,eb) + " $[m^2]$"]
        table.add_row(contents)
        contents = ["c", "${:.3e} \pm {:.3e} $".format(c,ec) + " $[ ]$"]
        table.add_row(contents)
        contents = ["$\chi^2$", "${:.3f}$".format(chiq)]
        table.add_row(contents)
        contents = ["$nDof$", "${:}$".format(nDof)]
        table.add_row(contents)
        contents = ["$\chi^2 / nDof$", "${:.3f}$".format(chiq/nDof)]
        table.add_row(contents)
            
        text += latextable.draw_latex_small(table, caption='Quadratische Regression: $n(\lambda) = a \cdot x^2 + b \cdot x + c$', label = "tab:" + "QuadReg") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "QuadReg.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

    def drawResults(self, colors, name = ""):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(5))
        header = ["Farbe", "Wellenlänge in nm", "$\delta_{min}$", "Brechzahl", "Erwartete Brechzahl Flintglas"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        
        #rot handling
        
        for color in colors:
            contents = ["$"+color.name+"$", str(color.wavelength), "${:.3f}° \pm {:.3f}°$".format(color.minimalDegree, color.minimalDegreeError)]
            contents.append( "${:.5f} \pm {:.5f}$".format(color.n, color.nError))
            contents.append("${:.5f}$".format(literaturwerte.n_schott_f2(color.wavelength * 10**(-3))))
            table.add_row(contents)
            
        text += latextable.draw_latex_small(table, caption='Ergebnisse Brechzahl', label = "tab:" + "nValues" + name) +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "nValues" + name + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
    
    def drawLatexRausch(self,color):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2))
        header = ["Wert", "Ergebnis"]
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(5)
        
        for i in range(len(color.right)):
            table.add_row(["$\psi_{" + "{:}".format(i+1) + "}$", str(color.rightDegree[i]) + "° {:1}'".format(color.rightMinute[i])])
        table.add_row(["",""])
        table.add_row(["$\sigma_\psi$", "{:.3f}° = ".format(sigma) + "{:.3f}'".format(degreeToMinute(sigma))])
        
        text += latextable.draw_latex_small(table, caption='Rauschmessung Tabelle', label = "tab:" + "Rausch") +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "Rausch.tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()
        
    def drawLatexMeasurement(self,colors, name = ""):
        text = ''
        table = texttable.Texttable()
        table.set_cols_align(["c"]*(2 + 4))
        header = ["Farbe", "Wellenlänge in nm", "Seite"]
        for x in range(2):
            header.append("$\psi_{:1}$".format(x+1))
        header.append("$\overline{\psi}$")
        table.header(header)
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(2)
        
        #rot handling
        
        for color in colors:
            contents = ["$"+color.name+"$", str(color.wavelength), "rechts"]
            if len(color.right) == 2:
                contents.append(str(color.rightDegree[0]) + "° {:1}'".format(color.rightMinute[0]))
                contents.append(str(color.rightDegree[1]) + "° {:1}'".format(color.rightMinute[1]))
                contents.append("{:.4f}°".format(color.rightAverage) + " $\pm$ {:.3f}'".format(degreeToMinute(color.rightError)))
                table.add_row(contents)
            else:
                contents.append("siehe Rauschmessung")
                contents.append("siehe Rauschmessung")
                contents.append("{:.4f}°".format(color.rightAverage) + " $\pm$ {:.3f}'".format(degreeToMinute(color.rightError)))
                table.add_row(contents)
            contents = ["$"+color.name+"$", str(color.wavelength), "links"]
            contents.append(str(color.leftDegree[0]) + "° {:1}'".format(color.leftMinute[0]))
            contents.append( str(color.leftDegree[1]) + "° {:1}'".format(color.leftMinute[1]))
            contents.append("{:.4f}".format(color.leftAverage) + "° $\pm$ {:.3f}'".format(degreeToMinute(color.leftError)))
            table.add_row(contents)
            
        text += latextable.draw_latex_small(table, caption='Rohdaten', label = "tab:" + "Raw" + name) +'\n'
    
        dir_path = os.path.dirname(os.path.realpath(__file__))
        text_file = codecs.open(dir_path + "/../Protokoll/tabellen/" + "Raw" + name + ".tex", "w", 'utf-8')
        text_file.write(text)
        text_file.close()

class color:
    """saves the measurements for a color, left and right are numpy arrays"""
    
    def __init__(self, name, wavelength, leftDegree, leftMinute, rightDegree, rightMinute):
        self.name = name
        self.wavelength = wavelength
        self.left = leftDegree + minuteToDegree(leftMinute)
        self.leftDegree = leftDegree
        self.leftMinute = leftMinute
        self.right = rightDegree + minuteToDegree(rightMinute)
        self.rightDegree = rightDegree
        self.rightMinute = rightMinute
        analyseLeft = analyse.mittelwert_stdabw(self.left)
        self.leftAverage = analyseLeft[0]
        self.leftError = sigma / np.sqrt(len(self.left))
        analyseRight = analyse.mittelwert_stdabw(self.right)
        self.rightAverage = analyseRight[0]
        self.rightError = sigma / np.sqrt(len(self.right))
        
        
        self.minimalDegree = abs(self.leftAverage - self.rightAverage) / 2
        self.minimalDegreeError = 0.5 * np.sqrt(self.leftError**2 + self.rightError**2)
        
        self.n = np.sin( ( np.radians(self.minimalDegree) + np.radians(60) )/2 ) / np.sin( np.radians(30) )
        self.nError = np.radians(self.minimalDegreeError) * np.cos( ( np.radians(self.minimalDegree) + np.radians(60) )/2 ) / (2*np.sin( np.radians(30)))
    def __str__(self):
        text = ""
        text += ("name: " + self.name + "\n")
        text += ("wavelength: " + str(self.wavelength) + "\n")
        text += ("delta_min: " + str(self.minimalDegree) + " +- " + str(self.minimalDegreeError) + "\n")
        text += ("n: " + str(self.n) + " +- " + str(self.nError) + "\n")
        return text
    def __repr__(self):
        text = ""
        text += ("name: " + self.name + ", ")
        text += ("wavelength: " + str(self.wavelength) + ", ")
        text += ("delta_min: " + str(self.minimalDegree) + " +- " + str(self.minimalDegreeError) + ", ")
        text += ("n: " + str(self.n) + " +- " + str(self.nError) + ", ")
        return text

def compare(colors):
    print("Schott Glas F2 (Flintglas)")
    for x in colors:
        print(x.name)
        print(str(x.n) + " +- " + str(x.nError))
        print(str(literaturwerte.n_schott_f2(x.wavelength * 10**(-3))))
        print("\n")

def dn_dlambda(a,ea,b,eb,c,ec, lamb):
    dn_dlambda = -2*b/lamb**3 - 4*a/lamb**5
    dn_dlambda_err = np.sqrt(ec**2 + (eb/lamb**2)**2 + (ea/lamb**4)**2)
    return (dn_dlambda,dn_dlambda_err)

def resolution(dn_dlambda, breite, del_min, del_min_err):
    A = dn_dlambda[0] * 2 * breite * np.sin(np.radians(30))/np.cos(np.radians(30+del_min/2))
    A_err = np.sqrt( (2*breite*np.sin(np.radians(30))/np.cos(np.radians(30+del_min/2)) * dn_dlambda[1])**2 
                    + (dn_dlambda[0] * breite * np.sin(np.radians(30)) * np.sin(np.radians(30+del_min/2)) / np.cos(np.radians(30+del_min/2))**2  * del_min_err)**2 )
    return abs(A), abs(A_err)

def lambdaByN(a,b,c,n, n_err):
    lamb = np.sqrt( b / (2*n-2*c) + np.sqrt( (b**2) / 4 * 1 / ((n-c)**2) + a / (n-c) ) )
    n += n_err
    lambP = np.sqrt( b / (2*n-2*c) + np.sqrt( (b**2) / 4 * 1 / ((n-c)**2) + a / (n-c) ) )
    n -= 2*n_err
    lambM = np.sqrt( b / (2*n-2*c) + np.sqrt( (b**2) / 4 * 1 / ((n-c)**2) + a / (n-c) ) )
    return lamb, lamb-lambP, lambM-lamb

rot_1_r_Degree = np.array([219, 219, 219, 219, 219, 219, 219, 219, 219, 219])
rot_1_r_Minute = np.array([17,19,18,18,19,20,19,19,18,19])
rot_1_l_Degree = np.array([314.5, 314.5])
rot_1_l_Minute = np.array([22,23])

gelb_1_r_Degree = np.array([218.5 , 218.5])
gelb_1_r_Minute = np.array([18,19])
gelb_1_l_Degree = np.array([315, 315])
gelb_1_l_Minute = np.array([20,19])

grün_1_r_Degree = np.array([218 , 218 ])
grün_1_r_Minute = np.array([28,28])
grün_1_l_Degree = np.array([315.5 , 315.5])
grün_1_l_Minute = np.array([9,9])

türkis_1_r_Degree = np.array([218 , 218])
türkis_1_r_Minute = np.array([0,0])
türkis_1_l_Degree = np.array([316, 316])
türkis_1_l_Minute = np.array([6,5])

blau_1_r_Degree = np.array([217.5, 217.5])
blau_1_r_Minute = np.array([4,4])
blau_1_l_Degree = np.array([316.5, 316.5 ])
blau_1_l_Minute = np.array([3,2])

blau_2_r_Degree = np.array([217 , 217])
blau_2_r_Minute = np.array([20,20])
blau_2_l_Degree = np.array([316.5, 316.5 ])
blau_2_l_Minute = np.array([15,16])

blau_3_r_Degree = np.array([216.5 , 216.5])
blau_3_r_Minute = np.array([8,9])
blau_3_l_Degree = np.array([317, 317])
blau_3_l_Minute = np.array([25,24])

violett_1_r_Degree = np.array([215.5, 215.5])
violett_1_r_Minute = np.array([16,15])
violett_1_l_Degree = np.array([318 , 318])
violett_1_l_Minute = np.array([11,11])


sigma = analyse.mittelwert_stdabw(np.array(minuteToDegree(rot_1_r_Minute)))[1]
#if sigma < minuteToDegree(1):
    #sigma = minuteToDegree(1)

rot_1 = color("rot_1", 643.85, rot_1_l_Degree, rot_1_l_Minute, rot_1_r_Degree, rot_1_r_Minute)
gelb_1 = color("gelb_1", 579.07, gelb_1_l_Degree, gelb_1_l_Minute, gelb_1_r_Degree, gelb_1_r_Minute)
grün_1 = color("gruen_1", 546.07, grün_1_l_Degree, grün_1_l_Minute, grün_1_r_Degree, grün_1_r_Minute)
türkis_1 = color("tuerkis_1", 508.58 , türkis_1_l_Degree, türkis_1_l_Minute, türkis_1_r_Degree, türkis_1_r_Minute)
blau_1 = color("blau_1", 479.99, blau_1_l_Degree, blau_1_l_Minute, blau_1_r_Degree, blau_1_r_Minute)
blau_2 = color("blau_2", 467.81, blau_2_l_Degree, blau_2_l_Minute, blau_2_r_Degree, blau_2_r_Minute)
blau_3 = color("blau_3", 435.83, blau_3_l_Degree, blau_3_l_Minute, blau_3_r_Degree, blau_3_r_Minute)
violett_1 = color("violett_1", 404.66, violett_1_l_Degree, violett_1_l_Minute, violett_1_r_Degree, violett_1_r_Minute)
colors = [rot_1, gelb_1, grün_1, türkis_1, blau_1, blau_2, blau_3, violett_1]
print(np.array(colors))

#Vergleiche mit Literatur




#alle ergebnisse ausgeben in Tabellen
#3)
#grad, todo histogramm
drawer = tableDrawer() 
drawer.drawLatexRausch(rot_1)
plt.figure()
plt.ylabel("Anzahl Messungen")
plt.xlabel("Bogenminuten in '")
plt.title("Rauschmessung, {}° + Bogenminute in Histogramm".format(rot_1.rightDegree[0]))
plt.subplots_adjust(hspace=0.0)
hist = plt.hist(rot_1.rightMinute, rasterized = True)
#plt.savefig(os.path.dirname(os.path.realpath(__file__)) + "/../Protokoll/grafiken/RauschGrafik.pdf")


#4)
#rohdaten
#raw
drawer.drawLatexMeasurement(colors)

#dispersionskurve
lambdas = []
n = []
nErrors = []
for x in colors:
    lambdas.append(x.wavelength)
    n.append(x.n)
    nErrors.append(x.nError)
x = 1/(np.array(lambdas)*10**(-9))**2
y = np.array(n)
y_err = np.array(nErrors)

dir_path = os.path.realpath(__file__)
a, ea, bq, ebq, c, ec, chiqq, corrs = nice_plots.nice_regression_plot_quad(x,y,y_err, xerror=np.zeros(1), xlabel='1/lambda^2 [1/nm^2]',
                         ylabel='n', ylabelresidue='Residuen', title='Quadratische Regression', save = dir_path + "/../../Protokoll/grafiken/QuadReg.pdf")
m, em, b, eb, chiql, corr = nice_plots.nice_regression_plot(x, y, y_err, xerror=np.zeros(1), xlabel='1/lambda^2 [1/nm^2]',
                         ylabel='n', ylabelresidue='Resiuden', title='Lineare Regression', save = dir_path + "/../../Protokoll/grafiken/LinReg.pdf")

#delta_min und n werte
drawer.drawResults(colors)

#Anpassungsrechnung zeigen
drawer.drawLinReg(m, em, b, eb, chiql, len(colors)-2)
drawer.drawQuadReg(a, ea, bq, ebq, c, ec, chiqq, len(colors)-3)

#Vergleiche
compare(colors)
plt.figure()
#xComp = np.array(x[0], x[len(x)-1])
#plt.plot(a*xComp**2 + b*xComp + c, xComp)
#plt.plot(literaturwerte.n_schott_f2(1/np.sqrt(xComp)))


#5)
#Auflösungsvermögen todo in tabelle
#Auflösungsvermögen bestimmen
dn_dlambda_gelb_1 = dn_dlambda(a, ea, bq, ebq, c, ec, gelb_1.wavelength * 10**(-9))
A_gelb_1_5 = resolution(dn_dlambda_gelb_1, 0.0015, gelb_1.minimalDegree, gelb_1.minimalDegreeError)
A_gelb_2 = resolution(dn_dlambda_gelb_1, 0.002, gelb_1.minimalDegree, gelb_1.minimalDegreeError)
print(A_gelb_1_5)
print(A_gelb_2)
delLambda = 2.11
print(gelb_1.wavelength/delLambda)
drawer.drawResolution(gelb_1, dn_dlambda_gelb_1, A_gelb_1_5, 1.5, A_gelb_2, 2)


#6)
#wellenlänge, invertierte formel um lambda bestimmen

gelb_He_r_Degree = np.array([218.5 , 218.5])
gelb_He_r_Minute = np.array([25,25])
gelb_He_l_Degree = np.array([315, 315])
gelb_He_l_Minute = np.array([18,17]) 

gelb_He = color("gelb_{He}", 587.56, gelb_He_l_Degree, gelb_He_l_Minute, gelb_He_r_Degree, gelb_He_r_Minute)
lamb, lambP, lambM = lambdaByN(a,bq,c,gelb_He.n, gelb_He.nError)

drawer.drawLatexMeasurement([gelb_He], "He")
drawer.drawResults([gelb_He], "He")
drawer.drawLambdaByN(gelb_He, lamb * 10**9, lambP * 10**9, lambM* 10**9)

#misc
