# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:59:22 2020

@author: Max_b
heavily amended by lewinb
"""

from praktikum import analyse
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['font.size'] = 24.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['figure.autolayout'] = True


def nice_regression_parameters(x, y, xerr=None, yerr=None):
    """Calculate and print a linear regression in a nice way."""
    if xerr is not None and xerr.all():
        (m, em, b, eb, chiq, corr) = analyse.lineare_regression_xy(x, y, xerr, yerr)
    else:
        (m, em, b, eb, chiq, corr) = analyse.lineare_regression(x, y, yerr)

    print('y = m x + b :: m = {:.3e} +/- {:.3e}; b = {:.3e} +/- {:.3e}; X^2/DoF = {:.3f} / {:d} = {:.3f}; Corr_m,b = {:.3f}'.format(
        m, em, b, eb, chiq, x.size-2, chiq/(x.size-2), corr))

    return (m, em, b, eb, chiq, corr)

def nice_regression_plot(x, y, yerror, xerror=np.zeros(1), xlabel='',
                         ylabel='', ylabelresidue='', title='', save=None):
    """Does a linear Regression with x (and y errors).
    x = array with x-coordinates,
    y = array with y-coordinates,
    xerror = array with errors of x,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue
    save = filename to save figure in"""
    plt.tight_layout()
    fig, axarray = plt.subplots(2, 1, figsize=(20,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    fig.tight_layout()
    axarray[1].grid(True)
    axarray[0].grid(True)
    # axarray[0].set_xlabel(xlabel)
    axarray[0].set_ylabel(ylabel)
    axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue)
   # axarray[0].set_ylim(np.amin(y) - 0.05 * np.amin(y), np.amax(y) + 0.05 * np.amax(y))

    if title:
        axarray[0].set_title(title)

    if(xerror.all()):
        axarray[0].errorbar(x, y, xerr=xerror, yerr=yerror, color='red', fmt='.',
                            marker='o', markeredgecolor='red')
    else:
        axarray[0].errorbar(x, y, yerr=yerror, color='red', fmt='.', marker='o',
                            markeredgecolor='red')

    (m,em,b,eb,chiq,corr) = nice_regression_parameters(x, y, xerror, yerror)


    axarray[0].plot(x, m*x+b, color='green')
    axarray[0].text(0.03, 0.9, 'chi^2/ndof = %g / %g = %g' % (
        chiq, x.size-2, chiq/(x.size-2)), transform=axarray[0].transAxes)

    residuals = y-(m*x+b)
    sigmaRes = np.sqrt((m*xerror)**2 + yerror**2)
    axarray[1].axhline(y=0., color='black', linestyle='--')
    axarray[1].errorbar(x, residuals, yerr=sigmaRes, color='red', fmt='.',
                        marker='o', markeredgecolor='red')

    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)
    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)

    fig.subplots_adjust(hspace=0.0)
    plt.show()
    if save:
        fig.savefig(save)

    return (m,em,b,eb,chiq,corr)

def nice_regression_parameters_quad(x, y, xerr=None, yerr=None):
    """Calculate and print a linear regression in a nice way."""
    if xerr is not None and xerr.all():
        (a, ea, b, eb, c, ec, chiq, corrs) = analyse.quadratische_regression_xy(x, y, xerr, yerr)
    else:
        (a, ea, b, eb, c, ec, chiq, corrs) = analyse.quadratische_regression(x, y, yerr)

    print('y = a x^2 + b x + c :: a = {:.3e} +/- {:.3e}; b = {:.3e} +/- {:.3e}; c = {:.3e} +/- {:.3e}; X^2/DoF = {:.3f} / {:d} = {:.3f}; Corrs = {:.3f};{:.3f};{:.3f}'.format(
        a, ea, b, eb, c, ec, chiq, x.size-3, chiq/(x.size-3), corrs[0], corrs[1], corrs[2]))

    return ( a, ea, b, eb, c, ec, chiq, corrs)

def nice_regression_plot_quad(x, y, yerror, xerror=np.zeros(1), xlabel='',
                         ylabel='', ylabelresidue='', title='', save=None):
    """Does a linear Regression with x (and y errors).
    x = array with x-coordinates,
    y = array with y-coordinates,
    xerror = array with errors of x,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue
    save = filename to save figure in"""
    plt.tight_layout()
    fig, axarray = plt.subplots(2, 1, figsize=(20,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    fig.tight_layout()
    axarray[1].grid(True)
    axarray[0].grid(True)
    # axarray[0].set_xlabel(xlabel)
    axarray[0].set_ylabel(ylabel)
    axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue)
   # axarray[0].set_ylim(np.amin(y) - 0.05 * np.amin(y), np.amax(y) + 0.05 * np.amax(y))

    if title:
        axarray[0].set_title(title)

    if(xerror.all()):
        axarray[0].errorbar(x, y, xerr=xerror, yerr=yerror, color='red', fmt='.',
                            marker='o', markeredgecolor='red')
    else:
        axarray[0].errorbar(x, y, yerr=yerror, color='red', fmt='.', marker='o',
                            markeredgecolor='red')

    (a, ea, b, eb, c, ec, chiq, corrs) = nice_regression_parameters_quad(x, y, xerror, yerror)


    axarray[0].plot(x, a*x**2 + b*x + c, color='green')
    axarray[0].text(0.03, 0.9, 'chi^2/ndof = %g / %g = %g' % (
        chiq, x.size-3, chiq/(x.size-3)), transform=axarray[0].transAxes)

    residuals = y-(a*x**2+b*x+c)
    #sigmaRes = np.sqrt((m*xerror)**2 + yerror**2)
    axarray[1].axhline(y=0., color='black', linestyle='--')
    #axarray[1].errorbar(x, residuals, yerr=sigmaRes, color='red', fmt='.',
    #                    marker='o', markeredgecolor='red')

    axarray[1].errorbar(x, residuals, yerr=yerror, color='red', fmt='.',
                        marker='o', markeredgecolor='red')

    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)
    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)

    fig.subplots_adjust(hspace=0.0)
    plt.show()
    if save:
        fig.savefig(save)

    return (a, ea, b, eb, c, ec, chiq, corrs)
