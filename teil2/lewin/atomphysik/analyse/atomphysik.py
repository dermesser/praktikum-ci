#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 30 10:34:10 2020

@author: lbo
"""

# If needed, adapt imports and/or install packages before running script.
import nice_plots
import numpy as np
import matplotlib.pyplot as plt
from praktikum import analyse, cassy
import texttable, latextable

import math
import os
from os import path
import re
from collections import namedtuple

KELVIN_OFF = 273.15


class Evaluator:

    def __init__(self, datadir, U_name='U_A1', T_name='T_B11'):
        """Initialize evaluator instance.

        datadir should contain directories for each side of the cube,
        containing <temp>.lab files (e.g. 75.lab):

            daten/
            ├── gold
            │   ├── 50.lab
            │   ├── 55.lab
            │   ├── 60.lab
            [...]

        Use as follows:
            e = Evaluator('datadir/')
            e.regression_measurements('weiss')
            e.calculate_averages()
            # etc.
        """
        # Temperatures in Kelvin!
        # All measurements are tagged with the device-specified uncertainty.
        self.Measurement = namedtuple('Measurement', ('T', 'eT', 'U', 'eU'))
        # Averaged measurements carry the sample standard deviation as eT/eU.
        self.Averaged = namedtuple('AveragedMeasurement', ('T','eT','U','eU'))
        self.sides = {}

        files = os.listdir(datadir)
        siderx = re.compile('^[\w_]+$')
        filenamerx = re.compile('(\d+)\.lab')
        for side in files:
            m = siderx.match(side)
            if not m:
                continue
            for file in os.listdir(path.join(datadir, side)):
                print(side, file)
                m = filenamerx.match(file)
                if not m or len(m.groups()) != 1:
                    continue
                data = cassy.CassyDaten(path.join(datadir, side, file))
                U = data.messung(1).datenreihe(U_name).werte
                T = data.messung(1).datenreihe(T_name).werte

                # Calculate errors
                if np.max(np.abs(U)) > 10:
                    eU = np.full_like(U, 0.15)
                    eU += 0.01*U
                else:
                    eU = np.full_like(U, 0.005)
                    eU += 0.01*U
                if np.max(np.abs(T)) > (70+KELVIN_OFF):
                    eT = np.full_like(T, 0.4)
                else:
                    eT = np.full_like(T, 0.2)

                self.sides.setdefault(side, {})
                self.sides[side][int(m.group(1))] = self.Measurement(T=T, eT=eT, U=U, eU=eU)

            # TODO

    def fakeinit(self):
        """Initialize with fake data"""
        self.sides = {
            'black': {
                50: self.Measurement(T=np.array([50.,50.1,50.1,49.9,50]),
                                     U=np.array([0.11, 0.10, 0.104, 0.103, 0.10])),
                55: self.Measurement(T=np.array([55.,55.1,55.1,54.9,55]),
                                     U=np.array([0.21, 0.20, 0.204, 0.203, 0.20])),
                },
            'white': {
                50: self.Measurement(T=np.array([50.,50.1,50.1,49.9,50]),
                                     U=np.array([0.11, 0.10, 0.104, 0.103, 0.10])),
                55: self.Measurement(T=np.array([55.,55.1,55.1,54.9,55]),
                                     U=np.array([0.21, 0.20, 0.204, 0.203, 0.20])),
                },
            }

    def regression_measurements(self, side, voltage_ampl=1e4):
        """Calculate and plot regressions for given side.


        Parameters
        ----------
        side : TYPE
            DESCRIPTION.
        voltage_ampl : TYPE, optional
            DESCRIPTION. The default is 1e4.

        Returns
        -------
        None.

        """
        assert side in self.sides
        data = self.sides[side]

        all_temps = []
        all_env_temps = []
        all_volts = []
        all_etemps = []
        all_eenv_temps = []
        all_evolts = []
        [(all_temps.extend(list(m.T)), all_volts.extend(list(m.U)),
          all_etemps.extend(list(m.eT)), all_evolts.extend(list(m.eU)))
         for m in data.values()]
        [(all_env_temps.extend(m.T), all_eenv_temps.extend(m.eT))
         for m in self.sides['umgebung'].values()]

        # Largest common number of elements
        LCN = 990

        all_env_temps = np.array(all_env_temps)[0:LCN]
        all_eenv_temps = np.array(all_eenv_temps)[0:LCN]
        all_temps = np.array(all_temps)[0:LCN]
        all_etemps = np.array(all_etemps)[0:LCN]
        all_volts = (np.array(all_volts) / voltage_ampl)[0:LCN]
        all_evolts = (np.array(all_evolts) / voltage_ampl)[0:LCN]

        all_delta_temps = all_temps**4 - all_env_temps**4
        all_delta_etemps = np.sqrt(
            (4 * all_temps**3 * all_etemps)**2 +
            (4 * all_env_temps**3 * all_eenv_temps)**2)

        (a, ea, b, eb, chiq, corr) = nice_plots.nice_regression_plot(
            all_delta_temps, all_volts, all_evolts, xerror=all_delta_etemps, xlabel='Δ(T^4) / K^4',
            ylabel='U(Thermosäule) / V', title=side, save=path.join('../img/', side+'_regr.pdf'))

        # Attempt calculating emission coefficient
        A_s=9.62e-4
        eA_s=2.75e-5
        A_e=4.15e-4
        eA_e=1.8e-5
        sigma=5.670373e-8
        r=0.109
        e_r=0.001
        sensitivity=0.276
        esensitivity = 0.0083

        emission_coeff = a*math.pi*r**2 / (sensitivity*A_s*sigma*A_e)
        eemission_coeff = emission_coeff * math.sqrt(
            (2*e_r/r)**2 + (esensitivity/sensitivity)**2 +
            (eA_s/A_s)**2 + (eA_e/A_e)**2 +
            (ea/a)**2)
        print(side, ':: emission', emission_coeff, '+/-', eemission_coeff)


    def regression_parameter_table(self, voltage_ampl=1e4):
        """Print a table-like representation of all regression parameters.

        Parameters
        ----------
        voltage_ampl : TYPE, optional
            DESCRIPTION. The default is 1e4.

        Returns
        -------
        None.

        """
        for side, data in self.sides.items():
            if side == 'umgebung':
                continue
            print(side.title())
            all_temps = []
            all_env_temps = []
            all_volts = []
            all_etemps = []
            all_eenv_temps = []
            all_evolts = []
            [(all_temps.extend(list(m.T)), all_volts.extend(list(m.U)),
              all_etemps.extend(list(m.eT)), all_evolts.extend(list(m.eU)))
             for m in data.values()]
            [(all_env_temps.extend(m.T), all_eenv_temps.extend(m.eT))
             for m in self.sides['umgebung'].values()]

            # Largest common number of elements
            LCN = 990

            all_env_temps = np.array(all_env_temps)[0:LCN]
            all_eenv_temps = np.array(all_eenv_temps)[0:LCN]
            all_temps = np.array(all_temps)[0:LCN]
            all_etemps = np.array(all_etemps)[0:LCN]
            all_volts = (np.array(all_volts) / voltage_ampl)[0:LCN]
            all_evolts = (np.array(all_evolts) / voltage_ampl)[0:LCN]

            all_delta_temps = all_temps**4 - all_env_temps**4
            all_delta_etemps = np.sqrt(
                (4 * all_temps**3 * all_etemps)**2 +
                (4 * all_env_temps**3 * all_eenv_temps)**2)

            (a, ea, b, eb, chiq, corr) = analyse.lineare_regression_xy(
                all_delta_temps, all_volts, all_delta_etemps, all_evolts)
            print('>> Regression:', (a, ea, b, eb, chiq, corr), 'Chi^2/DoF = ',
                  chiq/(all_volts.size-2))
            # Decorrelate
            (da, dea, db, deb, dchiq, dcorr) = analyse.lineare_regression_xy(
                all_delta_temps-np.mean(all_delta_temps), all_volts, all_delta_etemps, all_evolts)
            print('>> Decorrelated:', (da, dea, db, deb, dchiq, dcorr))


            # Shift X
            (sxa, sxea, sxb, sxeb, sxchiq, sxcorr) = analyse.lineare_regression_xy(
                all_delta_temps-all_delta_etemps, all_volts, all_delta_etemps, all_evolts)
            (sx2a, sx2ea, sx2b, sx2eb, sx2chiq, sx2corr) = analyse.lineare_regression_xy(
                all_delta_temps+all_delta_etemps, all_volts, all_delta_etemps, all_evolts)
            print('Systematic errors, temp shift: s.e. m = {:.3e} s.e. b = {:.3e}'.format(
                (sx2a-sxa)/2, (sx2b-sxb)/2))

            # Shift Y
            (sya, syea, syb, syeb, sychiq, sycorr) = analyse.lineare_regression_xy(
                all_delta_temps, all_volts-all_evolts, all_delta_etemps, all_evolts)
            (sy2a, sy2ea, sy2b, sy2eb, sy2chiq, sy2corr) = analyse.lineare_regression_xy(
                all_delta_temps, all_volts+all_evolts, all_delta_etemps, all_evolts)
            print('Systematic errors, voltage shift: s.e. m = {:.3e} s.e. b = {:.3e}'.format(
                (sy2a-sya)/2, (sy2b-syb)/2))

            print('>> Total systematic errors: s.e. m = {:.3e}; s.e. b = {:.3e}'
                  .format(math.sqrt(((sx2a-sxa)/2)**2 + ((sy2a-sya)/2)**2),
                          math.sqrt(((sx2b-sxb)/2)**2 + ((sy2b-syb)/2)**2)))


    def calculate_averages(self, voltage_ampl=1e4):
        """Returns a structure similar to self.sides, but with self.Averaged instead of self.Measurement."""
        averages = {}
        for side, measures in self.sides.items():
            averages[side] = {}
            for temp, measure in measures.items():
                avg_temp = measure.T.mean()
                std_temp = measure.T.var()**(1/2) / math.sqrt(measure.T.size)
                avg_volt = measure.U.mean() / voltage_ampl
                std_volt = measure.U.var()**(1/2) / voltage_ampl / math.sqrt(measure.T.size)
                avgd = self.Averaged(avg_temp, std_temp, avg_volt, std_volt)
                averages[side][temp] = avgd

        return averages

    def latex_uncertainty(self, val, unc, sysunc=None):
        """Print value with uncertainties in latex format.


        Parameters
        ----------
        val : TYPE
            DESCRIPTION.
        unc : TYPE
            DESCRIPTION.
        sysunc : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        if sysunc is None:
            return '$\\num{{ {:.2e} }} \\pm \\num{{ {:.1e} }}$'.format(val, unc)
        return '$\\num{{ {:.2e} }} \\pm \\num{{ {:.1e} }} \\mathrm{{ ~stat. }} \\pm \\num{{ {:.1e} }} \\mathrm{{ ~sys. }}$'.format(val, unc, sysunc)

    def latex_uncertainty3(self, val, unc, sysunc=None):
        """Print value with uncertainties in latex format, with 3 digits.


        Parameters
        ----------
        val : TYPE
            DESCRIPTION.
        unc : TYPE
            DESCRIPTION.
        sysunc : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        if sysunc is None:
            return '$\\num{{ {:.3e} }} \\pm \\num{{ {:.2e} }}$'.format(val, unc)
        return '$\\num{{ {:.3e} }} \\pm \\num{{ {:.2e} }} \\mathrm{{ ~stat. }} \\pm \\num{{ {:.2e} }} \\mathrm{{ ~sys. }}$'.format(val, unc, sysunc)


    def print_averages(self, voltage_ampl=1e4):
        """Format all measurement averages as Latex table."""
        # Depends on dict iteration order!!!
        averages = self.calculate_averages()

        def u_sys_uncert(u):
            umax = 10/voltage_ampl if u < 10/voltage_ampl else 30/voltage_ampl
            return 0.01*u+0.005*umax

        for side in averages:
            sidelines = []
            for temp, measure in averages[side].items():
                sideline = []
                sideline.append('{}'.format(temp))
                sideline.append(self.latex_uncertainty3(averages['umgebung'][temp].T, averages['umgebung'][temp].eT))
                sideline.append(self.latex_uncertainty3(measure.T, measure.eT))
                sideline.append(self.latex_uncertainty(measure.U, measure.eU, u_sys_uncert(measure.U)))
                sidelines.append(sideline)

            topline = ['Temp. / \\si{\\celsius}', 'Umgebungstemp. / K', '$\\bar T$/K', '$\\bar U_{TS} /$V']
            sidelines.reverse()
            sidelines.insert(0, topline)
            tt = texttable.Texttable()
            tt.set_cols_align(['c']*len(topline))
            tt.set_deco(tt.HEADER|tt.VLINES)
            tt.set_precision(2)
            tt.add_rows(sidelines)
            print(latextable.draw_latex(
                tt, label='tab:average_{}'.format(side),
                caption='Gemittelte Messungen, Seite {}'.format(side.title())))


    def calculate_emission(self, side, sensitivity=0.276, std_sensitivity=0.0083,
                           A_s=9.62e-4, eA_s=2.75e-5, A_e=4.1548e-4, eA_e=1.8e-5,
                           r=0.109, e_r=0.001, sigma=5.670373e-8):
        """Calculate emission coefficient and power values, and format as Latex tables.

        Parameters
        ----------
        side : TYPE
            DESCRIPTION.
        sensitivity : TYPE, optional
            DESCRIPTION. The default is 0.276.
        std_sensitivity : TYPE, optional
            DESCRIPTION. The default is 0.0083.
        A_s : TYPE, optional
            DESCRIPTION. The default is 9.62e-4.
        eA_s : TYPE, optional
            DESCRIPTION. The default is 2.75e-5.
        A_e : TYPE, optional
            DESCRIPTION. The default is 4.1548e-4.
        eA_e : TYPE, optional
            DESCRIPTION. The default is 1.8e-5.
        r : TYPE, optional
            DESCRIPTION. The default is 0.109.
        e_r : TYPE, optional
            DESCRIPTION. The default is 0.001.
        sigma : TYPE, optional
            DESCRIPTION. The default is 5.670373e-8.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        averages = self.calculate_averages()

        table_lines = []

        emissions = []
        eemissions = []

        for temp in sorted(list(averages[side].keys()), reverse=True):
            measure = averages[side][temp]
            T0 = averages['umgebung'][temp].T
            eT0 = averages['umgebung'][temp].eT
            T = measure.T
            eT = measure.eT

            # Sensitivity uncertainty is systematic, not used here
            power_actual = measure.U / sensitivity
            std_power_actual = measure.eU/sensitivity

            power_ideal = A_s * A_e * sigma * (T**4 - T0**4)/(r**2 * math.pi)
            std_power_ideal = power_ideal * math.sqrt(
                eA_s**2/A_s**2 + eA_e**2/A_e**2 + 4*e_r**2/r**2+
                ((4*T**3*eT)**2+(4*T0**3*eT0)**2)/(T**4-T0**4)**2)

            emission_coeff = power_actual/power_ideal
            std_emission_coeff = math.sqrt(emission_coeff**2 * (
                (std_power_actual/power_actual)**2 + (std_power_ideal/power_ideal)**2))

            # Systematic uncertainties
            def u_sys_uncert(u):
                umax = 10e-4 if u < 10e-4 else 30e-4
                return 0.01*u+0.005*umax

            u_sys = u_sys_uncert(measure.U)
            sys_power_actual = power_actual * math.sqrt(
                (u_sys/measure.U)**2 + (std_sensitivity/sensitivity)**2)
            sys_emission_coeff = sys_power_actual/power_ideal

            emissions.append(emission_coeff)
            eemissions.append(std_emission_coeff)

            #print("Temperature: {:d} :: Ideal Em.: {:f} +/- {:f} :: Real Em.: {:f} +/- {:f} :: Emission coefficient: {:.3f} +/- {:f}".format(
            #    temp, power_ideal, std_power_ideal, power_actual, std_power_actual,
            #    emission_coeff, std_emission_coeff))
            table_lines.append([str(temp), self.latex_uncertainty(power_ideal, std_power_ideal),
                                self.latex_uncertainty(power_actual, std_power_actual, sysunc=sys_power_actual),
                                '$\\num{{ {:.3f} }} \\pm \\num{{ {:.3f} }} \\mathrm{{ ~stat. }} \\pm \\num{{ {:.3f} }} \\mathrm{{ ~sys. }}$'
                                .format(emission_coeff, std_emission_coeff, sys_emission_coeff)])

        (emission_mean, eemission_mean) = analyse.gewichtetes_mittel(np.array(emissions), np.array(eemissions))

        tt = texttable.Texttable()
        tt.set_cols_align(['c']*4)
        tt.set_deco(tt.HEADER|tt.VLINES)
        tt.set_precision(2)

        table_lines.reverse()
        tt.add_rows([['T / \\si{\\celsius}', '$P_{ideal} / \\si{\\watt}$', '$P_{gemessen} / \\si{\\watt}$', '$\\varepsilon$']] + table_lines)
        print(latextable.draw_latex(
            tt, label='tab:emission_{}'.format(side),
            caption='Strahlungsdaten für {}. Gewichtetes Mittel $\\varepsilon = \\num{{ {:.3f} }} \pm \\num{{ {:.3f} }}$'
            .format(side.title(), emission_mean, eemission_mean)))
