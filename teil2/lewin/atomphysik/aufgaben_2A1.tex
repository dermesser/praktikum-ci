\section{2A1 Temperaturstrahlung}

Bei Plots und Bildern können Details durch Zoomen besser erkannt werden. Dadurch wird das
Layout übersichtlicher.

\begin{aufgabe}{Grundlagen}
  Knappe Beschreibung der theoretischen Grundlagen, Angabe der
  benötigten Formel(n), ohne Herleitung. Definition der verwendeten
  Formelzeichen.
\end{aufgabe}

Jeder Gegenstand, der wärmer als der absolute Nullpunkt ist, sendet ein
kontinuierliches Spektrum elektromagnetischer
Strahlung aus. Der Idealfall der Strahlungsemission und -absorption ist der Schwarze
Strahler: Dieser absorbiert Strahlung jeder Wellenlänge komplett (Absorptionskoeffizient
$\alpha = 1$). Reale Objekte sind nie schwarze Strahler, sondern graue Strahler;
allerdings können Gegenstände einen Absorptionskoeffizient sehr nahe 1 besitzen, und in
der Näherung als Schwarze Strahler behandelt werden.

Die spektrale Leistungsdichte $E_{\lambda,s}$ (in \si{\watt\per\meter\cubed}) eines
solchen Schwarzen Strahlers bei einer Temperatur $T$ wird durch das Plancksche
Strahlungsgesetz beschrieben (mit dem Planckschen Wirkungsquantum $h$, der
Boltzmannkonstante $k$, der Wellenlänge der Strahlung $\lambda$, und der
Vakuum-Lichtgeschwindigkeit $c$):

\begin{equation}
 E_{\lambda,s}(\lambda, T) = \frac{2 \pi h c^2}{\lambda^5} \frac{1}{e^{\frac{h
c}{\lambda k T}} - 1}
\end{equation}

Für jeden Gegenstand mit einem temperatur- und wellenlängenabhängigen
Absorptionskoeffizienten $\alpha_\lambda,~0 < \alpha_\lambda < 1$ gilt dann:

\begin{equation}
 E_\lambda(\lambda, T) / \alpha_\lambda(\lambda, T) = E_{\lambda, s}(\lambda, T)
\end{equation}

Nach dem Kirchhoffschen Gesetz gilt, dass der Emissionskoeffizient $\varepsilon$ gleich
dem Absorptionskoeffizienten $\alpha$ ist. Für die Messung hier ist $\varepsilon$
interessant, und wichtig, dass der Sensor einen Absorptionskoeffizienten von etwa $\alpha
\approx 1$ hat (obwohl dieser Wert in der Kalibration bestimmt wird).

Die Gesamt-Strahldichte eines Strahlers lässt sich schließlich durch Integration des
Planckschen Strahlungsgesetzes berechnen, sie beträgt

\begin{equation}
 E_s'(T) = \frac{\sigma}{\pi} T^4, \quad [E_s'] =
\si{\watt\per\meter\squared\per\steradian}, \quad \sigma = \frac{2 \pi^5 k^4}{15 h^3 c^2}
= \SI{5,67e-8}{\watt\per\meter\squared\per\kelvin\tothe4}
\end{equation}

Bei der Messung der abgestrahlten Leistung eines Gegenstands ist zu beachten, dass der
Sensor im Strahlungsgleichgewicht mit dem zu messenden Gegenstand steht, und die
eingestrahlte zu messende Leistung somit von der Differenz zwischen Sensortemperatur
$T_0$ und Temperatur $T$ (bzw. $T_{gemessen}$) des Gegenstands abhängt. Außerdem gilt für
nicht-Schwarze Strahler, dass der Emissionskoeffizient $\varepsilon < 1$ ist. Die
gemessene Leistung $P$ folgt also

\begin{equation}
 P = \varepsilon \frac{\sigma \Omega A_s}{\pi} (T^4 - T_0^4)
\end{equation}

mit der "`Senderfläche"' $A_s$ und dem Raumwinkel $\Omega$ des Sensors der Fläche $A_e$:

\begin{equation}
\label{eqn:strahlleistung}
 P = \varepsilon \frac{\sigma A_s A_e}{\pi r^2} (T^4 - T_0^4)
\end{equation}

Damit ist auch ersichtlich, dass $\varepsilon = P_{gemessen} / P_{ideal}$ ist.

Die Grundlagen für die Auswertung der Messdaten sind im Abschnitt 4 angegeben.

\clearpage
\FloatBarrier

\begin{aufgabe}{Versuchsaufbau und Versuchsdurchführung}
  Beschreibung des Versuchsaufbaus einschließlich beschrifteter Skizze
  oder Foto. Beschreibung der Versuchsdurchführung: Handgriffe an der
  Apparatur, verwendete Messwerterfassungseinstellungen, Messbereiche,
  etc.
\end{aufgabe}

\begin{figure}[p]
 \centering
%  \includegraphics[]{example-image-a}
 \includegraphics[scale=0.35]{img/seite_ann.png}
 % oben.jpg: 4032x3024 px, 300dpi, 34.14x25.60 cm, bb=0 0 968 726
 \caption{Versuchsaufbau von der Seite}
 \label{fig:seite}
\end{figure}
\begin{figure}[p]
 \centering
 \includegraphics[scale=0.35]{img/oben_ann.png}
 % oben.jpg: 4032x3024 px, 300dpi, 34.14x25.60 cm, bb=0 0 968 726
 \caption{Versuchsaufbau von oben}
 \label{fig:oben}
\end{figure}
\begin{figure}[h]
 \centering
 \includegraphics[scale=0.5]{img/skizze.pdf}
 \caption{Verschaltungsskizze}
 \label{fig:skizze}
\end{figure}


Im Zentrum des Aufbaus (\autoref{fig:seite}) steht der Leslie-Würfel mit seinen vier
goldenen/silbernen/weißen/schwarzen Oberflächen. Er ist mit Wasser gefüllt, und steht auf
einer Heiz-Rühr-Platte. Im Wasser hängt ein Thermometer, das mit dem CASSY-System
verbunden ist. Die Heizplatte kann eine Temperatur erreichen und mittels eines
Thermostats auch einigermaßen halten.

Neben dem Würfel steht auf einem auf einer Schiene laufenden Gestell die Thermosäule, die
hinter einem Schutzrohr von $\SI{15}{\centi\meter}$ verborgen ist. Die Anschlüsse sind
mit einem Verstärker verbunden, dessen Ausgänge wiederum mit dem CASSY-Aufnehmer
verbunden sind (\autoref{fig:skizze}), und eine Spannung proportional zur thermischen
Strahlungsaufnahme liefert. Der Verstärker wird auf eine Verstärkung von $10^4$
eingestellt, in den \textit{Low Drift}-Modus versetzt, sowie eine Zeitkonstante von
\SI{0.1}{\second} eingestellt. Dann wird mit dem Nullpunktregler die Spannung verändert,
bis CASSY \SI{0}{\volt} anzeigt.

CASSY wird wie folgt konfiguriert:

\begin{description}
 \item[Spannung (Thermosäule) \texttt{U\_A1}] \texttt{-10V..10V} mit Verstärkung $10^4$ am
Spannungsverstärker, solange möglich; bei höheren Spannungen \texttt{-30V..30V}
(Verstärker dann auf $10^3$) mit Korrektur $+\SI{0.6}{\volt}$ (da der Spannungsverstärker
beim Umschalten einen Offset aufweist)
 \item[Temperatur (Thermometer) \texttt{T\_B11}] \texttt{-20C..120C}
 \item[Messwerterfassung] \num{125} Werte über $\SI{6,25}{\second}$ aufgezeichnet
(Intervall $\SI{50}{\milli\second}$)
\end{description}

Die Thermosäule selbst durfte nicht (länger) angefasst werden, sodass sie die
Strahlungsleistung korrekt messen konnte. Vor dem Versuch mussten wir das Schutzglas auf
dem Rohr entfernen, und das Rohr bis zum Stiel schieben. Wir haben die Seriennummer
notiert -- 130803 --, und daraus nach Skript die Sensitivität
$\SI{0.276(009)}{\volt\per\watt}$ bestimmt. Im Folgenden ist dann die
Thermosäulensensitivität als $c_{TS} := 0,276$ definiert.

Nach der Messung der Umgebungstemperatur haben wir das Thermometer ohne Bodenberührung in
das Wasser im Würfel gehängt. Auf der Heizplatte haben wir das Wasser auf
$\SI{50}{\celsius}$ erhitzt, und nach dem folgenden Schritt jeweils um $\SI{5}{\kelvin}$
weiter bis auf $\SI{95}{\celsius}$ gebracht.

Zur Messung haben wir die Thermosäule zügig in einem kleinen Abstand (etwa 1
Millimeter) auf den Würfel gerichtet, der möglichst nicht vom Rohr berührt wird (Abstand
$\approx \SI{1(1)}{\milli\meter}$). Die gemessene Spannung benötigte 15 bis 25 Sekunden,
um sich halbwegs zu stabilisieren; dann haben wir jeweils eine Messreihe gestartet. Die
anderen drei Seiten des Würfels haben wir danach analog gemessen, bevor die Temperatur
weiter erhöht wurde. Die Messreihen haben wir einzeln abgespeichert: Je eine pro Seite
und Temperatur.

Nach der Messung jeder Seite haben wir die Thermosäule zum Abkühlen um 180\textdegree
gegen die Wand gedreht. In diesem Versuch kehrte die Spannung an der Thermosäule jedes
Mal bis auf $\SI{0}{\volt}$ zurück. Ab einer gewissen Temperatur, hier \SI{70}{\celsius},
mussten wir den Spannungsverstärker auf $10^3$ herunterregeln. Das CASSY haben wir dann
so eingestellt, dass die gemessenen Werte gleich blieben, d.h. die Eingangsspannung 10x
verstärkt wurde. Außerdem mussten wir einen Offset von \SI{-0.6}{\volt} korrigieren, der
vom Spannungsverstärker nach dem Umschalten der Verstärkung angelegt wurde.

Während jeder Heizphase haben wir erneut die Umgebungstemperatur gemessen. Aus
Zeitgründen mussten wir gegen Ende hin einige wenige Messreihen (Temperaturstufen)
auslassen.

\clearpage
\FloatBarrier

\begin{aufgabe}{Rohdaten}
  Ermitteln Sie aus Ihren Rohdaten für alle vier Oberflächen des
  Leslie-Würfels die mittlere Spannung samt Unsicherheit und
  tabellieren Sie diese.
\end{aufgabe}

Für die weitere Verwendung werden hier für jede Seite und jeden Temperaturschritt die
mittlere Spannung berechnet und die Standardabweichung aus der Stichprobe ermittelt.
Diese Daten sind für die Messingseite in \autoref{tab:average_gold} tabelliert, für die
schwarze Seite in \autoref{tab:average_schwarz}, für die silberne Seite in
\autoref{tab:average_silber}, und für die weiße Seite in \autoref{tab:average_weiss}.

Zusätzlich sind jeweils die Umgebungstemperaturen angegeben, bei denen die Messung
stattfand. Diese wurden vor jeder neuen Temperaturstufe gemessen und bei allen Tabellen
gleich. Außerdem ist zu beachten, dass die erste Spalte nur die ungefähre Temperatur des
Wasserbads zur Orientierung enthält, die eigentliche Temperaturen (von CASSY gemessen)
stehen in der dritten Spalte.

Die systematische Unsicherheit der Spannung beträgt wie für unser CASSY-System üblich
$1\,\% + 0,5\,\% \cdot U_{max}$.

\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			Temp. / \si{\celsius} & Umgebungstemp. / K & $\bar T$/K & $\bar U_{TS} /$V \\
			\midrule
			50 & $\num{ 2.937e+02 } \pm \num{ 4.42e-03 }$ & $\num{ 3.205e+02 } \pm \num{
3.66e-03 }$ & $\num{ 1.07e-04 } \pm \num{ 7.3e-08 } \mathrm{ ~stat. } \pm \num{ 6.1e-06 }
\mathrm{ ~sys. }$ \\
			55 & $\num{ 2.945e+02 } \pm \num{ 4.97e-03 }$ & $\num{ 3.258e+02 } \pm \num{
3.39e-03 }$ & $\num{ 1.44e-04 } \pm \num{ 1.7e-07 } \mathrm{ ~stat. } \pm \num{ 6.4e-06 }
\mathrm{ ~sys. }$ \\
			60 & $\num{ 2.942e+02 } \pm \num{ 4.32e-03 }$ & $\num{ 3.316e+02 } \pm \num{
4.66e-03 }$ & $\num{ 1.55e-04 } \pm \num{ 3.1e-07 } \mathrm{ ~stat. } \pm \num{ 6.5e-06 }
\mathrm{ ~sys. }$ \\
			65 & $\num{ 2.945e+02 } \pm \num{ 4.21e-03 }$ & $\num{ 3.363e+02 } \pm \num{
4.39e-03 }$ & $\num{ 2.09e-04 } \pm \num{ 2.2e-07 } \mathrm{ ~stat. } \pm \num{ 7.1e-06 }
\mathrm{ ~sys. }$ \\
			70 & $\num{ 2.944e+02 } \pm \num{ 5.73e-03 }$ & $\num{ 3.413e+02 } \pm \num{
5.09e-03 }$ & $\num{ 2.40e-04 } \pm \num{ 2.7e-07 } \mathrm{ ~stat. } \pm \num{ 7.4e-06 }
\mathrm{ ~sys. }$ \\
			75 & $\num{ 2.939e+02 } \pm \num{ 3.32e-03 }$ & $\num{ 3.458e+02 } \pm \num{
4.17e-03 }$ & $\num{ 2.42e-04 } \pm \num{ 1.8e-07 } \mathrm{ ~stat. } \pm \num{ 7.4e-06 }
\mathrm{ ~sys. }$ \\
			80 & $\num{ 2.938e+02 } \pm \num{ 5.54e-03 }$ & $\num{ 3.514e+02 } \pm \num{
4.69e-03 }$ & $\num{ 3.25e-04 } \pm \num{ 2.2e-07 } \mathrm{ ~stat. } \pm \num{ 8.2e-06 }
\mathrm{ ~sys. }$ \\
			90 & $\num{ 2.945e+02 } \pm \num{ 4.64e-03 }$ & $\num{ 3.639e+02 } \pm \num{
3.52e-03 }$ & $\num{ 3.51e-04 } \pm \num{ 2.2e-07 } \mathrm{ ~stat. } \pm \num{ 8.5e-06 }
\mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Gemittelte Messungen, Seite Gold}
	\label{tab:average_gold}
\end{table}
\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			Temp. / \si{\celsius} & Umgebungstemp. / K & $\bar T$/K & $\bar U_{TS} /$V \\
			\midrule
			50 & $\num{ 2.937e+02 } \pm \num{ 4.42e-03 }$ & $\num{ 3.210e+02 } \pm \num{
2.94e-03 }$ & $\num{ 4.66e-04 } \pm \num{ 2.2e-07 } \mathrm{ ~stat. } \pm \num{ 9.7e-06 }
\mathrm{ ~sys. }$ \\
			55 & $\num{ 2.945e+02 } \pm \num{ 4.97e-03 }$ & $\num{ 3.261e+02 } \pm \num{
4.36e-03 }$ & $\num{ 5.62e-04 } \pm \num{ 2.7e-07 } \mathrm{ ~stat. } \pm \num{ 1.1e-05 }
\mathrm{ ~sys. }$ \\
			60 & $\num{ 2.942e+02 } \pm \num{ 4.32e-03 }$ & $\num{ 3.304e+02 } \pm \num{
5.12e-03 }$ & $\num{ 6.36e-04 } \pm \num{ 5.4e-07 } \mathrm{ ~stat. } \pm \num{ 1.1e-05 }
\mathrm{ ~sys. }$ \\
			65 & $\num{ 2.945e+02 } \pm \num{ 4.21e-03 }$ & $\num{ 3.365e+02 } \pm \num{
5.65e-03 }$ & $\num{ 7.84e-04 } \pm \num{ 4.1e-07 } \mathrm{ ~stat. } \pm \num{ 1.3e-05 }
\mathrm{ ~sys. }$ \\
			70 & $\num{ 2.944e+02 } \pm \num{ 5.73e-03 }$ & $\num{ 3.410e+02 } \pm \num{
4.36e-03 }$ & $\num{ 9.03e-04 } \pm \num{ 1.8e-07 } \mathrm{ ~stat. } \pm \num{ 1.4e-05 }
\mathrm{ ~sys. }$ \\
			75 & $\num{ 2.939e+02 } \pm \num{ 3.32e-03 }$ & $\num{ 3.461e+02 } \pm \num{
4.58e-03 }$ & $\num{ 1.04e-03 } \pm \num{ 1.7e-07 } \mathrm{ ~stat. } \pm \num{ 2.5e-05 }
\mathrm{ ~sys. }$ \\
			80 & $\num{ 2.938e+02 } \pm \num{ 5.54e-03 }$ & $\num{ 3.514e+02 } \pm \num{
3.38e-03 }$ & $\num{ 1.17e-03 } \pm \num{ 6.3e-07 } \mathrm{ ~stat. } \pm \num{ 2.7e-05 }
\mathrm{ ~sys. }$ \\
			90 & $\num{ 2.945e+02 } \pm \num{ 4.64e-03 }$ & $\num{ 3.612e+02 } \pm \num{
6.71e-03 }$ & $\num{ 1.41e-03 } \pm \num{ 7.2e-07 } \mathrm{ ~stat. } \pm \num{ 2.9e-05 }
\mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Gemittelte Messungen, Seite Schwarz}
	\label{tab:average_schwarz}
\end{table}
\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			Temp. / \si{\celsius} & Umgebungstemp. / K & $\bar T$/K & $\bar U_{TS} /$V \\
			\midrule
			50 & $\num{ 2.937e+02 } \pm \num{ 4.42e-03 }$ & $\num{ 3.213e+02 } \pm \num{
3.85e-03 }$ & $\num{ 3.06e-05 } \pm \num{ 6.1e-08 } \mathrm{ ~stat. } \pm \num{ 5.3e-06 }
\mathrm{ ~sys. }$ \\
			55 & $\num{ 2.945e+02 } \pm \num{ 4.97e-03 }$ & $\num{ 3.261e+02 } \pm \num{
3.51e-03 }$ & $\num{ 3.63e-05 } \pm \num{ 1.1e-07 } \mathrm{ ~stat. } \pm \num{ 5.4e-06 }
\mathrm{ ~sys. }$ \\
			60 & $\num{ 2.942e+02 } \pm \num{ 4.32e-03 }$ & $\num{ 3.308e+02 } \pm \num{
4.42e-03 }$ & $\num{ 5.86e-05 } \pm \num{ 1.2e-07 } \mathrm{ ~stat. } \pm \num{ 5.6e-06 }
\mathrm{ ~sys. }$ \\
			65 & $\num{ 2.945e+02 } \pm \num{ 4.21e-03 }$ & $\num{ 3.361e+02 } \pm \num{
3.25e-03 }$ & $\num{ 5.30e-05 } \pm \num{ 2.3e-07 } \mathrm{ ~stat. } \pm \num{ 5.5e-06 }
\mathrm{ ~sys. }$ \\
			70 & $\num{ 2.944e+02 } \pm \num{ 5.73e-03 }$ & $\num{ 3.414e+02 } \pm \num{
4.83e-03 }$ & $\num{ 6.05e-05 } \pm \num{ 1.7e-07 } \mathrm{ ~stat. } \pm \num{ 5.6e-06 }
\mathrm{ ~sys. }$ \\
			75 & $\num{ 2.939e+02 } \pm \num{ 3.32e-03 }$ & $\num{ 3.464e+02 } \pm \num{
5.01e-03 }$ & $\num{ 6.01e-05 } \pm \num{ 2.0e-07 } \mathrm{ ~stat. } \pm \num{ 5.6e-06 }
\mathrm{ ~sys. }$ \\
			80 & $\num{ 2.938e+02 } \pm \num{ 5.54e-03 }$ & $\num{ 3.517e+02 } \pm \num{
4.05e-03 }$ & $\num{ 9.43e-05 } \pm \num{ 2.9e-07 } \mathrm{ ~stat. } \pm \num{ 5.9e-06 }
\mathrm{ ~sys. }$ \\
			90 & $\num{ 2.945e+02 } \pm \num{ 4.64e-03 }$ & $\num{ 3.635e+02 } \pm \num{
5.38e-03 }$ & $\num{ 1.24e-04 } \pm \num{ 1.8e-07 } \mathrm{ ~stat. } \pm \num{ 6.2e-06 }
\mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Gemittelte Messungen, Seite Silber}
	\label{tab:average_silber}
\end{table}
\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			Temp. / \si{\celsius} & Umgebungstemp. / K & $\bar T$/K & $\bar U_{TS} /$V \\
			\midrule
			50 & $\num{ 2.937e+02 } \pm \num{ 4.42e-03 }$ & $\num{ 3.204e+02 } \pm \num{
5.75e-03 }$ & $\num{ 4.53e-04 } \pm \num{ 1.1e-07 } \mathrm{ ~stat. } \pm \num{ 9.5e-06 }
\mathrm{ ~sys. }$ \\
			55 & $\num{ 2.945e+02 } \pm \num{ 4.97e-03 }$ & $\num{ 3.264e+02 } \pm \num{
3.58e-03 }$ & $\num{ 5.67e-04 } \pm \num{ 2.2e-07 } \mathrm{ ~stat. } \pm \num{ 1.1e-05 }
\mathrm{ ~sys. }$ \\
			60 & $\num{ 2.942e+02 } \pm \num{ 4.32e-03 }$ & $\num{ 3.311e+02 } \pm \num{
4.41e-03 }$ & $\num{ 6.54e-04 } \pm \num{ 3.6e-07 } \mathrm{ ~stat. } \pm \num{ 1.2e-05 }
\mathrm{ ~sys. }$ \\
			65 & $\num{ 2.945e+02 } \pm \num{ 4.21e-03 }$ & $\num{ 3.363e+02 } \pm \num{
4.22e-03 }$ & $\num{ 7.64e-04 } \pm \num{ 4.7e-07 } \mathrm{ ~stat. } \pm \num{ 1.3e-05 }
\mathrm{ ~sys. }$ \\
			70 & $\num{ 2.944e+02 } \pm \num{ 5.73e-03 }$ & $\num{ 3.417e+02 } \pm \num{
4.51e-03 }$ & $\num{ 8.89e-04 } \pm \num{ 4.2e-07 } \mathrm{ ~stat. } \pm \num{ 1.4e-05 }
\mathrm{ ~sys. }$ \\
			75 & $\num{ 2.939e+02 } \pm \num{ 3.32e-03 }$ & $\num{ 3.467e+02 } \pm \num{
3.79e-03 }$ & $\num{ 1.02e-03 } \pm \num{ 5.9e-07 } \mathrm{ ~stat. } \pm \num{ 2.5e-05 }
\mathrm{ ~sys. }$ \\
			80 & $\num{ 2.938e+02 } \pm \num{ 5.54e-03 }$ & $\num{ 3.511e+02 } \pm \num{
7.10e-03 }$ & $\num{ 1.13e-03 } \pm \num{ 4.9e-07 } \mathrm{ ~stat. } \pm \num{ 2.6e-05 }
\mathrm{ ~sys. }$ \\
			90 & $\num{ 2.945e+02 } \pm \num{ 4.64e-03 }$ & $\num{ 3.613e+02 } \pm \num{
3.75e-03 }$ & $\num{ 1.37e-03 } \pm \num{ 8.1e-07 } \mathrm{ ~stat. } \pm \num{ 2.9e-05 }
\mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Gemittelte Messungen, Seite Weiss}
	\label{tab:average_weiss}
\end{table}

\FloatBarrier
\clearpage

\begin{aufgabe}{Stefan-Boltzmann-Gesetz}
  Tragen Sie für alle vier Messreihen die Spannung als Funktion von
  $T^4$ auf und führen Sie eine lineare Regression unter Beachtung der
  Messunsicherheiten auf $T$ und $U$ durch.
\end{aufgabe}

Für die Auswertung unserer Messdaten, bei der eine Spannung an Thermoelementen gegen die
vierte Potenz der Temperatur aufgetragen wird, verwenden wir die Geradengleichung

\begin{equation}
 U(T) = m (T_{gemessen}^4 - T_0^4) + b
\end{equation}

Mit \autoref{eqn:strahlleistung} können wir die Steigung identifizieren: $m = \varepsilon
\frac{\sigma A_s A_e}{\pi r^2} c_{TS}$.

Die mithilfe der
\texttt{praktikum}-Bibliothek\footnote{\url{
https://grundpraktikum.physik.rwth-aachen.de/software/index.html}} erhaltenen
Regressionen sind für die goldene, schwarze, silberne, und weiße Seite in
\autoref{fig:regr_gold}, \autoref{fig:regr_schwarz}, \autoref{fig:regr_silber}, und
\autoref{fig:regr_weiss} abgebildet. Die Diagramme tragen die Thermosäulenspannung
$U_{TS}$ auf der y-Achse gegen die Differenz der vierten Temperaturpotenzen auf der
x-Achse auf. Unsicherheiten von $T^4$ werden mit der \textit{Methode der effektiven
Varianz} in y-Unsicherheiten in den Residuendarstellungen umgerechnet ($\sigma_{y_{i,eff}}
= \sqrt{\sigma_{y_i}^2 + (m \sigma_{x_i})^2}$) Die in diesen Regressionen benutzten
Unsicherheiten lauten wie folgt:

\begin{description}
 \item[Spannung] In y-Richtung -- die Digitalisierungsunsicherheit selbst ($\pm
\SI{5}{\milli\volt} + 0.01 U~(|U| < \SI{10}{\volt})$ bzw. $\pm \SI{15}{\milli\volt} +
0.01 U~(|U| < \SI{10}{\volt})$.

Die Unsicherheit der Digitalisierung ist aber sehr klein im Vergleich zu jener
Unsicherheit, die durch das Ausrichten der Thermosäule und dem Warten auf eine stabile
Spannung an der Thermosäule entsteht. Daher addieren wir jeweils noch 1\% der Spannung,
um diesen Fehler abzuschätzen. Zum Beispiel: Bei einer abgelesenen Spannung von
$\SI{5}{\volt}$ ist durchaus eine Schwankung von $\SI{50}{\milli\volt}$ abhängig vom
genauen Timing zu erwarten.

 \item[Temperatur] $\pm \SI{0.2}{\kelvin}$ ($T < \SI{70}{\celsius}$), $\pm
\SI{0.4}{\kelvin}$ ($T > \SI{70}{\celsius}$). Diese Werte stammen aus den
Gerätebeschreibungen des ersten Teils des Grundpraktikums. Da keine genaueren Daten
für das CASSY-Thermometer bereitstanden, erscheinen diese Werte großzügig gewählt. Auf
die Differenz $T_{gemessen}^4 - T_0^4$ ergibt das dann die Unsicherheit in x-Richtung:

\begin{equation}
 \sigma_{\Delta(T^4)}^2 = (4 T_{gemessen}^3 \sigma_{T_{gemessen}})^2 + (4 T_0^3
\sigma_{T_0})^2
\end{equation}
\end{description}

Die zugehörigen Parameter sind in \autoref{tab:regression_params} gezeigt. Das genaue
$\chi^2/DoF$ ist dabei in den Regressionsplots eingezeichnet.  Da $\varepsilon$ in $m$
steckt, kann man an der Steigung der Ausgleichsgeraden deutlich erkennen, dass die
Abstrahlung für die schwarze Seite am stärksten ist (d.h., stärkste Steigung der Geraden
$m$), die weiße Seite sich gleich danach einreiht, und die spiegelnde (silberne) Seite am
schlechtesten strahlt.

Die Anpassungsgüte ist relativ gut für die matten -- also die schwarze und weiße --
Seiten. Hier konnte relativ genau gemessen werden. Die anderen beiden weisen größere
Schwankungen auf, die auch in den Regressionen \autoref{fig:regr_gold} und
\autoref{fig:regr_silber} zu sehen sind. Insbesondere hat sich bei der silbernen Seite
während einiger Messungen zwischen $\SI{60}{\celsius}$ und $\SI{75}{\celsius}$ die
Abstrahlung nicht verändert. Ein Hauptgrund für die schlechte Anpassungsgüte ist
sicherlich, dass trotz recht hoher Genauigkeit der Messung die Messumstände ungenau
waren: Die Abschätzung, ab wann man misst (wann die Spannung nicht mehr "`schnell"'
steigt), hat hier einen großen Einfluss. Wenn dann die jeweilige Seite nur schwach
strahlt, kommen weitere Ungenauigkeiten beim Ablesen hinzu, die nur ungenügend als
statistische Messunsicherheiten berücksichtigt werden können (z.B. Timing).

Außerdem erkennt man eine sinusförmige Systematik der Abweichungen im Residuenplot.
Eine mögliche Erklärung ist die folgende: Die Reihenfolge meiner Messungen war zunächst
Weiß/Silber/Schwarz/Gold. In den Rohdaten ist sichtbar, dass sich das Wasser zwischen der
ersten und letzten Messung um bis zu \SI{1}{\kelvin} abgekühlt hat. Somit wurde Schwarz
immer kühler gemessen als z.B. Weiß. Nach den ersten Messungen fiel mir dies auf, und ich
habe die Reihenfolge der Seiten vertauscht, bzw. bei starker Abkühlung nachgeheizt.
Dieses Vorgehen fällt in den mittleren Temperaturbereich, wo in den Messungen eben diese
Tendenz zu erkennen ist.

Insgesamt zeigt sich aber, dass die $T^4$-Abhängigkeit des Stefan-Boltzmann-Gesetzes im
Rahmen unserer Messungen gültig ist.

\begin{figure}[p]
 \centering
 \includegraphics[scale=0.32]{img/gold_regr.pdf}
 % schwarz_regr.png: 1440x720 px, 72dpi, 50.79x25.40 cm, bb=0 0 1440 720
 \caption{Regression für die goldene Seite}
 \label{fig:regr_gold}
\end{figure}
\begin{figure}[p]
 \centering
 \includegraphics[scale=0.32]{img/schwarz_regr.pdf}
 % schwarz_regr.png: 1440x720 px, 72dpi, 50.79x25.40 cm, bb=0 0 1440 720
 \caption{Regression für die schwarze Seite}
 \label{fig:regr_schwarz}
\end{figure}
\begin{figure}[p]
 \centering
 \includegraphics[scale=0.32]{img/silber_regr.pdf}
 % schwarz_regr.png: 1440x720 px, 72dpi, 50.79x25.40 cm, bb=0 0 1440 720
 \caption{Regression für die silberne Seite}
 \label{fig:regr_silber}
\end{figure}
\begin{figure}[p]
 \centering
 \includegraphics[scale=0.32]{img/weiss_regr.pdf}
 % schwarz_regr.png: 1440x720 px, 72dpi, 50.79x25.40 cm, bb=0 0 1440 720
 \caption{Regression für die weiße Seite}
 \label{fig:regr_weiss}
\end{figure}

\begin{table}[h]
\hspace{-5em}
 \begin{tabular}{c|c|c|c|c}
  Seite & $m$ & $b$ & $\chi^2/DoF$ & $\rho_{mb}$ \\
  \midrule
  Gold & \num{3.939(006)e-14} & \num{-1.187(028)e-5} & \chisq{23133}{988}{23.4} &
\num{-0.94} \\
  Gold, dekorreliert & \num{3.939(006)e-14} & \num{0.224(01)e-5} &
\chisq{23133}{988}{23.4} &
\num{0.581} \\
  Gold, syst. Fehler & $\pm \num{.051e-14}$ & $\pm \num{.004e-4}$ & -- & -- \\
  \midrule
  Schwarz & \num{1.516(003)e-13} & \num{-1.844(126)e-5} & \chisq{840.4}{988}{0.85} &
\num{-.994} \\
  Schwarz, dekorreliert & \num{1.516(003)e-13} & \num{87.64(6)e-5} &
\chisq{840.4}{988}{0.85} & \num{0.75} \\
  Schwarz, syst. Fehler & $\pm \num{0.033e-13}$ & $\pm \num{0.635e-5}$ & -- & -- \\
  \midrule
  Silber & \num{1.235(003)e-14} & \num{-9.633(114)e-6} & \chisq{32194}{988}{42.7} &
\num{-0.94} \\
  Silber, dekorreliert & \num{1.235(003)e-14} & \num{64.10(43)e-6} &
\chisq{32194}{988}{42.7} & \num{0.46} \\
  Silber, syst. Fehler & $\pm \num{0.017e-14}$ & $\pm \num{0.379e-6}$ & -- & -- \\
  \midrule
  Weiß & \num{1.435(003)e-13} & \num{1.060(120)e-5} & \chisq{342.9}{988}{0.35} &
\num{-0.96} \\
  Weiß, dekorreliert & \num{1.435(003)e-13} & \num{0.8614(050)e-5} &
\chisq{342.9}{988}{0.35} & \num{0.744} \\
  Weiß, syst. Fehler & $\pm \num{0.031e-13}$ & $\pm \num{0.598e-5}$ & -- & -- \\
 \end{tabular}
 \caption{Regressionsparameter, mit Ergebnissen der dekorrelierten Regression und
Verschiebemethode für systematischen Fehler}
 \label{tab:regression_params}
\end{table}

\clearpage
\FloatBarrier

\begin{aufgabe}{Emissionsgrade der Seiten des Leslie-Würfels}
  Bestimmen Sie die Strahlungsstärken und Emissionsgrade der
  verschiedenen Seiten des Leslie-Würfels und vergleichen Sie sie
  miteinander. Was gibt es für Auffälligkeiten? Wie erklären Sie sich
  diese? Welche Seite ähnelt am meisten einem perfekten schwarzen
  Strahler?
\end{aufgabe}

Für die Leistung der idealen bzw. gemessenen Schwarzkörperstrahlung gilt:

\begin{subequations}
\begin{equation}
 P_{ideal} = \frac{A_s A_e}{r^2} \frac{\sigma}{\pi} (T_{gemessen}^4 - T_0^4)
\end{equation}
\begin{equation}
 P_{gemessen} = \frac{U_{TS}}{c_{TS}}, \quad c_{TS} = \num{0.276(009)}
\end{equation}
\end{subequations}

und für deren Unsicherheiten somit

\begin{subequations}
\begin{equation}
\uncertfrac{P_{ideal}} = \uncertfrac{A_s} +
\uncertfrac{A_e} + 4 \uncertfrac{r} + \frac{(4 T_{gemessen}^3 \sigma_{T_{gemessen}})^2 +
(4 T_0^3 \sigma_{T_0})^2}{(T_{gemessen}^4 - T_0^4)^2}
\end{equation}
\begin{equation}
 \uncertfrac{P_{gemessen}} = \uncertfrac{U_{TS}} + \uncertfrac{c_{TS}}^{(*)}
\qquad (*)~\text{nur für syst. Uns.}
\end{equation}
\end{subequations}

Der Emissionsgrad ist ebenfalls mit einer Messunsicherheit behaftet, die von den
Unsicherheiten der beiden Leistungswerte abhängt:

\begin{equation}
 \varepsilon = P_{gemessen} / P_{ideal} \Rightarrow \uncertfrac{\varepsilon} =
\uncertfrac{P_{gemessen}} + \uncertfrac{P_{ideal}}
\end{equation}

Die damit berechneten Werte sind in den folgenden Tabellen aufgeführt. Für die
systematischen Unsicherheiten wurde die gleiche Fehlerrechnung benutzt, allerdings mit
den systematischen Fehlern. Folgende Werte wurden für die Berechnung der
Unsicherheiten genutzt:

\begin{description}
 \item[Längen an der Thermosäule:] $\pm \SI{0,5}{\milli\meter}$, somit
 \item[$A_s$:] $\sigma_{A_s} = \frac{\pi}{2} l \sigma_l = \pm
\SI{2.75e-5}{\meter\squared}$
 \item[$A_e$:] $\sigma_{A_e} = \pm \SI{1.8e-5}{\meter\squared}$
 \item[Abstand $r$:] $\pm \SI{1}{\milli\meter}$, mit $r = \SI{15}{\centi\meter} -
\SI{4,2}{\centi\meter} + \SI{1}{\milli\meter} = \SI{10,9}{\centi\meter}$, da das Rohr den
Würfel knapp nie berührt
 \item[Temperaturen $T_0, T_m$:] Die Stichprobenstandardabweichung der
Temperatur jeder Messung.
 \item[Spannung] Die Stichprobenstandardabweichung der Spannung jeder Messung.
 \\
 \item[Spannung (systematisch)] Wie für CASSY üblich: $1\,\% + 0,5\,\% \cdot
U_{max}$
 \item[Sensitivität (systematisch)] Wie angegeben, für diese Thermosäule
$\SI{0.2760(0083)}{\volt\per\watt}$. Die Unsicherheit hier ist allerdings systematisch!
Sie wird nur in der Betrachtung der systematischen Unsicherheit verwendet.
\end{description}

In \autoref{tab:emission_gold}, \autoref{tab:emission_schwarz},
\autoref{tab:emission_silber}, und \autoref{tab:emission_weiss} sind die jeweiligen
Strahlungswerte für jede Temperaturstufe aufgeführt, und der zugehörige
Emissionskoeffizient beschrieben. Die gewichteten Mittelwerte finden sich in den
Tabellenunterschriften. Die zugehörigen Umgebungstemperaturen $T_0$ lassen sich z.\,B. in
\autoref{tab:average_gold} ablesen; sie werden hier nicht dupliziert. Die systematischen
Unsicherheiten berechnen sich aus der kombinierten systematischen Unsicherheit der
Spannung und der Sensitivität der Thermosäule.

Die Tabellenunterschriften enthalten auch den gewichteten Mittelwert mit
Standardabweichung des Emissionskoeffizienten. Laut diesen ist die schwarze Seite der
stärkste Emittent, die weiße folgt gleich darauf. Danach kommen die matte goldene und
zuletzt die silberne spiegelnde Seite.

Es bietet sich noch an, mithilfe von \autoref{eqn:strahlleistung}, d.h. der Steigung $m$
der Regressionsgeraden, den Koeffizienten $\varepsilon$ zu bestimmen:

\begin{subequations}
\begin{equation}
 U(T) \sim m \Delta (T_{gemessen}^4 - T_0^4) \Rightarrow m =
\frac{\Delta U_{TS}}{\Delta(T_{gemessen}^4 - T_0^4)}
\end{equation}
\begin{equation}
P_{gemessen} = \frac{U_{TS}}{c_{TS}}
\end{equation}
\begin{equation}
 \Rightarrow \frac{P_{gemessen}}{A_s A_e \sigma \Delta (T_{gemessen}^4 - T_0^4)} r^2 \pi =
 \frac{m r^2 \pi}{c_{TS} A_s A_e \sigma} = \varepsilon
\end{equation}
\begin{equation}
 \uncertfrac{\varepsilon} = 4\uncertfrac{r} + \uncertfrac{c_{TS}} + \uncertfrac{A_s}
+ \uncertfrac{A_e} + \uncertfrac{m}
\end{equation}
\end{subequations}

Daraus ergeben sich die Werte in \autoref{tab:emission_regression}, im Vergleich zu den
durch gewichtete Mittelwertbildung berechneten Emissionskoeffizienten (siehe Tabellen
unten). Zu sehen sind durchaus ähnliche Werte, wobei die lineare Regression allerdings
fast immer größere Emissionskoeffizienten liefert. Insgesamt sind die Werte jedoch im zu
erwartenden Bereich für diese Materialien, und relativ zueinander ebenfalls sinnvoll.

In der Literatur\footnote{\url{
https://www.thermografie-xtra.de/tipps-tricks/emissionsgrad-tabelle}} können wir die
Werte in \autoref{tab:referenzwerte} ablesen. Diese stimmen ungefähr mit den hier
gemessenen Werten überein; der polierte Wert ist niedriger als unserer, aber der Würfel
war auch nicht mehr komplett glattpoliert, sondern etwas gealtert; auch der Wert für
schwarzen Lack ist unseren Messungen zufolge etwas niedriger als der angegebene Wert.

\begin{table}
 \centering
 \begin{tabular}{r|l|l}
  Seite & $\varepsilon$ (lin. Reg.) & $\varepsilon$ (gew. Mitt.) \\
  \midrule
  Gold & \num{0.235(015)} & \num{0.220(004)}\\
  Schwarz & \num{0.906(057)} & \num{0.884(017)} \\
  Silber & \num{0.074(005)} & \num{0.061(001)} \\
  Weiß & \num{0.857(054)} & \num{0.869(017)}\\
 \end{tabular}
 \caption{Vergleich der auf zwei unterschiedlichen Wegen ermittelten
Emissionskoeffizienten}
 \label{tab:emission_regression}
\end{table}

\begin{table}
 \centering
 \begin{tabular}{r|l}
  Seite & $\varepsilon$ \\
  \midrule
  Messing, stumpf/fleckig & \num{0.22} \\
  Messing, poliert & \num{0.03} \\
  Lack, schwarz, stumpf & \num{0.97} \\
  Lack, weiß & \num{0.8} - \num{0.92} \\
 \end{tabular}
 \caption{Emissionskoeffizienten laut Literatur}
 \label{tab:referenzwerte}
\end{table}

\FloatBarrier
\clearpage

\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			T / \si{\celsius} & $P_{ideal} / \si{\watt}$ & $P_{gemessen} / \si{\watt}$ &
$\varepsilon$ \\
			\midrule
			50 & $\num{ 1.89e-03 } \pm \num{ 1.0e-04 }$ & $\num{ 3.87e-04 } \pm \num{
2.7e-07 } \mathrm{ ~stat. } \pm \num{ 2.5e-05 } \mathrm{ ~sys. }$ & $\num{ 0.204 } \pm
\num{ 0.011 } \mathrm{ ~stat. } \pm \num{ 0.013 } \mathrm{ ~sys. }$ \\
			55 & $\num{ 2.28e-03 } \pm \num{ 1.3e-04 }$ & $\num{ 5.23e-04 } \pm \num{
6.0e-07 } \mathrm{ ~stat. } \pm \num{ 2.8e-05 } \mathrm{ ~sys. }$ & $\num{ 0.230 } \pm
\num{ 0.013 } \mathrm{ ~stat. } \pm \num{ 0.012 } \mathrm{ ~sys. }$ \\
			60 & $\num{ 2.79e-03 } \pm \num{ 1.5e-04 }$ & $\num{ 5.60e-04 } \pm \num{
1.1e-06 } \mathrm{ ~stat. } \pm \num{ 2.9e-05 } \mathrm{ ~sys. }$ & $\num{ 0.201 } \pm
\num{ 0.011 } \mathrm{ ~stat. } \pm \num{ 0.010 } \mathrm{ ~sys. }$ \\
			65 & $\num{ 3.20e-03 } \pm \num{ 1.8e-04 }$ & $\num{ 7.56e-04 } \pm \num{
7.8e-07 } \mathrm{ ~stat. } \pm \num{ 3.4e-05 } \mathrm{ ~sys. }$ & $\num{ 0.236 } \pm
\num{ 0.013 } \mathrm{ ~stat. } \pm \num{ 0.011 } \mathrm{ ~sys. }$ \\
			70 & $\num{ 3.68e-03 } \pm \num{ 2.0e-04 }$ & $\num{ 8.69e-04 } \pm \num{
9.9e-07 } \mathrm{ ~stat. } \pm \num{ 3.7e-05 } \mathrm{ ~sys. }$ & $\num{ 0.236 } \pm
\num{ 0.013 } \mathrm{ ~stat. } \pm \num{ 0.010 } \mathrm{ ~sys. }$ \\
			75 & $\num{ 4.16e-03 } \pm \num{ 2.3e-04 }$ & $\num{ 8.77e-04 } \pm \num{
6.6e-07 } \mathrm{ ~stat. } \pm \num{ 3.8e-05 } \mathrm{ ~sys. }$ & $\num{ 0.211 } \pm
\num{ 0.012 } \mathrm{ ~stat. } \pm \num{ 0.009 } \mathrm{ ~sys. }$ \\
			80 & $\num{ 4.73e-03 } \pm \num{ 2.6e-04 }$ & $\num{ 1.18e-03 } \pm \num{
7.8e-07 } \mathrm{ ~stat. } \pm \num{ 4.6e-05 } \mathrm{ ~sys. }$ & $\num{ 0.248 } \pm
\num{ 0.014 } \mathrm{ ~stat. } \pm \num{ 0.010 } \mathrm{ ~sys. }$ \\
			90 & $\num{ 6.08e-03 } \pm \num{ 3.3e-04 }$ & $\num{ 1.27e-03 } \pm \num{
7.9e-07 } \mathrm{ ~stat. } \pm \num{ 4.9e-05 } \mathrm{ ~sys. }$ & $\num{ 0.209 } \pm
\num{ 0.012 } \mathrm{ ~stat. } \pm \num{ 0.008 } \mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Strahlungsdaten für Gold. Gewichtetes Mittel $\varepsilon = \num{ 0.220 } \pm
\num{ 0.004 }$}
	\label{tab:emission_gold}
\end{table}
\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			T / \si{\celsius} & $P_{ideal} / \si{\watt}$ & $P_{gemessen} / \si{\watt}$ &
$\varepsilon$ \\
			\midrule
			50 & $\num{ 1.93e-03 } \pm \num{ 1.1e-04 }$ & $\num{ 1.69e-03 } \pm \num{
8.0e-07 } \mathrm{ ~stat. } \pm \num{ 6.2e-05 } \mathrm{ ~sys. }$ & $\num{ 0.875 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.032 } \mathrm{ ~sys. }$ \\
			55 & $\num{ 2.30e-03 } \pm \num{ 1.3e-04 }$ & $\num{ 2.04e-03 } \pm \num{
9.8e-07 } \mathrm{ ~stat. } \pm \num{ 7.2e-05 } \mathrm{ ~sys. }$ & $\num{ 0.887 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.032 } \mathrm{ ~sys. }$ \\
			60 & $\num{ 2.69e-03 } \pm \num{ 1.5e-04 }$ & $\num{ 2.31e-03 } \pm \num{
1.9e-06 } \mathrm{ ~stat. } \pm \num{ 8.1e-05 } \mathrm{ ~sys. }$ & $\num{ 0.858 } \pm
\num{ 0.047 } \mathrm{ ~stat. } \pm \num{ 0.030 } \mathrm{ ~sys. }$ \\
			65 & $\num{ 3.22e-03 } \pm \num{ 1.8e-04 }$ & $\num{ 2.84e-03 } \pm \num{
1.5e-06 } \mathrm{ ~stat. } \pm \num{ 9.7e-05 } \mathrm{ ~sys. }$ & $\num{ 0.881 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.030 } \mathrm{ ~sys. }$ \\
			70 & $\num{ 3.65e-03 } \pm \num{ 2.0e-04 }$ & $\num{ 3.27e-03 } \pm \num{
6.5e-07 } \mathrm{ ~stat. } \pm \num{ 1.1e-04 } \mathrm{ ~sys. }$ & $\num{ 0.897 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.030 } \mathrm{ ~sys. }$ \\
			75 & $\num{ 4.19e-03 } \pm \num{ 2.3e-04 }$ & $\num{ 3.75e-03 } \pm \num{
6.1e-07 } \mathrm{ ~stat. } \pm \num{ 1.5e-04 } \mathrm{ ~sys. }$ & $\num{ 0.896 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.035 } \mathrm{ ~sys. }$ \\
			80 & $\num{ 4.74e-03 } \pm \num{ 2.6e-04 }$ & $\num{ 4.22e-03 } \pm \num{
2.3e-06 } \mathrm{ ~stat. } \pm \num{ 1.6e-04 } \mathrm{ ~sys. }$ & $\num{ 0.891 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.034 } \mathrm{ ~sys. }$ \\
			90 & $\num{ 5.77e-03 } \pm \num{ 3.2e-04 }$ & $\num{ 5.12e-03 } \pm \num{
2.6e-06 } \mathrm{ ~stat. } \pm \num{ 1.9e-04 } \mathrm{ ~sys. }$ & $\num{ 0.887 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.032 } \mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Strahlungsdaten für Schwarz. Gewichtetes Mittel $\varepsilon = \num{ 0.884 }
\pm \num{ 0.017 }$}
	\label{tab:emission_schwarz}
\end{table}
\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			T / \si{\celsius} & $P_{ideal} / \si{\watt}$ & $P_{gemessen} / \si{\watt}$ &
$\varepsilon$ \\
			\midrule
			50 & $\num{ 1.95e-03 } \pm \num{ 1.1e-04 }$ & $\num{ 1.11e-04 } \pm \num{
2.2e-07 } \mathrm{ ~stat. } \pm \num{ 2.0e-05 } \mathrm{ ~sys. }$ & $\num{ 0.057 } \pm
\num{ 0.003 } \mathrm{ ~stat. } \pm \num{ 0.010 } \mathrm{ ~sys. }$ \\
			55 & $\num{ 2.30e-03 } \pm \num{ 1.3e-04 }$ & $\num{ 1.32e-04 } \pm \num{
3.9e-07 } \mathrm{ ~stat. } \pm \num{ 2.0e-05 } \mathrm{ ~sys. }$ & $\num{ 0.057 } \pm
\num{ 0.003 } \mathrm{ ~stat. } \pm \num{ 0.009 } \mathrm{ ~sys. }$ \\
			60 & $\num{ 2.72e-03 } \pm \num{ 1.5e-04 }$ & $\num{ 2.12e-04 } \pm \num{
4.4e-07 } \mathrm{ ~stat. } \pm \num{ 2.1e-05 } \mathrm{ ~sys. }$ & $\num{ 0.078 } \pm
\num{ 0.004 } \mathrm{ ~stat. } \pm \num{ 0.008 } \mathrm{ ~sys. }$ \\
			65 & $\num{ 3.18e-03 } \pm \num{ 1.8e-04 }$ & $\num{ 1.92e-04 } \pm \num{
8.4e-07 } \mathrm{ ~stat. } \pm \num{ 2.1e-05 } \mathrm{ ~sys. }$ & $\num{ 0.060 } \pm
\num{ 0.003 } \mathrm{ ~stat. } \pm \num{ 0.007 } \mathrm{ ~sys. }$ \\
			70 & $\num{ 3.68e-03 } \pm \num{ 2.0e-04 }$ & $\num{ 2.19e-04 } \pm \num{
6.3e-07 } \mathrm{ ~stat. } \pm \num{ 2.1e-05 } \mathrm{ ~sys. }$ & $\num{ 0.060 } \pm
\num{ 0.003 } \mathrm{ ~stat. } \pm \num{ 0.006 } \mathrm{ ~sys. }$ \\
			75 & $\num{ 4.21e-03 } \pm \num{ 2.3e-04 }$ & $\num{ 2.18e-04 } \pm \num{
7.3e-07 } \mathrm{ ~stat. } \pm \num{ 2.1e-05 } \mathrm{ ~sys. }$ & $\num{ 0.052 } \pm
\num{ 0.003 } \mathrm{ ~stat. } \pm \num{ 0.005 } \mathrm{ ~sys. }$ \\
			80 & $\num{ 4.77e-03 } \pm \num{ 2.6e-04 }$ & $\num{ 3.41e-04 } \pm \num{
1.0e-06 } \mathrm{ ~stat. } \pm \num{ 2.4e-05 } \mathrm{ ~sys. }$ & $\num{ 0.072 } \pm
\num{ 0.004 } \mathrm{ ~stat. } \pm \num{ 0.005 } \mathrm{ ~sys. }$ \\
			90 & $\num{ 6.04e-03 } \pm \num{ 3.3e-04 }$ & $\num{ 4.49e-04 } \pm \num{
6.6e-07 } \mathrm{ ~stat. } \pm \num{ 2.6e-05 } \mathrm{ ~sys. }$ & $\num{ 0.074 } \pm
\num{ 0.004 } \mathrm{ ~stat. } \pm \num{ 0.004 } \mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Strahlungsdaten für Silber. Gewichtetes Mittel $\varepsilon = \num{ 0.061 }
\pm \num{ 0.001 }$}
	\label{tab:emission_silber}
\end{table}
\begin{table}[p]
\hspace{-5em}
		\begin{tabular}{c|c|c|c}
			T / \si{\celsius} & $P_{ideal} / \si{\watt}$ & $P_{gemessen} / \si{\watt}$ &
$\varepsilon$ \\
			\midrule
			50 & $\num{ 1.88e-03 } \pm \num{ 1.0e-04 }$ & $\num{ 1.64e-03 } \pm \num{
4.1e-07 } \mathrm{ ~stat. } \pm \num{ 6.0e-05 } \mathrm{ ~sys. }$ & $\num{ 0.873 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.032 } \mathrm{ ~sys. }$ \\
			55 & $\num{ 2.32e-03 } \pm \num{ 1.3e-04 }$ & $\num{ 2.05e-03 } \pm \num{
8.0e-07 } \mathrm{ ~stat. } \pm \num{ 7.3e-05 } \mathrm{ ~sys. }$ & $\num{ 0.884 } \pm
\num{ 0.049 } \mathrm{ ~stat. } \pm \num{ 0.031 } \mathrm{ ~sys. }$ \\
			60 & $\num{ 2.75e-03 } \pm \num{ 1.5e-04 }$ & $\num{ 2.37e-03 } \pm \num{
1.3e-06 } \mathrm{ ~stat. } \pm \num{ 8.3e-05 } \mathrm{ ~sys. }$ & $\num{ 0.863 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.030 } \mathrm{ ~sys. }$ \\
			65 & $\num{ 3.20e-03 } \pm \num{ 1.8e-04 }$ & $\num{ 2.77e-03 } \pm \num{
1.7e-06 } \mathrm{ ~stat. } \pm \num{ 9.5e-05 } \mathrm{ ~sys. }$ & $\num{ 0.865 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.030 } \mathrm{ ~sys. }$ \\
			70 & $\num{ 3.71e-03 } \pm \num{ 2.0e-04 }$ & $\num{ 3.22e-03 } \pm \num{
1.5e-06 } \mathrm{ ~stat. } \pm \num{ 1.1e-04 } \mathrm{ ~sys. }$ & $\num{ 0.868 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.029 } \mathrm{ ~sys. }$ \\
			75 & $\num{ 4.24e-03 } \pm \num{ 2.3e-04 }$ & $\num{ 3.71e-03 } \pm \num{
2.2e-06 } \mathrm{ ~stat. } \pm \num{ 1.4e-04 } \mathrm{ ~sys. }$ & $\num{ 0.874 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.034 } \mathrm{ ~sys. }$ \\
			80 & $\num{ 4.70e-03 } \pm \num{ 2.6e-04 }$ & $\num{ 4.08e-03 } \pm \num{
1.8e-06 } \mathrm{ ~stat. } \pm \num{ 1.6e-04 } \mathrm{ ~sys. }$ & $\num{ 0.868 } \pm
\num{ 0.048 } \mathrm{ ~stat. } \pm \num{ 0.033 } \mathrm{ ~sys. }$ \\
			90 & $\num{ 5.78e-03 } \pm \num{ 3.2e-04 }$ & $\num{ 4.97e-03 } \pm \num{
2.9e-06 } \mathrm{ ~stat. } \pm \num{ 1.8e-04 } \mathrm{ ~sys. }$ & $\num{ 0.859 } \pm
\num{ 0.047 } \mathrm{ ~stat. } \pm \num{ 0.031 } \mathrm{ ~sys. }$ \\
		\end{tabular}
	\caption{Strahlungsdaten für Weiss. Gewichtetes Mittel $\varepsilon = \num{ 0.869 }
\pm \num{ 0.017 }$}
	\label{tab:emission_weiss}
\end{table}

