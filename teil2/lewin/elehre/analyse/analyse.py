#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 15:38:32 2020

@author: lbo
"""

import numpy as np
from uncertainties import ufloat, umath, unumpy as unp, core
import matplotlib.pyplot as plt
from matplotlib import ticker
import nice_plots
from praktikum import analyse
from praktikum import cassy

import math
import os

DATAPATH = '../daten'
FIGPATH = '../img/auto'

TOTAL_L = 1.245e-3
TOTAL_L_U = ufloat(1.245e-3, .031e-3, 'sys')
TOTAL_C = 10.15e-6
TOTAL_C_U = ufloat(10.15e-6, .25e-6, 'sys')
TOTAL_R_L = 0.7

# Data files of resonance measurements keyed by actual resistance
RESONANCE_MEASURES = {
    1.25: ['resonanzkurve_1Ohm_1V_delta2Hz.lab',
        'resonanzkurve_1Ohm_1V_delta5Hz.lab',
        'resonanzkurve_1Ohm_1V_delta10Hz.lab'],
    3.7: ['resonanzkurve_3,7Ohm_3V_delta5Hz.lab'],
    5.4: ['resonanzkurve_5,1Ohm_1V_delta5Hz.lab',
          # This file starts a bit too late (phi < 43 degrees)
          #'resonanzkurve_5,1Ohm_2V_delta10Hz.lab',
          'resonanzkurve_5,1Ohm_3V_delta5Hz.lab'],
    7: ['resonanzkurve_6,7Ohm_5V_delta5Hz.lab'],
    8.5: ['resonanzkurve_8,5Ohm_5V_delta10Hz.lab'],
}

def datapath_of(f):
    return os.path.join(DATAPATH, f)

def figpath_of(f):
    return os.path.join(FIGPATH, f)

def sys_error(uf):
    return math.sqrt(sum([e**2 for v,e in uf.error_components().items() if v.tag=='sys']))

def stat_error(uf):
    return math.sqrt(sum([e**2 for v,e in uf.error_components().items() if v.tag is None]))

def reconstruct_error(nominal, err_from):
    return (ufloat(nominal/2, sys_error(err_from), 'sys')+ufloat(nominal/2, stat_error(err_from)))

def make_sys_ufloat(nominal, sys, stat):
    return (ufloat(nominal/2, sys, 'sys')+ufloat(nominal/2, stat))

def unp_weighted_avg(undarr):
    ns = unp.nominal_values(undarr)
    ss = unp.std_devs(undarr)
    return analyse.gewichtetes_mittel(ns, ss)

def write_csv(file, head, rows):
    with open(file, 'w') as f:
        for row in rows:
            assert len(row) == len(head)
        f.write(','.join([str(h) for h in head]) + '\n')
        rows = [','.join([str(c) for c in row]) + '\n' for row in rows]
        f.writelines(rows)


class CassyUtil:

    def cassy_plot(file, xaxis='n', datenreihen=None, datenreihen_secondary=None,
                   xlabel='', ylabel='', ylabel_secondary='', ytick=None, xtick=None,
                   title='', savefig=None, plotline=None, figsize=(15,10),
                   correction={}):
        """
        Plot the data in a CASSY file.

        Parameters
        ----------
        file : str
        xaxis : str, optional
            data row to use as xaxis
        datenreihen : list of tuples or str, optional
            List of data row labels or (label, plot legend label) tuples selecting data rows to plot
        xlabel : str
            Xlabel of plot
        ylabel : str
            Ylabel of plot
        title : str
            Plot title
        savefig : str
            Path to save plot to
        plotline: (m: float, b: float)
            Plot a linear regression mx+b
        correction: {symbol: offset, symbol: (offset, scale)}

        Returns
        -------
        None.

        """
        cd = cassy.CassyDaten(file)
        meas = cd.messung(1)
        if type(datenreihen) is str:
            datenreihen = [datenreihen]
        if datenreihen:
            datenreihen = [(dr, dr) if type(dr) is not tuple else dr for dr in datenreihen]
        else:
            datenreihen = [(dr.symbol, dr.symbol) for dr in meas.datenreihen]

        if datenreihen_secondary:
            datenreihen_secondary = [(dr, dr) if type(dr) is not tuple else dr for dr in datenreihen_secondary]
        else:
            datenreihen_secondary = []
        xs = meas.datenreihe(xaxis).werte

        fig = plt.figure(figsize=figsize)
        p = fig.add_subplot(111)
        if ytick:
            p.yaxis.set_major_locator(ticker.MultipleLocator(ytick))
        if xtick:
            p.xaxis.set_major_locator(ticker.MultipleLocator(xtick))
        plots = []
        for dr in datenreihen:
            werte = meas.datenreihe(dr[0]).werte
            if correction and dr[0] in correction:
                c = correction[dr[0]]
                if type(c) is tuple:
                    werte = c[0]+c[1]*werte
                else:
                    werte = c+werte
            plots.append(p.scatter(xs, werte, label=dr[1]))
        for dr in datenreihen_secondary:
            p2 = p.twinx()
            p2.set_ylabel(ylabel_secondary)
            plots.append(p2.scatter(xs, meas.datenreihe(dr[0]).werte, label=dr[1], color='pink'))
        if plotline:
            p.plot(xs, plotline[0]*xs+plotline[1], label='Regression', color='black')
        p.legend(plots, [dr[1] for dr in datenreihen] + [dr[1] for dr in datenreihen_secondary])
        p.set_xlabel(xlabel)
        p.set_ylabel(ylabel)
        p.set_title(title if title else file)
        p.grid(True, which='both')
        if savefig:
            fig.savefig(savefig)

    def cassy_to_numpy(file, measure=1):
        """
        Return a dict of numpy arrays for the measurements in a CASSY file.

        Parameters
        ----------
        file : TYPE
            DESCRIPTION.
        measure : TYPE, optional
            DESCRIPTION. The default is 1.

        Returns
        -------
        dict
            DESCRIPTION.

        """
        drs = cassy.CassyDaten(file).messung(measure).datenreihen
        return {dr.symbol: dr.werte for dr in drs}

    # Default uncertainties are from digitization. Noise is too small!
    def cassy_apply_uncertainties(cassydict, noises={'U_2': 2e-3, 'U': 2e-3, 'I': 70e-6}):
        """
        Takes a dict as returned by cassy_to_numpy

        Parameters
        ----------
        cassydict : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        def apply(k, series):
            for prefix in noises:
                if k.startswith(prefix):
                    series = unp.uarray(series, np.full_like(series, noises[prefix]))
                    return series
            return series

        return {k: apply(k, v) for k, v in cassydict.items()}

class Corrections:
    coil_resistance = ufloat(TOTAL_R_L, TOTAL_R_L*0.025, 'sys')

    def __init__(self, noise_measurements):
        if not noise_measurements:
            noise_measurements = NoiseMeasurements()
        self.nm = noise_measurements

        (vs, vse, vo, voe) = nm.voltage_regression()
        self.VOLTAGE_SCALE = ufloat(vs, vse, tag='sys')
        self.VOLTAGE_OFF = ufloat(vo, voe, tag='sys')

        (cs, cse, co, coe) = nm.current_regression()
        self.CURRENT_SCALE = ufloat(cs, cse, tag='sys')
        # As-if-zero: uncertainty greater than nominal value
        self.CURRENT_OFF = ufloat(0,0, tag='sys') # ufloat(co, coe)

    def correct_coil_voltage(self, U_L, I_2, coil_resistance=None):
        """
        Apply voltage corrections to the coil voltage,
        taking into account the coil resistance.
        """
        coil_resistance = coil_resistance if coil_resistance else self.coil_resistance
        def correct(ul, rl, i):
            return unp.sqrt(ul**2-(rl*i)**2)
        return correct(U_L, coil_resistance, I_2)

    def correct_voltage(self, volts):
        """
        Return array of corrected volts with systematic uncertainties.  This
        fixes measurement errors caused by CASSY.

        Parameters
        ----------
        volts : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        return (volts - self.VOLTAGE_OFF)/self.VOLTAGE_SCALE

    def correct_current(self, currents):
        """
        Return array of corrected currents with systematic uncertainties. This
        fixes measurement errors caused by CASSY.

        Parameters
        ----------
        currents : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        return (currents - self.CURRENT_OFF)/self.CURRENT_SCALE

    def correct_resistance(self, r, coil_resistance=None):
        """
        Correct the resistance by the measured coil resistance.

        Parameters
        ----------
        r : TYPE
            DESCRIPTION.
        coil_resistance : TYPE, optional
            DESCRIPTION. The default is ufloat(0.8,0.1).

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        coil_resistance = coil_resistance if coil_resistance else self.coil_resistance
        if type(r) is core.Variable:
            return r + coil_resistance
        return ufloat(r, r*0.025, 'sys') + coil_resistance


class NoiseMeasurements:
    """
    Handle noise and uncertainty measurements of CASSY equipment.
    """
    CURRENT_NOISE = ['rausch_strom_1.lab']
    VOLTAGE_NOISE = ['rausch_spannung_1_0-21.lab',
                     'rausch_spannung_2_0-21.lab',
                     'rausch_spannung_2volt_0Hz.lab',
                     'rausch_spannung_2volt_1kHz.lab',
                     'rausch_spannung_2volt_10kHz.lab']
    VOLTAGE_NOISE_CONST = [#'rausch_spannung_2volt_0Hz.lab',
                           'rausch_spannung_2volt_1kHz.lab']
    VOLTAGE_REGRESSION_NOISE = ['rausch_spannung_1_0-21.lab',
                                'rausch_spannung_2_0-21.lab']
    OFFSET_NOISE = ['rausch_spannung_0volt_0Hz.lab',
                    'rausch_spannung_0volt_10kHz.lab']

    def __init__(self):
        self.u_noise = [CassyUtil.cassy_to_numpy(datapath_of(f)) for f in self.VOLTAGE_REGRESSION_NOISE]
        self.i_noise = [CassyUtil.cassy_to_numpy(datapath_of(f)) for f in self.CURRENT_NOISE]

    def calculate_voltage_noise(self, plot=False):
        noise = [CassyUtil.cassy_to_numpy(datapath_of(f)) for f in self.VOLTAGE_NOISE_CONST]
        values = np.concatenate([n['U_B1'] for n in noise])
        scale, _, off, _ = self.voltage_regression()

        std = values.std(ddof=1)
        dist = values - values.mean()

        if plot:
            fig = plt.figure(figsize=(15,10))
            p = fig.add_subplot(111)
            p.hist(dist)
            p.set_xlabel('$\Delta U$ / V')
            p.set_ylabel('$n$')
            fig.savefig(figpath_of('spannung_rausch_verteilung.pdf'))

        return std

    def calculate_current_noise(self, plot=False):
        noise = [CassyUtil.cassy_to_numpy(datapath_of(f)) for f in self.CURRENT_NOISE]
        scale, _, off, _ = self.current_regression()
        values = np.concatenate([n['I_A1'] for n in noise])
        dist = values - np.concatenate([scale*n['I_2']+off for n in noise])
        std = dist.std(ddof=1)

        if plot:
            fig = plt.figure(figsize=(15,10))
            p = fig.add_subplot(111)
            p.hist(dist)
            p.set_xlabel('$\Delta I$ / A')
            p.set_ylabel('$n$')
            fig.savefig(figpath_of('strom_rausch_verteilung.pdf'))

        return std

    def voltage_regression(self):
        """
        Generate regressions from noise measurements for voltage.

        Returns
        -------
        (scale, scale error, offset, offset error) factors

        """
        range_max = 21
        statistical_error = range_max/2048/math.sqrt(12)
        stats = []
        for i, noise in enumerate(self.u_noise):
            (a, ea, b, eb, chiq, corr) = analyse.lineare_regression(
                noise['U_2'], noise['U_B1'],
                # np.full_like(noise['U_2'], statistical_error),
                np.full_like(noise['U_2'], statistical_error))
            stats.append((a, ea, b, eb, chiq, corr))

        print('===== Weighted average, VOLTAGE ====')
        (scale, scale_e) = analyse.gewichtetes_mittel(
            np.array([s[0] for s in stats]), np.array([s[1] for s in stats]))
        (offset, offset_e) = analyse.gewichtetes_mittel(
            np.array([s[2] for s in stats]), np.array([s[3] for s in stats]))
        print('Skalenfaktor:', scale, '+/-', scale_e)
        print('Offset:', offset, '+/-', offset_e)
        return (scale, scale_e, offset, offset_e)

    def current_regression(self):
        """
        Generate regressions from noise measurements for current.

        Returns
        -------
        (scale, scale error, offset, offset error) factors

        """
        range_max = 0.7
        statistical_error = range_max/2048/math.sqrt(12)
        stats = []
        for i, noise in enumerate(self.i_noise):
            (a, ea, b, eb, chiq, corr) = analyse.lineare_regression(
                noise['I_2'], noise['I_A1'],
                # np.full_like(noise['I_2'], statistical_error),
                np.full_like(noise['I_2'], statistical_error))
            stats.append((a, ea, b, eb, chiq, corr))

        print('===== Weighted average, CURRENT =====')
        (scale, scale_e) = analyse.gewichtetes_mittel(
            np.array([s[0] for s in stats]), np.array([s[1] for s in stats]))
        (offset, offset_e) = analyse.gewichtetes_mittel(
            np.array([s[2] for s in stats]), np.array([s[3] for s in stats]))
        print('Skalenfaktor:', scale, '+/-', scale_e)
        print('Offset:', offset, '+/-', offset_e)
        return (scale, scale_e, offset, offset_e)

    def plot_voltage_regression(self):
        for i, noise in enumerate(self.u_noise):
            nice_plots.nice_linear_regression_plot(
                noise['U_2'], noise['U_B1'], np.full_like(noise['U_2'], self.calculate_voltage_noise()),
                title='Spannung-Rauschmessung {:d}'.format(i+1),
                xlabel='$U_{PC}$ / V', ylabel='$U_{SC}$ / V',
                ylabelresidue='$U_{SC} - (m U_{PC} + b)$ / V',
                save=figpath_of('spannung_rauschmessung_regression_{:d}.pdf'.format(i+1)))

    def plot_current_regression(self):
        for i, noise in enumerate(self.i_noise):
            nice_plots.nice_linear_regression_plot(
                noise['I_2'], noise['I_A1'], np.full_like(noise['I_2'], self.calculate_current_noise()),
                # np.full_like(noise['U_2'], 21/2048/math.sqrt(12)),
                title='Strom-Rauschmessung {:d}'.format(i+1),
                xlabel='$I_{PC}$ / A', ylabel='$I_{SC}$ / A',
                ylabelresidue='$I_{SC} - (m I_{PC} + b)$ / A',
                save=figpath_of('strom_rauschmessung_regression_{:d}.pdf'.format(i+1)))


class ResonanceMeasurements:
    CORRECTIONS = {
        'resonanzkurve_1Ohm_1V_delta2Hz.lab': {'U_B1': 0.025}
            }

    def __init__(self, correct=None):
        self.corrections = correct

    def plot_resonance_currents(self, legend=False):
        meas = {
            1.25: (CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_1Ohm_1V_delta2Hz.lab')), 1),
            3.7: (CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_3,7Ohm_3V_delta5Hz.lab')), 3),
            5.4: (CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_5,1Ohm_3V_delta5Hz.lab')), 3),
            7: (CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_6,7Ohm_5V_delta5Hz.lab')), 5),
            8.5: (CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_8,5Ohm_5V_delta10Hz.lab')), 5),
        }

        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$I_0$ / A')
        p.set_xlim(800, 2250)
        p.set_ylim(0, 0.6)
        for k, (data, volts) in meas.items():
            p.scatter(data['f_1'], data['I_2']/volts*1.4142, label='R = {:.1f}'.format(k))
        p.grid()
        if legend:
            p.legend()
        fig.savefig(figpath_of('resonanzkurve_gemessen.pdf'))

        # Now add resistance curve for comparison
        r_s = np.array([correct.correct_resistance(r).n for r in RESONANCE_MEASURES.keys()])
        xs = np.linspace(np.full_like(r_s,800*2*math.pi),np.full_like(r_s,2250*2*math.pi),300)

        l = TOTAL_L
        c = TOTAL_C
        resonance = 1/np.sqrt(r_s**2 + (xs*l - 1/(xs*c))**2)

        plots = p.plot(xs/(2*math.pi), resonance, '--')
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$(I_0 / U_0) / \Omega^{-1}$')
        fig.savefig(figpath_of('resonanzkurve_gemessen_vergleich.pdf'))

        # Plot difference between model and measurement
        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$I_{gemessen} - I_0(f)$ / A')
        p.set_xlim(800, 2250)
        p.set_ylim(-3e-3,3e-3)

        ordered_meas = meas.items()
        for i, (r, (data, volts))  in enumerate(ordered_meas):
            xs = data['f_1']*2*math.pi
            r = correct.correct_resistance(r).n
            measured = data['I_2']/volts * 1.4142
            predicted = 1/np.sqrt(r**2 + (xs*l - 1/(xs*c))**2)
            p.plot(xs/(2*math.pi), measured - predicted)
        p.grid()
        fig.savefig(figpath_of('resonanzkurve_differenz.pdf'))

    def plot_phase_shifts(self, legend=False):
        meas = {
            1.25: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_1Ohm_1V_delta2Hz.lab')),
            3.7: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_3,7Ohm_3V_delta5Hz.lab')),
            5.4: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_5,1Ohm_3V_delta5Hz.lab')),
            7: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_6,7Ohm_5V_delta5Hz.lab')),
            8.5: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_8,5Ohm_5V_delta10Hz.lab')),
        }

        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$\\phi$ / °')
        p.set_xlim(800, 2250)
        p.set_ylim(-90, 90)
        for k, data in meas.items():
            p.scatter(data['f_1'], -data['&j_2'], label='R = {:.1f}'.format(k))
        p.grid()
        if legend:
            p.legend()
        fig.savefig(figpath_of('phasenverschiebung_gemessen.pdf'))

        # Phase shift curve
        l = TOTAL_L
        c = TOTAL_C
        r_s = np.array([correct.correct_resistance(r).n for r in RESONANCE_MEASURES.keys()])
        xs = np.linspace(np.full_like(r_s,800*2*math.pi), np.full_like(r_s, 2250*2*math.pi),300)
        phaseshift = np.arctan((xs*l-1/(xs*c))/(r_s))

        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$\\phi$ / °')
        p.set_xlim(800, 2250)
        p.set_ylim(-90, 90)

        for k, data in meas.items():
            p.scatter(data['f_1'], -data['&j_2'], label='R = {:.1f}'.format(k))
        plots = p.plot(xs/(2*math.pi), phaseshift/math.pi*180, '--')
        p.set_ylabel('$\\phi$ / °')
        p.set_xlabel('$f$ / Hz')
        if legend:
            p.legend()
        p.grid()
        fig.savefig(figpath_of('phasenverschiebung_vergleich.pdf'))

        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$\\phi_{gemessen} - \\phi(f)$ / °')
        p.set_xlim(800, 2250)
        p.set_ylim(-3, 3)
        p.grid()

        for (r, data) in meas.items():
            r = self.corrections.correct_resistance(r).n
            xs = data['f_1']*2*math.pi
            measured = -data['&j_2']
            predicted = np.arctan((xs*l-1/(xs*c))/r)/math.pi * 180
            p.plot(xs/(2*math.pi), measured-predicted)
        fig.savefig(figpath_of(('phasenverschiebung_differenz.pdf')))

    def plot_voltage_intersections(self, figsize=(15,10)):
        meas = {
            1.25: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_1Ohm_1V_delta10Hz.lab')),
            3.7: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_3,7Ohm_3V_delta5Hz.lab')),
            5.4: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_5,1Ohm_2V_delta10Hz.lab')),
            7: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_6,7Ohm_5V_delta5Hz.lab')),
            8.5: CassyUtil.cassy_to_numpy(datapath_of('resonanzkurve_8,5Ohm_5V_delta10Hz.lab')),
        }

        fig = plt.figure(figsize=figsize)
        p = fig.add_subplot(111)
        p.set_title('$U_C$ und $U_L$')
        p.set_xlabel('$f$ / Hz')
        p.set_ylabel('$U$ / V (skaliert mit $R$)')
        p.grid()

        for k, data in meas.items():
            r = self.corrections.correct_resistance(k)
            p.scatter(np.concatenate([data['f_1']]*2),
                      np.concatenate([data['U_A1'], data['U_B1']])/r.n,
                      label='{:.1f} Ohm'.format(k))
        p.legend()
        fig.savefig(figpath_of('uc_ul_messdaten.pdf'))


    def plot_overvoltage_for_resistors(self):
        (c_inv_rs, c_quality) = self.calculate_voltage_peak_quality('C')
        (l_inv_rs, l_quality) = self.calculate_voltage_peak_quality('L')

        # inverse resistances are equal. Choose one
        rs = 1/c_inv_rs

        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        p.set_xlabel('$(R + R_L)$ / $\Omega$')
        p.set_ylabel('$\\hat U / U_0$')
        p.grid()

        p.errorbar(unp.nominal_values(rs), unp.nominal_values(l_quality),
                   xerr=unp.std_devs(rs), yerr=unp.std_devs(l_quality),
                   fmt='o', label='$U_C$')
        # p.errorbar(unp.nominal_values(rs), unp.nominal_values(l_quality),
        #            xerr=unp.std_devs(rs), yerr=unp.std_devs(l_quality),
        #            fmt='o', label='$U_L$')
        p.legend()
        fig.savefig(figpath_of('qs_über_r.pdf'))

    def plot_rawdata(self, figsize=(20,13), correct=False, xtick=None, ytick=None, legend=False):
        for resist in RESONANCE_MEASURES.keys():
            for f in RESONANCE_MEASURES[resist]:
                r = self.corrections.correct_resistance(resist)
                meas = CassyUtil.cassy_to_numpy(datapath_of(f))
                fig = plt.figure(figsize=figsize)
                p = fig.add_subplot(111)
                p.set_title('Resonanzkurve '+f)
                p.set_xlabel('$f$ / Hz')
                p.set_ylabel('$U$ / V - $I$ / 0.1 A')
                p.grid()

                if ytick:
                    p.yaxis.set_major_locator(ticker.MultipleLocator(ytick))
                if xtick:
                    p.xaxis.set_major_locator(ticker.MultipleLocator(xtick))

                p2 = p.twinx()
                p2.set_ylabel('$\\phi$ / °')

                f1 = meas['f_1']
                i2 = meas['I_2']
                ul = meas['U_B1']
                uc = meas['U_A1']
                u0 = meas['U_2']
                phi = meas['&j_2']

                if correct:
                    ul = self.corrections.correct_coil_voltage(ul, i2)

                pul = p.scatter(f1, unp.nominal_values(ul), label='$U_L$')
                puc = p.scatter(f1, uc, label='$U_C$')
                pu0 = p.scatter(f1, u0, label='$U_0$')
                pi2 = p.scatter(f1, 10*i2, label='$I_2$')
                pphi = p2.scatter(f1, phi, label='$\\phi$', color='gray')

                if legend:
                    p.legend([pul,puc,pu0,pi2,pphi],
                             ['$U_L$','$U_C$','$U_0$', '$I_2$', '$\\phi$'])
                fig.savefig(figpath_of(f.replace('.','_')+'_custom_{}.pdf'.format('korrigiert' if correct else 'roh')))

    def generate_highresolution_plots(self):
        for resist in RESONANCE_MEASURES.keys():
            for f in RESONANCE_MEASURES[resist]:
                CassyUtil.cassy_plot(datapath_of(f),
                           xaxis='f_1', datenreihen=[('U_A1', '$U_C$'), ('U_B1', '$U_L$'), ('U_2', '$U_0$')],
                           xlabel='$f$ / Hz', ylabel='$U$ / V', ylabel_secondary='$\\phi$ / °',
                           title='Resonanzkurve: '+f, datenreihen_secondary=[('&j_2', '$\phi$')],
                           figsize=(60,40), ytick=0.05, xtick=10,
                           savefig=figpath_of(f+'_highres.pdf'),
                           correction=self.CORRECTIONS.get(f, None))

    def quality_resistance_regression(self, uinverse_R, uQs, typ=''):
        """
        Calculate and plot regression for measured quality. Arguments are ufloat ndarrays
        containing 1/R and Q_s values.

        Parameters
        ----------
        inverse_R : TYPE
            DESCRIPTION.
        Qs : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        inv_r = unp.nominal_values(uinverse_R)
        inv_r_e = unp.std_devs(uinverse_R)
        Qs = unp.nominal_values(uQs)
        Qs_e = unp.std_devs(uQs)

        # Shift method.
        param_left = analyse.lineare_regression(inv_r-inv_r_e, Qs, Qs_e)
        param_right = analyse.lineare_regression(inv_r+inv_r_e, Qs, Qs_e)
        m_sys = (param_left[0]-param_right[0])/2

        print(typ, '=====', 'Resistance vs. Quality')
        print('Systematic error on slope (left/right/error):', param_left[0], param_right[0], m_sys)
        (a, ea, b, eb, chiq, corr) = nice_plots.nice_linear_regression_plot(
            inv_r, Qs, Qs_e,
            xlabel='$1/R$', ylabel='$Q_s$', ylabelresidue='$Q_s - (m/R + b)$',
            title='Güte vs. $1/R$, {}'.format(typ), save=figpath_of('quality_resistance_{}.pdf'.format(typ)))
        return (a, ea, b, eb, chiq, corr)


    def find_intersection(self, base, seriesa, seriesb, environ=3, plot=False):
        """
        More or less generic intersection finder, based on quadratic interpolation.

        Parameters
        ----------
        base : TYPE
            DESCRIPTION.
        seriesa : TYPE
            DESCRIPTION.
        seriesb : TYPE
            DESCRIPTION.
        plot : TYPE, optional
            DESCRIPTION. The default is False.

        Returns
        -------
        base_val
            DESCRIPTION.
        interp_val
            DESCRIPTION.
        min_ix : TYPE
            DESCRIPTION.
        diff
            DESCRIPTION.

        """
        diff = np.abs(seriesa-seriesb)
        minix = np.argmin(diff)
        f_intersect_discrete = base[minix]

        frm, to = max(0, minix-environ), min(minix+environ+1, base.size)

        # Prepare quad. regression around intersection (+/- 3 points)
        a,b,c = np.polyfit(base[frm:to], seriesa[frm:to], 2)
        d,e,f = np.polyfit(base[frm:to], seriesb[frm:to], 2)

        p = (b-e)/(a-d)
        q = (c-f)/(a-d)
        f_intersect_1 = -p/2 - math.sqrt(p**2/4 - q)
        f_intersect_2 = -p/2 + math.sqrt(p**2/4 - q)
        f_intersect = f_intersect_1 if abs(f_intersect_1-f_intersect_discrete) < 20 else f_intersect_2
        interp_val = a*f_intersect**2+b*f_intersect+c

        if plot:
            fig = plt.figure(figsize=(15,10))
            p = fig.add_subplot(111)
            p.scatter(base[frm:to], seriesa[frm:to], label='A')
            p.scatter(base[frm:to], seriesb[frm:to], label='B')
            intp_base = np.linspace(base[frm], base[to], 50)
            p.plot(
                intp_base, a*intp_base**2+b*intp_base+c, label='A intp.')
            p.plot(
                intp_base, d*intp_base**2+e*intp_base+f, label='B intp.')
            p.legend()
            p.grid()

        return f_intersect, interp_val, minix, diff[minix]

    def calculate_voltage_peak_quality(
            self, voltage_type='C', out=datapath_of('spannungs_peaks'),
            uncert_limit=0.002, correct=False, area=(1300,1500)):
        """
        Calculate quality of LCR resonator by finding voltage intersections.

        Parameters
        ----------
        voltage : TYPE, optional
            'C' or 'L', which voltage to use.

        Returns
        -------
        (ndarray of ufloats 1/R, ndarray of ufloats Qs)

        """
        assert voltage_type in ('L','C')
        outfile = out + '_' + voltage_type + ('_korr' if correct else '_roh') + '.csv'
        csvhead = ('R', '1/R', 'sigma_1/R', 'peak_volt',
                   'sigma_peak_volt_stat', 'sigma_peak_volt_sys',
                   'base', 'sigma_base', 'sigma_base_sys', 'Qs', 'sigma_Qs',
                   'Qs_soll')
        csvrows = []
        Qs = []
        inv_rs = []

        for r in RESONANCE_MEASURES.keys():
            for f in RESONANCE_MEASURES[r]:
                dr = CassyUtil.cassy_to_numpy(datapath_of(f))
                dr = CassyUtil.cassy_apply_uncertainties(dr)
                key = 'U_A1' if voltage_type == 'C' else 'U_B1'
                voltage = dr[key]
                freq = dr['f_1']
                u_l, u_c = dr['U_B1'], dr['U_A1']
                i2 = dr['I_2']
                u_2 = dr['U_2']
                ur = self.corrections.correct_resistance(r)

                if correct:
                    voltage = self.corrections.correct_voltage(voltage)
                    u_2 = self.corrections.correct_voltage(u_2)
                    if voltage_type == 'L':
                        # voltage = self.corrections.correct_coil_voltage(voltage, i2)
                        pass

                # Find intersection
                print(f, '====', r, 'correct =', correct)
                peakfreq, peak, peak_ix, diff = self.find_intersection(
                    freq, unp.nominal_values(u_l), unp.nominal_values(u_c))
                print(f, 'intersect (closest):', peakfreq)

                # Reading error is +/- 1e-2 volts
                peak = make_sys_ufloat(peak, sys_error(voltage[peak_ix]), 1e-2)
                base = u_2[peak_ix]

                print(f, 'peak (+/- sys, stat):', peak, sys_error(peak), stat_error(peak))
                print(f, 'base (+/- sys, stat):', base, sys_error(base), stat_error(base))
                print(f, 'Q_s (+/- sys, stat):', peak/base, sys_error(peak/base), stat_error(peak/base))
                print(f, 'sqrt(L/C) vs measured', math.sqrt(TOTAL_L/TOTAL_C), peak/base * ur)

                csvrows.append((r, (1/ur).n, (1/ur).s, peak.n, stat_error(peak),
                               sys_error(peak), base.n, stat_error(base),
                               sys_error(base), (peak/base).n, stat_error(peak/base),
                               math.sqrt(TOTAL_L/TOTAL_C)/ur.n))
                Qs.append(peak/base)
                inv_rs.append(1/ur)

        write_csv(outfile, csvhead, csvrows)

        return (np.array(inv_rs), np.array(Qs))


    def find_resonance_frequency_by_intersection(self, correct=False):
        """
        Find the resonance frequency by finding the intersection of resonance curves for L/C.

        Returns
        -------
        ndarray of resonance frequencies

        """
        csvhead = ('R', 'delta_f', 'f0', 'sigma_f0', 'intersect_voltage_diff')
        csvdata = []
        resonances = []

        for r in RESONANCE_MEASURES.keys():
            for f in RESONANCE_MEASURES[r]:
                dr = CassyUtil.cassy_to_numpy(datapath_of(f))
                u_c = dr['U_A1']
                u_l = dr['U_B1']
                i2 = dr['I_2']
                f1 = dr['f_1']

                if correct:
                    u_c = self.corrections.correct_current(u_c)
                    u_l = self.corrections.correct_current(u_l)
                    # u_l = self.corrections.correct_coil_voltage(u_l, i2)

                intersect_freq, intersect_volt, min_ix, diff = self.find_intersection(
                    f1, unp.nominal_values(u_c), unp.nominal_values(u_l))
                print(f, '===== Frequency by intersection,', r)
                print(f, 'intersect (real, closest):', intersect_freq, f1[min_ix], 'diff:', diff)

                cr = self.corrections.correct_resistance(r)
                csvdata.append((r, f1[5]-f1[4], intersect_freq, (f1[5]-f1[4])/math.sqrt(12), diff))
                resonances.append(ufloat(intersect_freq, (f1[5]-f1[4])/math.sqrt(12)))

        write_csv(datapath_of('resonanz_schnittpunkt_{}.csv'.format('korr' if correct else 'roh')),
                  csvhead, csvdata)

        resonances = np.array(resonances)
        print('Resonance frequency by intersection: Weighted average:', unp_weighted_avg(resonances))
        return resonances

    def find_phase_45_quality(self):
        """Calculate frequency distance between two points with 45 degree
        phase difference.
        """
        csvhead = ('R', '1/R', 'sigma_1/R', 'f0', 'sigma_f0', 'delta_f', 'sigma_delta_f', 'Q_s', 'sigma_Q_s')
        csvrows = []
        Qs = []
        inv_rs = []
        resonances = []

        for r in RESONANCE_MEASURES.keys():
            for f in RESONANCE_MEASURES[r]:
                dr = CassyUtil.cassy_to_numpy(datapath_of(f))
                dr = CassyUtil.cassy_apply_uncertainties(dr)

                f1 = dr['f_1']
                phi = dr['&j_2']
                ur = self.corrections.correct_resistance(r)

                zero = np.full_like(phi, 0)
                plus_angle, minus_angle = np.full_like(phi, 45), np.full_like(phi, -45)
                reso_f, _, _, _ = self.find_intersection(f1, phi, zero, environ=10)
                plus_f, _, _, _ = self.find_intersection(f1, phi, plus_angle)
                minus_f, _, _, _ = self.find_intersection(f1, phi, minus_angle)
                diff_f = abs(plus_f-minus_f)

                print('===== Frequency width', f)
                print('left:', minus_f)
                print('resonance:', reso_f)
                print('right:', plus_f)
                print('diff:', diff_f)
                print('quality:', reso_f/diff_f)

                f0 = ufloat(reso_f, (f1[5]-f1[4])/math.sqrt(12))
                delta_f = ufloat(diff_f, (f1[5]-f1[4])/math.sqrt(6))

                csvrows.append(
                    (r, 1/ur.n, (1/ur).s, reso_f, (f1[5]-f1[4])/math.sqrt(12),
                     delta_f.n, delta_f.s, (f0/delta_f).n, (f0/delta_f).s))
                Qs.append(f0/delta_f)
                inv_rs.append(1/ur)
                resonances.append(ufloat(reso_f, (f1[5]-f1[4])/math.sqrt(12)))
        write_csv(datapath_of('phasediff_guete_{}.csv'.format('korr' if correct else 'roh')),
                  csvhead, csvrows)

        print('Resonance frequency by phase difference being zero:', unp_weighted_avg(resonances))

        return (np.array(inv_rs), np.array(Qs))

    def find_sqrt2_current_width(self, correct=True):
        csvhead = ('R', '1/R', 'sigma_1/R', 'f0', 'sigma_f0', 'delta_f', 'sigma_delta_f', 'Q_s', 'sigma_Q_s')
        csvrows = []
        inv_rs = []
        Qs = []

        for r in RESONANCE_MEASURES.keys():
            for f in RESONANCE_MEASURES[r]:
                dr = CassyUtil.cassy_to_numpy(datapath_of(f))
                dr = CassyUtil.cassy_apply_uncertainties(dr)
                i_2 = dr['I_2']
                f_1 = dr['f_1']

                # Does not make a difference here
                if correct:
                    i_2 = self.corrections.correct_current(i_2)

                # Find max
                max_ix = np.argmax(i_2)
                max_freq = f_1[max_ix]
                target_current = i_2[max_ix]/math.sqrt(2)

                target_base = np.full_like(f_1, target_current.n)
                left_intersect, _, _, _ = self.find_intersection(
                    f_1[0:max_ix], unp.nominal_values(i_2[0:max_ix]), target_base[0:max_ix])
                right_intersect, _, _, _ = self.find_intersection(
                    f_1[max_ix:], unp.nominal_values(i_2[max_ix:]), target_base[max_ix:])

                # diff = i_2 - target_current
                # left, right = diff[0:max_ix], diff[max_ix:]
                # left_ix, right_ix = np.argmin(np.abs(left)), max_ix+np.argmin(np.abs(right))
                # left_freq, right_freq = f_1[left_ix], f_1[right_ix]

                print('===== Current width,', r, 'correct =', correct)
                print('Min/max available freq:', np.min(f_1), np.max(f_1))
                print('Resonance freq.:', max_freq)
                print('Left/reso/right:', left_intersect, max_freq, right_intersect)
                print('Quality:', max_freq/(right_intersect-left_intersect))

                ur = self.corrections.correct_resistance(r)
                interval_f = f_1[5]-f_1[4]
                left_freq = ufloat(left_intersect, interval_f/math.sqrt(6))
                right_freq = ufloat(right_intersect, interval_f/math.sqrt(6))
                max_freq = ufloat(max_freq, interval_f/2)

                csvrows.append((
                    r, 1/ur.n, (1/ur).s, max_freq.n, max_freq.s,
                    (right_freq-left_freq).n, (right_freq-left_freq).s,
                    (max_freq/(right_freq-left_freq)).n, (max_freq/(right_freq-left_freq)).s))
                Qs.append(max_freq/(right_freq-left_freq))
                inv_rs.append(1/ur)

        write_csv(datapath_of('strom_peakbreite{}.csv'.format('_korr' if correct else '_roh')),
                  csvhead, csvrows)
        return (np.array(inv_rs), np.array(Qs))

def plot_example_functions():
    """Plot ideal functions."""

    # Resonance curve
    r_s = np.array([correct.correct_resistance(r).n for r in RESONANCE_MEASURES.keys()])
    xs = np.linspace(np.full_like(r_s,1),np.full_like(r_s,3000*2*math.pi),300)

    l = TOTAL_L
    c = TOTAL_C
    resonance = lambda xs, r_s: 1/np.sqrt(r_s**2 + (xs*l - 1/(xs*c))**2)

    fig = plt.figure(figsize=(15,10))
    p1 = fig.add_subplot(111)
    plots = p1.plot(xs/(2*math.pi), resonance(xs, r_s))
    p1.set_xlabel('$f$ / Hz')
    p1.set_ylabel('$1/Z \\sim I_0/U_0$')
    p1.set_xlim(0, 3000)
    p1.set_ylim(0,0.6)
    p1.legend(plots, ['$R_{{tot}} = {:.1f}$ Ohm'.format(correct.correct_resistance(r).n)
                     for r in RESONANCE_MEASURES.keys()])
    p1.grid()
    fig.savefig(figpath_of('resonanzkurve_theoretisch.pdf'))


    # Phase shift curve
    fig = plt.figure(figsize=(15,10))
    p = fig.add_subplot(111)

    phaseshift = lambda xs: np.arctan((xs*l-1/(xs*c))/r_s)
    plots = p.plot(xs/(2*math.pi), phaseshift(xs)/math.pi*180)
    p.legend(plots, ['$R_{{tot}} = {:.1f}$ Ohm'.format(correct.correct_resistance(r).n)
                     for r in RESONANCE_MEASURES.keys()])
    p.set_ylabel('$\\phi$ / °')
    p.set_xlabel('$f$ / Hz')
    p.grid()
    fig.savefig(figpath_of('phasenverschiebung_theoretisch.pdf'))

    # Plot current, resistance, and voltages
    fig = plt.figure(figsize=(15,10))
    p1 = fig.add_subplot(111)
    p2 = p1.twinx()

    r, r_l = 1.5, 2
    xs = np.linspace(np.full_like(r_s,1100),np.full_like(r_s,1700),100)
    predicted_current = resonance(xs*2*math.pi, r)
    imped_c = 1/(xs*2*math.pi*c)
    imped_l = xs*2*math.pi*l
    u_c, u_l = predicted_current*imped_c, predicted_current*np.sqrt(imped_l**2 + r_l**2)

    pi = p2.plot(xs, predicted_current, label='$I_0$', color='blue')[0]
    pxc = p1.plot(xs, imped_c, label='$X_C$', color='red')[0]
    pxl = p1.plot(xs, imped_l, label='$X_L$', color='pink')[0]
    puc = p1.plot(xs, u_c, label='$U_C$', color='green')[0]
    pul  = p1.plot(xs, u_l, label='$U_L$', color='orange')[0]

    p1.legend([pi,pxc,pxl,puc,pul], ['$I_0$', '$X_C$', '$X_L$', '$U_C$','$U_L$'])

    p1.set_xlabel('$f$ / Hz')
    p1.set_ylabel('$|X|$ / $\Omega$, $U$ / V')
    p2.set_ylabel('$I$ / A')
    fig.savefig(figpath_of('uc_ul_verschiebung.pdf'))

    return p1

nm = NoiseMeasurements()
correct = Corrections(nm)
rm = ResonanceMeasurements(correct=correct)

def plot_noise():
    nm.calculate_current_noise(plot=True)
    nm.calculate_voltage_noise(plot=True)
    nm.plot_current_regression()
    nm.plot_voltage_regression()

def calculate_resonance():
    """
    Drive calculation of resonance frequency tables.

    Returns
    -------
    None.

    """
    resonances = rm.find_resonance_frequency_by_intersection()
    resonances_corr = rm.find_resonance_frequency_by_intersection(correct=True)

    print('===== RESONANCE FREQUENCIES BY INTERSECTION =====')
    print('Raw:', analyse.gewichtetes_mittel(unp.nominal_values(resonances),
                                             unp.std_devs(resonances)))
    print('Corrected:', analyse.gewichtetes_mittel(unp.nominal_values(resonances_corr),
                                                   unp.std_devs(resonances_corr)))

def calculate_quality():
    """
    Drive calculation and regression of LCR quality. Results are saved as
    CSV tables and PDF plots.

    Returns
    -------
    None.

    """
    invr_C, qs_C = rm.calculate_voltage_peak_quality(voltage_type='C')
    invr_L, qs_L = rm.calculate_voltage_peak_quality(voltage_type='L')

    rm.quality_resistance_regression(invr_C, qs_C, typ='U_C')
    rm.quality_resistance_regression(invr_L, qs_L, typ='U_L')

    invr_C_corr, qs_C_corr = rm.calculate_voltage_peak_quality(voltage_type='C', correct=True)
    invr_L_corr, qs_L_corr = rm.calculate_voltage_peak_quality(voltage_type='L', correct=True)

    # rm.quality_resistance_regression(invr_C_corr, qs_C_corr, typ='_C_korr')
    # rm.quality_resistance_regression(invr_L_corr, qs_L_corr, typ='_L_korr')

    invr_Ph, qs_Ph = rm.find_phase_45_quality()

    rm.quality_resistance_regression(invr_Ph, qs_Ph, typ='Phasendiff')

    invr_curr, qs_curr = rm.find_sqrt2_current_width()
    invr_curr_corr, qs_curr_corr = rm.find_sqrt2_current_width(correct=True)

    rm.quality_resistance_regression(invr_curr, qs_curr, typ='Strom')

    predicted_invr = invr_C
    predicted_qs = predicted_invr * umath.sqrt(TOTAL_L_U/TOTAL_C_U)

    rm.quality_resistance_regression(predicted_invr, predicted_qs, typ='Vorhersage')

    # Do a total regression
    rm.quality_resistance_regression(
        np.concatenate([invr_C_corr, invr_L_corr, invr_Ph, invr_curr]),
        np.concatenate([qs_C_corr, qs_L_corr, qs_Ph, qs_curr]), typ='AlleMessungen')

    # Todo: find current maximum and 1/sqrt(2) values
    for r in RESONANCE_MEASURES:
        fig = plt.figure(figsize=(15,7))
        p = fig.add_subplot(111)
        ur = correct.correct_resistance(r)
        ixs = [ix for ix in range(0, invr_C.size) if abs(1/invr_C[ix] - ur) < 0.1]
        labels = ['$U_C$', # '$U_L$',
                  '$\\phi$', '$I_0$', '$\\sqrt{L/C}/R$', '$\\overline{Q_s}$']

        Qs_vals = np.concatenate([a[ixs] for a in [qs_C_corr, predicted_qs, qs_curr]])
        avg, std =  analyse.gewichtetes_mittel(unp.nominal_values(Qs_vals), unp.std_devs(Qs_vals))
        print('Resistance', r, 'weighted average of Qs:', avg, '+/-', std)

        for ix in ixs:
            puc = p.errorbar(1, unp.nominal_values(qs_C_corr)[ix],
                       unp.std_devs(qs_C_corr)[ix], fmt='o', color='red',
                       capsize=10)
            # pul = p.errorbar(2, unp.nominal_values(qs_L_corr)[ix],
            #            unp.std_devs(qs_L_corr)[ix], fmt='o', color='green',
            #            capsize=10)
            pphi = p.errorbar(2, unp.nominal_values(qs_Ph)[ix],
                       unp.std_devs(qs_Ph)[ix], fmt='o', color='blue',
                       capsize=10)
            pcurr = p.errorbar(3, unp.nominal_values(qs_curr)[ix],
                                unp.std_devs(qs_curr)[ix], fmt='o', color='green',
                                capsize=10)
            ppred = p.errorbar(4, unp.nominal_values(predicted_qs)[ix],
                       unp.std_devs(predicted_qs)[ix], fmt='o', color='black',
                       capsize=10)
            pweight = p.errorbar(5, [avg], [std], fmt='o', color='violet',
                                 capsize=10)

        p.set_xlabel('Messung')
        p.set_ylabel('$Q_s$')
        p.set_title('Güte bei $R_{{nominal}} = {:.1f}$'.format(r))
        p.xaxis.set_major_locator(ticker.FixedLocator(range(1, 6)))
        p.xaxis.set_major_formatter(ticker.FixedFormatter(labels))
        p.grid()
        fig.savefig(figpath_of(
            'guete_vergleich_{}.pdf'.format(str(round(r, 1)).replace('.',','))))


def plot_raw_data(resonance=True):
    """
    Plot various raw data series.

    Returns
    -------
    None.

    """

    if resonance:
        rm.plot_rawdata(legend=True)
        rm.plot_rawdata(correct=True,legend=False)
        rm.plot_resonance_currents()
        rm.plot_phase_shifts()
        rm.plot_overvoltage_for_resistors()
        rm.plot_voltage_intersections()
        # for resist in RESONANCE_MEASURES.keys():
        #     for f in RESONANCE_MEASURES[resist]:
        #         CassyUtil.cassy_plot(datapath_of(f),
        #                    xaxis='f_1',
        #                    datenreihen=[('U_A1', '$U_C$'), ('U_B1', '$U_L$'), ('U_2', '$U_0$'), ('I_2', '$I_0$')],
        #                    xlabel='$f$/Hz', ylabel='$U$/V', ylabel_secondary='$\\phi$', ytick=0.3,
        #                    title='Resonanzkurve: '+f, datenreihen_secondary=[('&j_2', '$\phi$')],
        #                    savefig=figpath_of(f+'.pdf'))
        #         # CassyUtil.cassy_plot(datapath_of(f),
        #         #            xaxis='f_1', datenreihen=[('&j_2', '$\\phi$')],
        #         #            title='Phasenverschiebung: ' + f,
        #         #            xlabel='$f$/Hz', ylabel='$\\phi$',
        #         #            savefig=figpath_of(f+'_phi.pdf'))

def generate_all_figures_tables():
    plot_raw_data(resonance=True)
    plot_example_functions()
    calculate_quality()
    calculate_resonance()
    plot_noise()