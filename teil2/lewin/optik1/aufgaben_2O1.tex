\section{2O1 Prismenspektrometer}

\begin{aufgabe}{Grundlagen}
  Knappe Beschreibung der theoretischen Grundlagen, Angabe der
  benötigten Formel(n), ohne Herleitung. Definition der verwendeten
  Formelzeichen.
\end{aufgabe}

Beim Übergang von Regionen unterschiedlicher Ausbreitungsgeschwindigkeit werden
elektromagnetische Wellen im Speziellen, aber auch Wellen ganz allgemein, gebrochen. Das
heißt, ihre Ausbreitungsrichtung ändert sich entsprechend des
Geschwindigkeitsunterschieds zwischen den Regionen. Für elektromagnetische Wellen mit der
Vakuumlichtgeschwindigkeit $c_0$ und einer Geschwindigkeit in einem Material $c_1$ wird
dann der Brechungsindex $n$ definiert als

\begin{equation}
 n := c_0 / c_1
\end{equation}

Nach dieser Definition hat das Vakuum, und näherungsweise auch Luft, einen
Brechungsindex $n_0 = 1$. Für die Brechung eines Lichtstrahls an einer Grenzfläche zweier
Materialien gilt das \textit{Snelliussche Brechungsgesetz}, in dem $\alpha_n$ die Winkel
zum Lot auf der Ein- bzw. Ausfallseite sind:

\begin{equation}
 \label{eqn:snellius}
 \frac{n_1}{n_2} = \frac{\sin(\alpha_2)}{\sin(\alpha_1)}
\end{equation}

Reale Materialien haben allerdings keinen konstanten Brechungsindex, sondern eine
Dispersionskurve $n(\lambda)$, d.h. der Brechungsindex hängt von der Wellenlänge ab.
Dieses Phänomen wird als Dispersion bezeichnet. Ist $\dv{n}{\lambda} < 0$, dann sprechen
wir von \textit{normaler Dispersion}, und blaues Licht wird stärker abgelenkt als rotes
Licht. Der Fall $\dv{n}{\lambda} > 0$ heißt \textit{anomale Dispersion}, und hat zur
Folge, dass längerwelliges Licht stärker abgelenkt wird als kürzerwelliges Licht.

Die Dispersionskurve hat somit sowohl Regionen normaler, als auch anomaler
Dispersion. Bei jenen Frequenzen, die mit den Elektronensystemen im Material Resonanz
erzeugen, erfährt die Dispersionskurve Einsdurchgänge. Die Dispersionskurve kann zwischen
zwei Einsdurchgängen $\omega_{0,1}, \omega_{0,2}$ dann näherungsweise wie
folgt parametrisiert werden:

\begin{equation}
 \label{eqn:sellmeier}
 n^2 - 1 = \frac{a_1}{1-b_1/\lambda^2} + \frac{a_2}{1- b_2/\lambda^2} + a
\end{equation}

Für unsere Auswertung benutzen wir die einfachere, modifizierte Sellmeier-Formel:

\begin{equation}
 \label{eqn:sellmeiercauchy}
 n(\lambda) = a + \frac{b}{\lambda^2} + \frac{b'}{\lambda^4}
\end{equation}

In diesem Versuch messen wir den Brechungsindex eines Glas-Prismas. Aufgrund
der Dispersion fächert es einen Lichtstrahl in die einzelnen Spektralkomponenten auf. Der
Lichtstrahl wird dabei um den Winkel $\delta$ von der Eintrittsrichtung abgelenkt. Wird
das Prisma dabei gedreht, ergibt sich bei einer bestimmten Ausrichtung gegen den
einfallenden Lichtstrahl ein minimaler Ablenkungswinkel $\delta_{min}$ -- konkret dann,
wenn das Prisma vom Lichtstrahl spiegelsymmetrisch durchlaufen wird, und Eintritts- sowie
Austrittswinkel zum Lot gleich sind. Aus dieser minimalen gesamten Ablenkung
$\delta_{min}$ zwischen ein- und ausfallendem Lichtstrahl und dem Schenkelwinkel
$\epsilon$ lässt sich der Brechungsindex bestimmen:

\begin{equation}
 \label{eqn:deltabrechung}
 n = \frac{\sin((\delta_{min} + \epsilon)/2)}{\sin(\epsilon / 2)}
\end{equation}

Im zweiten Teil des Versuchs soll das Auflösungsvermögen $A$ des Prismas bestimmt werden.
Kann bei einer Wellenlänge $\lambda$ ein Wellenlängenunterschied von $\Delta \lambda$
noch getrennt werden, dann ist $A$ definiert als

\begin{equation}
 \label{eqn:defaufloesung}
 A = \frac{\lambda}{\Delta \lambda}
\end{equation}

und kann bei einer Basisbreite des voll ausgeleuchteten Prismas $S$ bzw. der Breite des
einfallenden Lichtstrahls $a < S$ aus den bekannten Winkeln $\epsilon$ und $\delta_{min}$
sowie der Ableitung $\dv{n}{\lambda}$ der Dispersionskurve berechnet werden:

\begin{equation}
\label{eqn:aufloesungsvermoegen}
 A = \dv{n}{\lambda} S = \dv{n}{\lambda} 2 a \frac{\sin(\epsilon / 2)}{\cos((\delta_{min}
+ \epsilon)/2)}
\end{equation}

Um im dritten Teil des Versuchs vom Brechungsindex auf die Wellenlänge einer
Spektrallinie zu schließen, muss die Dispersionskurve \ref{eqn:sellmeiercauchy}
invertiert werden:

\begin{equation}
 \label{eqn:inverteddispersion}
 \lambda^2 = \frac{a b}{n - a}, \qquad b' \ll 1
\end{equation}

wobei $a, b$ die Regressionskonstanten aus \autoref{eqn:sellmeiercauchy} sind, und der
Faktor $b'$ vernachlässigt wird. \autoref{eqn:sellmeierinvertiert} ist die Umkehrung
für den Fall, dass $b'$ nicht vernachlässigt werden kann.

\FloatBarrier
\clearpage

\begin{aufgabe}{Versuchsaufbau und Versuchsdurchführung}
  Beschreibung des Versuchsaufbaus einschließlich beschrifteter Skizze
  oder Foto. Beschreibung der Versuchsdurchführung einschließlich
  aller Handgriffe an der Apparatur.
\end{aufgabe}

Der Versuch findet an einem Prismenspektrometer statt; das ist ein Goniometer mit
Kollimator- und Fernrohr, sowie einem mittig angebrachten, drehbaren Prismentisch, auf
dem das Prisma liegt. Während die Lichtquelle und das Kollimatorrohr fest befestigt
sind, kann der Winkel des Fernrohrs eingestellt werden. Als Lichtquelle haben wir
Edelgasglimmlampen und Metalldampflampen benutzt. Diese haben ein wohlbekanntes Spektrum
und genau definierte Spektrallinien, mithilfe derer wir die Dispersionskurve bestimmen
konnten. Die Lampen werden über ein Drosselnetzteil mit passender Spannung versorgt.

\begin{figure}[p]
 \centering
 \includegraphics[scale=0.8]{img/skizze.pdf}
 % skizze.pdf: 499x258 px, 72dpi, 17.60x9.10 cm, bb=0 0 499 258
 \caption{Skizze des Versuchsaufbaus (Quelle: S.\,\pageref{sec:sources})}
 \label{fig:aufbauskizze}
\end{figure}

\begin{figure}[p]
 \centering
 \includegraphics[scale=0.7]{img/aufbau.jpg}
%  \includegraphics{example-image}
 % aufbau.jpg: 0x0 px, 0dpi, 0.00x0.00 cm, bb=
 \caption{Foto des Aufbaus. Nicht zu sehen: Netzteil, Schlitzblende für dritten
Teilversuch}
 \label{fig:aufbaufoto}
\end{figure}

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.8]{img/spektrallinien.jpg}
 % spektrallinien.jpg: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Blick durchs Okular: Spektrallinien der Hg-Cd-Lampe.}
 \label{fig:spektrallinien_okular}
\end{figure}

Ein Linsensystem bündelt zunächst das Licht der Lichtquelle, bevor es das Prisma
durchläuft. Für die Beobachtung wird das Licht im Okular gebündelt und zur Betrachtung
wieder in einen kollimierten Strahl geformt. Dann sieht das Auge die Spektrallinien im
Unendlichen, überlagert durch ein Fadenkreuz im Okular. Die Sichtbarkeit des Fadenkreuz
haben wir durch Streulicht einer Schreibtischlampe weiter erhöht; dann wurden die
Spektrallinien mit einem hellen Abbild eines z.B. Blatts Papier überlagert, das
Fadenkreuz erschien kontrastreicher, und das Fernrohr konnte genauer ausgerichtet werden.

Lichtquelle und Okular waren auf dem Goniometer so montiert, dass die absolute
Winkelposition des Fernrohrs genau abgelesen werden kann ($\pm \SI{1}{\arcminute}$).
Maßgeblich in diesem Versuch war nur die Differenz zwischen den zwei Absolutwinkeln
$\psi_{1,2}$, unter denen der eine Spektrallinie beobachtet werden kann. Wir haben immer
die selbe der beiden Noniusskalen benutzt, sodass die Ergebnisse nicht unter einer
ungenauen Montage der zwei Skalen leiden.

Wir mussten außerdem das Prisma mit der Nummer 3 auf dem kleinen Tisch in der Mitte des
Aufbaus vor Gebrauch justieren: Die Mittellinie (Winkelhalbierende) des Prismas muss von
der Drehachse geschnitten werden, sodass sich die Lage des Prismas beim Drehen des
Prismentischs nicht signifikant verändert. Der Drehteller selbst lagert auf drei
Schrauben, mit denen wir unter Zuhilfenahme einer Libelle den Prismentisch eben
eingestellt haben. Das Fernrohr haben wir mittels einer Einstellschraube auf die
Spalte fokussiert, sodass die Linien scharf sichtbar waren, und den Spalt vor der Lampe
so eingestellt, dass er im Brennpunkt der Kollimatorlinse steht. Veränderte sich das
Spaltbild beim Bewegen des Okulars zudem nicht in der Höhe, und war die
Quecksilber-Doppellinie durch das Flintglas-Prisma visuell trennbar, war der Aufbau
korrekt justiert.

Im \textbf{ersten Teil} des Versuchs haben wir zuerst die abgelesene Winkelgenauigkeit
bestimmt. Dazu haben wir das Prisma so lange gedreht, bis eine spezifische
Spektrallinie beim Drehen des Prismas eine Umkehr der Bewegungsrichtung zeigte. Den
exakten Winkel, unter dem die beobachtete Spektrallinie die Richtungsumkehr zeigte, haben
wir gemessen, indem wir das Fernrohr mit Hilfe des eingebauten Fadenkreuzes präzise auf
die zu messende Linie ausgerichtet, und den Winkel an der Noniusskala abgelesen
haben. Nach einer Drehung des Prismas und Fernrohrs haben wir den Umkehrpunkt der
gleichen Linie in der anderen Richtung gesucht. Der Winkelunterschied zwischen den zwei
Umkehrpunkten $\psi_1, \psi_2$ ist $2 \delta_{min}$, also der maßgebliche Winkel für die
spätere Berechnung des Brechungsindex nach \autoref{eqn:deltabrechung}. Die beiden
Umkehrpunkte haben wir jeweils zehnmal gemessen und notiert. Daraus ergab sich die
Schätzung der Unsicherheit unserer manuellen Winkelmessung durch Betrachtung der
Stichprobenstandardabweichung.

Im \textbf{zweiten Teil} haben wir dann die Winkelpositionen der Spektrallinien nur noch
je zweimal gemessen, und die vorausgegangene Unsicherheitsmessung zur Berechnung der
Messunsicherheit verwendet. Eigentlich gemessen haben wir die Minimalwinkel
$\delta_{min}$, wie im vorigen Absatz beschrieben. Diese Messung haben wir mit
Spektrallinien über das gesamte Spektrum hinweg wiederholt. In der Auswertung haben wir
die erhaltenen Daten verwendet, um die Dispersionskurve zu ermitteln; dazu passen wir
eine Gerade bzw. Parabel an die modifizierte Sellmeier-Formel
\autoref{eqn:sellmeiercauchy}, mit $\lambda^{-2}$ auf der x-Achse und $n$ auf der
y-Achse. Es stellte sich heraus, dass der quadratische Anteil für eine ausreichende
Genauigkeit nicht benötigt war.

Um im \textbf{dritten Teil} das Auflösungsvermögen zu erhalten, betrachten wir die gelbe
Queck\-silber-Doppellinie, die einen Abstand von \SI{2.1}{\nm} aufweist. Aus
\autoref{eqn:aufloesungsvermoegen} und dem $\dv{n}{\lambda}$
(\autoref{eqn:dndlambda}) aus dem zweiten Versuchsschritt ermittelten wir
dann das Auflösungsvermögen. Dazu haben wir die Strahlbreite variiert, indem wir eine
Schlitzblende unterschiedlicher Breite vor die Kollimatorlinse eingesetzt haben. Diejenige
größte Strahlbreite, die gerade noch die Trennung der Spektrallinien erlaubt, bestimmt
somit das Auflösungsvermögen $A$ des Prismas.

Schließlich benutzten wir im \textbf{vierten Teil} unsere Dispersionskurve noch, um die
absolute Lage der Spektrallinien einer anderen Lichtquelle zu untersuchen. Dazu haben wir
die HgCd-Lampe durch eine Natriumlampe ersetzt, und wie im ersten Schritt über
$\delta_{min}$ den Brechungsindex bei der Wellenlänge der Natrium-D-Linie bestimmt. Aus
der oben bestimmten Dispersionskurve konnten wir dann die Wellenlänge der Spektrallinie
rekonstruieren.

\FloatBarrier
\clearpage

\begin{aufgabe}{Bestimmung der Winkelgenauigkeit}
  Vermessen Sie zunächst eine Spektrallinie auf einer Seite und
  wiederholen Sie den gesamten Messvorgang mindestens
  zehnmal. Tabellieren Sie Ihre Messwerte (in Grad und Bogenminuten)
  und stellen Sie sie als Häufigkeitsverteilung dar. Berechnen Sie die
  Standardabweichung als Maß für Ihre Winkelgenauigkeit.
\end{aufgabe}

In der Rauschmessung wird die zu erwartende Messunsicherheit beim Anvisieren und Ablesen
der Winkel der Spektrallinien gemessen. Wir haben die Lage der roten Cd-Linie wiederholt
gemessen, und zwar auf beiden messbaren Positionen (also beiden Prismenseiten). Die
gemessenen Werte sind der \autoref{tab:uncertainties} zu entnehmen.

Um aus beiden Messreihen die zu erwartende Standardabweichung zu bestimmen, zentrieren
wir sie erst um die jeweiligen Mittelwerte:

\[\psi_i' = \psi_i - \overline \psi_i\]

Die Stichprobenstandardabweichung der verschobenen Werte bleibt gleich,
da die Varianz invariant gegenüber konstanten Verschiebungen $c$ der Zufallsvariable ist:

\begin{align*}
 \mathrm{Var}(X - c) &= \langle (X-c)^2 \rangle - \langle X-c \rangle ^2 \\
 &= \langle X^2 - 2 X c + c^2 \rangle - (\langle X \rangle - c)^2 \\
 &= \langle X^2 \rangle - 2 c \langle X \rangle  + \langle c^2 \rangle - \langle X
\rangle ^2 + 2 c \langle X \rangle - c^2 \\
 &= \langle X^2 \rangle - \langle X \rangle ^2 = \mathrm{Var}(X)
\end{align*}

Mit den gemessenen, zentrierten Werten ergibt sich eine Standardabweichung für die
Winkelmessung von $\sigma_w := \sqrt{\frac{1}{N-1} \sum_i (\psi_i - \overline
\psi)^2} = \SI{58}{\arcsecond}$ (hier $N=20$). Da die Noniusskala eine
Ableseenauigkeit von nur \SI{60}{\arcsecond} besitzt, eine genauere Messung somit
nicht sinnvoll möglich war, setzen wir die Messunsicherheit auf

\begin{equation}
\label{eqn:winkelunsicherheit}
\sigma_w := \SI{60}{\arcsecond} = \SI{1}{\arcminute} = \SI{2.91e-4}{\radian}
\end{equation}

Die Verteilung der zentrierten Werte um Null ist in \autoref{fig:angle_uncertainty_dist}
gezeigt. Unter der Annahme, dass die Skala gleichmäßig unterteilt ist, treten keine
systematischen Messfehler auf, da nur Relativwinkel gemessen werden.

\begin{table}[h]
\centering
 \input{img/uncertainties.tex}
 \caption{Tabelle der gemessenen Werte in der Rauschmessung. Entgegen der Anleitung
wurden zwei Positionen gemessen.}
 \label{tab:uncertainties}
\end{table}

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.6]{img/winkelrauschen.pdf}
 % winkelrauschen.pdf: 0x0 px, -2147483648dpi, 0.00x0.00 cm, bb=
 \caption{Die Verteilung der Winkelmessungen, zentriert um den gemessenen Mittelwert.}
 \label{fig:angle_uncertainty_dist}
\end{figure}

\FloatBarrier
\clearpage

\begin{aufgabe}{Vermessung der Dispersionskurve eines Prismas}
  Vermessen Sie die Dispersionskurve des vorhandenen Prismas mit einer
  HgCd-Lampe. Messen Sie jede Linie $2\times$ auf jeder Seite aus,
  bzw.~messen Sie abwechselnd unabhängig voneinander, um grobe
  Messfehler auszuschließen. Tabellieren Sie zunächst die jeweils
  gemessenen Ablenkwinkel (in Grad und Bogenminuten) und ordnen Sie
  die jeweiligen Wellenlängen zu. Zeigen Sie die sich ergebende
  Dispersionskurve $n(\lambda)$. Führen Sie in geeigneter Weise eine
  Anpassungsrechnung mit der vereinfachten Sell\-mei\-er-For\-mel
  durch und vergleichen Sie Ihre Dispersionskurve mit einschlägigen
  Herstellerangaben.
\end{aufgabe}

Der \autoref{tab:ablenkwinkel} sind die Messwerte der Ablenkwinkel $\delta_{min}$ zu
entnehmen. Die Messunsicherheit jeder Messung beträgt gemäß
\autoref{eqn:winkelunsicherheit} $\SI{1}{\arcminute}/\sqrt{2} = \SI{0.71}{\arcminute}$
bzw. $\SI{0.0118}{\degree} = \SI{2.05e-4}{\radian}$.

Bei der Messung der Winkel wurde ursprünglich der Winkel $\psi_1$ der Hg-Linie
\SI{435.83}{\nm} gleich zweimal ein halbes Grad zu hoch abgelesen. Dies fiel erst bei der
Auswertung auf, da der entsprechende Messwert zu einem deutlich erhöhten Brechungsindex
bei dieser Spektrallinie führte, und im Residuenplot isoliert werden konnte. Da diese Art
Fehler zu Beginn bekannt war, konnte die Messung durch Abziehen von \ang{;30;} korrigiert
werden. Danach fiel die Unsicherheit in die selbe Größenordnung der anderen Messwerte.

\begin{table}[h]
\centering
 \input{img/measured_spectrals.tex}
 \caption{Gemessene Ablenkwinkel, Messunsicherheit jeweils $\sigma_w/\sqrt{2} =
\SI{42.5}{\arcsecond}$, sowie berechnete Brechungsindices (\autoref{eqn:deltabrechung})}
 \label{tab:ablenkwinkel}
\end{table}

Da die Berechnung des Brechungsindex gemäß \autoref{eqn:deltabrechung} stattfand,
verhalten sich die fortgepflanzte Messunsicherheiten wie

\begin{align}
 \label{eqn:sigma_n}
 \overline \psi_i =
(\psi_{i,1}+\psi_{i,2}) / 2 &\Rightarrow \sigma_{\overline \psi_i}^{(*)} = \sigma_w /
\sqrt{2}
\\
 \delta_{min} = (\overline \psi_2 - \overline \psi_1)/2  &\Rightarrow
\sigma_{\delta_{min}} = \sigma_{\overline \psi_i} / \sqrt{2} = \sigma_w / 2
\\
 \text{\autoref{eqn:deltabrechung}} &\Rightarrow \sigma_n = \frac{1}{2 \sin(\epsilon/2)}
\cos(\frac{\delta_{min}+\epsilon}{2}) \underbrace{\sigma_{\delta_{min}}}_{\sigma_w / 2}
\end{align}

% Legen wir fest, dass der mittlere Winkel $\overline \psi_i$ nicht genauer als die
% Skalenunterteilung gemessen werden kann, dann ist im Folgenden
%
% \[(*)~\sigma_{\overline \psi_i} = \max\qty(\frac{\sigma_w}{\sqrt{2}}, 1')\]

Die verwendeten Wellenlängen $\lambda$ sowie der Prismenwinkel
$\epsilon$ werden als frei von Unsicherheiten angenommen. Daraus ergeben sich die
Messunsicherheiten auf $n$.

In \autoref{fig:regression_lin} und \autoref{fig:regression_quad} sind die berechneten
linearen und quadratischen Regressionen eingezeichnet, deren Parameter sich in
\autoref{tab:regressionsparameter} finden, und sich auf die vereinfachte
Sellmeier-Gleichung beziehen:

\[
 n(\lambda) = a + \frac{b}{\lambda^2} + \frac{b'}{\lambda^4}
\]

Das lineare Modell \autoref{fig:regression_lin} ist offensichtlich nicht für unsere
Anwendung geeignet: Der Residuenplot zeigt eine parabelförmige systematische Abweichung,
und das Verhältnis $\chi^2/$Freiheitsgrade ist viel größer als 1. Besser hingegen passt
das quadratische Modell in \autoref{fig:regression_quad}: Die Abweichungen sind
kompatibel mit dem Modell. Der Residuenplot ist wohlverteilt, und das Verhältnis $\chi^2
/ $Freiheitsgrade beträgt ungefähr 1; beide Merkmale sprechen für eine gute
Anpassungsgüte. Die weitere Auswertung nutzt daher nur noch das quadratische Modell.

\begin{table}[h]
 \centering
 \input{img/regressionsparameter.tex}
 \caption{Parameter der Regressionen, für \autoref{eqn:sellmeiercauchy}}
 \label{tab:regressionsparameter}
\end{table}


\begin{figure}[p]
 \hspace{-5em}
 \includegraphics[scale=0.5]{img/brechungsindices.pdf}
 \caption{Berechnete Brechungsindizes, $\approx 1/\lambda^2$-Gesetz}
 \label{fig:brechungsindices}
\end{figure}
\begin{figure}[p]
 \includegraphics[scale=0.4]{img/regression_Lin.pdf}
 \caption{Lineare Regression durchgeführt für die berechneten Brechungsindices}
 \label{fig:regression_lin}
\end{figure}
\begin{figure}[p]
 \includegraphics[scale=0.4]{img/regression_Quad.pdf}
 \caption{Quadratische Regression durchgeführt für die berechneten Brechungsindices}
 \label{fig:regression_quad}
\end{figure}

Der Vergleich mit Herstellerangaben liefert, dass die Dispersionskurve des Prismenglas am
nächsten an den Dispersionskurven des schweren bleifreien Flintglases \textit{N-SF10}
(\autoref{fig:nsf10}) oder des schweren bleihaltigen Flintglases \textit{SF10}
(\autoref{fig:sf10}) des Herstellers Schott liegt (s. Quellen). Für die meisten
Wellenlängen beträgt die Abweichung $\Delta n < \num{5e-4}$; diese ist grafisch in
\autoref{fig:refractive_comparison} dargestellt. Andere Gläser haben deutlich andere
Brechungsindices, und werden hier nicht behandelt.

Auffallend ist, dass das Modell immer kleinere Werte als den
Literaturwert ergibt: Die Literaturwerte beider Gläser liegen oberhalb unseres
berechneten Modells der Dispersionskurve. Außerdem folgen die Abweichungen
einer nach unten geöffneten Parabel -- womöglich ist unser Modell ungenau im Rahmen der
hier eingezeichneten Abweichungen. Dies zeigt auch der Vergleich mit der violetten
Kurve, die aus dem Modell der Praktikumsbibliothek gewonnen wurde. Dabei ist zu beachten,
dass die Literaturwerte durchaus kompatibel mit unserem Modell sind: Die grünen Kurven
zeigen die $\pm 1\sigma$-Unsicherheit der Regression, berechnet aus den Unsicherheiten
der Regressionsparameter $\sigma_a, \sigma_b, \sigma_{b'}$. Die violette Kurve im unteren
Teil stellt die Differenz zur Dispersionskurve in der Bibliothek
\texttt{praktikum.literaturwerte} dar.

Die beiden schweren Flintgläser \textit{SF10} und \textit{N-SF10} sind dabei
nur kaum auseinanderzuhalten. Es ist also nicht oder nur schwer möglich, eine definitive
Entscheidung basierend auf unserer Messung zu treffen. Wollte man eine eindeutige
Entscheidung treffen, welches Glas im Prisma 3 vorliegt, böte sich noch eine
Dichtemessung an. Aufgrund des Bleioxid-Gehalts ist \textit{SF10} mit $\rho_{SF10} =
\SI{4.28}{\gram\per\cm\tothe3}$ mehr als 40\,\% dichter als das schwermetallfreie
\textit{N-SF10} mit $\rho_{N-SF10} = \SI{3.05}{\gram\per\cm\tothe3}$.

\begin{figure}[h]
 \hspace{-5em}
 \includegraphics[scale=0.5]{img/literaturvsmessung.pdf}
 % literaturvsmessung.pdf: 0x0 px, -180365231dpi, 0.00x0.00 cm, bb=
 \caption{Vergleich der gemessenen und dem Datenblatt von N-SF10 entnommenen
Brechungsindices. Im unteren Diagramm sind die Differenzen zwischen Literatur und Modell
für zwei Typen schweren Flintglases eingezeichnet, sowie die Abweichung von der
Modellkurve in der Praktikumsbibliothek.}
 \label{fig:refractive_comparison}
\end{figure}

\begin{figure}[p]
\centering
\begin{subfigure}[b]{0.45\textwidth}
 \includegraphics[width=\textwidth]{img/N-SF10.pdf}
 % N-SF10.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Vergleichswerte des Brechungsindex für Glass N-SF10 (bleifrei)}
 \label{fig:nsf10}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
 \includegraphics[width=\textwidth]{img/SF10.pdf}
 % N-SF10.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Vergleichswerte des Brechungsindex für Glass SF10}
 \label{fig:sf10}
\end{subfigure}
\end{figure}

\FloatBarrier
\clearpage

\begin{aufgabe}{Bestimmung des Auflösungsvermögens}
  Bestimmen Sie mit der gelben Hg-Doppellinie das Auflösungsvermögen
  des Prismas und vergleichen Sie es mit Ihrer Erwartung anhand der
  Dispersionskurve.
\end{aufgabe}

Zur Bestimmung des Auflösungsvermögens haben wir eine variable Spaltblende mit
Spaltbreiten zwischen \SI{0.5}{\mm} und \SI{6}{\mm} benutzt. In der Messung war die gelbe
Hg-Doppellinie bis hinunter zu einer Blende von \SI{1.5}{\mm} als solche erkennbar.

In unserem Versuch kann das Auflösungsvermögen mit \autoref{eqn:aufloesungsvermoegen}
bestimmt werden. Zur Erinnerung:

\[ A = \dv{n}{\lambda} 2 l \frac{\sin(\epsilon / 2)}{\cos((\delta_{min} +
\epsilon)/2)}\]

Die Werte betragen nun $l = \SI{1.5}{\mm}$, $\epsilon = \SI{60}{\degree}$, $\delta_{min,
Hg, gelb} = \SI{59}{\degree} \num{39}' \num{18}''$ (\autoref{tab:ablenkwinkel}). Die
Steigung $\dv{n}{\lambda}$ der Dispersionskurve beträgt für die Lage der gelben
Doppellinie ist die Ableitung der Dispersionskurve \autoref{eqn:sellmeiercauchy}:

\begin{equation}
 \label{eqn:dndlambda}
 \dv{n}{\lambda} = -\frac{2b}{\lambda^3} - \frac{4 b'}{\lambda^5}
\end{equation}

Setzen wir die Mitte der gelben Hg-Linie ein, die wir gemessen haben, dann erhalten wir

\[\dv{n}{\lambda} = \SI{-1.347e5}{\per\meter}\]

Bei Ersetzung von $\dv{n}{\lambda} \rightarrow \qty|\dv{n}{\lambda}|$ erhalten wir so bei
$l = \SI{1.5}{\mm}$

\[A = \num{4.0e2}\]

Die Definition des Auflösungsvermögen (\autoref{eqn:defaufloesung})
liefert uns auch eine untere Grenze für das Auflösungsvermögen. Für die gelbe
Hg-Doppellinie ist $\lambda_{Hg, doppel} = \SI{579}{\nm}$ und $\Delta \lambda =
\SI{2.11}{\nm}$, somit

\begin{equation}
 A_{erwartet} = \frac{\lambda}{\Delta \lambda} = \num{2.75e2}
\end{equation}

Wäre die Entscheidung für den Spalt mit $l=\SI{2}{\mm}$ bzw. den Spalt mit
$l=\SI{1}{\mm}$ gefallen, so würde das Auflösungsvermögen jeweils $A_+ = \num{5.4e2}$
bzw. $A_- = \num{2.3e2}$ betragen. Dies erlaubt eine grobe Abschätzung der
Unsicherheit: die Auflösung der verwendeten Methode ist also etwa $\Delta A =
\num{1.3e2}$. Da die Linie bei $l = \SI{1.5}{\mm}$ noch schwach als getrennt erkennbar
war, bei $l_- = \SI{1}{\mm}$ aber nicht mehr, und die untere Grenze bei \num{2.75e2}
liegt, muss das Auflösungsvermögen also in dieser Spanne liegen.

Das gemessene Auflösungsvermögen ist also größer, und deshalb lässt sich die Doppellinie
auch gut beobachten.

\FloatBarrier
\clearpage

\begin{aufgabe}{Vermessung einer unbekannten Spektrallinie}
  Tauschen Sie die Spektrallinie gegen eine Na-, Zn-, He- oder
  Ne-Lampe aus, und vermessen Sie eine prominente Linie dieser
  Lampe. Bestimmen Sie über die Invertierung der Dispersionskurve die
  Wellenlänge der Spektrallinie und vergleichen Sie sie mit dem
  Literaturwert.
\end{aufgabe}

\begin{table}[h]
 \centering
 \input{img/linienbestimmung.tex}
 \caption{Messergebnisse der Linienvermessung}
 \label{tab:linienbestimmung}
\end{table}

Invertiert man \autoref{eqn:sellmeiercauchy}, so erhält man

\begin{equation}
 \label{eqn:sellmeierinvertiert}
 \lambda(n) = + \sqrt{\frac{+ \sqrt{4 a b' + b^2 + 4 b' n} + b}{2n - 2a}}
\end{equation}

Diese Gleichung ist grafisch in \autoref{fig:dispersion_invertiert} dargestellt.

Die Messunsicherheit der Winkel pflanzt sich gemäß \autoref{eqn:sigma_n} auf den
Brechungsindex fort. Mit der Messunsicherheit des Brechungsindex setzen wir den größten
und den kleinsten zu erwartenden Wert in die Invertierung ein, und bestimmen aus den
erhaltenen Werten die Messunsicherheit $\sigma_\lambda =
(\lambda(n+\sigma_n)+\lambda(n-\sigma_n))/2$.

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.4]{img/dispersion_invertiert_Na-D.pdf}
 % dispersion_invertiert.pdf: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Invertierte Dispersionskurve und berechnete Wellenlänge der Na-Doppellinie}
 \label{fig:dispersion_invertiert}
\end{figure}

In \autoref{tab:linienbestimmung} ist das Ergebnis der Messung und daraus folgender
Berechnungen zu sehen. Die Unsicherheit ist wegen der kleinen Winkelunsicherheit auch für
den Brechungsindex sehr klein; die Abweichung der gemessenen Lage der Spektrallinie von
der idealen/eigentlichen Lage ist als $\Delta \lambda$ notiert, und beträgt hier etwa $2
\sigma_{\lambda}$. Ein Grund für die Abweichung könnte sein, dass die Na-Doppellinie im
Okular sehr hell und relativ breit erscheint. Dadurch wird auch die Positionierung des
Fadenkreuzes erschwert, und bei nur zwei Messungen pro Winkel ist dann eine Fehlmessung
nicht unwahrscheinlich. Deshalb ist die Wellenlänge der Doppellinie hier (und z.B.
auch in den Datenblättern der Glassorten weiter oben) als $\lambda_D = \SI{589.3}{\nm}$
angegeben, also der Mittelwert der Wellenlängen beider Linien.

Durch die komplizierte und fehleranfällige Vorgehensweise -- Umweg über Modellbestimmung
der Dispersionskurve, Umkehr dieser, und dann Messung des Brechungsindex für eine
bestimmte Spektrallinie -- ist dieses Verfahren nicht so genau wie andere, direktere
Verfahren. Wollte man die Wellenlänge exakt bestimmen, wäre zum Beispiel ein
hochauflösendes Gitter besser geeignet. Allerdings hat die Bestimmung mit Hilfe eines
Prismas im Vergleich dazu den Vorteil, relativ einfach realisierbar zu sein: Es sind
keine aufwendigen Herstellungsmethoden wie die eines Gitters nötig, es muss nur ein Stück
Flintglas einigermaßen genau geschliffen werden.

\section{Quellen}
\label{sec:sources}

\begin{description}
\item[\autoref{fig:aufbauskizze}] Gordian Edenhofer: \textit{Versuchsaufbau eines
Prismenspektrometers}, 2015.
\url{https://commons.wikimedia.org/wiki/File:Versuchsaufbau.svg}
\item[\autoref{fig:nsf10}, Literaturwerte N-SF10/SF10] Schott AG, Mainz\\
\url{https://shop.schott.com/medias/SCHOTT-datasheet-N-SF10.pdf}; \url{
https://shop.schott.com/medias/SCHOTT-Datenblatt-SF10.pdf}
\end{description}

