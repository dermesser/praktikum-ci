#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 16:37:53 2020

Analyze OptikII data.

@author: lbo
"""

from collections import namedtuple
import math
from os import path

import pandas
import matplotlib.pyplot as plt
import numpy as np
from uncertainties import ufloat
from uncertainties import umath
from uncertainties import unumpy as unp
from pylatex import table

from praktikum import analyse, literaturwerte
import nice_plots

CALIBRATION_CSV = '../daten/kalibrierung.csv'
WAVELENGTH_CSV = '../daten/wellenlänge_m_korrigiert.csv'
AIRPRESSURE_CSV = '../daten/luftdruck.csv'
FIGURE_OUTPUT = '../img/'

def read_gnumeric_csv(path):
    """Gnumeric exports many extra lines, which are empty."""
    df = pandas.read_csv(path)
    columns = {}
    for c in list(df):
        if 'Unnamed' not in c:
            columns[c] = (df[c].dropna())
    return columns

def npreverse(nd):
    return nd[-1::-1]

def plot_raw_values_with_averages(m, measurements, title='', savefig=None):
    # Generate scatter plot of raw measurements.
    fig = plt.figure(figsize=(15,10))
    p = fig.subplots(1, 1, sharex=True, gridspec_kw={'height_ratios': [5]})
    p.set_title(title)
    p.set_ylabel('$s_i$/µm')
    avgs = np.array([measurements[:,i].mean() for i in range(0, measurements.shape[1])])
    for i, meas in enumerate(measurements):
        p.scatter(m[i], meas, label='$s_{}$'.format(i+1))
    p.legend()
    p.grid()
    if savefig:
        fig.savefig(savefig)

def plot_discrete_errorbars(ks, title='', xlabel='', ylabel='', labels=None, save=None):
    # EXPERIMENTAL: Plot different k's
    nplots = ks.shape[0]

    fig = plt.figure(figsize=(15,10))
    #plots = fig.subplots(1, nplots, sharey=True)
    plot = fig.add_subplot(111)
    for i in range(nplots):
        plot.errorbar(np.arange(1, ks.shape[1]+1),
                   unp.nominal_values(ks[i]),
                   unp.std_devs(ks[i]), fmt='o', label=labels[i] if labels else str(i+1))
        plot.set_xlabel(xlabel)
        plot.set_ylabel(ylabel)
        plot.set_title(title)
        plot.grid(True)
        plot.legend()
    if save:
        fig.savefig(save)

class Latex:
    def fmt_num(n, prec=3, exp=False):
        l = 'e' if exp else 'f'
        fmtstr = '\\num{{{{ {{:.{}{}}} }}}}'.format(prec, l)
        return fmtstr.format(n)

    def fmt_unc(un, prec=3, sys=None, exp=True):
        v = un.n
        s = un.s

        if exp:
            v_p = int(math.log10(abs(v))) if v != 0 else 0
            if v_p < 0:
                v_p -= 1
            v_s = v / 10**v_p
            s_s = s / 10**v_p
            sys_s = sys / 10**v_p if sys else 0
            s_unc_dig = int(round(s_s * 10**(prec), 0))
            fmtstr = (
                '\\num{{{{ {{:.{}f}}({{:0{}}})e{{:d}} }}}}'.format(prec, prec) if not sys
                else '$\\num{{{{ {{:.{}f}}({{:0{}}})e{{:d}} }}}}~\\text{{{{(stat.)}}}}\
                    ~\\pm \\num{{{{ {{:.{}f}}e{{:d}} }}}}~\\text{{{{(sys.)}}}}$'.format(prec, prec, prec))
            return fmtstr.format(v_s, s_unc_dig, v_p, sys_s, v_p)
        else:
            v_p = 1
            v_s = v
            s_s = s
            s_unc_dig = int(round(s_s * 10**(prec), 0))
            sys_s = sys
            fmtstr = ('\\num{{{{ {{:.{}f}} ({{:0{}}}) }}}}'.format(prec, prec) if not sys
                      else '$\\num{{{{ {{:.{}f}} ({{:0{}}}) }}}}~\\text{{{{(stat.)}}}}\
                          ~\\pm \\num{{{{ {{:.{}f}} }}}}~\\text{{{{(sys.)}}}}$'.format(prec,prec,prec))
            return fmtstr.format(v_s, s_unc_dig, sys_s)


class Calibration:
    class Params:
        ATTRS = set(['rng', 'k', 'k_sys', 'all_ks', 'all_ks_sys', 'regrparams', 'a', 'b'])
        def __init__(self, **kwargs):
            for k, w in kwargs.items():
                assert (k in self.ATTRS), k
                setattr(self, k, w)
        def __repr__(self):
            return 'Calibration.Params({})'.format(
                ', '.join(['{} = {}'.format(k, getattr(self, k)) for k in sorted(self.ATTRS)]))

    # Params = namedtuple(
        # 'CalibrationParams', ('rng', 'k', 'all_ks', 'regrparams', 'a', 'b'))

    def __init__(self, path=CALIBRATION_CSV, ignore=['s2', 'm2'],
                 lmbda=ufloat(0.6328, 0.0001)):
        """Initialize Calibration experiment data.

        Calculations in this class happen in µm.
        """
        data = read_gnumeric_csv(path)
        self.data = data
        self.lmbda = lmbda
        self.m = []
        for col in sorted(data):
            if len(col) == 2 and col.startswith('m') and col not in ignore:
                self.m.append(data[col])
        self.m = np.array(self.m)
        self.measurements = np.array(list(
            map(lambda c: data[c],
                filter(lambda c: len(c) == 2 and c.startswith('s') and c not in ignore,
                       sorted(data.keys())))))

        # Calculate uncertainties for measurements.
        self.std_measures = np.full_like(self.measurements, 10/math.sqrt(12))

    def regressions(self, measure_range=None):
        """
        Calculate regressions.

        Parameters
        ----------
        measure_range : (int,int), optional
            DESCRIPTION. The default is None.

        Returns
        -------
        Calibration.Params

        """
        frm, to = measure_range if measure_range else (0, self.m[0].size)
        a_s = []
        b_s = []
        regrs = []

        for i, meas in enumerate(self.measurements):
            params = analyse.lineare_regression(
                self.m[i, frm:to], meas[frm:to], self.std_measures[i, frm:to])
            a, ea, b, eb, chiq, corr = params
            a_s.append(ufloat(a,ea))
            b_s.append(ufloat(b,eb))
            regrs.append(params)
        a_s = np.array(a_s)
        b_s = np.array(b_s)
        avg_a, std_a = analyse.gewichtetes_mittel(
            unp.nominal_values(a_s), unp.std_devs(a_s))
        avg_b, std_b = analyse.gewichtetes_mittel(
            np.array([p[2] for p in regrs]),
            np.array([p[3] for p in regrs]))
        a = ufloat(avg_a, std_a)
        b = ufloat(avg_b, std_b)

        k = self.lmbda.n/(2*a)
        k_s = self.lmbda.n/(2*a_s)
        k_sys = self.lmbda/(2*a.n)
        ks_sys = self.lmbda/(2*unp.nominal_values(a_s))

        print('----- Calibration: {} -----'.format((frm,to)))
        print('a = {:.5e} +/- {:.5e}'.format(avg_a, std_a))
        print('k = {:.5f} +/- {:.5f} +/- {:.5f} sys'.format(k.n, k.s, k_sys.s))

        return Calibration.Params(
            rng=(frm,to), k=k, k_sys=k_sys, all_ks=k_s, all_ks_sys=ks_sys, regrparams=regrs,
            a=a, b=b)

    def gen_all(self):
        """
        Generate three sets of plots for the different legs of the 'V'.

        Returns
        -------
        None.

        """
        for r in [(0,7),(7,17),None]:
            p = self.regressions(r)
            self.gen_plots(r)
            self.gen_tables(p)


    def gen_plots(self, params):
        """Generate plots for Calibration experiment.


        Parameters
        ----------
        measure_range : (int,int), optional
            Specify for which points to generate plot+regression. The default is None.

        Returns
        -------
        None.

        """
        m = self.m
        frm, to = params.rng
        for i, meas in enumerate(self.measurements):
            a, ea, b, eb, chiq, corr = nice_plots.nice_linear_regression_plot(
                m[i, frm:to], meas[frm:to], self.std_measures[i, frm:to],
                xlabel='$m$', ylabel='$s$/µm',
                ylabelresidue='$(s - (am+b))$ / µm',
                title='Kalibrierung, Messung {}, Werte {}'.format(i+1, (frm,to)),
                save=path.join(FIGURE_OUTPUT,
                               'kalibrierung_{}_{}_{}.pdf'.format(i+1,frm,to)))

        plot_raw_values_with_averages(self.m[:,frm:to], self.measurements[:,frm:to],
                                      title='Messdaten Kalibrierung',
                                      savefig=path.join(
                                          FIGURE_OUTPUT, 'messdaten_{}_{}.pdf'.format(frm,to)))


    def gen_tables(self, params):
        """
        Parameters
        ----------
        params : Calibration.Params
            Parameters for which to generate tables.

        Returns
        -------
        None.

        """
        frm, to = params.rng
        measmts = self.measurements.shape[0]

        # Generate measurement data table.
        tab = table.Tabular('r|'+'l'*self.measurements.shape[0], booktabs=True)
        tab.escape = False
        tab.add_row('m', *['$\Delta s_{:d} / \\si{{\\um}}$'.format(i)
                           for i in range(1, measmts+1)])
        tab.add_hline()
        tab.add_row(*[0]*(measmts+1))
        for i,m in enumerate(self.m[0]):
            tab.add_row(str(int(m)), *[Latex.fmt_num(n, prec=0) for n in self.measurements[:,i]])
        tab.generate_tex(path.join(FIGURE_OUTPUT, 'kalibrierung_tab_{}_{}'.format(frm,to)))

        # Generate overview table of parameters
        regrs = params.regrparams
        k = params.k
        fg = to-frm-3

        tab = table.Tabular('r|'+'l'*len(regrs), booktabs=True)
        tab.escape = False
        tab.add_row('Messung', *list(range(1,len(regrs)+1)))

        tab.add_hline()
        tab.add_row('$a / \\si{\\um}$', *[Latex.fmt_unc(a, prec=2) for a in map(lambda p: ufloat(p[0], p[1]), regrs)])
        tab.add_row('$b / \\si{\\um}$', *[Latex.fmt_unc(b, prec=2) for b in map(lambda p: ufloat(p[2], p[3]), regrs)])

        tab.add_hline()
        tab.add_row('$\\chi^2$', *[Latex.fmt_num(chi,prec=1) for chi in map(lambda p: p[4], regrs)])
        tab.add_row('FG', *([fg]*len(regrs)))
        tab.add_row('$\\chi^2/$FG', *[Latex.fmt_num(chi/fg,prec=1) for chi in map(lambda p: p[4], regrs)])

        tab.add_hline()
        tab.add_row('$k$', *[Latex.fmt_unc(k, prec=2) for i, k in enumerate(params.all_ks)])
        tab.add_row('$\overline k$', Latex.fmt_unc(params.k, prec=2), *(['']*(len(regrs)-1)))

        tab.generate_tex(path.join(FIGURE_OUTPUT, 'kalibrierung_regr_{}_{}'.format(frm,to)))

        # Spit out averaged parameters
        latex = '''
\\begin{{equation}}
\label{{eqn:averaged_regression_{}_{}}}
k = \\frac{{m \\lambda}}{{2 s}} = \\frac{{\\lambda}}{{2 a}} = {}
\\end{{equation}}
'''
        latex = latex.format(frm,to,
            Latex.fmt_unc(k, exp=False, prec=5))
        with open(path.join(FIGURE_OUTPUT, 'regr_param_eq_{}_{}.tex'.format(frm,to)), 'w') as f:
            f.write(latex)


class Wavelength:
    class Params:
        ATTRS = set(['e', 'f', 'rng', 'params',
                     'lmbda', 'lmbda_sys', 'all_lmbdas', 'all_lmbdas_sys'])
        def __init__(self, **kwargs):
            for k, w in kwargs.items():
                assert (k in self.ATTRS)
                setattr(self, k, w)
        def __repr__(self):
            return 'Wavelength.Params({})'.format(
                ', '.join(['{} = {}'.format(k, getattr(self, k)) for k in sorted(self.ATTRS)]))

    # Contains all parameters gained from calculating a regression.
    # Params = namedtuple('WavelengthParams',
    #                     ('e', 'f', 'rng', 'params',
    #                      'lmbda', 'lmbda_sys', 'all_lmbdas', 'all_lmbdas_sys'))

    def __init__(self, path=WAVELENGTH_CSV, ignore=[]):
        """
        Read initial measurement data. Calculations happen in µm if not
        otherwise noted.

        Parameters
        ----------
        path : TYPE, optional
            DESCRIPTION. The default is WAVELENGTH_CSV.
        ignore : TYPE, optional
            DESCRIPTION. The default is [].

        Returns
        -------
        None.

        """
        data = read_gnumeric_csv(path)
        self.m = []
        for col in sorted(data):
            if len(col) == 2 and col.startswith('m'):
                self.m.append(data[col])
        self.m = np.array(self.m)
        self.measurements = np.array(list(
            map(lambda c: data[c],
                filter(lambda c: len(c) == 2 and c.startswith('s') and c not in ignore,
                       data.keys()))))
        self.std_measures = np.full_like(self.m, 10/math.sqrt(12))

    def regressions(self, params):
        """


        Parameters
        ----------
        params : Calibration.Params
            Calibration parameters to use.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        frm, to = params.rng if params.rng else (0, self.m.size+1)
        e_s = []
        f_s = []
        regrs = []

        for i, meas in enumerate(self.measurements):
            regr = analyse.lineare_regression(
                self.m[i,frm:to], meas[frm:to], self.std_measures[i, frm:to])
            e, ee, f, ef, chiq, corr = regr
            e_s.append(ufloat(e,ee))
            f_s.append(ufloat(f,ef))
            regrs.append(regr)

        e_s = np.array(e_s)
        f_s = np.array(f_s)
        avg_e, std_e = analyse.gewichtetes_mittel(
            unp.nominal_values(e_s), unp.std_devs(e_s))
        avg_f, std_f = analyse.gewichtetes_mittel(
            np.array([p[2] for p in regrs]),
            np.array([p[3] for p in regrs]))
        e = ufloat(avg_e, std_e)
        f = ufloat(avg_f, std_f)
        lmbda = 2*params.k.n*e
        all_lmbdas = 2*params.k.n*e_s

        # Systematic error on lambda
        k_total_err = math.sqrt(params.k.s**2 + params.k_sys.s**2)
        k = ufloat(params.k.n, k_total_err)
        lmbda_sys = 2*k*e.n
        all_lmbdas_sys = 2*k*unp.nominal_values(e_s)

        print('----- Wavelength {} -----'.format((frm,to)))
        print('a = {:.5e} +/- {:.5e}'.format(avg_e, std_e))
        print('lmbda = {:.5f} +/- {:.5f} stat +/- {:.5f} sys'.format(lmbda.n, lmbda.s, lmbda_sys.s))

        return Wavelength.Params(
            e=e, f=f, lmbda=lmbda, lmbda_sys=lmbda_sys, rng=params.rng, params=regrs,
            all_lmbdas=all_lmbdas, all_lmbdas_sys=all_lmbdas_sys)

    def gen_tables(self, params):
        frm, to = params.rng
        measmts = self.measurements.shape[0]

        tab = table.Tabular('rl'*measmts, booktabs=True)
        tab.escape = False
        tab.add_row(*[e for l in [['$m_{}$'.format(i), '$s_{} / \\si{{\\um}}$'.format(i)] for i in range(1, measmts+1)]
                             for e in l])
        tab.add_hline()
        tab.add_row(*([0]*measmts*2))
        for i in range(frm, to):
            tab.add_row(*[e for l in [[int(self.m[j,i]), Latex.fmt_num(self.measurements[j,i], prec=0)] for j in range(0, self.measurements.shape[0])]
                                           for e in l])
        tab.generate_tex(path.join(FIGURE_OUTPUT, 'wellenlaenge_messung_{}_{}'.format(frm,to)))

        # Generate overview table of parameters
        regrs = params.params
        fg = to-frm-2

        tab = table.Tabular('r|'+'l'*len(regrs), booktabs=True)
        tab.escape = False
        tab.add_row('Messung', *list(range(1,len(regrs)+1)))
        tab.add_hline()
        tab.add_row('$e / \\si{\\um}$', *[Latex.fmt_unc(e) for e in
                                          map(lambda p: ufloat(p[0], p[1]), regrs)])
        tab.add_row('$f / \\si{\\um}$', *[Latex.fmt_unc(f) for f in
                             map(lambda p: ufloat(p[2], p[3]), regrs)])
        tab.add_hline()
        tab.add_row('$\\chi^2$', *[Latex.fmt_num(chi, prec=3) for chi in
                                   map(lambda p: p[4], regrs)])
        tab.add_row('FG', *([fg]*len(regrs)))
        tab.add_row('$\\chi^2/$FG', *[Latex.fmt_num(chi/fg, prec=3) for chi in
                                      map(lambda p: p[4], regrs)])

        tab.add_hline()
        tab.add_row('$\\lambda / \\si{\\nm}$',
                    *[Latex.fmt_unc(l*1e3, sys=1e3*params.all_lmbdas_sys[i].s, prec=1, exp=False)
                      for i,l in enumerate(params.all_lmbdas)])
        tab.add_row('$\\overline \\lambda / \\si{\\nm}$',
                    Latex.fmt_unc(params.lmbda*1e3, sys=1e3*params.lmbda_sys.s, prec=1, exp=False),
                    *(['']*(len(regrs)-1)))
        tab.generate_tex(path.join(FIGURE_OUTPUT, 'wellenlaenge_regr_{}_{}'.format(frm,to)))


    def gen_plots(self, params):
        frm, to = params.rng

        # Generate scatter plot of raw measurements.
        plot_raw_values_with_averages(self.m[:,frm:to], self.measurements[:,frm:to],
                                      title='Messdaten Wellenlänge',
                                      savefig=path.join(
                                          FIGURE_OUTPUT, 'messdaten_wellenlaenge_{}_{}.pdf'.format(frm,to)))

        for i, meas in enumerate(self.measurements):
            e, ee, f, ef, chiq, corr = nice_plots.nice_linear_regression_plot(
                self.m[i, frm:to], meas[frm:to], self.std_measures[i, frm:to],
                xlabel='$m$', ylabel='$s$/µm', ylabelresidue='$(s - (e m+f))$ / µm',
                title='Wellenlänge grün, Messung {}, Werte {}'.format(i+1, (frm,to)),
                save=path.join(FIGURE_OUTPUT,
                               'wellenlaenge_regr_{}_{}_{}.pdf'.format(i+1,frm,to)))

class Refraction:
    Params = namedtuple('RefractionParams',
                        ('params', 'fg', 'wavelength_params', 'n', 'n_sys',
                         'dndp', 'dndp_sys'))

    def __init__(self, path=AIRPRESSURE_CSV, ignore=[],
                 glass_len=0.01):
        """
        Initialize Refraction calculations. NOTE: All values converted into metric units!

        Parameters
        ----------
        path : TYPE, optional
            DESCRIPTION. The default is AIRPRESSURE_CSV.
        ignore : TYPE, optional
            DESCRIPTION. The default is [].
        glass_len : TYPE, optional
            DESCRIPTION. The default is 0.01.

        Returns
        -------
        None.

        """
        data = read_gnumeric_csv(path)
        self.L = glass_len
        # We can use a "unified" m column here
        self.m = data['m']
        # Convert to Pascal
        self.measurements = 1e2*np.array(list(
            map(lambda c: npreverse(data[c]),
                filter(lambda c: c.startswith('p') and c not in ignore,
                       sorted(data.keys())))))
        self.averaged = unp.uarray(np.mean(self.measurements, 0),
                                   np.std(self.measurements, 0, ddof=1))
        self.envpress = 1e2*ufloat(data['envpress'][0], 0.5)

    def regressions(self, wavparams):
        """
        Calculate regressions and refractive index. NOTE: Calculations here happen in meters.

        Parameters
        ----------
        wavparams : Wavelength.Params
            DESCRIPTION.

        Returns
        -------
        None.

        """
        # Convert to meter
        lmbda = 1e-6*wavparams.lmbda
        lmbda_sys = 1e-6*wavparams.lmbda_sys
        lmbda_sys = ufloat(lmbda.n, (lmbda+lmbda_sys).s)

        (g, eg, h, eh, chiq, corr) = analyse.lineare_regression(
            self.m, unp.nominal_values(self.averaged), unp.std_devs(self.averaged))

        # Calculate dn/dP and refractive index.
        dndp = lmbda.n / (2*self.L*ufloat(g, eg))
        dndp_sys = lmbda / (2*self.L*g)
        n = self.envpress.n * dndp
        n_sys = self.envpress * dndp_sys

        print('----- Refractive index {} -----'.format(wavparams.rng))
        print('g =', ufloat(g,eg), 'h =', ufloat(h, eh))
        print('dn/dp =', dndp)
        print('n =', n)
        print('n_sys =', n_sys)

        return self.Params(params=(g, eg, h, eh, chiq, corr), fg=self.averaged.size,
                           wavelength_params=wavparams,
                           n=n, n_sys=n_sys, dndp=dndp, dndp_sys=dndp_sys)

    def regressions_all_points(self, wavparams):
        """
        Calculate regressions and refractive index, through all points instead of
        the averages.
        NOTE: Calculations here happen in meters.

        Parameters
        ----------
        wavparams : Wavelength.Params
            DESCRIPTION.

        Returns
        -------
        None.

        """
        # Convert to meter
        lmbda = 1e-6*wavparams.lmbda
        lmbda_sys = 1e-6*wavparams.lmbda_sys
        lmbda_sys = ufloat(lmbda.n, (lmbda+lmbda_sys).s)

        (g, eg, h, eh, chiq, corr) = analyse.lineare_regression(
            np.full_like(self.measurements, self.m).flatten(),
            self.measurements.flatten(),
            np.full_like(self.measurements, unp.std_devs(self.averaged)).flatten())

        # Calculate dn/dP and refractive index.
        dndp = lmbda.n / (2*self.L*ufloat(g, eg))
        dndp_sys = lmbda / (2*self.L*g)
        n = self.envpress.n * dndp
        n_sys = self.envpress * dndp_sys

        print('----- Refractive index, all points, {} -----'.format(wavparams.rng))
        print('g =', ufloat(g,eg), 'h =', ufloat(h, eh))
        print('dn/dp =', dndp)
        print('n =', n)
        print('n_sys =', n_sys)

        return self.Params(params=(g, eg, h, eh, chiq, corr), fg=self.measurements.size,
                           wavelength_params=wavparams,
                           n=n, n_sys=n_sys, dndp=dndp, dndp_sys=dndp_sys)

    def gen_plots(self, params):
        frm, to = params.wavelength_params.rng

        # Regression plot
        params1 = nice_plots.nice_linear_regression_plot(
            self.m, 1e-2*unp.nominal_values(self.averaged),
            1e-2*unp.std_devs(self.averaged)/math.sqrt(self.measurements.shape[0]),
            xlabel='$m$', ylabel='$p /$hPa', ylabelresidue='$(p - (g m + h))$ / hPa',
            title='Brechungsindex der Luft - Regression',
            save=path.join(FIGURE_OUTPUT, 'brechungsindex_regr_{}_{}.pdf'.format(frm, to)))

        # Experimental: Plot all data at once.
        params2 = nice_plots.nice_linear_regression_plot(
            np.full_like(self.measurements, self.m).flatten(), 1e-2*self.measurements.flatten(),
            np.full_like(self.measurements, 1e-2*unp.std_devs(self.averaged)).flatten(), xlabel='$m$',
            ylabel='$p /$hPa', ylabelresidue='$(p - (g m + h)) / hPa$',
            title='Brechungsindex der Luft - Einzelwertregression',
            save=path.join(FIGURE_OUTPUT, 'brechungsindex_regr_all_{}_{}.pdf'.format(frm, to)))

        # Measurement data plot
        fig = plt.figure(figsize=(15,10))
        p = fig.add_subplot(111)
        plts = p.plot(self.m, 1e-2*self.measurements.T, 'o')
        p.set_title('Messdaten Luftdruck / Phasenunterschied')
        p.set_xlabel('$m$')
        p.set_ylabel('$p$/hPa')
        p.legend(iter(plts), ['$p_{{{:d}}}$'.format(i) for i in range(1, self.measurements.shape[0]+1)])
        p.grid()
        fig.savefig(path.join(FIGURE_OUTPUT, 'brechungsindex_messung.pdf'))

        # # Plot refractive index.
        # dndp = params.dndp
        # fig = plt.figure(figsize=(15,10))
        # p, p2 = fig.subplots(2,1, sharex=True, gridspec_kw={'height_ratios': [5,2]})
        # xs = np.arange(0, 1050, 10)
        # ys = 1 + dndp.n * 1e2 * xs
        # ys_lit = np.vectorize(lambda p: literaturwerte.brechungsindex_luft(0.532, p=p))(xs)
        # p.plot(xs, ys, label='n(p)', color='blue')
        # p.plot(xs, ys_lit, color='green', label='$n_{lit}$')
        # p.plot(xs, ys+dndp.s * 1e2 * xs, label='$\\sigma_n$', color='orange')
        # p.plot(xs, ys-dndp.s * 1e2 * xs, label='$\\sigma_n$', color='orange')

        # p2.plot(xs, ys-ys_lit, color='blue')
        # p2.plot(xs, ys+dndp.s*1e2*xs-ys_lit, color='orange')
        # p2.plot(xs, ys-dndp.s*1e2*xs-ys_lit, color='orange')
        # p2.plot(xs, np.zeros_like(xs), color='green')

        # p.set_xlabel('$p$ / hPa')
        # p.set_ylabel('$n$')
        # p.set_title('Brechungsindex / Luftdruck')
        # p.get_yaxis().get_major_formatter().set_useOffset(False)
        # p2.set_ylabel('$n - n_{lit}$')
        # p.grid()
        # p.legend()
        # p2.grid()
        # fig.savefig(path.join(FIGURE_OUTPUT, 'brechungsindex_luft.pdf'))

    def gen_tables(self, params):
        """
        Generate LaTeX tables.

        Returns
        -------
        None.

        """
        pcols, prows = self.measurements.shape

        # Measurements
        tab = table.Tabular('r|'+pcols*'c'+'|c|c', booktabs=True)
        tab.escape = False
        tab.add_row('$m$',
                    *['$p_{{{:d}}}$'.format(i) for i in range(1, pcols+1)],
                    '$\\sigma_{p}$',
                    '$\overline{p} \\pm \\sigma_{\\overline p}$')
        tab.add_hline()
        averaged = unp.uarray(unp.nominal_values(self.averaged),
                              unp.std_devs((self.averaged)/math.sqrt(self.measurements.shape[0])))
        for i in range(0, prows):
            tab.add_row(int(self.m[i]),
                        *[int(1e-2*self.measurements[j,i]) for j in range(0, pcols)],
                        Latex.fmt_num(1e-2*unp.std_devs(self.averaged[i]), prec=1),
                        Latex.fmt_unc(1e-2*averaged[i], exp=False, prec=0))

        tab.generate_tex(path.join(FIGURE_OUTPUT, 'brechungsindex_messung'))

        # Generate overview table of parameters
        regr = params.params
        frm, to = params.wavelength_params.rng
        fg = params.fg-2

        tab = table.Tabular('rl', booktabs=True)
        tab.escape = False
        tab.add_row('Parameter', 'Wert')
        tab.add_hline()
        tab.add_row('$g / \\si{\\hecto\\pascal}$', Latex.fmt_unc(1e-2*ufloat(regr[0], regr[1]), prec=2))
        tab.add_row('$h / \\si{\\hecto\\pascal}$', Latex.fmt_unc(1e-2*ufloat(regr[2], regr[3]), prec=2))
        tab.add_hline()
        tab.add_row('$\\chi^2$', Latex.fmt_num(regr[4], prec=1))
        tab.add_row('FG', fg)
        tab.add_row('$\\chi^2/$FG', Latex.fmt_num(regr[4]/fg, prec=1))

        tab.add_hline()
        f = lambda p: literaturwerte.brechungsindex_luft(params.wavelength_params.lmbda.n, p=p)-1
        tab.add_row('$p_{Atm}/$hPa', Latex.fmt_unc(self.envpress*1e-2,exp=False,prec=1))
        tab.add_row('$(\Delta n / \Delta p)_{Luft} /$hPa$^{-1}$',
                    Latex.fmt_unc(1e2*params.dndp, sys=1e2*params.dndp_sys.s, prec=2))
        tab.add_row('$(\Delta n / \Delta p)_{lit} /$hPa$^{-1}$',
                    '~'+Latex.fmt_num(np.array([f(p+1)-f(p) for p in range(0, 1020, 25)]).mean(), prec=2, exp=True))
        tab.add_row('$n_{Luft} - 1$',
                    Latex.fmt_unc(params.n, sys=params.n_sys.s, prec=2))
        tab.add_row('$n_{lit} - 1$ @ \\SI{20}{\\celsius}',
                    '~'+Latex.fmt_num(
                        literaturwerte.brechungsindex_luft(
                            params.wavelength_params.lmbda.n,
                            p=self.envpress.n*1e-2)-1, exp=True, prec=4))
        tab.generate_tex(path.join(FIGURE_OUTPUT, 'brechungsindex_regr_{}_{}'.format(frm,to)))

# Ranges for calibration data.
A = (0,7)
B = (7,16)
ALL = None
B = (7,16)

cal = Calibration()
wav = Wavelength()
ref = Refraction()

def run_all(tab=True, plot=True):
    (cal_all, cal_a, cal_b) = (
        cal.regressions(ALL),
        cal.regressions(A),
        cal.regressions(B))

    for p in [cal_all, cal_a, cal_b]:
        if tab:
            cal.gen_tables(p)
        if plot:
            cal.gen_plots(p)
    if plot:
        plot_discrete_errorbars(np.array([cal_all.all_ks, cal_a.all_ks, cal_b.all_ks]),
                                title='k', labels=['A+B', 'A', 'B'], xlabel='$k$', ylabel='Messung',
                                save=path.join(FIGURE_OUTPUT, 'ks_vergleich.pdf'))

    (wav_all, wav_a, wav_b) = (
        wav.regressions(cal_all),
        wav.regressions(cal_a),
        wav.regressions(cal_b))

    for w in [wav_all, wav_a, wav_b]:
        if tab:
            wav.gen_tables(w)
        if plot:
            wav.gen_plots(w)

    (ref_all, ref_a, ref_b) = (
        ref.regressions_all_points(wav_all),
        ref.regressions_all_points(wav_a),
        ref.regressions_all_points(wav_b))

    for r in [ref_all, ref_a, ref_b]:
        if tab:
            ref.gen_tables(r)
        if plot:
            ref.gen_plots(r)
