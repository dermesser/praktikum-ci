# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 16:40:05 2020

@author: Max
"""

from praktikum import cassy
from praktikum import analyse
import numpy as np
import os
import nice_plots
from scipy import constants
import texttable
import latextable
import codecs

class traverser:
    def traverse_direcrories(self, root_dir):
        return [directories for path, directories, files in os.walk(root_dir)][0]
           
    def traverse_files(self, root_dir):
        return [files for path, directories, files in os.walk(root_dir)][0]
    
    def return_path(self, root_dir):
        return [path for path, directories, files in os.walk(root_dir)][0]
      
class cassy_mean:
   
    def mean(self, filename):
        data = cassy.CassyDaten(filename).messung(1)
        means = np.zeros(2)
        means[0] = analyse.mittelwert_stdabw(data.datenreihe('&J_A11').werte)[0]
        means[1] = analyse.mittelwert_stdabw(data.datenreihe('U_B1').werte)[0]
        return means
        
    def deviation(self, filename):
        data = cassy.CassyDaten(filename).messung(1)
        deviation = np.zeros(2)
        deviation[0] = analyse.mittelwert_stdabw(data.datenreihe('&J_A11').werte)[1]
        deviation[1] = analyse.mittelwert_stdabw(data.datenreihe('U_B1').werte)[1]
        return deviation
        
class cassy_combine:
    
    def combine(self, directory, files, data_row):
        
        combined_data = np.empty(0)
        for filename in files:
            data = cassy.CassyDaten(directory + "\\" + filename).messung(1)
            combined_data = np.concatenate((combined_data, data.datenreihe(data_row).werte))
        return combined_data.flatten()

trav = traverser()
means = cassy_mean()
directories = trav.traverse_direcrories(os.getcwd())
files = trav.traverse_files(directories[0])
path = trav.return_path(os.getcwd())
all_data = np.zeros((len(directories)-1, 2, len(files) ))
deviation_volage = np.zeros((len(directories)-1, len(files) ))
deviation_temperature = np.zeros((len(directories)-1, len(files) ))

for dir_counter,directory in enumerate(directories):
    if directory == '__pycache__' :
        continue
    for file_counter, file in enumerate(files):
        mean = means.mean(path + "\\" + directory + "\\" + file)
        all_data[dir_counter][0][file_counter] = mean[0]
        all_data[dir_counter][1][file_counter] = mean[1]
        diviations = means.deviation(path + "\\" + directory + "\\" + file)
        deviation_volage[dir_counter][file_counter] = diviations[1]
        deviation_temperature[dir_counter][file_counter] = diviations[0]
        
"""automated tables for latex """
text = ''
wanted_temperatures = ["50", "55", "60", "65", "70", "75", "80", "90"]
for j, directory in enumerate(directories): 
    if directory == '__pycache__' or directory == 'Raum_vor_Messung':
        continue
    table = texttable.Texttable()
    table.set_cols_align(["c"]*3)
    table.header(["Temp in °C", "Wassertemp in °C", "Spannung in mV"])
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    table.set_precision(3)
    for i in range(len(wanted_temperatures)):
        table.add_row([wanted_temperatures[i], '$ \\num{{ {:.2e} }}'.format(all_data[j][0][i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(deviation_temperature[j][i]),
                     '$ \\num{{ {:.2e} }}' .format( all_data[j][1][i])+ " \pm " + ' \\num{{ {:.2e} }}$'.format(deviation_volage[j][i])])
    
    text += latextable.draw_latex(table, label='eingestellte Temperatur, gem. Temperatur, gem. Spannung, Seite: '+ directory, caption='eingestellte Temperatur, gem. Spannung, gem. Temperatur, Seite: ' + directory) +'\n'

text_file = codecs.open(path + '\\..\\img\\394386_1A1_tab.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()


c_to_kelvin = 273.15        

for i, data in enumerate(all_data):
    if i == 2:
        continue
    data[0] = (data[0]+c_to_kelvin)**4



combiner = cassy_combine()

""" Next: the calculation for the Stefan-Boltzmann T^4 equation"""

for directory in directories:
    if directory == '__pycache__':
        continue
    voltage = combiner.combine(directory, files, 'U_B1')
    temperature = combiner.combine(directory, files, '&J_A11')
    error_voltage = np.zeros(len(voltage))
    error_temperature = np.zeros(len(temperature))
    if len(error_temperature) != len(error_voltage):
        print('error arrays not same length!')
    for i, number in enumerate(voltage):
        if i < 125*6:
            error_voltage[i] = 0.0048 
        else:
            error_voltage[i] = 0.14
    for i, number in enumerate(temperature):
        if number < 70:
            error_temperature[i] = 0.2
        else:
            error_temperature[i] = 0.4
    temperature = temperature + c_to_kelvin
    error_temperature = 4*temperature**3*error_temperature
    temperature = temperature**4
    nice_plots.nice_regression_plot(temperature, voltage, error_voltage, 
                                    error_temperature, xlabel= 'Temperatur^4/K^4', ylabel='Spannung/mV', title=directory, save=path + '\\..\\img\\394386_1A1_linear_regression_' + directory + '.pdf') #shows boltzmann
    

""" Next: calculation for the emission coefficient """

A_s = np.pi/4 * 0.035**2
A_e = np.pi/4 * 0.023**2
length = 0.108

STC = constants.sigma
pre_factor = A_s * A_e * STC / (length**2 * np.pi)
error_measure_device = 0.0005

error_A_s = 2 * A_s * error_measure_device / 0.035
error_A_e = 2 * A_e * error_measure_device / 0.023
error_length = 0.003
T_0 = all_data[2][0] + c_to_kelvin
error_temperature_0 = np.zeros(len(T_0))
for i, number in enumerate(T_0):
   if number < 70:
        error_temperature_0[i] = 0.2
   else:
        error_temperature_0[i] = 0.4

text=''
for i in range(5):
    if i == 2:
        continue
    table = texttable.Texttable()
    table.set_cols_align(["c"]*4)
    table.header(["Temp in °C", "$P_i$/$\si{watt}$", "$P_i$/$\si{watt}$", "$\epsilon$"])
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    P_measure = all_data[i][1]/0.16
    P_measure = P_measure / 1E4
    P_ideal = pre_factor * (all_data[i][0] - (all_data[2][0]+ c_to_kelvin)**4)
    emission_coef = P_measure / P_ideal
    
    T = all_data[i][0]**0.25
    error_temperature = np.zeros(len(T))
    for j, number in enumerate(T):
        if number < 70:
            error_temperature[j] = 0.2
        else:
            error_temperature[j] = 0.4
    error_P_ideal = P_ideal * np.sqrt( error_A_s**2/A_s**2 + error_A_e**2/A_e**2 + 4*error_length**2/length**2 
                                      + ((4*T**3*error_temperature)**2 + (4*T_0**3*error_temperature_0)**2)/(T**4 - T_0**4)**2 )
   
    error_P_measure = deviation_volage[i]/1E4
    
    error_emmission_coeff = emission_coef * np.sqrt( error_P_ideal**2/P_ideal**2 + error_P_measure**2/P_measure**2)
    
    mittelwerte = analyse.gewichtetes_mittel(emission_coef, error_emmission_coeff)
    print(analyse.gewichtetes_mittel(emission_coef, error_emmission_coeff), directories[i] )
    
    sys_error_P_measure = np.zeros(len(P_ideal))
    for j in range(len(P_ideal)):
        if int(wanted_temperatures[j]) < 80:
            sys_error_P_measure[j] = P_measure[j] * np.sqrt((0.01*P_measure[j]*0.16+ 0.005*10/1E4)**2/P_measure[j]**2 +0.03**2)
        else:
            sys_error_P_measure[j] = P_measure[j] * np.sqrt((0.01*P_measure[j]*0.16+ 0.005*30/1E4)**2/P_measure[j]**2 +0.03**2)
        sys_error_emission_coeff = emission_coef[j]*sys_error_P_measure[j]/P_measure[j] 
        table.add_row([wanted_temperatures[j], '$ \\num{{ {:.2e} }}'.format(P_ideal[j]) + " \pm " + ' \\num{{ {:.2e} }}'.format(error_P_ideal[j])+'stat$', 
                       '$ \\num{{ {:.2e} }}'.format(P_measure[j]) + " \pm " + ' \\num{{ {:.2e} }}'.format(error_P_measure[j])+'\,stat\,' + ' \\num{{ {:.2e} }}'.format(sys_error_P_measure[j])+'\,sys$',
                       '$ \\num{{ {:.2e} }}'.format(emission_coef[j]) + " \pm " + ' \\num{{ {:.2e} }}'.format(error_emmission_coeff[j])+'\,stat\,' + ' \\num{{ {:.2e} }}'.format(sys_error_emission_coeff)+'\,sys$ '])

    text += latextable.draw_latex(table, label='P_i,P_g_epsilon_Fehler_Seite:'+ directories[i], caption='$P_i$,$P_g$ und $\epsilon $ inkl. Fehler, Seite: ' + directories[i], resize=True) +'\n'


text_file = codecs.open(path + '\\..\\img\\394386_1A1_tab_P_eps.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()






    
        
        
        
        