# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:12:52 2020

@author: Max
"""

import numpy as np
from praktikum import analyse
from praktikum import cassy
import matplotlib.pyplot as plt
import texttable
import latextable
import codecs


plt.rcParams['font.size'] = 30.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['figure.autolayout'] = True

class Error_calc:
    def statistical_error(self):
        voltage_U_A2 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('U_A2').werte
        voltage_U_B2 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('U_B2').werte
        current_1 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('I_1').werte
        current_2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('I_1').werte
        
        mean_A2 = analyse.mittelwert_stdabw(voltage_U_A2)
        mean_B2 = analyse.mittelwert_stdabw(voltage_U_B2)
        mean_current_1 = analyse.mittelwert_stdabw(current_1)
        mean_current_2 = analyse.mittelwert_stdabw(current_2)
       
        anzahl = len(voltage_U_A2)
        print('Spule Anzahl an Messungen {}'.format(anzahl))
        print('Spule U_A2: {:.6f} +- {:.6e} ,Unsicherheit vom Mittelwert: {}'.format(mean_A2[0], mean_A2[1], mean_A2[1]/anzahl**(1/2)))
        print('Spule U_B2: {:.6f} +- {:.6f} ,Unsicherheit vom Mittelwert: {}'.format(mean_B2[0], mean_B2[1], mean_B2[1]/anzahl**(1/2)))
        voltage_U_A2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('U_A2').werte
        voltage_U_B2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('U_B2').werte
        mean_A2 = analyse.mittelwert_stdabw(voltage_U_A2)
        mean_B2 = analyse.mittelwert_stdabw(voltage_U_B2)
        anzahl = len(voltage_U_B2)
        print('Kondensator Anzahl an Messungen {}'.format(anzahl))
        print('Kondensator U_A2: {:.5f} +- {:.5f} ,Unsicherheit vom Mittelwert: {}'.format(mean_A2[0], mean_A2[1], mean_A2[1]/anzahl**(1/2)))
        print('Kondensator U_B2: {:.5f} +- {:.5f} ,Unsicherheit vom Mittelwert: {}'.format(mean_B2[0], mean_B2[1], mean_B2[1]/anzahl**(1/2)))
        print('Strom Messung 1: {:.6f} +- {:.6f}'.format(mean_current_1[0], mean_current_1[1]))
        print('Strom Messung 2: {:.6f} +- {:.6f}'.format(mean_current_2[0], mean_current_2[1]))
        voltage_U_1_1 = voltage_U_A2 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('U_1').werte
        voltage_U_1_2 = voltage_U_A2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('U_1').werte
        mean_U_1_1 = analyse.mittelwert_stdabw(voltage_U_1_1)
        mean_U_1_2 = analyse.mittelwert_stdabw(voltage_U_1_2)
        print('U_1 Messung 1: {:.9f} +- {:.9f}'.format(mean_U_1_1[0], mean_U_1_1[1]))
        print('U_1 Messung 2: {:.9f} +- {:.9f}'.format(mean_U_1_2[0], mean_U_1_2[1]))
    
    
        
       
    def systematic_error(self):
        voltage_U_A2 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('U_A2').werte
        voltage_U_B2 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('U_B2').werte
        current_offset = 0.00049
        mean_A2 = analyse.mittelwert_stdabw(voltage_U_A2)
        mean_B2 = analyse.mittelwert_stdabw(voltage_U_B2)
        offset_A2 = mean_A2[0]
        offset_B2 = mean_B2[0]
       
        voltage_U_A2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('U_A2').werte -offset_A2
        voltage_U_B2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('U_B2').werte -offset_B2
        mean_A2 = analyse.mittelwert_stdabw(voltage_U_A2)
        mean_B2 = analyse.mittelwert_stdabw(voltage_U_B2)
      
        scale_A2 = 0.7/mean_A2[0]
        scale_B2 = 0.7/mean_B2[0]
        print('A2 Offset: {} Skalierung {}'.format(offset_A2, scale_A2))
        print('B2 Offset: {} Skalierung {}'.format(offset_B2, scale_B2))
        return(offset_A2, scale_A2, offset_B2, scale_B2, current_offset)
        
        
    def freq_error(self, reso, top, down, error):
        gute = reso/(top-down)
        error_gute= (top/reso)**2*((error/top)**2+ (error/reso)**2) + (down/reso)**2*((error/down)**2+ (error/reso)**2)
        error_gute = error_gute**(1/2)
        print('{} +- {}'.format(gute, error_gute))

class Visualizer:
    def plot_data(self, file_name):
        coil_resistance = 0.9
        voltage_1 = cassy.CassyDaten(file_name).messung(1).datenreihe('U_1').werte
        voltage_capacitor = cassy.CassyDaten(file_name).messung(1).datenreihe('U_B2').werte
        voltage_coil = cassy.CassyDaten(file_name).messung(1).datenreihe('U_A2').werte
        angle_diff = -cassy.CassyDaten(file_name).messung(1).datenreihe('&j_1').werte
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        current = cassy.CassyDaten(file_name).messung(1).datenreihe('I_1').werte
        corrected_voltage =((voltage_coil)**2-(coil_resistance*current)**2)**(1/2)
        fig, axs = plt.subplots(2, figsize=(10,10))
        plt.grid(True)
        axs[0].grid(True)
        plt.xlabel('frequency')
        axs[0].set(title=file_name)
        axs[0].set(ylabel ='voltage')
        axs[1].set(ylabel='phase difference')
        axs[0].plot(frequency, voltage_1, label='Effektive Amplitude')
        axs[0].plot(frequency, voltage_capacitor, label='Spannung am Kondensator')
        axs[0].plot(frequency, corrected_voltage, label='Korriegierte Spannung am Spule')
        axs[0].plot(frequency, voltage_coil, label='Spannung an der Spule')
        axs[1].plot(frequency, angle_diff, label='Phasendifferenz')
        axs[0].legend()
        axs[1].legend()
        plt.show()
        
    def plot_data_cut(self, file_name, left=0, right=10):
        error = Error_calc()
        systematic = error.systematic_error()
        coil_resistance = 0.9
        voltage_capacitor = systematic[3]*cassy.CassyDaten(file_name).messung(1).datenreihe('U_B2').werte-systematic[2]
        voltage_coil = systematic[1]*cassy.CassyDaten(file_name).messung(1).datenreihe('U_A2').werte-systematic[0]
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        current = cassy.CassyDaten(file_name).messung(1).datenreihe('I_1').werte
        corrected_voltage =((voltage_coil)**2-(coil_resistance*current)**2)**(1/2)
        fig, axs = plt.subplots(1, figsize=(10,10))
        plt.grid(True)
        axs.grid(True)
        axs.set_xlim(left=1375, right=1425)
        axs.set_ylim(bottom=left, top=right)
        plt.xlabel('frequency')
        axs.set(title=file_name)
        axs.set(ylabel ='voltage')
        axs.plot(frequency, voltage_capacitor, label='Spannung am Kondensator', marker='o')
        axs.plot(frequency, corrected_voltage, label='Korriegierte Spannung am Spule', marker='o')
        axs.plot(frequency, voltage_coil, label='Spannung an der Spule', marker='o')
        axs.legend()
        plt.show()
        
    def plot_current_all_fine(self):
        error = Error_calc()
        systematic = error.systematic_error()
        current_1_ohm = cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_1_ohm = cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        current_5_1_ohm = cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_5_1_ohm = cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        current_6_1_ohm = cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_6_1_ohm = cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        current_10_ohm = cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_10_ohm = cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        current_11_ohm = cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_11_ohm = cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        plt.figure( figsize=(20,10))
        plt.title('Alle Feinmessungen des Stroms')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Strom in Ampere')
        plt.grid(True)
        plt.plot(frecuency_1_ohm, current_1_ohm, label='1 Ohm')
        plt.plot(frecuency_5_1_ohm, current_5_1_ohm, label='5.1 Ohm')
        plt.plot(frecuency_6_1_ohm, current_6_1_ohm, label='6.1 Ohm')
        plt.plot(frecuency_10_ohm, current_10_ohm, label='10 Ohm')
        plt.plot(frecuency_11_ohm, current_11_ohm, label='11 Ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_currentallfine.pdf')
        plt.show()
        
    def plot_current_all_grob(self):
        error = Error_calc()
        systematic = error.systematic_error()
        current_1_ohm = cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_1_ohm = cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        current_5_1_ohm = cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_5_1_ohm = cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        current_6_1_ohm = cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_6_1_ohm = cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        current_10_ohm = cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_10_ohm = cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        current_11_ohm = cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('I_1').werte-systematic[4]
        frecuency_11_ohm = cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        plt.figure( figsize=(20,10))
        plt.title('Alle Grobmessungen des Stroms')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Strom in Ampere')
        plt.grid(True)
        plt.plot(frecuency_1_ohm, current_1_ohm, label='1 Ohm')
        plt.plot(frecuency_5_1_ohm, current_5_1_ohm, label='5.1 Ohm')
        plt.plot(frecuency_6_1_ohm, current_6_1_ohm, label='6.1 Ohm')
        plt.plot(frecuency_10_ohm, current_10_ohm, label='10 Ohm')
        plt.plot(frecuency_11_ohm, current_11_ohm, label='11 Ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_currentallgrob.pdf')
        plt.show()
        
    def plot_voltage_all_grob(self):
        error = Error_calc()
        systematic = error.systematic_error()
        voltage_coil_1_ohm =  systematic[1]*cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_1_ohm = systematic[3]*cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_1_ohm = cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
       
        voltage_coil_5_1_ohm =  systematic[1]*cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_5_1_ohm = systematic[3]*cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_5_1_ohm = cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        
        voltage_coil_6_1_ohm =  systematic[1]*cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_6_1_ohm = systematic[3]*cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_6_1_ohm = cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        
        voltage_coil_10_ohm =  systematic[1]*cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_10_ohm = systematic[3]*cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_10_ohm = cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        
        voltage_coil_11_ohm =  systematic[1]*cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_11_ohm = systematic[3]*cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_11_ohm = cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
      
        plt.figure( figsize=(20,10))
        plt.title('Alle Grobmessungen der Spannung')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Spannung in Volt')
        plt.grid(True)
        plt.plot(frecuency_1_ohm, voltage_coil_1_ohm, label = 'Spule, 1 Ohm')
        plt.plot(frecuency_1_ohm, voltage_capacitor_1_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_5_1_ohm, voltage_coil_5_1_ohm, label = 'Spule, 5.1 Ohm')
        plt.plot(frecuency_5_1_ohm, voltage_capacitor_5_1_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_6_1_ohm, voltage_coil_6_1_ohm, label = 'Spule, 6.1 Ohm')
        plt.plot(frecuency_6_1_ohm, voltage_capacitor_6_1_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_10_ohm, voltage_coil_10_ohm, label = 'Spule, 10 Ohm')
        plt.plot(frecuency_10_ohm, voltage_capacitor_10_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_11_ohm, voltage_coil_11_ohm, label = 'Spule, 11 Ohm')
        plt.plot(frecuency_11_ohm, voltage_capacitor_11_ohm, label='Kondensator, 1 Ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_voltageallgrob.pdf')
        plt.show()
        
    def plot_voltage_all_fine(self):
        error = Error_calc()
        systematic = error.systematic_error()
        voltage_coil_1_ohm =  systematic[1]*cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_1_ohm = systematic[3]*cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_1_ohm = cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
       
        voltage_coil_5_1_ohm =  systematic[1]*cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_5_1_ohm = systematic[3]*cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_5_1_ohm = cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        
        voltage_coil_6_1_ohm =  systematic[1]*cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_6_1_ohm = systematic[3]*cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_6_1_ohm = cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        
        voltage_coil_10_ohm =  systematic[1]*cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_10_ohm = systematic[3]*cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_10_ohm = cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        
        voltage_coil_11_ohm =  systematic[1]*cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('U_A2').werte-systematic[0]
        voltage_capacitor_11_ohm = systematic[3]*cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('U_B2').werte-systematic[2]
        frecuency_11_ohm = cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
      
        plt.figure( figsize=(20,10))
        plt.title('Alle Feinmessungen der Spannung')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Spannung in Volt')
        plt.grid(True)
        plt.plot(frecuency_1_ohm, voltage_coil_1_ohm, label = 'Spule, 1 Ohm')
        plt.plot(frecuency_1_ohm, voltage_capacitor_1_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_5_1_ohm, voltage_coil_5_1_ohm, label = 'Spule, 5.1 Ohm')
        plt.plot(frecuency_5_1_ohm, voltage_capacitor_5_1_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_6_1_ohm, voltage_coil_6_1_ohm, label = 'Spule, 6.1 Ohm')
        plt.plot(frecuency_6_1_ohm, voltage_capacitor_6_1_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_10_ohm, voltage_coil_10_ohm, label = 'Spule, 10 Ohm')
        plt.plot(frecuency_10_ohm, voltage_capacitor_10_ohm, label='Kondensator, 1 Ohm')
        
        plt.plot(frecuency_11_ohm, voltage_coil_11_ohm, label = 'Spule, 11 Ohm')
        plt.plot(frecuency_11_ohm, voltage_capacitor_11_ohm, label='Kondensator, 1 Ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_voltageallfine.pdf')
        plt.show()
        
    def plot_angle_all_grob(self):

        angle_1_ohm = -cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('&j_1').werte
        frecuency_1_ohm = cassy.CassyDaten('1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        angle_5_1_ohm = -cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('&j_1').werte
        frecuency_5_1_ohm = cassy.CassyDaten('5_1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        angle_6_1_ohm = -cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('&j_1').werte
        frecuency_6_1_ohm = cassy.CassyDaten('6_1_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        angle_10_ohm = -cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('&j_1').werte
        frecuency_10_ohm = cassy.CassyDaten('10_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        angle_11_ohm = -cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('&j_1').werte
        frecuency_11_ohm = cassy.CassyDaten('11_ohm\\grob.lab').messung(1).datenreihe('f_1').werte
        plt.figure( figsize=(20,10))
        plt.title('Alle Grobmessungen der Phase')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Phase in Grad')

        plt.grid(True)
        plt.plot(frecuency_1_ohm, angle_1_ohm, label='1 Ohm')
        plt.plot(frecuency_5_1_ohm, angle_5_1_ohm, label='5.1 Ohm')
        plt.plot(frecuency_6_1_ohm, angle_6_1_ohm, label='6.1 Ohm')
        plt.plot(frecuency_10_ohm, angle_10_ohm, label='10 Ohm')
        plt.plot(frecuency_11_ohm, angle_11_ohm, label='11 Ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_anglediffallgrob.pdf')
        plt.show()
           
    def plot_angle_all_fine(self):

        angle_1_ohm = -cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('&j_1').werte
        frecuency_1_ohm = cassy.CassyDaten('1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        angle_5_1_ohm = -cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('&j_1').werte
        frecuency_5_1_ohm = cassy.CassyDaten('5_1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        angle_6_1_ohm = -cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('&j_1').werte
        frecuency_6_1_ohm = cassy.CassyDaten('6_1_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        angle_10_ohm = -cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('&j_1').werte
        frecuency_10_ohm = cassy.CassyDaten('10_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        angle_11_ohm = -cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('&j_1').werte
        frecuency_11_ohm = cassy.CassyDaten('11_ohm\\fein.lab').messung(1).datenreihe('f_1').werte
        plt.figure( figsize=(20,10))
        plt.grid(True)
        plt.title('Alle Feinmessungen der Phase')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Phase in Grad')
        plt.plot(frecuency_1_ohm, angle_1_ohm, label='1 Ohm')
        plt.plot(frecuency_5_1_ohm, angle_5_1_ohm, label='5.1 Ohm')
        plt.plot(frecuency_6_1_ohm, angle_6_1_ohm, label='6.1 Ohm')
        plt.plot(frecuency_10_ohm, angle_10_ohm, label='10 Ohm')
        plt.plot(frecuency_11_ohm, angle_11_ohm, label='11 Ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_anglediffallfine.pdf')
        plt.show()
        
    def plot_current(self, file_name):
        
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        current = cassy.CassyDaten(file_name).messung(1).datenreihe('I_1').werte
        current_max = max(current)
        fig, axs = plt.subplots(1, figsize=(10,10))
        plt.grid(True)
        axs.grid(True)
        plt.xlabel('frequency')
        axs.set(title=file_name + ' Max: ' + str(current_max))
        axs.set(ylabel ='current')
        horizontal_line = np.ones(len(frequency)) * current_max /2**(1/2)
        new = current-horizontal_line
        axs.plot(frequency, current-horizontal_line, label='I - I_max/sqrt(2)')
        new_low = new[:int(np.where(new==max(new))[0]):]
        new_high = new[int(np.where(new==max(new))[0])::]
        low_border_freq = frequency[len(new_low[new_low<0])+1]
        high_border_freq = frequency[-len(new_high[new_high<0])]
        highest_freq = frequency[int(np.where(new==max(new))[0])]
        gute = highest_freq/ (high_border_freq - low_border_freq)
        print( ' {} / ({} - {}) = {}'.format(highest_freq, high_border_freq, low_border_freq, gute))
        axs.legend()
        plt.show()
     
        
    def plot_phase(self, file_name):
        
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        angle_diff = -cassy.CassyDaten(file_name).messung(1).datenreihe('&j_1').werte
        plt.figure(figsize=(20,10))
        plt.plot(frequency, angle_diff)
        plt.show()
        new_low = angle_diff -45
        new_high = angle_diff +45
        print( new_low[new_low<0])
        print(new_high[new_high<0] )
        high = frequency[-len(new_low[new_low<0])]
        low = frequency[-len(new_high[new_high<0])]
        gute = 1400 / (high - low)
        print( ' {} / ({} - {}) = {}'.format(1400,  low,high, abs(gute)))
        
    def plot_phase_zoom(self, file_name, save=False):
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        angle_diff = -cassy.CassyDaten(file_name).messung(1).datenreihe('&j_1').werte
        freq_diff = frequency[1]- frequency[0]
        index_smaller_zero = len(angle_diff[angle_diff<0])-1
        mitte_freq = (frequency[index_smaller_zero+1] + frequency[index_smaller_zero])/2
        mitte_angle = (angle_diff[index_smaller_zero+1] + angle_diff[index_smaller_zero])/2
        angle_diff_border = abs(angle_diff[index_smaller_zero+1]-angle_diff[index_smaller_zero])
        reso = mitte_freq
        
        print('{} Hz   {} ° '.format(mitte_freq, mitte_angle))
        plt.figure(figsize=(20,10))
        plt.title('0° Phasengrenze')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Phasenunterschied in Grad')
        plt.plot(frequency, angle_diff, marker='o')
        plt.plot([mitte_freq, mitte_freq], [angle_diff[index_smaller_zero+1], angle_diff[index_smaller_zero]], color='red')
        plt.plot( [frequency[index_smaller_zero+1],frequency[index_smaller_zero]],[mitte_angle, mitte_angle], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_angle-2*angle_diff_border,mitte_angle+2*angle_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_phase0cross.pdf')
        plt.show()
       

        index_smaller_minus_45 = len(angle_diff[angle_diff<-45])-1
        mitte_freq = (frequency[index_smaller_minus_45+1] + frequency[index_smaller_minus_45])/2
        mitte_angle = (angle_diff[index_smaller_minus_45+1] + angle_diff[index_smaller_minus_45])/2
        angle_diff_border = abs(angle_diff[index_smaller_minus_45+1]-angle_diff[index_smaller_minus_45])
        print('{} Hz   {} ° '.format(mitte_freq, mitte_angle))
        low_border_freq = mitte_freq
        plt.figure(figsize=(20,10))
        plt.title('-45° Phasengrenze')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Phasenunterschied in Grad')
        plt.plot(frequency, angle_diff, marker='o')
        plt.plot([mitte_freq, mitte_freq], [angle_diff[index_smaller_minus_45+1], angle_diff[index_smaller_minus_45]], color='red')
        plt.plot( [frequency[index_smaller_minus_45+1],frequency[index_smaller_minus_45]],[mitte_angle, mitte_angle], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_angle-2*angle_diff_border,mitte_angle+2*angle_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_phaseminus45cross.pdf')
        plt.show()
      
        
      
        index_smaller_plus_45 = len(angle_diff[angle_diff<45])-1
        mitte_freq = (frequency[index_smaller_plus_45+1] + frequency[index_smaller_plus_45])/2
        mitte_angle = (angle_diff[index_smaller_plus_45+1] + angle_diff[index_smaller_plus_45])/2
        angle_diff_border = abs(angle_diff[index_smaller_plus_45+1]-angle_diff[index_smaller_plus_45])
        print('{} Hz   {} ° '.format(mitte_freq, mitte_angle))
        high_border_freq = mitte_freq
        plt.figure(figsize=(20,10))
        plt.title('45° Phasengrenze')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Phasenunterschied in Grad')
        plt.plot(frequency, angle_diff, marker='o')
        plt.plot([mitte_freq, mitte_freq], [angle_diff[index_smaller_plus_45+1], angle_diff[index_smaller_plus_45]], color='red')
        plt.plot( [frequency[index_smaller_plus_45+1],frequency[index_smaller_plus_45]],[mitte_angle, mitte_angle], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_angle-2*angle_diff_border,mitte_angle+2*angle_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_phaseplus45cross.pdf')
        plt.show()
        return reso, high_border_freq, low_border_freq
        
     
    def plot_current_zoom(self, file_name, save=False):
        error = Error_calc()
        systematic = error.systematic_error()
     
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        current = cassy.CassyDaten(file_name).messung(1).datenreihe('I_1').werte -systematic[4]
        freq_diff = frequency[1]- frequency[0]

        index_highest = np.where(current==max(current))[0]
        
        mitte_freq = frequency[index_highest]
        reso = mitte_freq
        mitte_current =current[index_highest]
        current_diff_border = abs(current[index_highest+1]-current[index_highest])
      
        print('Peak')
        print('{} Hz   {} A '.format(mitte_freq, mitte_current))
        plt.figure(figsize=(20,10))
        plt.title('Resonanz des Stroms')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Strom in Ampere')
        plt.plot(frequency, current, marker='o')
        plt.plot([mitte_freq, mitte_freq], [mitte_current +0.0003, mitte_current - 0.0003], color='red')
        plt.plot( [frequency[index_highest-1],frequency[index_highest+1]],[mitte_current, mitte_current], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_current-4*current_diff_border,mitte_current+4*current_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_currentpeakcross.pdf')
        plt.show()
       
        current_low, current_high = np.split(current, index_highest)

        index_low_border = len(current_low[current_low< mitte_current/2**(1/2)])-1
        
        mitte_freq = (frequency[index_low_border] + frequency[index_low_border+1])/2
        low_border_freq = mitte_freq
        mitte_current =(current[index_low_border]+current[index_low_border+1])/2
        current_diff_border = abs(current[index_low_border+1]-current[index_low_border])
      
        print('Grenze unten')
        print('{} Hz   {} A  '.format(mitte_freq, mitte_current))
        print('delta {} Hz   delta {} A  '.format(freq_diff, abs(current[index_low_border]-current[index_low_border+1])))
       
        plt.figure(figsize=(20,10))
        plt.title('Untere Grenze des Stroms')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Strom in Ampere')
        plt.plot(frequency, current, marker='o')
        plt.plot([mitte_freq, mitte_freq], [current[index_low_border], current[index_low_border+1]], color='red')
        plt.plot( [frequency[index_low_border],frequency[index_low_border+1]],[mitte_current, mitte_current], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_current-4*current_diff_border,mitte_current+4*current_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_currentlowcross.pdf')
        plt.show()
      
        mitte_current =current[index_highest]
        index_high_border = len(current_high[current_high > mitte_current/2**(1/2)])-1 + len(current_low)
     
        mitte_freq = (frequency[index_high_border] + frequency[index_high_border+1])/2
        mitte_current =(current[index_high_border]+current[index_high_border+1])/2
        current_diff_border = abs(current[index_high_border+1]-current[index_high_border])
        high_border_freq = mitte_freq
        print('Grenze oben')
        print('{} Hz   {} A  '.format(mitte_freq, mitte_current))
        print('delta {} Hz   delta {} A  '.format(freq_diff, abs(current[index_high_border]-current[index_high_border+1])))
       
        plt.figure(figsize=(20,10))
        plt.title('Obere Grenze des Stroms')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Strom in Ampere')
        plt.plot(frequency, current, marker='o')
        plt.plot([mitte_freq, mitte_freq], [current[index_high_border], current[index_high_border+1]], color='red')
        plt.plot( [frequency[index_high_border],frequency[index_high_border+1]],[mitte_current, mitte_current], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_current-4*current_diff_border,mitte_current+4*current_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_currenthighcross.pdf')
        plt.show()
        return reso[0], high_border_freq, low_border_freq
        
    def plot_voltage_zoom(self, file_name, save=False):
        error = Error_calc()
        systematic = error.systematic_error()

     
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        voltage_capacitor = systematic[3]*cassy.CassyDaten(file_name).messung(1).datenreihe('U_B2').werte-systematic[2]
        voltage_coil = systematic[1]*cassy.CassyDaten(file_name).messung(1).datenreihe('U_A2').werte-systematic[0]
        freq_diff = frequency[1]- frequency[0]
        
        border_index = len(voltage_capacitor[voltage_capacitor > voltage_coil]) - 1
        if abs(voltage_capacitor[border_index]- voltage_coil[border_index]) < abs(voltage_capacitor[border_index+1]- voltage_coil[border_index+1]):
            
            mitte_voltage =( voltage_capacitor[border_index]+ voltage_coil[border_index])/2
            top_voltage = voltage_capacitor[border_index]
            low_voltage = voltage_coil[border_index]
        else:
            mitte_voltage =( voltage_capacitor[border_index+1]+ voltage_coil[border_index+1])/2
            low_voltage = voltage_capacitor[border_index+1]
            top_voltage = voltage_coil[border_index+1]
        mitte_freq = (frequency[border_index] + frequency[border_index+1])/2
        voltage_diff_border = abs(voltage_coil[border_index+1]-voltage_coil[border_index])
      
        plt.figure(figsize=(20,10))
        plt.title('Schneidepunkt der Spannungen')
        plt.xlabel('Frequenz in Hertz')
        plt.ylabel('Spannung in Volt')
        plt.plot(frequency, voltage_capacitor, marker='o')
        plt.plot(frequency, voltage_coil, marker='o')
        plt.plot([mitte_freq, mitte_freq], [low_voltage, top_voltage], color='red')
        plt.plot( [frequency[border_index],frequency[border_index+1]],[mitte_voltage, mitte_voltage], color='red')
        plt.grid(True)
        plt.xlim(mitte_freq-3*freq_diff, mitte_freq+3*freq_diff)
        plt.ylim(mitte_voltage-4*voltage_diff_border,mitte_voltage+4*voltage_diff_border)
        if save:
            plt.savefig('img\\394386_2E1_voltagecross.pdf')
        plt.show()
        print('Spannung {}\ndelta Spannung {}'.format(mitte_voltage, top_voltage- low_voltage))
        return mitte_voltage, top_voltage, low_voltage
        
        
    def vergleicher(self):
        ohm_1 = np.array([5.89, 5.6951, 5.7213, 5.7151])
        ohm_5 = np.array([1.8, 1.7894, 1.8393, 1.8259])
        ohm_6 = np.array([1.55, 1.544, 1.5681, 1.5641])
        ohm_10 = np.array([1.02,1.0039,1.0277, 1.0182])
        ohm_11 = np.array([0.93,0.9129,0.9452, 0.9293])
        
        widerstand = np.array([1, 5.3, 6.3, 10.1, 11.1])
        ohm_1_raw = np.ones(4)*widerstand[0]
        ohm_5_raw = np.ones(4)*widerstand[1]
        ohm_6_raw = np.ones(4)*widerstand[2]
        ohm_10_raw = np.ones(4)*widerstand[3]
        ohm_11_raw = np.ones(4)*widerstand[4]
        
        widerstand_error = np.array([0.1, 0.1, 0.1*2**(1/2), 0.1, 0.1*2**(1/2) ])
        ohm_1_raw_error = np.ones(4)*widerstand_error[0]
        ohm_5_raw_error = np.ones(4)*widerstand_error[1]
        ohm_6_raw_error = np.ones(4)*widerstand_error[2]
        ohm_10_raw_error = np.ones(4)*widerstand_error[3]
        ohm_11_raw_error = np.ones(4)*widerstand_error[4]
        
        
        
        ohm_1_error = np.array([0.44,0.0017,0.0017,0.0062])
        ohm_5_error = np.array([0.04,0.0025,0.0025,0.0038])
        ohm_6_error = np.array([0.03,0.0087,0.0089,0.0025])
        ohm_10_error = np.array([0.01,0.0063,0.0065,0.001])
        ohm_11_error = np.array([0.01,0.0094,0.0096,0.001])
        
        ohm_1_error = ohm_1_error/ohm_1**2
        ohm_5_error = ohm_5_error/ohm_5**2
        ohm_6_error = ohm_6_error/ohm_6**2
        ohm_10_error = ohm_10_error/ohm_10**2
        ohm_11_error = ohm_11_error/ohm_11**2
        
        xaxis = np.linspace(0, 12, 1000)
        c_durch_l = (10.28E-6/1.291E-3)**(1/2)
        yaxis_1 = xaxis * c_durch_l + 0.9*c_durch_l
        yaxis_2 = xaxis * c_durch_l + c_durch_l
        yaxis_3 = xaxis * c_durch_l + 0.8*c_durch_l
        
        plt.figure(figsize=(20,10))
        plt.title('Vergleich der Güten')
        plt.xlabel('Widerstand in Ohm')
        plt.ylabel('1/Güte')
        plt.grid(True)
        plt.errorbar(ohm_1_raw, 1/ohm_1, yerr=ohm_1_error, xerr=ohm_1_raw_error)
        plt.errorbar(ohm_5_raw, 1/ohm_5, yerr=ohm_5_error, xerr=ohm_5_raw_error)
        plt.errorbar(ohm_6_raw, 1/ohm_6, yerr=ohm_6_error, xerr=ohm_6_raw_error)
        plt.errorbar(ohm_10_raw, 1/ohm_10, yerr=ohm_10_error, xerr=ohm_10_raw_error)
        plt.errorbar(ohm_11_raw, 1/ohm_11, yerr=ohm_11_error, xerr=ohm_11_raw_error)
        plt.plot(xaxis, yaxis_1, label='R_L = 0.9 ohm')
        plt.plot(xaxis, yaxis_2, label='R_L = 1 ohm')
        plt.plot(xaxis, yaxis_3, label='R_L = 0.8 ohm')
        plt.legend()
        plt.savefig('img\\394386_2E1_vergleichgute.pdf')
        plt.xlim(9,12)
        plt.ylim(0.8,1.2)
        plt.savefig('img\\394386_2E1_vergleichgutezoom.pdf')
        plt.errorbar(6.3, 1/1.6355, yerr=0.0075/1.6355**2, xerr=2**(1/2)*0.1, label='Oszilloskop')
        plt.xlim(5,7)
        plt.ylim(0.4,0.8)
        plt.savefig('img\\394386_2E1_vergleichguteoszi.pdf')
        plt.show()
        
    def histo_current(self):
        current_1 = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('I_1').werte
        current_2 = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('I_1').werte
        plt.figure(figsize=(20,10))
        plt.grid(True)
        plt.hist(current_1, bins=300, range=(0.00049999,0.00050001))
        plt.show()
        plt.figure(figsize=(20,10))
        plt.title('Histogramm Rauschmessung I_1 Strom')
       
        plt.grid(True)
        plt.xlabel('Strom in A')
        plt.ylabel('Anzahl')
        plt.hist(current_2, bins=100, range=(0.00049999,0.00050001))
        plt.savefig('img\\394386_2E1_histocurrent.pdf')
        plt.show()
        
    def histo_voltage(self):
        voltage = cassy.CassyDaten('rauschmessung_spule.lab').messung(1).datenreihe('U_A2').werte
        plt.figure(figsize=(20,10))
        plt.grid(True)
        plt.hist(voltage, bins=300 )
        plt.show()
        voltage = cassy.CassyDaten('rauschmessung_kondensator.lab').messung(1).datenreihe('U_A2').werte
   
        plt.figure(figsize=(20,10))
        plt.title('Histogramm Rauschmessung U_A2 Messung am Kondensator')
        plt.xlabel('Spannung in V')
        plt.ylabel('Anzahl')
        plt.grid(True)
        plt.hist(voltage, bins=60)
        plt.savefig('img\\394386_2E1_histovoltage.pdf')
        plt.show()


class Programm:
    def plotter(self):
        plotter = Visualizer()
        plotter.plot_angle_all_grob()
        plotter.plot_current_all_grob()
        plotter.plot_voltage_all_grob()
        plotter.plot_angle_all_fine()
        plotter.plot_current_all_fine()
        plotter.plot_voltage_all_fine()
        
    def cross_plotter_save(self, file_name):
        plotter = Visualizer()
        plotter.plot_current_zoom(file_name, save=True)
        plotter.plot_voltage_zoom(file_name,save=True)
        plotter.plot_phase_zoom(file_name,save=True)
        
    def cross_plotter(self, file_name):
        plotter = Visualizer()
        current = plotter.plot_current_zoom(file_name)
        voltage = plotter.plot_voltage_zoom(file_name)
        phase = plotter.plot_phase_zoom(file_name)
        return phase, current, voltage
        
    def info_giver_frequency(self, file_name):
        frequency = cassy.CassyDaten(file_name).messung(1).datenreihe('f_1').werte
        print('Niedrigste Frequenz {} Hz, \nHöchste Frequenz {} Hz, \nSchrittweite {} Hz\n'.format(frequency[0], frequency[-1], frequency[1]- frequency[0])  )
       

    def table_builder_raw_data(self):
        ohm_1 = self.cross_plotter('1_ohm\\fein.lab')
        ohm_5 = self.cross_plotter('5_1_ohm\\fein.lab')
        ohm_6 = self.cross_plotter('6_1_ohm\\fein.lab')
        ohm_10 = self.cross_plotter('10_ohm\\fein.lab')
        ohm_11 = self.cross_plotter('11_ohm\\fein.lab')
        text = ''  
        table = texttable.Texttable()
        table.set_cols_align(["c"]*6)
        table.header(["Wert", "$\\SI{1}{\\ohm}$","$\\SI{5,1}{\\ohm}$", "$\\SI{6,1}{\\ohm}$", "$\\SI{10}{\\ohm}$", "$\\SI{11}{\\ohm}$"])
        table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
        table.set_precision(3)
        table.add_row(['\\multicolumn{5}{c}{Strom}','','','','',''])
        table.add_row(['Resonanzfrequenz', '$\\SI{{ {} (12)}}{{\\hertz}} $'.format(ohm_1[0][0]),
                       '$\\SI{{ {} (17)}}{{\\hertz}} $'.format(ohm_5[0][0]),
                       '$\\SI{{ {} (58)}}{{\\hertz}}  $'.format(ohm_6[0][0]),
                       '$\\SI{{ {} (40)}}{{\\hertz}} $'.format(ohm_10[0][0]),
                       '$\\SI{{ {} (58)}}{{\\hertz}} $'.format(ohm_11[0][0])])
        table.add_row(['obere Grenze', '$\\SI{{ {} (12)}}{{\\hertz}} $'.format(ohm_1[0][1]),
                       '$\\SI{{ {} (17)}}{{\\hertz}} $'.format(ohm_5[0][1]),
                       '$\\SI{{ {} (58)}}{{\\hertz}}  $'.format(ohm_6[0][1]),
                       '$\\SI{{ {} (40)}}{{\\hertz}} $'.format(ohm_10[0][1]),
                       '$\\SI{{ {} (58)}}{{\\hertz}} $'.format(ohm_11[0][1])])
        table.add_row(['untere Grenze', '$\\SI{{ {} (12)}}{{\\hertz}} $'.format(ohm_1[0][2]),
                       '$\\SI{{ {} (17)}}{{\\hertz}} $'.format(ohm_5[0][2]),
                       '$\\SI{{ {} (58)}}{{\\hertz}}  $'.format(ohm_6[0][2]),
                       '$\\SI{{ {} (40)}}{{\\hertz}} $'.format(ohm_10[0][2]),
                       '$\\SI{{ {} (58)}}{{\\hertz}} $'.format(ohm_11[0][2])])
        table.add_row(['\\multicolumn{5}{c}{Phase}','','','','',''])
        table.add_row(['Resonanzfrequenz', '$\\SI{{ {} (12)}}{{\\hertz}} $'.format(ohm_1[1][0]),
                       '$\\SI{{ {} (17)}}{{\\hertz}} $'.format(ohm_5[1][0]),
                       '$\\SI{{ {} (58)}}{{\\hertz}}  $'.format(ohm_6[1][0]),
                       '$\\SI{{ {} (40)}}{{\\hertz}} $'.format(ohm_10[1][0]),
                       '$\\SI{{ {} (58)}}{{\\hertz}} $'.format(ohm_11[1][0])])
        table.add_row(['obere Grenze', '$\\SI{{ {} (12)}}{{\\hertz}} $'.format(ohm_1[1][1]),
                       '$\\SI{{ {} (17)}}{{\\hertz}} $'.format(ohm_5[1][1]),
                       '$\\SI{{ {} (58)}}{{\\hertz}}  $'.format(ohm_6[1][1]),
                       '$\\SI{{ {} (40)}}{{\\hertz}} $'.format(ohm_10[1][1]),
                       '$\\SI{{ {} (58)}}{{\\hertz}} $'.format(ohm_11[1][1])])
        table.add_row(['untere Grenze', '$\\SI{{ {} (12)}}{{\\hertz}} $'.format(ohm_1[1][2]),
                       '$\\SI{{ {} (17)}}{{\\hertz}} $'.format(ohm_5[1][2]),
                       '$\\SI{{ {} (58)}}{{\\hertz}}  $'.format(ohm_6[1][2]),
                       '$\\SI{{ {} (40)}}{{\\hertz}} $'.format(ohm_10[1][2]),
                       '$\\SI{{ {} (58)}}{{\\hertz}} $'.format(ohm_11[1][2])])
        
      
        error_1_ohm = (ohm_1[2][1] -ohm_1[2][2])/3**(1/2)
        error_5_ohm = (ohm_5[2][1] -ohm_5[2][2])/3**(1/2)
        error_6_ohm = (ohm_6[2][1] -ohm_6[2][2])/3**(1/2)
        error_10_ohm = (ohm_10[2][1] -ohm_10[2][2])/3**(1/2)
        error_11_ohm = (ohm_11[2][1] -ohm_11[2][2])/3**(1/2)
        
        table.add_row(['\\multicolumn{5}{c}{Spannung}','','','','',''])
        table.add_row(['Spannungsüberhöhung', '$\\SI{{ {:.4f} ({:.4f})}}{{\\volt}} $'.format(ohm_1[2][0],  error_1_ohm),
                     '$\\SI{{ {:.4f} ({:.4f})}}{{\\volt}} $'.format(ohm_5[2][0],  error_5_ohm),
                     '$\\SI{{ {:.4f} ({:.4f})}}{{\\volt}} $'.format(ohm_6[2][0],  error_6_ohm),
                     '$\\SI{{ {:.5f} ({:.5f})}}{{\\volt}} $'.format(ohm_10[2][0],  error_10_ohm),
                     '$\\SI{{ {:.5f} ({:.5f})}}{{\\volt}} $'.format(ohm_11[2][0],  error_11_ohm)])
        
        text += latextable.draw_latex(table, label='tab:raw_data', caption='Abgelesene Werte inklusive Fehler', resize=True) +'\n'

        text_file = codecs.open('img\\394386_2E1_tabledataraw.tex', "w", 'utf-8')
        text_file.write(text)
        text_file.close()
        
        
        
        
        
        