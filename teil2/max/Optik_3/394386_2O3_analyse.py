# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 15:21:35 2020

@author: Max
"""

import numpy as np
import nice_plots
from praktikum import analyse
import matplotlib.pyplot as plt
import texttable
import latextable
import codecs
from praktikum import literaturwerte


def automated_regression_table(header, data, ndof, file=None):
    if len(header) != len(data)+1:
        print('header size not data size')
        return 0
    text = ''
    for i in range(len(ndof)):
        ndof[i] =ndof[i] -2   
    ndof = np.array(ndof)
    data = np.transpose(data)    
    table = texttable.Texttable()
    table.set_cols_align(["c"]*(len(header)))
    
    #table.header(header)
    table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
    table.set_precision(8)
    data_new = [0,0,0,0,0,0,0,0]
    data_new[0] = header
    data_new[1] = np.concatenate((['Steigung'],data[0]))
    data_new[2] = np.concatenate((['Fehler Steigung'],data[1]))
    data_new[3] = np.concatenate((['Achsenabschnitt'],data[2]))
    data_new[4] = np.concatenate((['Fehler Achsenabschnitt'],data[3]))
    chiqndof = data[4]/ndof
    data_new[5] = np.concatenate((['$ \\chi^2 $'],data[4]))
    data_new[6] = np.concatenate((['Freiheitsgrade($ndof$)'],ndof))
    data_new[7] = np.concatenate((['$ \\frac{\\chi^2}{ndof} $'], chiqndof))

    table.add_rows(data_new)
 

    text = latextable.draw_latex(table, label='tab:kappa_raw', caption='Einstellung der Mikrometerschraube nach der jeweiligen Anzahl der Maxima inkl. Messfehler') +'\n'
    
    
    
    
    text_file = codecs.open(file, "w", 'utf-8')
    text_file.write(text)
    text_file.close()
    



laser = 632.8E-9
number_of_maxima_7 = np.array([0, 1, 2, 3, 4, 5, 6, 7])

number_of_maxima_130 = np.array([0,10, 20 ,30 ,40 ,50 ,60 ,70 ,80 ,90 ,100 ,110 ,120 , 130])
number_of_maxima_150 = np.array([0,10, 20 ,30 ,40 ,50 ,60 ,70 ,80 ,90 ,100 ,110 ,120 , 130, 140, 150])

number_of_maxima_160 = np.array([ 20 ,30 ,40 ,50 ,60 ,70 ,80 ,90 ,100 ,110 ,120 , 130, 140])
number_of_maxima_180 = np.array([9, 19 ,29 ,39 ,49 ,59 ,69 ,79 ,90 ,100 ,110 ,120 , 130, 141, 151, 161, 171, 181])
number_of_maxima_180_3 = np.array([9, 19 ,29 ,39 ,49 ,59 ,69 ,79 ,89 ,99 ,110 ,120 , 130, 140, 150, 160, 170, 180])

number_of_maxima_160_raw = np.array([ 0, 10, 20 ,30 ,40 ,50 ,60 ,70 ,80 ,90 ,100 ,110 ,120 , 130, 140, 150, 160])
number_of_maxima_180_raw = np.array([0, 10, 20 ,30 ,40 ,50 ,60 ,70 ,80 ,90 ,100 ,110 ,120 , 130, 140, 150, 160, 170, 180])



kappa_measure_1 = np.array([0, 6, 11, 18, 25, 31, 37, 44, 51, 58, 64, 72, 78, 84])/100 + 7
kappa_measure_1 *= 1E-3
kappa_measure_2 = np.array([0, 6, 12, 18, 24, 31, 37, 44, 51, 57, 64, 71, 77, 84, 91, 98]) / 100 + 7
kappa_measure_2 *= 1E-3
kappa_measure_3 = np.array([0, 6, 12, 18, 25, 31, 37, 44, 51, 57, 64, 71, 77, 84, 91, 97])/100 + 7
kappa_measure_3 *= 1E-3


lambda_measure_1 = np.array([ 5, 10, 15, 21, 26, 32, 38, 44, 49, 55, 60, 66, 72, 78, 84, 89, 95, 100])/100 + 7
lambda_measure_1 *= 1E-3
lambda_measure_2 = np.array([ 10, 15, 20, 26, 31, 37, 42, 48, 54, 59, 65, 71, 77 ]) / 100 + 7
lambda_measure_2 *= 1E-3
lambda_measure_3 = np.array([ 5, 10, 15, 21, 26, 32, 38, 43, 49, 55, 60, 66, 72, 77, 83, 89, 94, 100]) /100 + 7
lambda_measure_3 *= 1E-3


lambda_measure_1_raw = np.array([0, 5, 10, 15, 21, 26, 32, 38, 44, 49, 55, 60, 66, 72, 78, 84, 89, 95, 100])/100 + 7
lambda_measure_1_raw *= 1E-3
lambda_measure_2_raw = np.array([0, 5,  10, 15, 20, 26, 31, 37, 42, 48, 54, 59, 65, 71, 77, 83, 88 ]) / 100 + 7
lambda_measure_2_raw *= 1E-3
lambda_measure_3_raw = np.array([0, 5, 10, 15, 21, 26, 32, 38, 43, 49, 55, 60, 66, 72, 77, 83, 89, 94, 100]) /100 + 7
lambda_measure_3_raw *= 1E-3





pressure_measure_0 = np.array([980, 975, 977, 966, 953, 957, 960, 956, 964, 968])
pressure_measure_1 = np.array([874, 872, 870, 873, 873, 866, 870, 866, 860, 855])
pressure_measure_2 = np.array([780, 762, 766, 762, 773, 768, 765, 763, 757, 759])
pressure_measure_3 = np.array([663, 664, 660, 676, 660, 656, 672, 670, 673, 665])
pressure_measure_4 = np.array([573, 570, 558, 560, 562, 558, 568, 551, 562, 550])
pressure_measure_5 = np.array([471, 480, 460, 472, 469, 456, 449, 464, 468, 463])
pressure_measure_6 = np.array([360, 363, 362, 380, 355, 354, 358, 367, 355, 350])
pressure_measure_7 = np.array([266, 261, 250, 258, 250, 260, 250, 245, 251, 244])



kappa_measure_1_error = np.ones(len(kappa_measure_1)) * 1E-5 /12**(1/2)
kappa_measure_2_error = np.ones(len(kappa_measure_2)) * 1E-5 /12**(1/2)
kappa_measure_3_error = np.ones(len(kappa_measure_3)) * 1E-5 /12**(1/2)


lambda_measure_1_error = np.ones(len(lambda_measure_1)) * 1E-5 /12**(1/2)
lambda_measure_2_error = np.ones(len(lambda_measure_2)) * 1E-5 /12**(1/2)
lambda_measure_3_error = np.ones(len(lambda_measure_3)) * 1E-5 /12**(1/2)


lambda_measure_1_error_raw = np.ones(len(lambda_measure_1_raw)) * 1E-5 /12**(1/2)
lambda_measure_2_error_raw = np.ones(len(lambda_measure_2_raw)) * 1E-5 /12**(1/2)
lambda_measure_3_error_raw = np.ones(len(lambda_measure_3_raw)) * 1E-5 /12**(1/2)


kappa_reg_1 = nice_plots.nice_linear_regression_plot(number_of_maxima_130, kappa_measure_1, kappa_measure_1_error, title='Regression für k, Messung 1' ,xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m', save='img\\394386_2O3_kappareg1.pdf')
kappa_reg_2 = nice_plots.nice_linear_regression_plot(number_of_maxima_150, kappa_measure_2, kappa_measure_2_error, title='Regression für k, Messung 2',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m', save='img\\394386_2O3_kappareg2.pdf')
kappa_reg_3 = nice_plots.nice_linear_regression_plot(number_of_maxima_150, kappa_measure_3, kappa_measure_3_error, title='Regression für k, Messung 3',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m', save='img\\394386_2O3_kappareg3.pdf')




kappa_1 = laser/(kappa_reg_1[0]*2)
kappa_2 = laser/(kappa_reg_2[0]*2)
kappa_3 = laser/(kappa_reg_3[0]*2)

error_kappa_1 = laser/2 * kappa_reg_1[1]/kappa_reg_1[0]**2
error_kappa_2 = laser/2 * kappa_reg_2[1]/kappa_reg_2[0]**2
error_kappa_3 = laser/2 * kappa_reg_3[1]/kappa_reg_3[0]**2

error_kappa = np.array([error_kappa_1, error_kappa_2])

kappa_mittel = analyse.gewichtetes_mittel([kappa_1, kappa_2], error_kappa)
pressure_mean_0 = analyse.mittelwert_stdabw(pressure_measure_0)

print( 'Die 3 Kappa sind: {:.5f} +- {:.5f}, {:.5f} +- {:.5f}, {:.5f} +- {:.5f}'.format(kappa_1, error_kappa_1, kappa_2, error_kappa_2, kappa_3, error_kappa_3 )   )
print('Das gewichtete Mittel ist {:.6f} +- {:.6f}'.format(kappa_mittel[0], kappa_mittel[1]))


lambda_reg_1_raw  = nice_plots.nice_linear_regression_plot(number_of_maxima_180_raw, lambda_measure_1_raw, lambda_measure_1_error_raw, title='Regression für lambda, Messung 1',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m', save='img\\394386_2O3_lambdaregraw1.pdf')
lambda_reg_2_raw  = nice_plots.nice_linear_regression_plot(number_of_maxima_160_raw, lambda_measure_2_raw, lambda_measure_2_error_raw, title='Regression für lambda, Messung 2',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m',  save='img\\394386_2O3_lambdaregraw2.pdf')
lambda_reg_3_raw  = nice_plots.nice_linear_regression_plot(number_of_maxima_180_raw, lambda_measure_3_raw, lambda_measure_3_error_raw, title='Regression für lambda, Messung 3',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m',  save='img\\394386_2O3_lambdaregraw3.pdf')



lambda_reg_1  = nice_plots.nice_linear_regression_plot(number_of_maxima_180, lambda_measure_1, lambda_measure_1_error, title='Regression für lambda, Messung 1 überarbeitet',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m',  save='img\\394386_2O3_lambdareg1.pdf')
lambda_reg_2  = nice_plots.nice_linear_regression_plot(number_of_maxima_160, lambda_measure_2, lambda_measure_2_error, title='Regression für lambda, Messung 1 überarbeitet',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m', save='img\\394386_2O3_lambdareg2.pdf')
lambda_reg_3  = nice_plots.nice_linear_regression_plot(number_of_maxima_180_3, lambda_measure_3, lambda_measure_3_error, title='Regression für lambda, Messung 1 überarbeitet',xlabel='Anzahl der Maxima', ylabel='Wert auf Schraube/m', ylabelresidue='Residuum/m', save='img\\394386_2O3_lambdareg3.pdf')

lambda_1 = 2 * kappa_mittel[0] * lambda_reg_1[0]
lambda_2 = 2 * kappa_mittel[0] * lambda_reg_2[0]
lambda_3 = 2 * kappa_mittel[0] * lambda_reg_3[0]

error_lambda_1 = 2 * kappa_mittel[0] * lambda_reg_1[1]
error_lambda_2 = 2 * kappa_mittel[0] * lambda_reg_2[1]
error_lambda_3 = 2 * kappa_mittel[0] * lambda_reg_3[1]

error_lambda = np.array([error_lambda_1, error_lambda_2, error_lambda_3])

lambda_mittel = analyse.gewichtetes_mittel([lambda_1, lambda_2, lambda_3], error_lambda)
lambda_error_ges = (0.096E-9**2 + lambda_mittel[1]**2)**(1/2)

print( 'Die 3 Lambda sind: {:.5e} +- {:.5e}, {:.5e} +- {:.5e}, {:.5e} +- {:.5e}'.format(lambda_1, error_lambda_2, lambda_2, error_lambda_2, lambda_3, error_lambda_3 )   )
print('Das gewichtete Mittel ist {:.5e} +- {:.5e}'.format(lambda_mittel[0], lambda_mittel[1]))

plt.figure(figsize=(20,10))
plt.grid(True)
plt.title('lambdawerte')
plt.errorbar(  np.array([lambda_1, lambda_2, lambda_3]),[1,2,3], xerr=error_lambda)
plt.show()

"""
plt.figure(figsize=(20,10))
plt.grid(True)
plt.title('kappawerte')
#plt.errorbar(  np.array([kappa_1, kappa_2, kappa_3]),[1,2,3], xerr=error_kappa)
plt.show()

"""
pressure_mean_0 = analyse.mittelwert_stdabw(pressure_measure_0)
pressure_mean_1 = analyse.mittelwert_stdabw(pressure_measure_1)
pressure_mean_2 = analyse.mittelwert_stdabw(pressure_measure_2)
pressure_mean_3 = analyse.mittelwert_stdabw(pressure_measure_3)
pressure_mean_4 = analyse.mittelwert_stdabw(pressure_measure_4)
pressure_mean_5 = analyse.mittelwert_stdabw(pressure_measure_5)
pressure_mean_6 = analyse.mittelwert_stdabw(pressure_measure_6)
pressure_mean_7 = analyse.mittelwert_stdabw(pressure_measure_7)


pressure_means = np.array([pressure_mean_0[0], pressure_mean_1[0], pressure_mean_2[0], pressure_mean_3[0],
                           pressure_mean_4[0], pressure_mean_5[0], pressure_mean_6[0], pressure_mean_7[0]])

pressure_mean_diviation_0 = pressure_mean_0[1]/8**(1/2)
pressure_mean_diviation_1 = pressure_mean_1[1]/8**(1/2)
pressure_mean_diviation_2 = pressure_mean_2[1]/8**(1/2)
pressure_mean_diviation_3 = pressure_mean_3[1]/8**(1/2)
pressure_mean_diviation_4 = pressure_mean_4[1]/8**(1/2)
pressure_mean_diviation_5 = pressure_mean_5[1]/8**(1/2)
pressure_mean_diviation_6 = pressure_mean_6[1]/8**(1/2)
pressure_mean_diviation_7 = pressure_mean_7[1]/8**(1/2)


pressure_mean_diviations = np.array([pressure_mean_diviation_0, pressure_mean_diviation_1, pressure_mean_diviation_2, pressure_mean_diviation_3,
                                     pressure_mean_diviation_4, pressure_mean_diviation_5, pressure_mean_diviation_6, pressure_mean_diviation_7])

pressure_reg = nice_plots.nice_linear_regression_plot(number_of_maxima_7, pressure_means, pressure_mean_diviations,title='Regression des Druckes', xlabel='Anzahl der Maxima', ylabel='Druck/hPa', ylabelresidue='Residuum/hPa', save='img\\394386_2O3_pressurereg.pdf')

delta_np = 1/pressure_reg[0] * lambda_mittel[0]/ 2E-2
delta_np_error = 1/ pressure_reg[0]**2 *pressure_reg[1] * lambda_mittel[0]/ 2E-2


delta_np_error_sys =  lambda_error_ges/(2*0.01*pressure_reg[0])
delta_np_error_ges = (delta_np_error**2 +delta_np_error_sys**2)**(1/2)

"""automated tables for latex """
text = ''

   
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["Maxima", "Messung 1 in $\\si{{\metre}}$ ", "Messung 2 in $\\si{{\metre}}$", "Messung 3 in $\\si{{\metre}}$"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)
for i in range(len(number_of_maxima_150)):
    if i > len(number_of_maxima_130)-1:
        table.add_row([number_of_maxima_150[i], '-',
                  '$ \\num{{ {:.2e} }}'.format(kappa_measure_2[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_measure_2_error[i]),
                   '$ \\num{{ {:.2e} }}'.format(kappa_measure_3[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_measure_3_error[i])])
        continue
    table.add_row([number_of_maxima_150[i], '$ \\num{{ {:.2e} }}'.format(kappa_measure_1[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_measure_1_error[i]),
                  '$ \\num{{ {:.2e} }}'.format(kappa_measure_2[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_measure_2_error[i]),
                   '$ \\num{{ {:.2e} }}'.format(kappa_measure_3[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_measure_3_error[i])])
text += latextable.draw_latex(table, label='tab:kappa_raw', caption='Einstellung der Mikrometerschraube nach der jeweiligen Anzahl der Maxima inkl. Messfehler') +'\n'




text_file = codecs.open('img\\394386_2O3_tabkapparaw.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()


text = ''

   
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["Maxima", "Messung 1 in $\\si{{\metre}}$ ", "Messung 2 in $\\si{{\metre}}$", "Messung 3 in $\\si{{\metre}}$"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)
for i in range(len(number_of_maxima_180_raw)):
    if i > len(number_of_maxima_160_raw)-1:
        table.add_row([number_of_maxima_180_raw[i], 
                  '$ \\num{{ {:.2e} }}'.format(lambda_measure_1_raw[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_measure_1_error_raw[i]),
                  '-',
                   '$ \\num{{ {:.2e} }}'.format(lambda_measure_3_raw[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_measure_3_error_raw[i])])
        continue
    table.add_row([number_of_maxima_180_raw[i], '$ \\num{{ {:.2e} }}'.format(lambda_measure_1_raw[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_measure_1_error_raw[i]),
                  '$ \\num{{ {:.2e} }}'.format(lambda_measure_2_raw[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_measure_2_error_raw[i]),
                   '$ \\num{{ {:.2e} }}'.format(lambda_measure_3_raw[i]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_measure_3_error_raw[i])])
text += latextable.draw_latex(table, label='tab:lambda_raw', caption='Einstellung der Mikrometerschraube nach der jeweiligen Anzahl der Maxima inkl. Messfehler') +'\n'




text_file = codecs.open('img\\394386_2O3_tablambdaraw.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()


text = ''

table = texttable.Texttable()
table.set_cols_align(["c"]*11)
table.header(["Maxima", "1/$\\si{{\hecto\pascal}}$ ", "2/$\\si{{\hecto\pascal}}$ ", "3/$\\si{{\hecto\pascal}}$ ", "4/$\\si{{\hecto\pascal}}$ ","5/$\\si{{\hecto\pascal}}$ ","6/$\\si{{\hecto\pascal}}$ ","7/$\\si{{\hecto\pascal}}$ ","8/$\\si{{\hecto\pascal}}$ ", "9/$\\si{{\hecto\pascal}}$ ", "10/$\\si{{\hecto\pascal}}$ "])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

pressure_measure_0 = np.concatenate(([0],pressure_measure_0))
table.add_row(pressure_measure_0)
pressure_measure_1 = np.concatenate(([1],pressure_measure_1))
table.add_row(pressure_measure_1)
pressure_measure_2 = np.concatenate(([2],pressure_measure_2))
table.add_row(pressure_measure_2)
pressure_measure_3 = np.concatenate(([3],pressure_measure_3))
table.add_row(pressure_measure_3)
pressure_measure_4 = np.concatenate(([4],pressure_measure_4))
table.add_row(pressure_measure_4)
pressure_measure_5 = np.concatenate(([5],pressure_measure_5))
table.add_row(pressure_measure_5)
pressure_measure_6 = np.concatenate(([6],pressure_measure_6))
table.add_row(pressure_measure_6)
pressure_measure_7 = np.concatenate(([7],pressure_measure_7))
table.add_row(pressure_measure_7)

text += latextable.draw_latex(table, label='tab:pressureraw', caption='Druckmessungen bei erreichter Anzahl an Maxima', resize=True) +'\n'




text_file = codecs.open('img\\394386_2O3_tabpressureraw.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()


text = ''

   
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["Parameter", "Messung 1", "Messung 2", "Messung 3"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['Steigung/m', '$ \\num{{ {:.2e} }}'.format(kappa_reg_1[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_reg_1[1]),
              '$ \\num{{ {:.2e} }}'.format(kappa_reg_2[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_reg_2[1]),
               '$ \\num{{ {:.2e} }}'.format(kappa_reg_3[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_reg_3[1])])

table.add_row(['Achsenabschnitt/m', '$ \\num{{ {:.2e} }}'.format(kappa_reg_1[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_reg_1[3]),
              '$ \\num{{ {:.2e} }}'.format(kappa_reg_2[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_reg_2[3]),
               '$ \\num{{ {:.2e} }}'.format(kappa_reg_3[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(kappa_reg_3[3])])


table.add_row(['$ \\chi^2 $', '$ \\num{{ {:.2f} }}$'.format(kappa_reg_1[4]),
              '$ \\num{{ {:.2f} }}$'.format(kappa_reg_2[4]),
               '$ \\num{{ {:.2f} }}$'.format(kappa_reg_3[4])])


table.add_row(['Freiheitsgrade ($ndof$)', '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_130)-2),
              '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_150)-2),
               '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_130)-2)])


table.add_row(['$ \\frac{\\chi^2}{ndof} $', '$ \\num{{ {:.2e} }}$'.format(kappa_reg_1[4]/(len(number_of_maxima_130)-2)),
              '$ \\num{{ {:.2e} }}$'.format(kappa_reg_2[4]/(len(number_of_maxima_150)-2)),
               '$ \\num{{ {:.2e} }}$'.format(kappa_reg_3[4]/(len(number_of_maxima_150)-2))])







text += latextable.draw_latex(table, label='tab:kappa_reg', caption='Parameter der linearen Regression für k', resize=True) +'\n'

text_file = codecs.open('img\\394386_2O3_tabkappareg.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()



text = ''  
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["Parameter", "Messung 1", "Messung 2", "Messung 3"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['Steigung/m', '$ \\num{{ {:.2e} }}'.format(lambda_reg_1[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_1[1]),
              '$ \\num{{ {:.2e} }}'.format(lambda_reg_2[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_2[1]),
               '$ \\num{{ {:.2e} }}'.format(lambda_reg_3[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_3[1])])

table.add_row(['Achsenabschnitt/m', '$ \\num{{ {:.2e} }}'.format(lambda_reg_1[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_1[3]),
              '$ \\num{{ {:.2e} }}'.format(lambda_reg_2[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_2[3]),
               '$ \\num{{ {:.2e} }}'.format(lambda_reg_3[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_3[3])])


table.add_row(['$ \\chi^2 $', '$ \\num{{ {:.2f}  }}$'.format(lambda_reg_1[4]),
              '$ \\num{{ {:.2f}  }}$'.format(lambda_reg_2[4]),
               '$ \\num{{ {:.2f}  }}$'.format(lambda_reg_3[4])])


table.add_row(['Freiheitsgrade ($ndof$)', '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_180)-2),
              '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_160)-2),
               '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_180_3)-2)])


table.add_row(['$ \\frac{\\chi^2}{ndof} $', '$ \\num{{ {:.2e} }}$'.format(lambda_reg_1[4]/(len(number_of_maxima_180)-2)),
              '$ \\num{{ {:.2e} }}$'.format(lambda_reg_2[4]/(len(number_of_maxima_160)-2)),
               '$ \\num{{ {:.2e} }}$'.format(lambda_reg_3[4]/(len(number_of_maxima_180_3)-2))])

text += latextable.draw_latex(table, label='tab:lambda_reg', caption='Parameter der linearen Regression der überarbeiteten Werte für $\\lambda$', resize=True) +'\n'

text_file = codecs.open('img\\394386_2O3_tablambdareg.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()


text = ''  
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["Parameter", "Messung 1", "Messung 2", "Messung 3"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['Steigung/m', '$ \\num{{ {:.2e} }}'.format(lambda_reg_1_raw[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_1_raw[1]),
              '$ \\num{{ {:.2e} }}'.format(lambda_reg_2_raw[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_2_raw[1]),
               '$ \\num{{ {:.2e} }}'.format(lambda_reg_3_raw[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_3_raw[1])])

table.add_row(['Achsenabschnitt/m', '$ \\num{{ {:.2e} }}'.format(lambda_reg_1_raw[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_1_raw[3]),
              '$ \\num{{ {:.2e} }}'.format(lambda_reg_2_raw[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_2_raw[3]),
               '$ \\num{{ {:.2e} }}'.format(lambda_reg_3_raw[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(lambda_reg_3_raw[3])])


table.add_row(['$ \\chi^2 $', '$ \\num{{ {:.2f}  }}$'.format(lambda_reg_1_raw[4]),
              '$ \\num{{ {:.2f}  }}$'.format(lambda_reg_2_raw[4]),
               '$ \\num{{ {:.2f}  }}$'.format(lambda_reg_3_raw[4])])


table.add_row(['Freiheitsgrade ($ndof$)', '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_180_raw)-2),
              '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_160_raw)-2),
               '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_180_raw)-2)])


table.add_row(['$ \\frac{\\chi^2}{ndof} $', '$ \\num{{ {:.2e} }}$'.format(lambda_reg_1_raw[4]/(len(number_of_maxima_180_raw)-2)),
              '$ \\num{{ {:.2e} }}$'.format(lambda_reg_2_raw[4]/(len(number_of_maxima_160_raw)-2)),
               '$ \\num{{ {:.2e} }}$'.format(lambda_reg_3_raw[4]/(len(number_of_maxima_180_raw)-2))])

text += latextable.draw_latex(table, label='tab:lambda_reg_raw', caption='Parameter der linearen Regression der Werte für $\\lambda$', resize=True) +'\n'

text_file = codecs.open('img\\394386_2O3_tablambdaregraw.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()

text = ''  
table = texttable.Texttable()
table.set_cols_align(["c"]*2)
table.header(["Parameter", "Wert"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['Steigung/hPa', '$ \\num{{ {:.2e} }}'.format(pressure_reg[0]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(pressure_reg[1])])

table.add_row(['Achsenabschnitt/hPa', '$ \\num{{ {:.2e} }}'.format(pressure_reg[2]) + " \pm " + ' \\num{{ {:.2e} }}$'.format(pressure_reg[3])])


table.add_row(['$ \\chi^2 $', '$ \\num{{ {:.2f}  }}$'.format(pressure_reg[4])])


table.add_row(['Freiheitsgrade ($ndof$)', '$ \\num{{ {:.2f} }}$'.format(len(number_of_maxima_7)-2)])


table.add_row(['$ \\frac{\\chi^2}{ndof} $', '$ \\num{{ {:.2e} }}$'.format(pressure_reg[4]/(len(number_of_maxima_7)-2))])

text += latextable.draw_latex(table, label='tab:pressure_reg', caption='Parameter der linearen Regression der Werte für die Druckmessung') +'\n'

text_file = codecs.open('img\\394386_2O3_tabpressurereg.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()



text = ''  
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["", "Messung 1", "Messung 2", "Messung 3"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['k', '$ \\num{{ {:.2e} }}'.format(kappa_1) + " \pm " + ' \\num{{ {:.2e} }}$'.format(error_kappa_1),
              '$ \\num{{ {:.2e} }}'.format(kappa_2) + " \pm " + ' \\num{{ {:.2e} }}$'.format(error_kappa_2),
               '$ \\num{{ {:.2e} }}'.format(kappa_3) + " \pm " + ' \\num{{ {:.2e} }}$'.format(error_kappa_3)])

text += latextable.draw_latex(table, label='tab:kappa_mittel', caption='Mittelwerte für k') +'\n'

text_file = codecs.open('img\\394386_2O3_tabkappamittel.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()



text = ''  
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["", "Messung 1", "Messung 2", "Messung 3"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['$\\lambda$/nm', '$ \\num{{ {:.2e} }}'.format(lambda_1) + " \pm " + ' \\num{{ {:.2e} }}$'.format(error_lambda_1),
              '$ \\num{{ {:.2e} }}'.format(lambda_2) + " \pm " + ' \\num{{ {:.2e} }}$'.format(error_lambda_2),
               '$ \\num{{ {:.2e} }}'.format(lambda_3) + " \pm " + ' \\num{{ {:.2e} }}$'.format(error_lambda_3)])

text += latextable.draw_latex(table, label='tab:lambda_mittel', caption='Mittelwerte für $\\lambda$') +'\n'

text_file = codecs.open('img\\394386_2O3_tablambdamittel.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()


text = ''  
table = texttable.Texttable()
table.set_cols_align(["c"]*4)
table.header(["Maxima", "Mittlerer Druck/hPa", "Standardabweichung/hPa", "Fehler auf den Mittelwert/hPa"])
table.set_deco(texttable.Texttable.HEADER | texttable.Texttable.VLINES)
table.set_precision(3)

table.add_row(['$0$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_0[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_0[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_0)])
table.add_row(['$1$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_1[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_1[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_1)])
table.add_row(['$2$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_2[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_2[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_2)])
table.add_row(['$3$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_3[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_3[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_3)])
table.add_row(['$4$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_4[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_4[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_4)])
table.add_row(['$5$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_5[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_5[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_5)])
table.add_row(['$6$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_6[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_6[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_6)])
table.add_row(['$7$', '$ \\num{{ {:.2e} }}$'.format(pressure_mean_7[0]),
              '$ \\num{{ {:.2e} }}$'.format(pressure_mean_7[1]),
               '$ \\num{{ {:.2e} }}$'.format(pressure_mean_diviation_7)])

text += latextable.draw_latex(table, label='tab:druck_mittelwerte', caption='Mittelwerte für den Druck') +'\n'

text_file = codecs.open('img\\394386_2O3_tabdruckmittel.tex', "w", 'utf-8')
text_file.write(text)
text_file.close()




pressure_x = np.linspace(100, 1200, 1200)


plt.figure(figsize=(20,10))
plt.grid(True)
plt.xlabel('Druck/hPa')
plt.ylabel('Brechungsindex')
plt.title('Brechungsindex Vergleich')
plt.plot(pressure_x, literaturwerte.brechungsindex_luft(538, p=pressure_x), label='Literaturwerte')
plt.plot(pressure_x, 1-delta_np*pressure_x, label='Berechnung')
plt.legend()
plt.savefig('img\\394386_2O3_literaturvergleich.pdf')
plt.show()





