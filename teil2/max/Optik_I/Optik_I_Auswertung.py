# -*- coding: utf-8 -*-
"""
Created on Sat Sep  5 09:34:59 2020

@author: Max
"""
import numpy as np
from praktikum import analyse
import matplotlib.pyplot as plt

from praktikum import literaturwerte


plt.rcParams['font.size'] = 26.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['figure.autolayout'] = True


def nice_regression_parameters(x, y, xerr=None, yerr=None):
    """Calculate and print a linear regression in a nice way."""
    if xerr is not None and xerr.all():
        (m, em, b, eb, chiq, corr) = analyse.lineare_regression_xy(x, y, xerr, yerr)
    else:
        (m, em, b, eb, chiq, corr) = analyse.lineare_regression(x, y, yerr)

    print('y = m x + b :: m = {:.3e} +/- {:.3e}; b = {:.3e} +/- {:.3e}; X^2/DoF = {:.3f} / {:d} = {:.3f}; Corr_m,b = {:.3f}'.format(
        m, em, b, eb, chiq, x.size-2, chiq/(x.size-2), corr))

    return (m, em, b, eb, chiq, corr)

def nice_quadratic_regression_plot(x, y, yerror, xerror=np.zeros(1), xlabel='',
                                   ylabel='', ylabelresidue='', title='', save=None):
    """Does a quadratic Regression with x (and y errors).
    x = array with x-coordinates,
    y = array with y-coordinates,
    xerror = array with errors of x,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue
    save = filename to save figure in"""
    plt.tight_layout()
    fig, axarray = plt.subplots(2, 1, figsize=(20,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    fig.tight_layout()
    axarray[1].grid(True)
    axarray[0].grid(True)
    # axarray[0].set_xlabel(xlabel)
    axarray[0].set_ylabel(ylabel)
    axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue)

    if title:
        axarray[0].set_title(title)

    if(xerror.all()):
        axarray[0].errorbar(x, y, xerr=xerror, yerr=yerror, color='red', fmt='.',
                            marker='o', markeredgecolor='red')
    else:
        axarray[0].errorbar(x, y, yerr=yerror, color='red', fmt='.', marker='o',
                            markeredgecolor='red')

    if xerror is not None and xerror.all():
        (a, ea, b, eb, c, ec, chiq, corrs) = analyse.quadratische_regression_xy(x, y, xerror, yerror)
    else:
        (a, ea, b, eb, c, ec, chiq, corrs) = analyse.quadratische_regression(x, y, yerror)

    axarray[0].plot(x, a*x**2+b*x+c, color='green')
    axarray[0].text(0.03, 0.9, '$\\chi^2$/ndof = %g / %g = %g' % (
        chiq, x.size-3, chiq/(x.size-3)), transform=axarray[0].transAxes)

    residuals = y-(a*x**2+b*x+c)
    sigmaRes = np.sqrt(((2*a*x+b)*xerror)**2 + yerror**2)
    axarray[1].axhline(y=0., color='black', linestyle='--')
    axarray[1].errorbar(x, residuals, yerr=sigmaRes, color='red', fmt='.',
                        marker='o', markeredgecolor='red')

    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)
    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)

    fig.subplots_adjust(hspace=0.0)
    plt.show()
    if save:
        fig.savefig(save)

    return (a, ea, b, eb, c, ec, chiq, corrs)


def nice_linear_regression_plot(x, y, yerror, xerror=np.zeros(1), xlabel='',
                         ylabel='', ylabelresidue='', title='', save=None):
    """Does a linear Regression with x (and y errors).
    x = array with x-coordinates,
    y = array with y-coordinates,
    xerror = array with errors of x,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue
    save = filename to save figure in"""
    plt.tight_layout()
    fig, axarray = plt.subplots(2, 1, figsize=(20,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    fig.tight_layout()
    axarray[1].grid(True)
    axarray[0].grid(True)
    # axarray[0].set_xlabel(xlabel)
    axarray[0].set_ylabel(ylabel)
    axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue)

    if title:
        axarray[0].set_title(title)

    if(xerror.all()):
        axarray[0].errorbar(x, y, xerr=xerror, yerr=yerror, color='red', fmt='.',
                            marker='o', markeredgecolor='red')
    else:
        axarray[0].errorbar(x, y, yerr=yerror, color='red', fmt='.', marker='o',
                            markeredgecolor='red')

    (m,em,b,eb,chiq,corr) = nice_regression_parameters(x, y, xerror, yerror)


    axarray[0].plot(x, m*x+b, color='green')
    axarray[0].text(0.03, 0.9, '$\\chi^2$/ndof = %g / %g = %g' % (
        chiq, x.size-2, chiq/(x.size-2)), transform=axarray[0].transAxes)

    residuals = y-(m*x+b)
    sigmaRes = np.sqrt((m*xerror)**2 + yerror**2)
    axarray[1].axhline(y=0., color='black', linestyle='--')
    axarray[1].errorbar(x, residuals, yerr=sigmaRes, color='red', fmt='.',
                        marker='o', markeredgecolor='red')

    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)
    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)

    fig.subplots_adjust(hspace=0.0)
    plt.show()
    if save:
        fig.savefig(save)

    return (m,em,b,eb,chiq,corr)

plt.rcParams['font.size'] = 26.0
wavelengths = np.array([643.85, 579.07, 546.07, 508.58, 479.99, 467.81, 435.83, 404.66])
literature = np.array([1.72206, 0 , 1.7343, 0, 1.748, 0, 1.76191, 1.77578])

noise_measure = np.array([319 + 4/60, 319 + 5/60, 319 + 6/60, 319 + 5/60, 319 + 4/60, 319 + 5/60, 319 + 4/60,
                          319 + 4/60, 319 + 5/60, 319 + 6/60])

left_measure = np.array([319 + 4.5/60, 319.5 + 25/60, 320 + 28/60, 321 + 19.5/60, 322 + 5/60, 322.5, 323.5 + 17/60, 325.5])
rigth_measure = np.array([201 + 3.5/60, 200 + 10.5/60, 199.5 + 9/60, 198.5 + 20.5/60, 198 + 0.5/60, 197.5 + 8/60, 196 + 19.5/60, 194.5 + 6/60])

error_psi  = analyse.mittelwert_stdabw(noise_measure)[1]

minimal_angle = (left_measure - rigth_measure) / 2

plt.figure( figsize=(20,10))
plt.grid(True)
plt.title('Histogramm der Rauschmessung')
plt.xlabel('Winkel in °')
plt.ylabel('Anzahl')
plt.tight_layout()
plt.hist(noise_measure, bins=12, range=(319,319.24), rwidth=0.9)
plt.savefig('394386_2O1_rauschhisto.pdf')
plt.show()

plt.figure(figsize=(20,10))
plt.grid(True)
plt.title('Mittelwerte linke Messung')
plt.xlabel('Wellenlänge in nm')
plt.ylabel('Winkel in °')
plt.tight_layout()
plt.plot(wavelengths, left_measure,marker='o', linewidth=0)
plt.savefig('394386_2O1_linkemessung.pdf')
plt.show()

plt.figure(figsize=(20,10))
plt.grid(True)
plt.title('Mittelwerte rechte Messung')
plt.xlabel('Wellenlänge in nm')
plt.ylabel('Winkel in °')
plt.tight_layout()
plt.plot(wavelengths, rigth_measure,marker='o', linewidth=0)
plt.savefig('394386_2O1_rechtemessung.pdf')
plt.show()

plt.figure(figsize=(20,10))
plt.grid(True)
plt.title('Winkel der minimalen Brechung')
plt.xlabel('Wellenlänge in nm')
plt.ylabel('Winkel in °')
plt.tight_layout()
plt.plot(wavelengths, minimal_angle, marker='o')
plt.savefig('394386_2O1_deltamin.pdf')
plt.show()


wavelengths /= 1E9
error_psi_rl = error_psi/ np.sqrt(2)
error_minimal_angle = error_psi

frac_wavelengths = 1 / wavelengths**2

error_n = 1/2 * np.cos( np.radians(minimal_angle + 60)/2 ) /  np.sin( np.radians( 30 ) ) * np.radians(error_minimal_angle)

n = np.sin(( np.radians(minimal_angle) + np.radians(60) ) / 2) / np.sin( np.radians( 30 ) )

lin_reg = analyse.lineare_regression(frac_wavelengths, n, error_n)
quad_reg = analyse.quadratische_regression(frac_wavelengths, n, error_n)



nice_linear_regression_plot(frac_wavelengths, n, error_n, xlabel='1/lambda^2 in 1/nm^2', ylabel='Brechungsindex n', ylabelresidue='n - Erwartung', title='lineare Regression', save='394386_2O1_linreg.pdf')
nice_quadratic_regression_plot(frac_wavelengths, n, error_n,xlabel='1/lambda^2 in 1/nm^2', ylabel='Brechungsindex n', ylabelresidue='n - Erwartung', title='quadratische Regression', save='394386_2O1_quadreg.pdf')



dndlambda = -2*quad_reg[2]/wavelengths[1]**3-4*quad_reg[0]/wavelengths[1]**5

A1 = dndlambda * 2 * 0.001 * np.sin( np.radians(60/2 )) / np.cos(np.radians( (minimal_angle[1] + 60)/2))
A2 = dndlambda * 2 * 0.0005 * np.sin( np.radians(60/2 )) / np.cos(np.radians( (minimal_angle[1] + 60)/2))


print('Das Auflösungsvermoögen liegt zwischen {:.3f} und {:.3f}'.format(A1, A2))


a = quad_reg[4]
b = quad_reg[2]
c = quad_reg[0]
n_zink = 1.7241 
inverted_lambda = np.sqrt( n_zink/c - (4*a*c-b**2)/(4*c**2) ) - b / (2*c)
lambda_zink = ( 1/inverted_lambda)**(1/2)
print(lambda_zink)

n_zink = 1.724 
inverted_lambda = np.sqrt( n_zink/c - (4*a*c-b**2)/(4*c**2) ) - b / (2*c)
lambda_zink = ( 1/inverted_lambda)**(1/2)
print(lambda_zink)

n_zink = 1.7239
inverted_lambda = np.sqrt( n_zink/c - (4*a*c-b**2)/(4*c**2) ) - b / (2*c)
lambda_zink = ( 1/inverted_lambda)**(1/2)
print(lambda_zink)


wavelengths *= 1E6

n_lit = literaturwerte.n_schott_nsf10(wavelengths)

