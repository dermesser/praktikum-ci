# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:11:58 2020

@author: Max_b
"""

from praktikum import cassy
from praktikum import analyse
import numpy as np
import matplotlib.pyplot as plt

name = '12.Bund-A-Saite.lab'
datei  = cassy.CassyDaten(name).messung(1)

time = datei.datenreihe('t').werte
voltage = datei.datenreihe('U_A1').werte
fastft0,fastft1 = analyse.fourier_fft(time,voltage)
fastft0 = fastft0[:int(len(fastft0)/2):]
fastft1 = fastft1[:int(len(fastft1)/2):]
plt.figure()

plt.plot(fastft0,fastft1)
plt.xlabel('frequency/Hz', fontsize=16)
plt.ylabel('amplitude', fontsize=16)
plt.show()

name = 'ASaite1fünftel.lab'
datei  = cassy.CassyDaten(name).messung(1)

time = datei.datenreihe('t').werte
voltage = datei.datenreihe('U_A1').werte
fastft0,fastft1 = analyse.fourier_fft(time,voltage)
fastft0 = fastft0[:int(len(fastft0)/2):]
fastft1 = fastft1[:int(len(fastft1)/2):]
plt.figure()

plt.plot(fastft0,fastft1)
plt.xlabel('frequency/Hz', fontsize=16)
plt.ylabel('amplitude', fontsize=16)
plt.show()