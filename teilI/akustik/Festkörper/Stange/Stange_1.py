# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 15:27:50 2020

@author: 394493: Alexander Kohlgraf
"""

from praktikum import analyse
from praktikum import cassy
import numpy as np
import matplotlib.pyplot as plt

def dichte(L, M, D, eL, eM, eD, esL):
    roh = (4/np.pi) * M * 1/(L * D**2)
    eStatRoh = roh * np.sqrt( (eM/M)**2 + (eL/L)**2 + 4*(eD/D)**2 )
    eSysRoh = roh * (esL/L)
    return [roh, eStatRoh, eSysRoh]

def schallgeschw(f, L, ef, eL, esL):
    v= 2 * L * f
    eStatV= v * np.sqrt( (eL/L)**2 + (ef/f)**2 )
    eSysV= v * (esL/L)
    return [v, eStatV, eSysV]

def emod(f, L, M, D, ef, eL, eM, eD, esL):
    E = (16/np.pi) * f**2 * L * M * D**-2
    eStatE = E * np.sqrt( 4*(ef/f)**2 + (eL/L)**2 + (eM/M)**2 + 4*(eD/D)**2 )
    eSysE = E *(esL/L)
    return [E, eStatE, eSysE]

def f0(baseAdress):
    f = []
    for x in range(1,6):
        inputfile = baseAdress + str(x) + ".lab"
        data = cassy.CassyDaten(inputfile)
        
        t = data.messung(1).datenreihe('t').werte
        U = data.messung(1).datenreihe('U_A1').werte
        
        #Bereinigung der niedrigen Frequenzen
        fft = analyse.fourier_fft(t, U)
        fft = analyse.untermenge_daten(fft[0],fft[1],100,max(fft[0]))
        
        #Grafische Darstellung der Fourier Analyse
        plt.figure()
        plt.plot(fft[0], fft[1])
        f.append(analyse.peakfinder_schwerpunkt(fft[0],fft[1]))
        plt.title(str(x) + ".lab: Stärkste Frequenz: {0:5f}".format(f[x-1]))
        plt.show()
        fAna = analyse.mittelwert_stdabw(f)
    return [fAna[0], fAna[1]/np.sqrt(len(f))]

def D0(Darray):
    DAna = analyse.mittelwert_stdabw(Darray)
    return [DAna[0], DAna[1]/np.sqrt(len(Darray))]

#Stange 6
inputfile = "Stäbe/Stab 6/"
L1 = np.array([129.9,(1e-1)/np.sqrt(12),(0.7*1e-1)/np.sqrt(3)])*1e-2         #m
M1 = np.array([1237.1,(1e-1)/np.sqrt(12)])*1e-3         #kg
D1 = np.array(D0(np.array([11.998, 11.998, 11.998, 11.998, 11.998, 11.998, 11.997, 11.997, 11.997, 11.997])*1e-3)) #m
f1 = f0(inputfile)   #Hz
print(inputfile)
print("[Länge,statFehler,sysFehler] in m = "+str(L1))
print("[Masse,Fehler] in kg = "+str(M1))
print("[Durchmesser,Fehler] in m = "+str(D1))
print("[Frequenz,Fehler] in Hz = "+str(f1))
dichte1 = dichte(L1[0], M1[0], D1[0], L1[1], M1[1], D1[1], L1[2])
schallgeschw1 = schallgeschw(f1[0], L1[0], f1[1], L1[1], L1[2])
emod1 = emod(f1[0], L1[0], M1[0], D1[0], f1[1], L1[1], M1[1], D1[1], L1[2])
print("[Dichte, Fehler] = " + str(dichte1))
print("[Schallgeschwindigkeit, statFehler, sysFehler] = " + str(schallgeschw1))
print("[Elastizitätskonstante, statFehler, sysFehler] = " + str(emod1))
print("Die Länge wurde mit Maßband vermessen, Die Masse mit der gestellten Waage, Der Durchmesser wurde über 10 Werte gemittelt, welche dem Messprotokoll entnommen werden können")
print("Die Frequenz wurde aus FourierTransformationen von 5 Messungen gemittelt")
print("Schätzung: Messing")
print("")

#Stange 1
inputfile = "Stäbe/Stab 1/"
L2 = np.array([150,(1e-1)/np.sqrt(12),(0.7*1e-1)/np.sqrt(3)])*1e-2         #m
M2 = np.array([1325.5,(1e-1)/np.sqrt(12)])*1e-3         #kg
D2 = np.array(D0(np.array([12, 12, 11.99, 11.99, 11.99, 11.99, 11.99, 11.99, 11.99, 11.99])*1e-3)) #m
f2 = f0(inputfile)   #Hz
print(inputfile)
print("[Länge,statFehler,sysFehler] in m = "+str(L2))
print("[Masse,Fehler] in kg = "+str(M2))
print("[Durchmesser,Fehler] in m = "+str(D2))
print("[Frequenz,Fehler] in Hz = "+str(f2))
dichte2 = dichte(L2[0], M2[0], D2[0], L2[1], M2[1], D2[1], L2[2])
schallgeschw2 = schallgeschw(f2[0], L2[0], f2[1], L2[1], L2[2])
emod2 = emod(f2[0], L2[0], M2[0], D2[0], f2[1], L2[1], M2[1], D2[1], L2[2])
print("[Dichte, statFehler, sysFehler] = " + str(dichte2))
print("[Schallgeschwindigkeit, statFehler, sysFehler] = " + str(schallgeschw2))
print("[Elastizitätskonstante, statFehler, sysFehler] = " + str(emod2))
print("Es fehlen noch systematische fehler")
print("Die Länge wurde mit Maßband vermessen, Die Masse mit der gestellten Waage, Der Durchmesser wurde über 10 Werte gemittelt, welche dem Messprotokoll entnommen werden können")
print("Die Frequenz wurde aus FourierTransformationen von 5 Messungen gemittelt")
print("Schätzung: Eisen")
print("")

#Stange 12
inputfile = "Stäbe/Stab 12/"
L3 = np.array([129.9,(1e-1)/np.sqrt(12),(0.7*1e-1)/np.sqrt(3)])*1e-2         #m
M3 = np.array([1302.4,(1e-1)/np.sqrt(12)])*1e-3         #kg
D3 = np.array(D0(np.array([11.96,11.96,11.96,11.96,11.96,11.96,11.97,11.97,11.97,12])*1e-3)) #m
f3 = f0(inputfile)   #Hz
print(inputfile)
print("[Länge,statFehler,sysFehler] in m = "+str(L3))
print("[Masse,Fehler] in kg = "+str(M3))
print("[Durchmesser,Fehler] in m = "+str(D3))
print("[Frequenz,Fehler] in Hz = "+str(f3))
dichte3 = dichte(L3[0], M3[0], D3[0], L3[1], M3[1], D3[1], L3[2])
schallgeschw3 = schallgeschw(f3[0], L3[0], f3[1], L3[1], L3[2])
emod3 = emod(f3[0], L3[0], M3[0], D3[0], f3[1], L3[1], M3[1], D3[1], L3[2])
print("[Dichte, statFehler, sysFehler] = " + str(dichte3))
print("[Schallgeschwindigkeit, statFehler, sysFehler] = " + str(schallgeschw3))
print("[Elastizitätskonstante, statFehler, sysFehler] = " + str(emod3))
print("Es fehlen noch systematische fehler")
print("Die Länge wurde mit Maßband vermessen, Die Masse mit der gestellten Waage, Der Durchmesser wurde über 10 Werte gemittelt, welche dem Messprotokoll entnommen werden können")
print("Die Frequenz wurde aus FourierTransformationen von 5 Messungen gemittelt")
print("Schätzung: Kupfer")
print("")