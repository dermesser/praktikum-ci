#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 14:36:49 2020

@author: lbo
"""

import math
import sys

import numpy as np

import praktikum as prak
from praktikum import cassy
from praktikum import analyse

import matplotlib
import matplotlib.ticker
import matplotlib.pyplot as plt

""" To be run in the analyze/ subdirectory."""

matplotlib.rcParams["figure.dpi"] = 300

## ANALYZE "PING" EXPERIMENT

DATA_FILES = ["gerade1_schall.lab", "gerade2_schall.lab", "gerade3_1_schall.lab",
              "gerade3_schall.lab"]
DATA_FILES = ["../raw_data/"+d for d in DATA_FILES]

def cassyinfo():
    for f in DATA_FILES:
        cassy.CassyDaten(f).info()

def merge_individual_measurements(cassy_data, measurement='n'):
    length = sum(cassy_data.messung(i).datenreihe(measurement).werte.size
                 for i in range(1, cassy_data.anzahl_messungen()))
    merged = np.zeros(length)
    off = 0
    for i in range(1, cassy_data.anzahl_messungen()):
        series = cassy_data.messung(i).datenreihe(measurement)
        n = series.werte.size
        merged[off:off+n] = series.werte
        off += n
    return merged

def average_individual_measurements(cassy_files, measurement='n'):
    """Take average of all points in several measurements of multiple files;
    return one point per measurement.

    cassy_files is a list of cassy files, each of which has a number of
        measurements containing one with the `measurement` symbol.
    """
    means = []
    errors = []
    # Process all files and concatenate extracted points.
    for cassy_data in cassy_files:
        for i in range(0, cassy_data.anzahl_messungen()):
            values = cassy_data.messung(i+1).datenreihe(measurement).werte

            sigma_t = np.std(values, ddof=1)
            sigma_mean = sigma_t #/ math.sqrt(values.size)
            print(values, np.mean(values), "sigma", sigma_t, "sigma mean", sigma_mean)
            means.append(np.mean(values))
            errors.append(sigma_mean)
    return (np.array(means), np.array(errors))

def plot_raw_data(cassy_files):
    """Just plot raw t vs R data"""
    (rb1_avg, rb1_err) = average_individual_measurements(cassy_files, 'R_B1')
    (dt_avg, dt_err) = average_individual_measurements(cassy_files, '&Dt_A1')

    fig = plt.figure(dpi=300)
    sp = fig.add_subplot(111)
    sp.grid()
    sp.set_xlabel('t / s')
    sp.set_ylabel('R / kOhm')
    sp.errorbar(dt_avg, rb1_avg, xerr=dt_err, yerr=rb1_err, fmt='.')

def analyze_measurement(cassy_files, r_to_d_const=1, r_to_d_err=0.001):
    """Run all analysis steps including plots, regression, and residual plots."""

    # Process CASSY files into series of averaged steps, one for each measurement.
    (rb1_avg, rb1_err) = average_individual_measurements(cassy_files, 'R_B1')
    # Calculate error on d (distance) from errors on R_B1. The error on
    # r_to_d_const is systematic and comes in below using the shift method.
    dist_err = (r_to_d_const * rb1_err)
    dist_avg = r_to_d_const * rb1_avg
    (dt_avg, dt_err) = average_individual_measurements(cassy_files, '&Dt_A1')

    # errors cannot be zero, otherwise analyse.linear_regression barfs!
    dist_err += 1e-9
    dt_err += 1e-9

    # PLOT data first (dist/time)
    fig = plt.figure(dpi=300)
    sp = fig.add_subplot(111)
    sp.errorbar(dt_avg, dist_avg, xerr=dt_err, yerr=dist_err, fmt='.')
    sp.set_xlabel('dt / sec')
    sp.set_ylabel('s / m')
    sp.set_xlim(0, 0.002)
    sp.set_ylim(0, .4)

    # ANALYZE: "Verschiebemethode", shift values using sigma_a (r_to_d_err) as systematic error
    # Assume tape measure to be accurate
    dist_plus = (rb1_avg+rb1_err) * r_to_d_const
    dist_minus = (rb1_avg-rb1_err) * r_to_d_const
    dist_plus_params = analyse.lineare_regression_xy(dt_avg, dist_plus, dt_err, dist_err)
    dist_minus_params = analyse.lineare_regression_xy(dt_avg, dist_minus, dt_err, dist_err)
    sigma_v_sys = abs((dist_plus_params[0]-dist_minus_params[0]))/2

    # ANALYZE: linear regression with errors in both variables.
    (a, ea, b, eb, chiq, corr) = analyse.lineare_regression_xy(dt_avg, dist_avg, dt_err, dist_err)
    print("raw linreg parameters:", a, ea, b, eb, chiq, corr)
    print("data points:", dist_avg.size)
    print('Sound velocity:', a, '+/-', ea, '(stat) +/-', sigma_v_sys, '(sys) X^2/(n-ddof)', chiq/(rb1_avg.size-2))
    # PLOT: Draw linear regression
    reg_x = np.array([0, 1])
    reg_y = np.array([b, b + a])

    # PLOT: linear regression
    sp.plot(reg_x, reg_y)
    sp.grid()
    sp.ticklabel_format(axis='x', style='sci', scilimits=(0,0))

    # For residual plot:
    def linreg(x):
        return a*x + b

    # PLOT: Residuals
    resid_x = dt_avg
    resid_y = np.vectorize(linreg)(resid_x) - dist_avg
    fig = plt.figure(dpi=300)
    rpl = fig.add_subplot(111)
    rpl.set_ylim(np.min(resid_y)-np.max(dist_err), np.max(resid_y)+np.max(dist_err))
    rpl.set_xlim(0, 0.002)
    rpl.grid()
    rpl.ticklabel_format(axis='both', style='sci', scilimits=(0,0))
    rpl.set_xlabel('t / s')
    rpl.set_ylabel('(s - (a t + b)) / m')
    rpl.errorbar(resid_x, resid_y, xerr=dt_err, yerr=dist_err, fmt='o')

    return None

def analyze_resistance_distance(plot=True):
    """Analyze calibration data for potentiometer"""
    # y = d
    y = np.array([0.11, 0.14, 0.17, 0.2, 0.23, 0.26, 0.29])
    y_ = y - np.mean(y) + 1e-6
    # Read error on measuring tape
    sy = np.ones(y.size) * 0.001
    # x = R
    x = np.array([0.320, 0.499, 0.680, 0.866, 1.058, 1.242, 1.425])
    x_ = x - np.mean(x) + 1e-6
    sx = np.ones(x.size) * (3/4096.)/math.sqrt(12)
    params = analyse.lineare_regression_xy(x_, y_, sx, sy)
    (a, _, b, _, _, _) = params
    print("resistance [a, ea, b, eb, chiq, corr]:", params)

    if plot:
        fig1 = plt.figure(dpi=300)
        fig1 = fig1.add_subplot(111)
        fig1.grid(True)
        fig1.set_xlabel('R / kOhm')
        fig1.set_ylabel('s / m')
        fig1.set_xlim(-.6, .6)
        fig1.set_ylim(-0.1, 0.1)
        fig1.errorbar(x_, y_, fmt='.', xerr=sx, yerr=sy)
        print(x[-1])
        fig1.plot(np.array([x_[0], x_[-1]]), np.array([b+x_[0]*a, b+x_[-1]*a]), color='red')

        #total_err = np.sqrt((y * sx)**2 + (x * sy)**2)
        # "Hinweise zur Datenverarbeitung":
        total_err = np.sqrt(sy**2 + (a * sx)**2)

        fig2 = plt.figure(dpi=300)
        fig2 = fig2.add_subplot(111)
        fig2.grid(True)
        fig2.set_xlim(-.6, .6)
        fig2.set_ylim(-0.002, 0.002)
        fig2.set_xlabel('R / kOhm')
        fig2.set_ylabel('(d - (a R + b)) / m')
        resids = (a*x_+b) - y_
        fig2.errorbar(x_, resids, xerr=sx, yerr=total_err, fmt='.')

    return params


(dist_a, dist_ea, _, _, dist_chiq, _) = analyze_resistance_distance(plot=False)

data1 = cassy.CassyDaten(DATA_FILES[3])
data2 = cassy.CassyDaten(DATA_FILES[1])
data3 = cassy.CassyDaten(DATA_FILES[0])
analyze_measurement([data1, data2, data3], r_to_d_const=dist_a, r_to_d_err=dist_ea)