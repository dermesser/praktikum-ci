#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 08:15:06 2020

@author: lbo
"""

import math
import sys

import numpy as np

import praktikum as prak
from praktikum import cassy
from praktikum import analyse

import matplotlib
import matplotlib.ticker
import matplotlib.pyplot as plt

CASSY_FILE = '../raw_data/resonanz_alle.lab'

def cassyinfo():
    cassy.CassyDaten(CASSY_FILE).info()

def analyze_peaks(measure, voltsym='U_A1', freqsym='f_B1'):
    """Takes a CASSY Messung containing Voltage and Frequency."""
    voltage = measure.datenreihe(voltsym).werte
    freq = measure.datenreihe(freqsym).werte

    # PLOT raw data
    fig = plt.figure(dpi=300, figsize=(12,5))
    sp = fig.add_subplot(111)
    sp.set_xlabel('f / Hz')
    sp.set_ylabel('U / V')
    sp.grid(True)
    sp.scatter(freq, voltage, marker='.')

    # PLOT individual peaks
    fig = plt.figure(dpi=300, figsize=(10, 20))
    peakranges = [(395,420), (800,815), (1200,1215), (1600,1615), (2000,2015), (2395, 2420)]
    i = 1
    for (xmin, xmax) in peakranges:
        sp = fig.add_subplot(len(peakranges), 2, i)
        sp.scatter(freq, voltage)
        sp.set_xlabel('f{} / Hz'.format(i-1))
        sp.set_ylabel('U / V')
        sp.set_xlim(xmin, xmax)
        #sp.ticklabel_format(axis='x', style='sci', scilimits=(4,0))
        sp.grid(True)
        i += 1

    return (voltage,freq)

def peak_regression():
    peaks = np.array([407, 805, 1207, 1607, 2007, 2407])
    errors = np.array([1] * len(peaks))
    resonance = np.array([1, 2, 3, 4, 5, 6])

    (a, ea, b, eb, chiq, corr) = analyse.lineare_regression(resonance, peaks, errors)
    print('a, ea, b, eb, chiq, corr:', a, ea, b, eb, chiq, corr)

    # Plot data and regression
    fig = plt.figure(dpi=300, figsize=(8,12))
    sp = fig.add_subplot(211)
    sp.set_xlabel('n')
    sp.set_ylabel('f / Hz')
    sp.grid(True)
    sp.errorbar(resonance, peaks, yerr=errors, fmt='.')
    sp.plot([0, resonance[-1]], [b, b+resonance[-1]*a], 'r-')

    # Shift method
    peaks_plus = peaks + errors
    peaks_minus = peaks - errors
    fit_plus = analyse.lineare_regression(resonance, peaks_plus, errors)
    fit_minus = analyse.lineare_regression(resonance, peaks_minus, errors)
    print(fit_plus)
    print(fit_minus)
    sigma_a_sys = (fit_plus[0]-fit_minus[0])/2
    print('sigma a (sys):', sigma_a_sys)

    resids = peaks - (a * resonance + b)
    total_err = errors

    sp2 = fig.add_subplot(212)
    sp2.set_xlabel('n')
    sp2.set_ylabel('(f_n - (a n + b)) / Hz')
    sp2.grid(True)
    sp2.errorbar(resonance, resids, yerr=total_err, fmt='.')

peak_regression()
voltage, freq = analyze_peaks(cassy.CassyDaten(CASSY_FILE).messung(1))