#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 17:10:58 2020

@author: lbo
"""

import math
import sys

import numpy as np

import praktikum as prak
from praktikum import cassy
from praktikum import analyse

import matplotlib
import matplotlib.ticker
import matplotlib.pyplot as plt
from scipy import optimize

CASSY_FILES = ["../raw_data/knoten_lewin.lab",
               "../raw_data/knoten_norman.lab",
               "../raw_data/knoten_norman2.lab",
               "../raw_data//knoten_norman3.lab"]


def cassyinfo():
    for f in CASSY_FILES:
        cassy.CassyDaten(f).info()


def plot_raw_data(cassy_files, center=False):
    fig = plt.figure(dpi=150, figsize=(20, 15))
    for (i,f) in enumerate(cassy_files):
        p = fig.add_subplot(2,2,i+1)
        p.set_xlabel('R / kOhm')
        p.set_ylabel('U / V')
        p.set_title('Messung {}'.format(i+1))
        p.grid(True)

        y = f.messung(1).datenreihe('U_A1').werte
        x = f.messung(1).datenreihe('R_B1').werte
        if center:
            y = y - np.mean(y)
        p.scatter(x, y, marker='.')

# not fitting a quadratic regression due to poor data quality.
# instead, show peaks in detail

def find_range(series, start, end):
    """Find start/end indices for series with bounds start/end."""
    startix = -1
    endix = -1
    for (i, v) in enumerate(series):
        if v > start and startix < 0:
            startix = i-1
        if v > end and endix < 0:
            endix = i+1
        if startix > 0 and endix > 0:
            break
    return (startix, endix)

def plot_details(cassy_file, ranges):
    x = cassy_file.messung(1).datenreihe('R_B1').werte
    y = cassy_file.messung(1).datenreihe('U_A1').werte

    for r in ranges:
        (x_start, x_end) = find_range(x, r[0], r[1])
        fig = plt.figure(dpi=300)
        sp = fig.add_subplot(111)
        sp.grid(True)
        sp.set_xlabel('R / kOhm')
        sp.set_ylabel('U / V')
        sp.set_xlim(x[x_start], x[x_end])
        sp.set_ylim(-0.125+np.min(y[x_start:x_end]), 0.125+np.max(y[x_start:x_end]))
        sp.scatter(x, y, marker='.')

def peak_polynomic(cassy_file, x_from, x_to):
    x = cassy_file.messung(1).datenreihe('R_B1').werte
    y = cassy_file.messung(1).datenreihe('U_A1').werte
    x_start, x_end = find_range(x, x_from, x_to)

    coeffs = np.polyfit(x[x_start:x_end], y[x_start:x_end], 4)

    fity = coeffs[0] * x**4 + coeffs[1] * x**3 + coeffs[2] * x**2 + coeffs[3] * x + coeffs[4]
    fig = plt.figure(dpi=300)
    sp = fig.add_subplot(111)
    sp.set_xlim(x_from, x_to)
    sp.set_ylim(-0.2+np.min(fity[x_start:x_end]), 0.1+np.max(fity[x_start:x_end]))
    sp.scatter(x, y, marker='.')
    sp.scatter(x, fity, marker='.', color='red')

    print(coeffs)

def peak_sin_fit(cassy_file, x_from, x_to):
    x = cassy_file.messung(1).datenreihe('R_B1').werte
    y = cassy_file.messung(1).datenreihe('U_A1').werte
    centered = y - np.mean(y)
    x_start, x_end = find_range(x, x_from, x_to)

    # http://scipy-lectures.org/intro/scipy/auto_examples/plot_curve_fit.html
    def fit_sin(x, a, b, c):
        return a * np.sin(x * b + c)
    params, cov = optimize.curve_fit(fit_sin, x[x_start:x_end], centered[x_start:x_end], p0=[0.5,2*math.pi/0.4, 0.1])
    print(params, cov)

    fig = plt.figure(dpi=300)
    p = fig.add_subplot(111)
    p.set_xlabel('R / kOhm')
    p.set_ylabel('U / V')
    p.grid(True)

    p.scatter(x[x_start:x_end], centered[x_start:x_end], marker='.')
    p.scatter(x[x_start:x_end], params[0] * np.sin(x[x_start:x_end] * params[1] + params[2]), marker='.', color='red')

def analyze_peak_regression():
    # The following data are the peak locations, averaged across the three measurements.
    n_series6 = np.array([1,2,3,4,5,6])
    n_series6_err = 1e-8 * np.ones(n_series6.shape)
    r_series6 = np.array([[3.015,2.99,3.0],
                          [3.25,3.25,3.24],
                          [3.465,3.45,3.455],
                          [3.68,3.675,3.7],
                          [3.91,3.88,3.89],
                          [4.13,4.12,4.13]])
    r_series6_avg = np.mean(r_series6, 1)
    r_series6_err = np.std(r_series6, axis=1, ddof=1)

    # The following data are the peak locations, not averaged, for one regression
    # Prepare read-out data for regression (see table in protocol)
    n_series = np.array([1, 2, 3, 4, 5, 6] * 3)
    r_series = np.array([3.015, 3.25, 3.465, 3.68, 3.91, 4.13,
                         2.99, 3.25, 3.45, 3.675, 3.88, 4.12,
                         3.0, 3.24, 3.455, 3.7, 3.89, 4.13])
    r_err = np.array(0.01 * np.ones(r_series.shape))
    n_err = np.array(1e-8 * np.ones(n_series.shape))

    assert n_series.size == r_series.size

    # Constants for converting resistance to distance
    r_to_d_const = 0.1622
    r_to_d_err = 1.022e-3

    def analyze(n_series, r_series, n_err, r_err, title='Regression'):
        d_series = r_series * r_to_d_const
        d_err = r_err * r_to_d_const
        parameters = analyse.lineare_regression_xy(n_series, d_series, n_err, d_err)
        print('lin fit: a, ea, b, eb, chisq, corr:', parameters)

        # Verschiebemethode
        d_plus = (r_series) * (r_to_d_const + r_to_d_err)
        d_minus = (r_series) * (r_to_d_const - r_to_d_err)

        reg_plus = analyse.lineare_regression_xy(n_series, d_plus, n_err, d_err)
        reg_minus = analyse.lineare_regression_xy(n_series, d_minus, n_err, d_err)
        sigma_v = (reg_plus[0]-reg_minus[0])/2
        print('sigma (sys.) on v:', sigma_v)


        fig = plt.figure(dpi=300, figsize=(8,8))
        sp = fig.add_subplot(211)
        sp.set_xlabel('n')
        sp.set_ylabel('d / m')
        sp.set_xlim(0,7)
        sp.set_ylim(0.4, 0.7)
        sp.set_title(title)
        sp.grid(True)
        sp.errorbar(n_series, d_series, yerr=d_err, fmt='.')
        sp.plot(n_series, parameters[0]*n_series + parameters[2], 'r-')

        sp2 = fig.add_subplot(212)
        sp2.set_xlabel('n')
        sp2.set_ylabel('(d - (a*n+b)) / m')
        sp2.grid(True)
        sp2.set_ylim(-.005, .005)
        sp2.set_xlim(0,7)
        sp2.errorbar(n_series, d_series - (parameters[0] * n_series + parameters[2]), yerr=d_err, fmt='o')

    analyze(n_series, r_series, n_err, r_err, title='Regression across original points')
    analyze(n_series6, r_series6_avg, n_series6_err, r_series6_err, title='Pre-regression averaged')


data_files = [cassy.CassyDaten(f) for f in CASSY_FILES]

# Plot local peaks for reading extremal points:
#plot_details(data_files[0], [(2.95, 3.1), (3.15, 3.35), (3.4, 3.5), (3.6, 3.7), (3.9, 4),                             (4.1, 4.2)])
#plot_details(data_files[2], [(2.9,3.1), (3.1,3.3), (3.3,3.5), (3.6,3.75), (3.8,3.95), (4,4.2)])
#plot_details(data_files[3], [(2.9,3.1), (3.1,3.3), (3.3,3.5), (3.5,3.9), (3.8,3.95), (4,4.2)])