# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 14:59:22 2020

@author: Max_b
"""

from praktikum import analyse
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['font.size'] = 24.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0


def niceRegressionPlots(x, y, yerror, xerror=np.zeros(1), xlabel='', ylabel='', ylabelresidue=''):
    """Does a linear Regression with x (and y errors).
    x = array with x-coordinates,
    y = array with y-coordinates,
    xerror = array with errors of x,
    yerror = array with errors of y,
    xlabel = name on x-axis,
    ylabel = name on y-axis,
    ylabelresidue = name on residue """
    
    fig, axarray = plt.subplots(2, 1, figsize=(20,10), sharex=True, gridspec_kw={'height_ratios': [5, 2]})
    axarray[1].grid(True)
    axarray[0].grid(True)
    if(xerror.all() != 0):
        axarray[0].errorbar(x, y, xerr=xerror, yerr=yerror, color='red', fmt='.', marker='o', markeredgecolor='red')
    else:
        axarray[0].errorbar(x, y, yerr=yerror, color='red', fmt='.', marker='o', markeredgecolor='red')
    axarray[0].set_xlabel(xlabel)
    axarray[0].set_ylabel(ylabel)
    if(xerror.all() != 0):
        m,em,b,eb,chiq,corr = analyse.lineare_regression_xy(x, y, xerror, yerror)
    else:
        m,em,b,eb,chiq,corr = analyse.lineare_regression(x, y, yerror)
    print('m = (%g +- %g),   b = (%g +- %g) ,  chi2/dof = %g / %g  corr = %g' % (m, em, b, eb, chiq, len(x)-2, corr))
    axarray[0].plot(x, m*x+b, color='green')
    sigmaRes = np.sqrt((m*xerror)**2 + yerror**2)
    axarray[1].axhline(y=0., color='black', linestyle='--')
    axarray[1].errorbar(x, y-(m*x+b), yerr=sigmaRes, color='red', fmt='.', marker='o', markeredgecolor='red')
    axarray[1].set_xlabel(xlabel)
    axarray[1].set_ylabel(ylabelresidue)
    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)
    ymax = max([abs(x) for x in axarray[1].get_ylim()])
    axarray[1].set_ylim(-ymax, ymax)
    plt.tight_layout()
    fig.subplots_adjust(hspace=0.0)
    plt.show()