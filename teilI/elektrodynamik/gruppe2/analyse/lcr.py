#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 12:12:55 2020

@author: lbo
"""

import os
from os import path
import re

import bokeh
from bokeh import plotting
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as sig

from praktikum import cassy
from praktikum import analyse

def KOPPLUNG_DATA_FILES():
    prefix = '../data/kopplung'
    fs = sorted(os.listdir(prefix))
    return [path.join(prefix, fn) for fn in filter(lambda f: f.endswith('lab'), fs)]

def SCHWEBUNG_DATA_FILES():
    prefix = '../data/schwebung'
    fs = sorted(os.listdir(prefix))
    return [path.join(prefix, fn) for fn in filter(lambda f: f.endswith('lab'), fs)]

def file_plot_title(filename: str):
    filename = path.basename(filename)
    if filename.startswith('Kopplung'):
        filename = filename.replace('.lab', '')
        return '_'.join(filename.split('_')[1:3])
    elif filename.startswith('Schwebung'):
        filename = filename.replace('Schwebung', '')
        filename = filename.replace('.lab', '')
        return filename
    raise Exception("Unknown filename pattern {}", filename)


plt.rcParams['font.size'] = 24.0
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Arial'
plt.rcParams['font.weight'] = 'bold'
plt.rcParams['axes.labelsize'] = 'medium'
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.linewidth'] = 1.2
plt.rcParams['lines.linewidth'] = 2.0

def cassy_info(f):
    cassy.CassyDaten(f).info()

def cassy_data(f):
    return cassy.CassyDaten(f).messung(1)

def plot_data_bokeh(f, chan1='U_B1', chan2=None, frac=1/4):
    plotting.output_file('plot.html')
    fig = plotting.figure(plot_width=800, plot_height=400)
    data = cassy_data(f)
    t = data.datenreihe('t').werte
    U = data.datenreihe(chan1).werte
    npts = t.size
    assert t.size == U.size
    t = t[0:int(frac*npts)]
    U = U[0:int(frac*npts)]
    fig.line(t, U)

    plotting.show(fig)

def plot_data_subplots(files, ixs=[0,1,2,3,4,5,6], chan1='U_B1', chan2=None, frac=1/4):
    fig = plt.figure(figsize=(20, 30), tight_layout=True)
    n = len(ixs)
    first = None
    for i, ix in enumerate(ixs):
        if not first:
            first = fig.add_subplot(n,1,i+1)
            p = first
        else:
            p = fig.add_subplot(n, 1, i+1)
        if i+1 == len(ixs):
            p.set_xlabel('t / ms')
            p.set_ylabel('U / V')
        else:
            p.set_xticks([])
            pass
        data = cassy_data(files[ix])
        t = data.datenreihe('t').werte
        U = data.datenreihe(chan1).werte

        npts = t.size
        t = t[0:int(frac*npts)] * 1e3
        U = U[0:int(frac*npts)]
        p.plot(t, U, label=file_plot_title(files[ix]) + ' U_1')
        if chan2:
            U2 = data.datenreihe(chan2).werte
            U2 = U2[0:int(frac*npts)]
            p.plot(t, U2, label=file_plot_title(files[ix]) + ' U_2')
        p.legend()
    plt.show()

def fourier_analysis(t, U, fast=True, title=None, label=None,
                     axes=None, bokeh=False):
    npts = min(t.size, U.size)
    t = t[0:npts]
    U = U[0:npts]
    if fast:
        freq, amp = analyse.fourier_fft(t, U)
    else:
        freq, amp = analyse.fourier(t, U)
    freq_ = freq[32:224]
    amp_ = amp[32:224]

    # approx_max = analyse.peak(freq, amp, 500, 1500)
    # print('Approx. freq maximum:', approx_max)
    peaks, props = sig.find_peaks(amp_, prominence=2)
    print(freq_[peaks], props)

    if not bokeh:
        if not axes:
            fig = plt.figure(figsize=(15,10))
            p = fig.add_subplot(111)
        else:
            p = axes
        p.grid(True)
        p.plot(freq_, amp_, '-o', label=label or 'FT')
        p.set_xlabel('f / Hz')
        p.set_ylabel('Intensität')
        if title:
            p.set_title(title)
        if label:
            p.legend()
    else:
        plotting.output_file('fft.html')
        fig = plotting.figure(plot_width=800, plot_height=400,
                              x_axis_label='f / Hz', y_axis_label='Intensität')
        fig.circle(freq_, amp_,line_color='blue')
        plotting.show(fig)


# [plot_data(SCHWEBUNG_DATA[i]) for i in range(len(SCHWEBUNG_DATA))]
# plot_data_subplots(SCHWEBUNG_DATA, range(0, len(SCHWEBUNG_DATA)))
# plot_data_subplots(KOPPLUNG_DATA_FILES(), range(0, len(KOPPLUNG_DATA_FILES())), chan2='U_B2')
# plot_data_subplots(SCHWEBUNG_DATA_FILES(), range(0, len(SCHWEBUNG_DATA_FILES())))

def analyze_all_fourier():
    print('==== SCHWEBUNG ====')
    for i in [5, 1, 2, 3, 4, 6]:
        f = SCHWEBUNG_DATA_FILES()[i]
        data = cassy_data(f)
        t = data.datenreihe('t').werte
        U = data.datenreihe('U_B1').werte
        fourier_analysis(t, U, title=file_plot_title(f), bokeh=False)
    print('==== KOPPLUNG ====')
    for i in [0,1,2,3,4,5,6]:
        f = KOPPLUNG_DATA_FILES()[i]
        print('==', file_plot_title(f), '==')
        data = cassy_data(f)
        t = data.datenreihe('t').werte
        U = data.datenreihe('U_B1').werte

        fig = plt.figure(figsize=(15,10), tight_layout=True)
        b1 = fig.add_subplot(1,1,1)
        print('U_B1')
        fourier_analysis(t, U, axes=b1, title=file_plot_title(f), label='U_1')
        print('U_B2')
        U = data.datenreihe('U_B2').werte
        fourier_analysis(t, U, axes=b1, title=file_plot_title(f), label='U_2')
        b1.legend()

analyze_all_fourier()