#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 15:53:52 2020

@author: lbo
"""

import nice_plots
from praktikum import analyse, cassy

import numpy as np
import matplotlib.pyplot as plt

from collections import namedtuple
import math

# Run in `analyse` directory.

DATA_FILE = '../data/widerstand/1000ohmwiderstand1.lab'

def cassy_info(f=DATA_FILE):
    cassy.CassyDaten(f).info()

# https://docs.python.org/3/library/collections.html#namedtuple-factory-function-for-tuples-with-named-fields
ResistanceMeasurement = namedtuple('ResistanceMeasurement', ['U', 'sU', 'I', 'sI', 'n'])

# TODO: check preset values
def preprocess_raw_data(f=DATA_FILE, digi_error_u=10./2048, digi_error_i=0.3/2048):
    """From raw CASSY data, calculate averaged values and statistical errors.

    Returns array of ResistanceData.
    """
    data = cassy.CassyDaten(f)
    processed = []
    for i in range(1, data.anzahl_messungen()+1):
        messung = data.messung(i)
        U_values = messung.datenreihe('U_B1').werte
        I_values = messung.datenreihe('I_A1').werte
        assert U_values.shape == I_values.shape
        U = np.mean(U_values)
        I = np.mean(I_values)
        sU = max(digi_error_u, np.std(U_values, ddof=1))
        sI = max(digi_error_i, np.std(I_values, ddof=1))
        n = U_values.size

        # Average statistical error:
        # np.mean(extract_column(rd, 1))

        processed.append(ResistanceMeasurement(U, sU, I, sI, n))
    return processed

def extract_column(tuples, index):
    """Return index'th (int) entries from a series of tuples."""
    return np.array([t[index] for t in tuples], dtype=np.float32)

def plot_raw_resist_all(f=DATA_FILE):
    fig = plt.figure(figsize=(20, 10))
    p = fig.add_subplot(111)
    p.grid(True)
    p.set_xlabel('I / mA')
    p.set_ylabel('U / V')

    data = cassy.CassyDaten(f)
    Us = []
    Is = []
    for i in range(1, data.anzahl_messungen()+1):
        messung = data.messung(i)
        U_values = messung.datenreihe('U_B1').werte
        I_values = messung.datenreihe('I_A1').werte
        assert U_values.shape == I_values.shape
        I_values = I_values * 1e3
        for u in U_values:
            Us.append(u)
        for i in I_values:
            Is.append(i)

    p.scatter(np.array(Is), np.array(Us), marker='.')

def plot_raw_resist(resistance_data):
    U = extract_column(resistance_data, 0)
    sU = extract_column(resistance_data, 1)
    I = extract_column(resistance_data, 2)
    sI = extract_column(resistance_data, 3)

    print(sI, sU)
    fig = plt.figure(figsize=(20,10))
    p = fig.add_subplot(111)
    p.grid(True)
    p.errorbar(I, U, xerr=sI, yerr=sU, fmt='.')

def calculate_regression(resistance_data):
    """Calculate and plot linear regression. Expects output from
    preprocess_raw_data()."""
    U = extract_column(resistance_data, 0)
    sU = extract_column(resistance_data, 1)
    I = extract_column(resistance_data, 2)
    sI = extract_column(resistance_data, 3)

    m, em, b, eb, chiq, corr = nice_plots.nice_regression_plot(
        I, U, xerror=sI, yerror=sU, xlabel='I / A',ylabel='U / V',
        ylabelresidue='(U_i - (m I + b)) / V')

    # Centered data
    U_ctr = U - np.mean(U)
    I_ctr = I - np.mean(I)
    param_ctr = analyse.lineare_regression_xy(I_ctr, U_ctr, sI, sU)
    print('Parameters for centered regression:', param_ctr)

    # Shift method. (without measurement-bounds error)
    sU_sys = 0.01*U / math.sqrt(3)
    sI_sys = 0.02*I / math.sqrt(3)

    # In U
    U_regr_plus = analyse.lineare_regression_xy(I, U+sU_sys, sI, sU)
    U_regr_minus = analyse.lineare_regression_xy(I, U-sU_sys, sI, sU)
    R_from_U_sigma = (abs(U_regr_plus[0]-m)+abs(U_regr_minus[0]-m))/2

    # In I
    I_regr_plus = analyse.lineare_regression_xy(I+sI_sys, U, sI, sU)
    I_regr_minus = analyse.lineare_regression_xy(I-sI_sys, U, sI, sU)
    R_from_I_sigma = (abs(I_regr_plus[0]-m)+abs(I_regr_minus[0]-m))/2

    # Add quadratically
    R_sys_total = math.sqrt(R_from_U_sigma**2 + R_from_I_sigma**2)

    print('systematic R errors: (U-shift) {:.5f} (I-shift) {:.5f}'.format(
        R_from_U_sigma, R_from_I_sigma))
    print('R = {:.3f} +/- {:.3f} (stat) +/- {:.3f} (sys)'.format(
        m, em, R_sys_total))
