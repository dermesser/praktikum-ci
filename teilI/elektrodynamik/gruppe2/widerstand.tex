\section{Charakterisierung: Widerstand}
\label{sec:widerstaende}

\subsection{Versuchsbeschreibung}

\subsubsection{Ziel}

In diesem Versuch werden wir den zur Charakterisierung des Kondensators verwendeten
Widerstand und dessen vom Hersteller angegebene Unsicherheit Prüfen.

\subsubsection{Grundlagen}

Wir messen den Strom im Aufbau, sowie die Spannung $U_R$ welche am Widerstand abfällt. Da
keine anderen Bauteile vorhanden sind, muss die gesamte Spannung $U_0$ der
Spannungsquelle am Widerstand abfallen, somit nutzen wir in diesem Versuch folgende
Notation:

\[
U = U_0 = U_R
\]

In unserem Versuchsaufbau gilt das Ohmsche Gesetz:

\begin{equation}
\label{eq:ohm}
U = I \cdot R
\end{equation}

Wir erkennen die Form einer linearen Gleichung. Wenn wir also in einem Versuch die
Spannung gegen die Stromstärke auftragen, erwarten wir, dass $R = U / I$ die
resultierende Steigung ist.

\subsection{Versuchsaufbau}

Wir nutzen ein Sensor CASSY als Messgerät und Spannungsquelle. Die Spannungsquelle wird
in Reihe mit dem Strommesser des Sensor CASSY und dem zu messenden Widerstand geschaltet.
Parallel zu dem Widerstand ist der Spannungsmesser angeschlossen. Die Spannungsquelle
wurde während des Aufbaus ausgeschaltet und ist am besten selbst nicht mit Strom
versorgt, um Unfällen oder Kurzschlüssen vorzubeugen. Das Sensor-CASSY war mit einem
Rechner verbunden, der die Software CASSY LAB installiert hatte.

\begin{figure}[H]
\centering

\begin{subfigure}{0.49\textwidth}
\begin{circuitikz}[european resistors]
\draw (0,0) to[battery1, l=$U_0$] (0,3) to[ammeter, l=I] (2,3) to[R, l=R] (2,0) -- (0,0);
\draw (2,3) to[voltmeter, l=$U_R$] (4,3) -- (4,0) -- (2,0);
\end{circuitikz}
\caption{Schaltkreis}
\end{subfigure}
\begin{subfigure}{0.49\textwidth}
\centering
\includegraphics[scale=0.3]{img/widerstand_aufbau.jpeg}
\caption{Versuchsaufbau mit \SI{1}{\kilo\ohm}}
\label{fig:widerstandaufbau}
\end{subfigure}
\caption{Aufbau: Widerstandsmessung}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{l|l}
Gerät & Anzahl\\
\hline
\rule{0pt}{2.5ex} Sensor CASSY & 1\\
\SI{1}{\kilo\ohm} Widerstand & 1\\
Kabel rot/blau & 4\\
\end{tabular}
\caption{Verwendete Gegenstände}
\end{table}

\subsection{Versuchsdurchführung}

Der zu charakterisierende Widerstand war im Kondensator-Versuch
(\autoref{sec:kondensator}) möglichst groß gewählt worden, um eine möglichst langsame
auf- und Entladung erkennen zu können. Es wurde dort ein Messintervall von
\SI{10}{\mu\second} verwendet. Daher nutzten wir bei der Charakterisierung des
Widerstandes wieder eine Messrate von \SI{10}{\mu\second}, diesmal bei Einzelmessungen
von jeweils 125 Messpunkten. Wir stellten eine Gleichspannung an der Spannungsquelle ein,
und maßen die 125 Punkte. Dann erhöhten wir die Spannung um kleine Schritte von circa
\SI{0,5}{\volt} und wiederholten die 125 Messungen. Dies wiederholten wir, bis wir knapp
unter dem Maximum unseres Messbereichs von \SI{10}{\volt} waren. Verlustfaktoren, wie in
Kapitel 4.5 der Versuchsanleitung beschrieben, treten in diesem Versuch nicht auf, da es
sich um Gleichstrom handelt.

\begin{table}[H]
\centering
\begin{tabular}{l|l}
Messparameter & Einstellung\\
\hline
\rule{0pt}{2.5ex} Messbereich Spannung & $\pm$ \SI{10}{\volt}\\
Messbereich Strom & $\pm$ \SI{0.3}{\ampere}\\
Messintervall & \SI{10}{\mu\second}\\
Messpunkte & \num{16000}\\
Kanal Strom & $\mathrm{I_{A1}}$\\
Kanal Spannung & $\mathrm{U_{B1}}$
\end{tabular}
\caption{Messparameter}
\end{table}

\subsection{Auswertung}

\subsubsection{Rohdaten}

   \begin{figure}[H]
 \centering
 \includegraphics[scale=0.4]{img/widerstand_rohdaten.png}
  \caption{Rohdaten der Widerstandsmessung}
 \label{fig:widerstandroh}
\end{figure}

Als wir die Rohdaten betrachteten fiel uns auf, dass, trotz unserer Einstellung von 125
Messungen, 126 Mesungen durchgeführt worden sind. Dies könnte von einem "`off-by-one
error"' in der Software stammen, wie es in der Informatik bei Listen und Arrays üblich
ist. Dort wird nämlich der nullte Eintrag, ebenfalls als ein weiterer Eintrag gezählt. Für
unsere Auswertung ist der weitere Messwert jedoch nicht hinderlich. Diese Eigenheit
sollte jedoch bei Algorithmen, die auf den Messwerten des Sensor CASSY
beruhen, berücksichtigt werden.

Weiterhin ist für jeden Messpunkt eine relativ große horizontale Streuung zu beobachten.
Diese rührt daher, dass bei unserem großen Widerstand nur ein kleiner Strom floß. Das
Digitalisierungsintervall beträgt mit unseren Einstellungen bereits
$\SI{0.146}{\milli\ampere}$, und kleine Schwankungen dieses geringen Stroms manifestieren
sich dann schon in einer relativ großen Verteilung der Messungen (normalerweise etwa
drei unterschiedliche Werte, in einem dem Digitalisierungsintervall entsprechenden
Abstand). Wir hätten den Messbereich auf $\SI{0.1}{\ampere}$ einschränken sollen, um hier
eine bessere Messung zu erhalten.

\subsubsection{Modell}

Als Modell haben wir eine lineare Gleichung angenommen. Augenscheinlich scheint diese
Vermutung mit den Werten übereinzustimmen. Also führen wir eine lineare Regression durch.

\[
U = a \cdot I + b
\]

Wobei wir aufgrund von \autoref{eq:ohm} einen y-Achsenabschnitt b von 0 erwarten würden.
Aus einer linearen Regression erhalten wir $\chisq{0.65648}{18}{0.037}$.

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{img/widerstand_regression.png}
\caption{Lineare Regression}
\label{fig:widerstandlinreg}
\end{figure}

Wobei unsere Steigung \[a = \SI{991.587(10939)e0}{\volt\per\ampere}\] beträgt, und unser
y-Achsenabschnitt \[b = \SI{0.374(61)e0}{\volt}\] Die Kovarianz dieser beiden Werte
beträgt \[\rho_{ab} = \num{-0.848e0}\]

Man sieht, dass die Fehlerbalken der gemittelten Werte aufgrund der Umrechnung der großen
Messunsicherheit des Stroms deutlich zu groß geraten sind. Diese Umrechnung geschieht
wie gewohnt mit

\[
\sigma_{U,resid} = \sqrt{\sigma_U^2 + (a \sigma_I)^2}
\]

Das spiegelt sich auch im $\chi^2/\mathrm{ndof}$-Wert wieder, der deutlich zu klein ist.
Ein kleinerer Messbereich mit geringerem Digitalisierungsintervall hätte unsere Messung
deutlich verbessert.

\subsubsection{Analyse}

Um unsere Steigung $a$ zu erhalten, haben wir eine lineare Regression durchgeführt. Für
die einzelnen Messpunkte der linearen Regression stehen uns mehrere Messwerte zur
Verfügung. So haben wir für jede eingestellte Spannung 126 Messwerte aufgezeichnet. Aus
diesen bestimmen wir das gewichtete Mittel, sowie die Unsicherheit des Mittelwertes. Nun
wird mit diesen gemittelten Werten als Stützpunkten eine lineare Regression durchgeführt.

Die Unsicherheit des Mittelwertes ist kleiner als die Digitalisierung, wurde also durch
den Digitalisierungsunsichereit ersetzt ($\sigma = \max(\sigma_{\overline x},
\sigma_{digi})$). Sie betrugen für unsere Messeinstellungen $\sigma_{I} =
\frac{\SI{0.3}{\ampere}}{2048} = \SI{1.465e-4}{\ampere}$ und $\sigma_U =
\frac{\SI{10}{\volt}}{2048} = \SI{4.883e-3}{\volt}$. Dies setzt sich daraus zusammen,
dass das Sensor CASSY eine Auflösung von 12 Bit hat, wobei ein Bit für das Vorzeichen der
Zahl verwendet wird. Somit ist die Auflösung $\frac{Messbereich}{2^{11}}$. Dies erklärt
auch die in \autoref{fig:widerstandlinreg} großen Fehlerbalken des Residuenplots.

Die bestimmte Kovarianz der Messung folgt aus den nicht gemittelten Werten. Zieht man die
Mittelwerte der Messwerte von den Messwerten ab und führt die lineare Regression erneut
durch (d.h. mit den "`zentrierten"' Werten), so geht die Kovarianz $\rho{ab}$ gegen 0
($\order{\num{e-4}}$), während Steigung und $\chi^2$ unverändert bleiben (im Rahmen
unserer Genauigkeit). Der y-Achsenabschnitt geht in diesem Fall auch gegen 0
($\order{\num{e-5}}$).

Für die Regression ohne Mittelwertanpassung ist unser y-Achsenabschnitt $b$ signifikant
ungleich 0. Im besonderen wird ein negativer Strom bei \SI{0}{\volt} gemessen. Wir gehen
davon aus, dass das Messgerät falsch kalibriert war, da ein negativer Strom mit den
verbauten Teilen und abgeschalteter Spannungsquelle nicht möglich sein sollte. Dieser
Offset hat jedoch keine Auswirkungen auf unsere Steigung, da er konstant ist. Der Wert a
ist nun unser erwarteter Widerstand:

\[
a = R = \SI{991.587(10939)}{\ohm}
\]

Die statistische Unsicherheit folgt aus der linearen Regression. Zu diesem Zweck musste
man die Unsicherheiten auf $I$ und $U$ in die lineare Regression miteinbeziehen.

Der systematische Fehler unserer Messung erfolgt mit der Verschiebemethode, welche wir mit
den systematischen Fehler von U und I durchführen. Diese lauten $\sigma_{I,sys} =
(\num{0.02} \cdot I)/\sqrt{3}$ und $\sigma_{U,sys} = (\num{0.01} \cdot U)/\sqrt{3}$,
wobei U und I jeweils für den gemessenen Wert steht. Somit ist der
systematische Fehler bei größeren Messwerten größer. In der Verschiebemethode addieren
wir den systematischen Fehler von U auf alle gemittelten Werte von U und führen eine
lineare Regression durch. Die resultierende Steigung nennen wir $R_{U+}$. Ziehen
wir den systematischen Fehler von den gemittelten Werten ab und führen wieder eine
lineare Regression durch, ergibt sich die Steigung $R_{U-}$. Der durch $U$ verursachte
systematische Fehler auf R ergibt sich durch:

\[
\sigma_{a_{U}sys} = \frac{\qty(\abs{R_{U+} - a}) + \qty(\abs{R_{U-} - a})}{2} =
\SI{5.725}{\ohm}
\]

Wobei a die durch die normale lineare Regression bestimmte Steigung ist. Die Werte
$R_{I+}$ und $R_{I-}$ bestimmt man analog durch horizontales Verschieben und es folgt:

\[
\sigma_{a_{I}sys} = \frac{\qty(\abs{R_{I+} - a}) + \qty(\abs{R_{I-} - a})}{2} = \SI{11.451}{\ohm}
\]

Dies ist der aus der Stromstärke folgende systematische Fehler. Diese beiden
systematischen Fehler ergeben nun den gesamten systematischen Fehler unserer Steigung:

\[
\sigma_{a,sys} = \sqrt{\sigma_{a_{I}sys}^2 + \sigma_{a_{U}sys}^2} = \SI{12.803}{\ohm}
\]

Für den Fehlerbalken des Residuenplots nutzten wir:

\begin{equation}
\label{eq:errorgesresR}
\sigma_{ges} = \sqrt{\sigma_U^2 + \qty(a \sigma_I)^2}
\end{equation}

wobei $\sigma_U$ und $\sigma_I$ die gewählten Digitalisierungsunsicherheiten sind, die
wir anstelle der statistischen Mittelwertstandardabweichung nutzen (da sie größer sind).

\subsubsection{Endergebnisse}

Wir haben unseren Widerstand an der zur Verfügung gestellten Messbrücke vermessen. Diese
hat eine Unsicherheit von \SI{0.25}{\%} und gab ein Ergebnis von \SI{994.2(25)}{\ohm}.
Unsere Messung ergab also:

\[
\SI[parse-numbers=false]{(991,587 \pm 10,939(stat) \pm 12,803(sys))}{\ohm}
\]

Somit liegt unsere Messung knapp außerhalb des 1-$\sigma$-Intervalls der Messbrücke. Auch
mit der Angabe des Herstellers von \SI{1000(50)}{\ohm}, die man am Widerstand ablesen
konnte, stimmt unsere Messung überein.

\begin{table}[h]
\centering
\begin{tabular}{l|l}
Parameter & Wert \\
\hline
\rule{0pt}{2.5ex} Steigung a & \SI[parse-numbers=false]{(991,587 \pm 10,939(stat) \pm
12,803(sys))}{\ohm}\\
y-Achsenabschnitt b & \SI{0.374(61)e0}{\volt}\\
$\frac{\chi^2}{ndof}$ & $\chisq{0.65648}{18}{0.037}$\\
$\sigma_{a_{I}sys}$ & \SI{11.451}{\ohm}\\
$\sigma_{a_{U}sys}$ & \SI{5.725}{\ohm}\\
\end{tabular}
\caption{Zusammenfassung}
\end{table}

\subsubsection{Diskussion}

Die Fehlerbalken des Residuenplots sind zu groß. Dies rührt primär vom zu großen
Digitalisierungsintervall und sekundär vom großen Widerstand. Hätten wir einen kleineren
Widerstand gewählt, so würde nach \autoref{eq:errorgesresR} der Unsicherheitsanteil von I
weniger ausgeprägt sein. Somit wären auch die Fehlerbalken weniger stark ausgeprägt.

In den Begriffen der statistischen Unsicherheit unseres Messwertes befindet sich der zu
erwartende Wert in einer Entfernung von \num{0.24}$\sigma$ von der Herstellerangabe.
Somit ist unser Wert hinreichend genau bestimmt.

\FloatBarrier
