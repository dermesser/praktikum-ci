\documentclass[a4paper,12pt]{scrartcl}
\usepackage[ngerman]{babel}
\usepackage{xcolor}
\usepackage[margin=2.5cm]{geometry}
\usepackage[german]{varioref}
\usepackage{physics}
\usepackage{siunitx}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{float}
\usepackage{placeins}

\usepackage[no-math]{fontspec}
\setmainfont{STIX}
\usepackage[notext]{stix}

\sisetup{
math-ohm = \Omega,
exponent-product = \cdot
}

\hypersetup{
colorlinks,
breaklinks,
urlcolor=blue,
linkcolor=blue,
pdftitle=Grundpraktikum: Thermodynamik,
pdfauthor=AUTOR,
pdfcreator=XeLaTeX}

\newcommand{\TODO}[1]{{\large \textcolor{red}{\textbf{TODO:}}} \emph{#1}}

%opening
\subject{Physikalisches Grundpraktikum\\RWTH Aachen}
\title{Gasgesetze}
\subtitle{Thermodynamik}
\author{Lewin Bormann \& Maximilian Bartel}
\date{\today}

\begin{document}
\captionsetup[subfigure]{justification=centering}

\maketitle

\tableofcontents

\section{Grundlagen und Versuchsbeschreibung}

\subsection{Ziele}

Überprüfung der Gültigkeit der idealen Gasgleichung $p V = n R T$ (innerhalb unseres
Messbereichs) und Messung der Boltzmannkonstante, sowie die Bestimmung des absoluten Nullpunktes.

\subsection{Grundlagen}

Die Gasgleichung für ideale Gase $p V = n R T = N k_B T$ liefert eine gute Abschätzung
für das Verhalten der meisten Gase unter dem Einfluss verschiedener Größen:

\begin{description}
 \item[p] Druck
 \item[V] Volumen
 \item[n] Stoffmenge
 \item[$\mathbf{N_A}$] Avogadrokonstante $N_A = \SI{6.022e23}{\per\mole}$
 \item[R] Universelle Gaskonstante $R = N_A k_B$
 \item[$\mathbf{k_B}$] Boltzmannkonstante $k_B =\SI{1.38065e-23}{\joule\per\kelvin}$
 \item[T] Temperatur in Kelvin
 \item[N] Anzahl der Moleküle
\end{description}

Durch die Relation $n = N / N_A$ und $R = k_B N_A$ lässt sich zwischen den Schreibformen
wechseln.

Um so einfach zu sein, trifft diese Gleichung aber zwei Annahmen: (i) Gasmoleküle sind
punktförmig, haben also kein inneres Volumen; (ii) es gibt keine Wechselwirkungen
zwischen Molekülen. Falls man diese Annahmen nicht treffen kann, bietet sich die
van-der-Waals-Gleichung an, die diese Effekte berücksichtigt: $\left(p + n^2 a /
V^2\right) (V - n b) = n R T$, mit \emph{Kohäsionsdruck} $a$ und \emph{Kovolumen} $b$.
Die Ähnlichkeit zur idealen Gasgleichung ist offensichtlich, und beim idealen Gas sind $a
= b = 0$.

Aus der idealen Gasgleichung lassen sich drei weitere Gesetze herleiten, die jeweils
aus Festsetzen einer Größe resultieren:

\begin{description}
 \item[$V/T =$ const.] \emph{Gesetz von Gay-Lussac}: Das Volumen ist bei $p, n$ konstant
proportional zur Temperatur: $V \propto T$
 \item[$p/T =$ const.] \emph{Gesetz von Amontons}: Der Druck ist bei $V, n$ konstant
proportional zur Temperatur: $p \propto T$
 \item[$p V =$ const.] \emph{Gesetz von Boyle-Mariotte}: Der Druck ist umgekehrt
proportional zum Volumen: $p \propto 1/V$.
\end{description}

In unseren Versuchen sind wir immer von einer konstanten Stoffmenge ausgegangen.

\subsection{Versuchsaufbau}

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.5]{img/aufbau1.jpg}
 % aufbau1.jpg: 950x501 px, 72dpi, 33.51x17.67 cm, bb=0 0 950 501
 \caption{Versuchsaufbau: Links Spritze in Wasserbad auf Heizung, rechts Dreiwegeventil
am Manometer und rechts davon das Thermometer.}
 \label{fig:aufbau1}
\end{figure}

Eine Glasspritze wird in einem Glasmantel von Wasser umgeben. Dieses dient als Wärmebad
und kann von einem Heizgerät aufgeheizt werden. Dabei puffert es die Wärmeenergie und
sorgt so für eine konstante Temperatur. Der Messfühler eines digitalen Thermometers wird
durch eine der beiden Öffnungen im Wasser platziert und misst dort die Temperatur.
Spritze und Gewindestange werden über Stangen und durch Klemmen an einem Stativfuß
fixiert. Das Heizgerät selber hat keine Einstellung für die Ausgangsleistung, sondern
wird mit einem zeitgesteuerten Relais verbunden. Dieses lässt einen die Zeit steuern, die
das Heizgerät an ist. Hierbei ist zu beachten, das keine bestimmte Temperatur angesteuert
werden kann, man muss als Durchführender evtl. nachsteuern. Die Glasspritze ist an ihrem
Kolben mit einer Gewindestange verbunden. Mit dieser
lässt sich das Volumen einstellen und an einer Messskala an der Spritze ablesen. Der Ausgang der Spritze ist durch einen Schlauch mit
einem Dreiwegeventil verbunden, das an ein Barometer gekoppelt ist. Das Barometer misst
den Druck im Zylinder.

\paragraph{Dichtheit}

Um die gleichbleibende Molekülanzahl zu gewährleisten, muss die Aperatur auf ihre
Dichtheit überprüft werden. Dafür wird der Kolben mit geöffnetem Dreiwegeventil in eine
mittige Position gebracht  und daraufhin der Hahn so gedreht, dass die Aperatur von der
Umgebungsluft abgetrennt ist. Dann wird der Druck mit Hilfe des Kolbens auf ca. \SI{1800}{\milli\bar} erhöht und es
wird eine Druckmessung von 5 min gestartet.

Sollte diese Position zufriedenstellend dicht sein, wird ein Druck von ca.
\SI{650}{\milli\bar} eingestellt und wieder eine Druckmessung von 5 min durchgeführt.
Auch hier sollte der Druck konstant bleiben. Speziell bei uns war unser erster Aufbau so
undicht, dass knapp $\SI{65}{\milli\bar\per\min}$ ausgetreten sind. Nachdem wir den
Aufbau wechselten, traten beim Überdruck $\SI{1}{\milli\bar\per\min}$ aus. Bei der
Unterdruckmessung veränderte sich der Druck nicht. Diese Abweichungen sind so gering,
dass wir sie im Rahmen unserer Genauigkeit vernachlässigen können.

\FloatBarrier

\section{Isotherme Messung}

\subsection{Durchführung}
\label{sec:durchfuehrungsisotherm}

Isotherm bedeutet gleichbleibende Temperatur. Das Wasserbad kompensiert dabei die kleinen
Temperaturunterschiede, die bei einer Kompression entstehen, so dass wir in unserer
Messreihe von einer isothermen Zustandänderung ausgehen können.

Zunächst wird die Wasser- bzw. Lufttemperatur im Messvolumen $T_Z$ gemessen. Anschließend
starten wir unsere Messung von $p(V)$ und des Volumens $V_0$. Dabei verschieben wir
unseren Kolben auf verschiedene Volumina und lesen das Volumen und den Druck für die
entsprechende Stelle ab. Das Volumen $V$ für die Rechnung ist allerdings nicht das
Volumen innerhalb der Spritze, sondern setzt sich zusammen durch $V = V_0 + V_S$;  das
Schlauchvolumen $V_S$ bleibt beim Verstellen der Spritze konstant.

Eine weitere Messung wird bei einer hohen Temperatur, wie sie nach der isochoren Messung
in \autoref{sec:durchfuehrungisochor} erreicht wurde, durchgeführt. Bei uns wurde die
erste Messung bei $t_0\equiv\SI{22.2}\celsius$ ("`kalt"') und die zweite bei $t_0\equiv\SI{80.5}\celsius$
durchgeführt ("`heiß"'/"`warm"').

\subsection{Auswertung}

\label{sec:auswertungisotherm}

\subsubsection{Rohdaten}

\begin{figure}[H]
\centering
 \hspace{-5em}
 \begin{subfigure}{\textwidth}
 \centering
  \includegraphics[scale=0.7]{img/raw_kalt.png}
  \caption{Rohdaten: Isotherme Messung bei $t = \SI{22.2}{\celsius}$.}
 \end{subfigure}

 \hspace{-5em}
 \begin{subfigure}{\textwidth}
 \centering
  \includegraphics[scale=0.7]{img/raw_warm.png}
  \caption{Rohdaten: Isotherme Messung bei $t = \SI{80.5}{\celsius}$.}
 \end{subfigure}
 \caption{Rohdaten der isothermen Messung; links die beobachteten Isothermen, rechts die
Auftragung von $1/p$.}
 \label{fig:isothermraw}
\end{figure}

\subsubsection{Unsicherheiten}
\label{sec:isothermunsicher}
Die fundamentalen Messunsicherheiten in diesem Experiment stammen zunächst vom Manometer,
das vom Hersteller mit einer Unsicherheit "`das größere von $\pm \SI{3}{\milli\bar} / \pm
0.1\%$"' beschrieben wird, und dem Thermometer mit einer Unsicherheit von $\pm 0.1\%$ des
Messwerts. Beim Thermometer liegt die Unsicherheit in dem hier genutzten Messbereich
unter der Ablesegenauigkeit, sodass wir die Ablesegenauigkeit von $\SI{0.1}{\kelvin}$ als
Unsicherheit nutzen.

Beim Volumen lesen wir das eingestellte Volumen optisch ab, und zwar so, dass der rechte
Rand des breiten Strichs am Kolben mit dem gewünschten Skalenstrich in Deckung steht. Da
wir bei diesem Versuch den Kolben selbst einstellen und die gleiche Person konsistent dies nach
ihrem Auge eingestellt hat, schätzen wir unsere Genauigkeit hier auf etwa
$\SI{0.1}{\milli\liter}$ (die wahren Einstellwerte streuen in einem sehr schmalen
Bereich um die Skalenstriche). Die Spritzenskala selbst hat keine spezifizierte
Unsicherheit; wir vernachlässigen sie hier.

\subsubsection{Analyse}
\label{sec:isothermanalyse}

Für die Auswertung benutzen wir eine lineare Regression von $1/p$ über $V_0$. Diese
Möglichkeit ist am einfachsten, um unsere gewünschten Größen, insb. die
Boltzmannkonstante zu berechnen. Die Unsicherheiten im Residuenplot sind durch
$\sigma_{resid} = \sqrt{\sigma_{1/p}^2 + (a \sigma_{V_0})^2}$ gegeben.

\begin{figure}[h]
\centering
 \hspace{-5em}
 \begin{subfigure}{\textwidth}
 \centering
  \includegraphics[scale=0.8]{img/regr_kalt.png}
  \caption{Rohdaten: Isotherme Regression bei $t = \SI{22.2}{\celsius}$.}
  \label{fig:isothermregressiona}
 \end{subfigure}

 \hspace{-5em}
 \begin{subfigure}{\textwidth}
 \centering
  \includegraphics[scale=0.8]{img/regr_warm.png}
  \caption{Rohdaten: Isotherme Messung bei $t = \SI{80.5}{\celsius}$.}
 \end{subfigure}
 \caption{Regression durch die Messung der Isothermen ($1/p = a V_0 + b$). Die Abszisse
beschreibt $V_0 / \si{\milli\liter}$.}
 \label{fig:isothermregression}
\end{figure}

Das Volumen der Spritze $V_0$ wird bereits bei der Dichtheitsmessung auf $V_0 =
\SI{50}{\milli\liter}$ eingestellt. Für die beiden Messungen erhalten wir jeweils die in
\autoref{tab:isothermregression} aufgeführten Parameter. Mit der Verschiebemethode
in $1/p$ und $V_0$ erhalten wir für $a$ einen systematischen Fehler, der jeweils
mindestens sechs Größenordnungen kleiner als der Wert selbst ist, sodass wir ihn ab hier
ignorieren.

\begin{table}[h!]
\hspace{-2em}
\begin{tabular}{c|cl|cl}
 Parameter & \multicolumn{2}{c|}{Messung kalt $t_0=\SI{22.2}{\celsius}$} & \multicolumn{2}{c|}{Messung warm $t_0=\SI{80.5}{\celsius}$}  \\
 \hline
 \rule{0pt}{2.5ex} $a $ & $\num{1.861e-5} \pm
\num{.004e-5}$ & $\si{\per\milli\bar\per\milli\liter}$ &
$\num{1.540e-5} \pm\num{.002e-5}$ & $\si{\per\milli\bar\per\milli\liter}$ \\

\rule{0pt}{2.5ex}
 $b $ & $\num{9.163e-5} \pm \num{.192e-5}$ & $\si{\per\milli\bar}$ & $\num{8.893e-5}
\pm\num{.158e-5}$ & $\si{\per\milli\bar}$ \\

\rule{0pt}{2.5ex}
 $\chi^2$ & \multicolumn{2}{c|}{$1.45$} & \multicolumn{2}{c}{$6.82$} \\

 $\sigma_{ab}$ & \multicolumn{2}{c|}{$-0.95$} & \multicolumn{2}{c}{$-0.96$} \\

 FG & \multicolumn{2}{c|}{12} & \multicolumn{2}{c}{14} \\
 $\chi^2/FG$ & \multicolumn{2}{c|}{0.12} & \multicolumn{2}{c}{0.49} \\
\end{tabular}
\caption{Parameter der linearen Regressionen mit $1/p = a V_0 + b$. (Freiheitsgrade
$\equiv$ FG)}
\label{tab:isothermregression}
\end{table}

\paragraph{Volumen und Schlauchvolumen}
\label{sec:volumen}

Aus den Parametern in \autoref{tab:isothermregression} lässt sich das Schlauchvolumen
$V_S$ bestimmen: $V_S = a/b$. In der kalten Messung ist $V_{S1} = \SI{4.92}{\milli\liter}
\pm \SI{0.13}{\milli\liter}$, in der warmen $V_{S2} = \SI{5.77}{\milli\liter}
\pm \SI{0.12}{\milli\liter}$. Die Fehler wurden jeweils nach Gl. 3.18 in der Anleitung bestimmt:

\begin{align}
\sigma_{V_S} = V_S \sqrt{\frac{\sigma_a}{a^2} - 2 \rho \frac{\sigma_a \sigma_b}{ab} +
\frac{\sigma_b^2}{b^2}}
\label{eq:sigmaV_s}
\end{align}

Die beiden Werte sind offensichtlich nicht kompatibel; die erste Messung $V_{S1}$ ist
aufgrund der konstanten Temperatur (und Hinweisen der Betreuer) als vertrauenswürdiger zu
betrachten, sodass wir weiterhin mit $V_S = V_{S1}$ rechnen werden. Der Grund für die
Diskrepanz liegt vermutlich u.a. daran, dass bei der kalten Messung die Temperatur
praktisch konstant war, während sie bei der heißen Messung durch Schwankungen von mehr als
einem Grad Celsius keine so gute Genauigkeit erreichen konnte.

\paragraph{Stoffmenge}

Auch die Stoffmenge $N = \num{1.33e21} \pm \num{0.01e21}$ haben wir aus dem gleichen
Grund beim kalten Experiment gemessen und für die Auswertung beider Experimente genutzt.
Sie berechnet sich durch $N = \frac{N_A  (V_0 + V_S)  \rho_L}{M_L}$. Für die Messung bei
Raumtemperatur haben wir bei einer relativen Luftfeuchte von 36\,\%, einer Raumtemperatur
von $\SI{22.2}{\celsius}$, und einem Umgebungsdruck $p_0 = \SI{986}{\milli\bar}$ mithilfe
der Praktikumsbibliothek die Molare Masse $M_L = \SI{28.842}{\gram\per\mole} \pm
\SI{0.1}{\gram\per\meter\cubed} $ und die Luftdichte $\rho_L =
\SI{1.159}{\kilogram\per\meter\cubed} \pm \SI{5}{\gram\per\meter\cubed}$ berechnet. Da
die Messunsicherheit der Luftfeuchte an der Mini-Wetterstation unbekannt ist und die
Genauigkeit der Näherungen in der Praktikumsbibliothek ebenfalls nicht genauer
spezifiziert wird (bis auf "`Gute Näherung"'), haben wir die Unsicherheit jeweils auf
$\approx \pm 0.5\,\%$ festgelegt; die Dichte und molare Masse schwanken schließlich nicht
frei, sondern in engen Grenzen, und die gute Näherung sollte eine solche Genauigkeit
erreichen.

Wir definieren $V_{start}\equiv V_0 + V_S$. Durch Fehlerfortpflanzung erhalten wir als
Fehler auf $V_{start}$
\begin{align}
\sigma_{V_{start}}
&=\sqrt{\sigma_{V_0}^2 + \sigma_{V_S}^2} = \SI{0.15}{\milli\liter}
\end{align}
Dabei ist $\sigma_{V_S}=\SI{0.13}{\milli\liter}$ mit \autoref{eq:sigmaV_s} berechnet und $\sigma_{V_0}=\SI{0.1}{\milli\liter}$ ist unsere Fehler auf das Ablesen. Strenggenommen sind die beiden Unsicherheiten
korreliert; dies haben wir hier aber aufgrund der kleinen Effekte außen vor gelassen.

Unseren Fehler auf $N$ erhalten wir durch Fehlerfortpflanzung

\begin{align}
\sigma_N &= \sqrt{\left(\frac{N_A
\rho_L}{M_L} \sigma_{V_{start}}\right)^2 + \left(\frac{N_A V_{start}}{M_L}
\sigma_{\rho_L}\right)^2 + \left(\frac{N_A V_{start} \rho_L}{M_L^2}
\sigma_{M_L}\right)^2}\\
&= \num{0.01e21}
\end{align}

\paragraph{Boltzmannkonstante}

Die Geradengleichung lautet

\begin{align}
\frac{1}{p}=\frac{1}{Nk_BT}V_0 +\frac{1}{Nk_BT}V_S
\end{align}

Durch Koeffizientenvergleich erhalten wir für die
Boltzmannkonstante

\begin{align}
k_B = \frac{1}{a N (273.15 + t_0)}
\end{align}

Dabei ist $T_0=t_0+273.15 $ Damit erhalten wir als kombinierte Fehler mit $\sigma_a$ aus
der Regression \autoref{tab:isothermregression} und $\sigma_{T_0} = \SI{0.1}{\kelvin}$
(angegebene Temperaturunsicherheit):

\begin{align}
\sigma_{k_B} = \sqrt{\left(\frac{1}{a^2 N T_0} \sigma_a\right)^2 = \left(\frac{1}{a N^2
T_0} \sigma_N\right)^2 + \left(\frac{1}{a N T_0^2} \sigma_{T_0}\right)^2}
\end{align}

Da die systematische Unsicherheit in $a$ auf einen sehr kleinen Wert bestimmt wurde, und
wir hier keine weiteren einführen, vernachlässigen wir sie.

\subsubsection{Endergebnisse}
\label{sec:isothermergebnis}

Mit den in der Anleitung und \autoref{sec:isothermanalyse} gegebenen Gleichungen erhalten
wir, unter der Vernachlässigung der erwähnten systematischen Fehler:

\begin{description}
 \item[Kalte Messung] $k_B = \num{1.369e-23} \pm \SI{0.145e-23}{\joule\per\kelvin}
\text{(stat.)}$
 \item[Heiße Messung] $k_B = \num{1.382e-23} \pm \SI{0.040e-23}{\joule\per\kelvin}
\text{(stat.)}$
\end{description}

\subsubsection{Diskussion}

Beide Werte sind mit dem Theoriewert, der durch das SI auf $k_B =
\SI{1.380649e-23}{\joule\per\kelvin}$ festgelegt wurde, kompatibel. Er liegt für beide

Messungen deutlich innerhalb des 1$\sigma$-Intervalls (\autoref{fig:vergleichkb}). Unser
$\chi^2/\mathrm{FG}=0.12$ bzw. $0.49$ sind etwas zu klein, was darauf hinweisen könnte,
dass wir unsere Fehler sogar zu grob abgeschätzt haben. Auf dies deuten auch unsere
Residuenplots (\autoref{fig:isothermregression}), deren Unsicherheiten zu groß sind. Der
große Unterschied der Unsicherheiten auf $k_B$ stammt zum einen Teil daher, dass die
Messung bei höherer Temperatur eine kleinere Messunsicherheit $\sigma_a$ aufwies, und ist
weiterhin durch die unterschiedlichen verwendeten Werte zu erklären (wie z.B. $\rho_L$,
$M_L$, $T_0$).

\FloatBarrier

\section{Isochore Messung}

\subsection{Durchführung}
\label{sec:durchfuehrungisochor}

Isochor bedeutet gleichbleibendes Volumen. Statt das Volumen zu verändern, wird nun also
bei gleichbleibendem Volumen die Temperatur geändert. Der Glaskolben wird nicht bewegt.

Um den Aufbau zu erwärmen wird das Heizgerät unter dem Wasserbad eingeschaltet. Die
Spritze wird nicht direkt erhitzt, da dieser sehr empfindlich ist und das Wasser den
Zylinder gleichmäßiger erhitzt. Es ist zu beachten, dass das Thermometer im Wasser sein
sollte.

Nach dem das Relais auf Stufe 5 eingeschaltet wurde, wird die Messung gestartet. Während
der Aufbau sich erwärmt, werden Temperatur und Druck in regelmäßigen Abständen notiert.
Man sollte eine Endtemperatur von ca. $\SI{80}{\celsius}$ anstreben.

\subsection{Auswertung}
\label{sec:auswertungisochor}

Wenn wir die Gerade auch auf einer Abszisse mit Einheit $\si{\celsius}$ auftragen (s.
\autoref{fig:absolutezero}), dann lässt sich auch direkt die Lage des absoluten
Nullpunkts (in $\si{\celsius}$) abschätzen, und zwar dort, wo $p(T = T_0) = 0$, da
Moleküle als punktförmig, ohne Volumen, angenommen werden. $\SI{0}{\celsius} - T_0 =
\Delta T_0$ ist dann wegen $b / \Delta T_0 = \Delta p / \Delta T = m \Rightarrow \Delta
T_0 = b/m$.

\subsubsection{Rohdaten}

\begin{figure}[h]
 \hspace{-3em}\includegraphics[scale=0.7]{img/isochor_raw.png}
 % isochor_raw.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Rohdaten der isochoren Messreihe: zwischen 30 und 50 $\si{\celsius}$ wurde der
Druck in Schritten von $\SI{0.5}{\celsius}$ aufgezeichnet, da die Temperatur nur langsam
stieg. Die Wegnahme dieser Daten führt zu keiner relevanten Veränderung der
Regressionsparameter.}
 \label{fig:isochorroh}
\end{figure}

\subsubsection{Unsicherheiten}

In diesem Versuch haben die Geräte selbst die gleichen Unsicherheiten wie in der
isothermen Messung. Also besitzt das Manometer eine Unsicherheit von
$\pm\SI{0.1}{\percent}$ bzw. $\pm\SI{3}{\milli\bar}$ und wie auch in
\autoref{sec:isothermunsicher} setzten wir die Unsicherheit auf $\SI{0.1}{\kelvin}$. Da
aber durch das An- und Ausschalten des Relais die Temperatur um $\SI{0.8}{\celsius}$
geschwankt ist, benutzen wir statt der 0.1 K eine Unsicherheit von 0.4 K. Für die
Auswertung benutzen wir Ergebnisse aus dem isothermen Versuch in
\autoref{sec:auswertungisotherm}. Das bedeutet speziell wir benutzen:

\begin{table}[H]
\label{tab:isoschorpara}
\centering
\begin{tabular}{c|c}
Parameter & Wert$\pm$Fehler\\
\hline\
Volumen Schlauch & $4.92\pm\SI{0.13}{\milli\liter}$\\
Teilchenanzahl & $1.33\pm\SI{0.01e21}{}$
\end{tabular}
\caption{Werte und Fehler}
\end{table}

\subsubsection{Analyse}

\begin{figure}[h]
\hspace{-3em}
 \includegraphics[scale=0.7]{img/isochor_regr.png}
 % isochor_regr.png: 0x0 px, 0dpi, nanxnan cm, bb=
 \caption{Lineare Regression $p = m t + b$ für die isochore Messreihe. Auf der Abszisse
ist die Temperatur in \si{\celsius} aufgetragen.}
 \label{fig:isochorregr}
\end{figure}

Aufgetragen haben wir den gemessenen Druck gegen die Temperatur. Aus unseren Fehlern auf
die beiden Messwerte und der Messwerten erhalten wir durch lineare Regression eine Gerade
mit folgenden Parametern:

\begin{table}[H]
\label{tab:isochorgerade}
\centering
\begin{tabular}{c|c}
Parameter & Wert\\
\hline
Steigung & $3.09\pm\SI{0.01}{\milli\bar\per\kelvin}$\\
Ordinatenabschnitt  & $915.0\pm\SI{0.6}{\milli\bar}$\\
$\mathrm{\chi^2}$ & 54.34\\
FG & 74\\
$\chi^2/\mathrm{FG}$ & 0.734\\
Korrelationskoeffizient & $-0.953$ \\
\end{tabular}
\caption{Ergebnisse der Regression}
\label{tab:isochorparameter}
\end{table}

Wenn wir jetzt die ideale Gasgleichung betrachten, können wir sie in eine Form bringen, in der wir per Koeffizientenvergleich unsere Parameter physikalisch interpretieren können.

\begin{align}
p V &= n R T\\
\Leftrightarrow \;\;\;\;\;\;\;\;p &= \frac{n R}{V} T\\
\Leftrightarrow \;\;\;\;\;\;\;\;p &= \frac{n R}{V}t + \frac{n R}{V}T_0\\
\Leftrightarrow \;\;\;\;\;\;\;\;p &= \frac{n R}{V_0+V_S}t + \frac{n R}{V_0+V_S}T_0
\end{align}

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.7]{img/absolutezero.png}
 % absolutezero.png: 1500x1000 px, 250dpi, 15.24x10.16 cm, bb=0 0 432 288
 \caption{zur Bestimmung des absoluten Nullpunkts.}
 \label{fig:absolutezero}
\end{figure}

Also erhalten wir für die Steigung $\equiv m = \frac{n R}{V_0+V_S}$ und für den
Ordinatenabschnitt $\equiv b=\frac{n R}{V_0+V_S}T_0$. Um daraus den absoluten Nullpunkt
bestimmen zu können rechnen wir also

\begin{align}
T_0&=-\frac{b}{m}\\
&=
-\frac{\SI{915.0}{\milli\bar}}{\SI{3.09}{\milli\bar\per\celsius}} = -\SI{296.5}{\celsius}
\end{align}

Für die Unsicherheit auf den absoluten Nullpunkt erhalten wir aus der Fehlerfortpflanzung

\begin{align}
\sigma_{T_0} =
T_0\sqrt{\left(\frac{\sigma_m}{a}\right)^2-2\rho\frac{\sigma_m\sigma_b}{mb}+\left(\frac{
\sigma_b } { b } \right)^2 } =\SI{1.25}{\kelvin}
\end{align}

Weiterhin können wir aus unserer Messung auch die Boltzmannkonstante bestimmen.

\begin{align}
m &= \frac{N k_B}{V_S+V_0}\\
\Leftrightarrow \;\;\;\;\;\;\;\; k_B &= \frac{(V_S+V_0)m}{N} \\
&= \SI{1.27e-23}{\joule\per\kelvin}
\end{align}

Bei der Fehlerbrechnung haben wir mögliche Korrelationen zwischen den Parametern, außer bei $a$ und $b$, nicht
beachtet. Als Werte der Parameter haben die schon genannten aus
\autoref{tab:isochorparameter} benutzt. Daraus ergibt sich aös Unsicherheit

\begin{align}
\sigma_{k_B} &= k_B\sqrt{\left(\frac{\sigma_{V_S}}{V_S}\right)^2+\left(\frac{\sigma_{V_0}}{V_0}\right)^2+\left(\frac{\sigma_m}{m}\right)^2+\left(\frac{\sigma_N}{N}\right)^2}\\
&=\SI{3.54e-25}{\joule\per\kelvin}
\end{align}

Da wir nur die signifikanten Stellen des Fehlers betrachten, ist unser Fehler
$\SI{4e-25}{\joule\per\kelvin}$.

\paragraph{Vergleich}

Wir können die gemessene Steigung $m$ mit der theoretischen Steigung $m' := N
k_B/V_{start}$ vergleichen. Dann erhalten wir mit dem Literaturwert von $k_B$

\begin{align}
m' &= \frac{N k_B}{V_{start}} = \frac{\num{1.33e21} k_B}{\SI{54.92e-6}{\meter\cubed}} \\
&= \SI{3.34}{\milli\bar\per\kelvin} \qqtext{und} \\
\left(\frac{\sigma_{m'}}{m'}\right)^2 &= \left( \frac{\sigma_N}{N} \right)^2 +
\left( \frac{V_{start}}{v_{start}} \right)^2 \\
\Rightarrow \sigma_{m'} &= \SI{0.03}{\milli\bar\per\kelvin}
\end{align}

Mit dieser Steigung, die um $\SI{0.25}{\milli\bar\per\kelvin}$ größer als unsere gemessene
ist, erhalten wir für den absoluten Nullpunkt mit unserem $b$ eine deutlich bessere
Schätzung:

\begin{align}
\left( \frac{\sigma_{T_0'}}{T_0'} \right)^2 &= \left( \frac{\sigma_b}{b} \right)^2 +
\left( \frac{\sigma_{m'}}{m'} \right)^2 \\
\Rightarrow \sigma_{m'} &= \SI{2.2}{\celsius} \\
T_0' = -\frac{b}{m} &= - \num{273.95} \pm \SI{2.2}{\celsius} \\
\end{align}

\subsubsection{Endergebnisse}

Mit diesem Versuch erhalten wir für den absoluten Nullpunkt
$T_0 = \num{-296.50} \pm \SI{1.25}{\celsius}$ (das ist nicht kompatibel mit der
eigentlichen Lage bei \SI{-273.15}{\celsius}). Die zweite Abschätzung von
$-\num{273.95} \pm \SI{2.2}{\celsius}$, die den tatsächlichen Wert von $k_B$ benutzt,
liegt allerdings viel näher am eigentlichen Absolutnullpunkt.

Die Boltzmannkonstante wird bestimmt zu $k_B = \num{1.27e-23} \pm
\SI{0.04e-23}{\joule\per\kelvin}$; dieses Ergebnis ist nicht kompatibel mit dem
Literaturwert $k_B = \SI{1.380649e-23}{\joule\per\kelvin}$.

Die lineare Regression selber hat ein $\chi^2/\mathrm{FG}=0.734$. Dies ist ein
zufriedenstellendes Ergebnis, wenn man bedenkt, dass die Temperatur des Wassers so stark
geschwankt hat. Der Residuenplot bei dieser Messung hat Auffälligkeiten, z.\,B. sind die
Messpunkte nicht gleichmäßig ober- und unterhalb verteilt, sondern treten in Gruppen auf.
Man sieht als andeutungsweise das langsame Schwanken der Temperatur.

\subsubsection{Diskussion}

Bei diesem Versuch treten viele Unsicherheiten auf. So ist zunächst Luft natürlich kein
ideales Gas und die Temperatur des Wasserbads überträgt sich auch nicht präzise auf die
Luft im Zylinder. Die Messung des Thermometers hat also nur mittelbar etwas mit der
Temperatur der Luft im Zylinder zu tun.

Für die Lage des absoluten Nullpunkts macht es also wenig Sinn, aus einer Messung im
Bereich $\SI{20}{\celsius}-\SI{80}{\celsius}$ auf die Lage einer Nullstelle im Abstand
von \SI{300}{\kelvin} zu extrapolieren.

Da $k_B$ aus der Steigung bestimmt wird, verlässt sich auch dieses Ergebnis auf die
Präzision des Temperatur-Druck-Verhältnis. Da dieses, wie erklärt, aber nicht präzise
ist, ist auch das Ergebnis mit einer hohen Wahrscheinlichkeit nicht in der Nähe des
erwarteten Werts, selbst wenn die geschätzten Unsicherheiten diese Abweichung nicht
hergeben. Eine mögliche Erklärung für die Diskrepanz zwischen erwarteter und beobachteter
Steigung ist die gegenüber dem Wasserbad verzögerte Erwärmung der Luft im Zylinder, und
eine Pufferwirkung des Schlauchvolumens, das immerhin 10\,\% des Zylindervolumens betrug.

\section{Fazit}

\begin{figure}[h]
 \centering
 \includegraphics[scale=0.8]{img/vergleich.png}
 % vergleich.png: 1550x1162 px, 300dpi, 13.12x9.84 cm, bb=0 0 372 279
 \caption{Vergleich mit Unsicherheiten der gemessenen Werte mit dem theoretischen Wert.
Vertikale Auftragung für bessere Übersicht; der Wert bei $y = 0$ ist der Theoriewert.}
 \label{fig:vergleichkb}
\end{figure}

Aus den drei Versuchsreihen haben wir die ungefähre Lage des absoluten Nullpunkts, das
Schlauchvolumen am verwendeten Apparat, und drei Schätzungen für die Boltzmannkonstante
$k_B$ erhalten.

\paragraph{Schlauchvolumen}

In \autoref{sec:volumen} haben wir zwei unterschiedliche Werte für das Volumen $V_S$, das
dem Volumen der Apparatur außerhalb der Glasspritze entspricht, erhalten. Anekdotisch ist
die heißere Messung ungenau; das mag an der schlechten Temperaturpräzision liegen. Der
"`kalt"' gemessene Wert von \SI{4.92}{\milli\liter} ergab gute Ergebnisse für die
weiteren Berechnungen, während der "`heiß"' gemessene Wert für alle Größen (Stoffmenge,
$k_B$) große Abweichungen von den erwarteten Werten ergab.

\paragraph{Nullpunkt}

Den absoluten Nullpunkt der Temperatur konnten wir nur ungefähr bestimmen. Unsere
Temperaturmessung erlaubte keine exakte Bestimmung der Geradensteigung in
\autoref{fig:isochorregr}, und damit erst recht keine Bestimmung der Nullstelle dieser
Geraden in einer Entfernung vom Messbereich, die dem fünffachen des Messbereichs selbst
entsprach.

\paragraph{Boltzmannkonstante}

Zur Kombination der Messwerte benutzen wir nur die beiden isothermen $k_B$, da das
Ergebnis der isochoren Messung nicht kompatibel ist (eine Rechnung mit äußerem Fehler
lassen wir aus). Wir können also den gewichteten Mittelwert mit innerem Fehler berechnen.
Damit ist

\begin{align}
 \bar k_B &= \frac{ \sum_i \frac{k_{B,i}}{\sigma_i^2}}{\sum_i \frac{1}{\sigma_i^2}} \\
 \sigma_{\bar k_B} &= \sqrt{\frac{1}{\sum_i \frac{1}{\sigma_i^2}}}
\end{align}

Daraus erhalten wir mit den Ergebnissen aus \autoref{sec:isothermergebnis}

\begin{align}
 \bar k_B &= \num{1.381e-23} \pm \SI{0.04e-23}{\joule\per\kelvin}
\end{align}

Letztendlich sind die meisten Fehler und Abweichungen aber wahrscheinlich auf unsere
Grundannahme, die ideale Gasgleichung, zurückzuführen. Kein Gas ist ideal.

\end{document}
