# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 10:45:56 2020

@author: 394493: Alexander Kohlgraf
"""

import numpy as np
import matplotlib.pyplot as plt
from praktikum import analyse
from praktikum import literaturwerte

N=0

def isotherm(p, V, ep, eV, p0, T00, T0, V0, wet):
    p_inv = 1/p
    ep_inv = ep * p_inv**2
    
    reg = analyse.lineare_regression_xy(V, p_inv, eV, ep_inv)
    efit = np.sqrt(ep_inv**2 + reg[0]**2 * eV**2)
    
    rho = literaturwerte.dichte_luft(wet, p0, T00)
    erho = 0.005 * rho
    mol = literaturwerte.molare_masse_luft(wet, p0, T00)
    emol = 0.005 * mol
    
    Vs = reg[2]/reg[0]
    eVs = Vs * np.sqrt((reg[1]/reg[0])**2 + (reg[3]/reg[2])**2 - (2 * (reg[5]*reg[1]*reg[3])/(reg[0]*reg[2])))

    global N
    if N==0 :
        N = (6.02214086*1e23 * (V0 + Vs) * rho)/mol
    eN = N * np.sqrt((np.sqrt(eV[0]**2 + eVs**2)/(V0+Vs))**2 + (erho)**2 + (emol)**2)
    
    k = (1e22)/(reg[0]*N*(T0+273.15))
    eT0 = 0.1
    ek = k * np.sqrt((reg[1]/reg[0])**2 + (eN/N)**2 + (eT0/T0)**2)
    
    p_inv = p_inv + ep_inv
    esyspinvp = analyse.lineare_regression_xy(V, p_inv, eV, ep_inv)[1]
    p_inv = p_inv - 2*ep_inv
    esyspinvm = analyse.lineare_regression_xy(V, p_inv, eV, ep_inv)[1]
    esyspinv = (esyspinvp - esyspinvm)/2
    
    
    V = V + eV
    esysVp = analyse.lineare_regression_xy(V, p_inv, eV, ep_inv)[1]
    V = V - 2*eV
    esysVm = analyse.lineare_regression_xy(V, p_inv, eV, ep_inv)[1]
    esysV = (esysVp - esysVm)/2
    
    
    plt.figure()
    plt.subplot(211)
    #plt.title("Raumtemperatur({0:4f}): Datenpunkte und Fit / Residuenplot mit Chiq: {1:4f}".format(T0, reg[4]))
    plt.xlim(20,95)
    plt.ylim(0,0.002)
    plt.xlabel("Volumen in mL")
    plt.ylabel("1/p in 1/mbar")
    plt.errorbar(V,p_inv,xerr=eV,yerr=ep_inv,fmt="o")
    plt.plot(np.linspace(20,95), reg[0]*np.linspace(20,95) + reg[2])
    plt.subplot(212)
    plt.errorbar(V, p_inv - reg[0]*V - reg[2], yerr=efit, fmt="o")
    plt.plot(np.linspace(20,95),np.zeros(len(np.linspace(20,95))))
    plt.ylim(-0.00001,0.00001)
    plt.xlim(20,95)
    plt.xlabel("Volumen in mL")
    plt.ylabel("Fehler auf 1/p in 1/mbar")
    plt.show()
    
    print("Startvolumen {0:4f} ml".format(V0))
    print("Messtemperatur {0:4f} °C".format(T0))
    print("Freiheitsgrade: {0:6f}".format(len(p_inv)-2))
    print("chiq: {0:6f}".format(reg[4]))
    print("Steigung a: {0:.2e} +- {1:.2e}".format(reg[0], reg[1]))
    print("y-achsenabschnitt b: {0:.2e} +- {1:.2e}".format(reg[2],reg[3]))
    print("Volumen des Schlauches Vs: {0:4f} +- {1:4f}".format(Vs,eVs))
    print("Teilchenzahl N: {0:.2e}".format((N)))
    print("Boltzmann Konstante SI wäre mal 1e-23: {0:.3e}e-22 +- {1:.3e}e-22".format(k, ek))
    print("Literaturwert Boltzmann(in unseren maßen): {0:.4e}e-22".format(1.38064852))
    print("Systematischer Fehler auf 1/p ist: {0:.2e}".format(esyspinv))
    print("Systematischer Fehler auf V ist: {0:.2e}".format(esysV))
    print("")
    print("[a,ea,b,eb,chiq,corr]")
    print(reg)
    print("")
    print("")
    return [Vs,eVs,N,eN, k,ek]
    
def isochor(p, T, ep, eT, Vconst, Vs, eVs, N, eN, eV):
    reg = analyse.lineare_regression_xy(T,p,np.ones(len(T))*eT, np.ones(len(p))*ep)
    efit = np.sqrt(ep**2 + reg[0]**2 * eT**2)
    
    m = (N*1.38064852*1e-23)/(Vconst + Vs) * 1e1 #einheitenumrechnung korrektur zu unseren einheiten
    em = m * np.sqrt((eN/N)**2 + (np.sqrt(eV**2 + eVs**2)/(Vconst + Vs))**2)
    
    k = 1e22 * reg[0] * (Vconst + Vs) / N
    ek = k * np.sqrt((reg[1]/reg[0])**2 + (np.sqrt(eV**2 + eVs**2)/(Vconst + Vs))**2 + (eN/N)**2)
    
    T_abs0 = -reg[2]/reg[0]
    eT_abs0 = T_abs0 * np.sqrt((reg[1]/reg[0])**2 + (reg[3]/reg[2])**2)
    
    T = T + np.ones(len(p))*eT
    esysTp = analyse.lineare_regression_xy(T,p,np.ones(len(T))*eT, np.ones(len(p))*ep)[1]
    T = T - 2*np.ones(len(p))*eT
    esysTm = analyse.lineare_regression_xy(T,p,np.ones(len(T))*eT, np.ones(len(p))*ep)[1]
    esysT = (esysTp - esysTm)/2
    
    p = p + np.ones(len(p))*ep
    esyspp = analyse.lineare_regression_xy(T,p,np.ones(len(T))*eT, np.ones(len(p))*ep)[1]
    p = p - 2*np.ones(len(p))*ep
    esyspm = analyse.lineare_regression_xy(T,p,np.ones(len(T))*eT, np.ones(len(p))*ep)[1]
    esysp = (esyspp - esyspm)/2
    
    plt.figure()
    plt.subplot(211)
    #plt.title("Isochor({0:4f}): Datenpunkte und Fit / Residuenplot mit Chiq: {1:4f}".format(Vconst, reg[4]))
    plt.xlim(20,90)
#    plt.ylim(0,0.002)
    plt.xlabel("Temperatur in °C")
    plt.ylabel("Druck in mbar")
    plt.errorbar(T,p,xerr=eT,yerr=ep,fmt="o")
    plt.plot(np.linspace(20,90), reg[0]*np.linspace(20,90) + reg[2])
    plt.subplot(212)
    plt.errorbar(T, p - reg[0]*T - reg[2], yerr=efit, fmt="o")
    plt.plot(np.linspace(20,90),np.zeros(len(np.linspace(20,90))))
    #plt.ylim(-0.00001,0.00001)
    plt.xlim(20,90)
    plt.xlabel("Temperatur in °C")
    plt.ylabel("Fehler auf Druck in mbar")
    plt.show()
  
    print("Freiheitsgrade: {0:6f}".format(len(p)-2))
    print("chiq: {0:6f}".format(reg[4]))
    print("Steigung a: {0:.2e} +- {1:.2e}".format(reg[0], reg[1]))
    print("y-achsenabschnitt b: {0:.2e} +- {1:.2e}".format(reg[2],reg[3]))
    print("Erwartete Steigung: {0:.2e} +- {1:.2e}".format(m,em))
    print("Boltzmann Konstante SI wäre mal 1e-23: {0:.3e}e-22 +- {1:.3e}e-22".format(k, ek))
    print("Literaturwert Boltzmann(in unseren maßen): {0:.4e}e-22".format(1.38064852))
    print("Absoluter Nullpunkt bei p->0: {0:.23} +- {1:.23}".format(T_abs0,eT_abs0))
    print("Systematischer Fehler auf p ist: {0:.2e}".format(esysp))
    print("Systematischer Fehler auf T ist: {0:.2e}".format(esysT))
    print("")
    print("[a,ea,b,eb,chiq,corr]")
    print(reg)
    print("")
    print("")
    
    return[k,ek]

#Messweete für Isotherme Messungen
p1 = np.array([513,540,573,609,650,697,752,817,897,993,1099,1235,1420,1660])
V1 = np.array([90,85,80,75,70,65,60,55,50,45,40,35,30,25])
eV1 = np.ones(len(V1)) * 0.1

p2 = np.array([693,733,783,834,901,978,1068,1184,1300,1460,1675])
V2 = np.array([80,75,70,65,60,55,50,45,40,35,30])
eV2 = np.ones(len(V2)) * 0.1

#Isotherme Messungen
isotherm1 = isotherm(p1, V1, 0.5, eV1, 993, 23.2, 23.2, 45, 0.36) #muss als erstes ausgeführt werden
print("")
print("Isochore Messung hat unser N ruiniert, denn es ist wohl Gas eingeströmt")
print("Auch hat sich die temperatur beid er zweiten Messung bei der hälfte der werte auf 82 abgesenkt")
isotherm2 = isotherm(p2, V2, 0.5, eV2, 993, 23.2, 83.3, 45, 0.36)

#MEsswerte der Isochoren Messung
pi1 = np.array([575,581,592,602,615,626,632,640,649,657,662,666,677,684,688,693])
Ti1 = np.array([23.4,25,29,33.3,39,44.5,48.3,53.2,57.9,62.0,65.5,68.0,73.8,77.9,80,83])
print("Es wird einen Fehler geben, da Vs nicht kosntant ist, wie die vorherigen messungen gezeigt haben")
eV = 0.1

#Isochore Messungen
isochorBad = isochor(pi1, Ti1, 0.5, 0.1, 80, isotherm1[0], isotherm1[1], isotherm1[2], isotherm1[3], eV)

isochor1 = isochor(pi1[3:], Ti1[3:],0.5, 0.1, 80, isotherm1[0], isotherm1[1], isotherm1[2], isotherm1[3], eV)

boltzAverage = analyse.gewichtetes_mittel(np.array([isotherm1[4], isotherm2[4], isochor1[0]]),np.array([isotherm1[5], isotherm2[5], isochor1[1]]))
print("Da die Messungen verträglich sind können wir den gemittelten Durchschnitt unserer Boltzmann Konstanten nehmen: {0:4f}e-22 +- {1:4f}e-22".format(boltzAverage[0],boltzAverage[1]))